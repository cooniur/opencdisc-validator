/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import org.junit.Test;

/**
 * @author Tim Stone
 */
public class ProcessTokenTest {
    /**
     * Ensures that an exception is thrown if two process tokens are not unique.
     * <p>
     * Covers ticket #36<br>
     * Details: http://www.opencdisc.org/forum/multiple-runs-close-proximity-data-cross-study-report-bleeding
     */
    @Test(expected=IllegalArgumentException.class)
    public void ProcessTokenNotUnique() {
        new ProcessToken("duplicate");
        new ProcessToken("duplicate");
    }

    /**
     * Ensures that ten back-to-back auto generated tokens are generated uniquely.
     * <p>
     * Covers ticket #36<br>
     * Details: http://www.opencdisc.org/forum/multiple-runs-close-proximity-data-cross-study-report-bleeding
     */
    @Test
    public void ProcessTokenUniquelyGenerated() {
        for (int i = 0; i < 10; ++i) {
            ProcessToken.getRandomToken();
        }
    }
}
