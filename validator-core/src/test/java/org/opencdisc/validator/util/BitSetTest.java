/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Tim Stone
 */
public class BitSetTest {
    /**
     * Ensures that a BitSet that has been added to is non-empty
     */
    @Test
    public void NonEmpty() {
        BitSet set = new BitSet();

        set.add(1);

        assertFalse(set.isEmpty());
    }

    /**
     * Ensures that an intersection of two BitSets containing the same values produces
     * a non-empty result
     */
    @Test
    public void NonEmptyIntersection() {
        BitSet original = new BitSet();
        BitSet intersection = new BitSet();

        original.add(1);
        intersection.add(1);

        original.intersect(intersection);

        assertFalse(original.isEmpty());
    }

    /**
     * Ensures that an intersection of two BitSets containing differing values produces
     * an empty result
     */
    @Test
    public void EmptyIntersection() {
        BitSet original = new BitSet();
        BitSet intersection = new BitSet();

        original.add(1);
        intersection.add(2);

        original.intersect(intersection);

        assertTrue(original.isEmpty());
    }

    /**
     * Ensures that the union of an empty BitSet with a non-empty BitSet produces a
     * non-empty result
     */
    @Test
    public void NonEmptyUnion() {
        BitSet original = new BitSet();
        BitSet union = new BitSet();

        union.add(1);

        original.union(union);

        assertFalse(original.isEmpty());
    }

    /**
     * Ensures that a copy of a BitSet is the same as the original
     */
    @Test
    public void ValidCopy() {
        BitSet original = new BitSet();

        original.add(1);

        BitSet copy = BitSet.copy(original);

        assertFalse(original.isEmpty());
        assertFalse(copy.isEmpty());

        copy.intersect(original);

        assertFalse(copy.isEmpty());
    }
}
