/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.SourceOptions.Type;

/**
 * @author Tim Stone
 */
public class CombinedDataSourceTest {
    @Test
    public void CombinedSourcesAllRecords() throws Exception {
        SourceOptions lbchOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"));
        SourceOptions lbheOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"));

        lbchOptions.setName("LB");
        lbheOptions.setName("LB");
        lbchOptions.setSubname("LBCH");
        lbheOptions.setSubname("LBHE");
        lbchOptions.setType(Type.Delimited);
        lbheOptions.setType(Type.Delimited);
        lbchOptions.setDelimiter(",");
        lbheOptions.setDelimiter(",");

        DataSource lbch = new DelimitedDataSource(lbchOptions);
        DataSource lbhe = new DelimitedDataSource(lbheOptions);

        assertTrue(lbch.test());
        assertTrue(lbhe.test());

        DataSource combined = new CombinedDataSource(lbch, lbhe);
        List<DataRecord> records = combined.getRecords();

        assertEquals(4, records.size());
    }

    @Test
    public void CombinedSourcesVaryingVariables() throws Exception {
        SourceOptions lbchOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"));
        SourceOptions lbteOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbte.csv"));

        lbchOptions.setName("LB");
        lbteOptions.setName("LB");
        lbchOptions.setSubname("LBCH");
        lbteOptions.setSubname("LBTE");
        lbchOptions.setType(Type.Delimited);
        lbteOptions.setType(Type.Delimited);
        lbchOptions.setDelimiter(",");
        lbteOptions.setDelimiter(",");

        DataSource lbch = new DelimitedDataSource(lbchOptions);
        DataSource lbte = new DelimitedDataSource(lbteOptions);

        assertTrue(lbch.test());
        assertTrue(lbte.test());

        DataSource combined = new CombinedDataSource(lbch, lbte);
        List<DataRecord> records = combined.getRecords();

        assertEquals(4, records.size());
        assertTrue(records.get(0).definesVariable("NEW"));
    }

    /**
     * Assertion: Data records from combined data sources have a system-internal variable
     *     added to them
     *
     * @throws Exception
     */
    @Test
    public void CombinedSourcesAugmentRecordsWithDataSet() throws Exception {
        SourceOptions lbchOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"));
        SourceOptions lbheOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"));

        lbchOptions.setName("LB");
        lbheOptions.setName("LB");
        lbchOptions.setSubname("LBCH");
        lbheOptions.setSubname("LBHE");
        lbchOptions.setType(Type.Delimited);
        lbheOptions.setType(Type.Delimited);
        lbchOptions.setDelimiter(",");
        lbheOptions.setDelimiter(",");

        DataSource lbch = new DelimitedDataSource(lbchOptions);
        DataSource lbhe = new DelimitedDataSource(lbheOptions);

        assertTrue(lbch.test());
        assertTrue(lbhe.test());

        DataSource combined = new CombinedDataSource(lbch, lbhe);
        List<DataRecord> records = combined.getRecords();

        assertEquals(4, records.size());
        assertTrue(records.get(0).definesVariable("VAL:DATASET"));
        assertEquals(new DataEntry("LBCH"), records.get(0).getValue("VAL:DATASET"));
        assertTrue(records.get(2).definesVariable("VAL:DATASET"));
        assertEquals(new DataEntry("LBHE"), records.get(2).getValue("VAL:DATASET"));
    }

    /**
     * Assertion: Combined data sources should represent the metadata of each component
     *     source as part of a combined data source returned by getMetadata().
     *
     * @throws Exception
     */
    @Test
    public void CombinedSourcesHaveCombinedMetadata() throws Exception {
        SourceOptions lbchOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"));
        SourceOptions lbheOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"));

        lbchOptions.setName("LB");
        lbheOptions.setName("LB");
        lbchOptions.setSubname("LBCH");
        lbheOptions.setSubname("LBHE");
        lbchOptions.setType(Type.Delimited);
        lbheOptions.setType(Type.Delimited);
        lbchOptions.setDelimiter(",");
        lbheOptions.setDelimiter(",");

        DataSource lbch = new DelimitedDataSource(lbchOptions);
        DataSource lbhe = new DelimitedDataSource(lbheOptions);

        assertTrue(lbch.test());
        assertTrue(lbhe.test());

        DataSource combined = new CombinedDataSource(lbch, lbhe);
        DataSource metadata = combined.getMetadata();

        assertTrue(metadata.isComposite());
        assertTrue(metadata.isMetadata());

        List<DataRecord> records = metadata.getRecords();

        assertEquals(10, records.size());
        assertEquals(new DataEntry("LBCH"), records.get(0).getValue("DATASET"));
        assertEquals(new DataEntry("LBHE"), records.get(5).getValue("DATASET"));
    }

    /**
     * Assertion: Replicated combined metadata should be able to be combined with normal
     *     replicated metadata.
     *
     * @throws Exception
     */
    @Test
    public void CombinedSourceMetadataCanCombineWithNormalMetadata() throws Exception {
        SourceOptions lbchOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"));
        SourceOptions lbheOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"));
        CombinedDataSource global = new CombinedDataSource("GLOBAL", "System");

        lbchOptions.setName("LB");
        lbheOptions.setName("LB");
        lbchOptions.setSubname("LBCH");
        lbheOptions.setSubname("LBHE");
        lbchOptions.setType(Type.Delimited);
        lbheOptions.setType(Type.Delimited);
        lbchOptions.setDelimiter(",");
        lbheOptions.setDelimiter(",");

        DataSource lbch = new DelimitedDataSource(lbchOptions);
        DataSource lbhe = new DelimitedDataSource(lbheOptions);

        assertTrue(lbch.test());
        assertTrue(lbhe.test());

        DataSource combined = new CombinedDataSource(lbch, lbhe);
        DataSource metadata = combined.getMetadata();

        global.add(lbch.getMetadata().replicate());
        global.add(metadata.replicate());
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}
