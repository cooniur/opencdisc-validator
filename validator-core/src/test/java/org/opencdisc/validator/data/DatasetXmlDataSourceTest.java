/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.opencdisc.validator.SourceOptions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tim Stone
 */
public class DatasetXmlDataSourceTest {
    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }

    @Test
    public void MetadataIsCorrect() throws Exception {
        SourceOptions options =
            new SourceOptions(this.pathTo("/samples/data/DatasetXmlDataSource/ta.xml"));
        Set<String> variables = new HashSet<String>(Arrays.asList(
            "STUDYID", "DOMAIN", "ARMCD", "ARM", "TAETORD", "ETCD", "ELEMENT", "TABRANCH", "TATRANS", "EPOCH"
        ));

        options.setName("TA");

        DataSource source = new DatasetXmlDataSource(options);

        assertTrue(source.test());
        assertEquals(variables, source.getVariables());
    }

    @Test
    public void CanReadReferenceDataRecords() throws Exception {
        SourceOptions options =
            new SourceOptions(this.pathTo("/samples/data/DatasetXmlDataSource/ta.xml"));

        options.setName("TA");

        DataSource source = new DatasetXmlDataSource(options);

        assertTrue(source.test());
        assertEquals(2, source.getRecords().size());
        assertFalse(source.hasRecords());
    }

    @Test
    public void CanReadSubjectDataRecords() throws Exception {
        SourceOptions options =
            new SourceOptions(this.pathTo("/samples/data/DatasetXmlDataSource/dm.xml"));

        options.setName("DM");

        DataSource source = new DatasetXmlDataSource(options);

        assertTrue(source.test());
        assertEquals(10, source.getRecords().size());
        assertEquals(1, source.getRecords().size());
        assertFalse(source.hasRecords());
    }

    @Test
    public void CanReadSubjectDataRecordsFromDefineWithNonsense() throws Exception {
        SourceOptions options =
            new SourceOptions(this.pathTo("/samples/data/DatasetXmlDataSource/dm.xml"));

        options.setName("DM");
        options.setDefine(this.pathTo("/samples/data/DatasetXmlDataSource/define-with-nonsense.xml"));

        DataSource source = new DatasetXmlDataSource(options);

        assertTrue(source.test());
        assertEquals(10, source.getRecords().size());
        assertEquals(1, source.getRecords().size());
        assertFalse(source.hasRecords());
    }
}
