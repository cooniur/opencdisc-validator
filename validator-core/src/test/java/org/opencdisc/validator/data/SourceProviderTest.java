/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.SourceOptions.Type;

/**
 *
 * @author Tim Stone
 */
public class SourceProviderTest {
    @Test
    public void CreateCombinedSource() throws Exception {
        SourceOptions lbchOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"));
        SourceOptions lbheOptions =
            new SourceOptions(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"));

        lbchOptions.setName("LB");
        lbheOptions.setName("LB");
        lbchOptions.setType(Type.Delimited);
        lbheOptions.setType(Type.Delimited);
        lbchOptions.setDelimiter(",");
        lbheOptions.setDelimiter(",");

        SourceProvider provider = new SourceProvider();

        provider.addSource(lbchOptions);
        provider.addSource(lbheOptions);

        assertTrue(provider.containsSource("LB"));

        DataSource source = provider.getSource("LB");
        List<DataRecord> records = source.getRecords();

        assertTrue(source.isComposite());
        assertEquals(4, records.size());
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}
