/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import org.junit.Test;
import org.opencdisc.validator.SourceOptions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Tim Stone
 */
public class ControlledTerminologyDataSourceTest {
    /**
     * Assertion:
     *
     */
    @Test
    public void ControlledTerminologyDataSourceMergesTerms() throws Exception {
        SourceOptions options =
                new SourceOptions(this.pathTo("/samples/data/ControlledTerminologyDataSource/ct.txt"));

        options.setName("ct");

        DataSource source = new ControlledTerminologyDataSource(options);

        assertTrue(source.test());

        List<DataRecord> records = source.getRecords();

        assertEquals(4, records.size());

        assertEquals("Anisocytes", records.get(1).getValue(
            ControlledTerminologyDataSource.DECODE_COLUMN).toString()
        );
        assertEquals("ANISO", records.get(3).getValue(
            ControlledTerminologyDataSource.DECODE_COLUMN).toString()
        );
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}
