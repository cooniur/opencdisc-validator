/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import static org.junit.Assert.*;

import org.junit.Test;
import org.opencdisc.validator.SourceOptions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tim Stone
 */
public class DelimitedDataSourceTest {
    @Test
    public void NormalDelimitedDataSource() throws Exception {
        SourceOptions options =
                new SourceOptions(this.pathTo("/samples/data/DelimitedDataSource/normal.csv"));
        Set<String> variables = new HashSet<String>(Arrays.asList("A", "B", "C", "D"));

        options.setName("normal");
        options.setDelimiter(",");

        DataSource source = new DelimitedDataSource(options);

        assertTrue(source.test());
        assertEquals(variables, source.getVariables());

        source.getRecords();

        assertEquals(2, source.getRecordCount());
    }

    @Test
    public void HeaderlessDelimitedDataSourceWithTest() throws Exception {
        SourceOptions options =
            new SourceOptions(this.pathTo("/samples/data/DelimitedDataSource/headerless.csv"));
        Set<String> variables = new HashSet<String>(Arrays.asList("V1", "V2", "V3", "V4"));

        options.setName("headerless");
        options.setDelimiter(",");
        options.hasHeader(false);

        DataSource source = new DelimitedDataSource(options);

        assertTrue(source.test());
        assertEquals(variables, source.getVariables());

        source.getRecords();

        assertEquals(2, source.getRecordCount());
    }

    public void HeaderlessDelimitedDataSource() throws Exception {
        SourceOptions options =
                new SourceOptions(this.pathTo("/samples/data/DelimitedDataSource/headerless.csv"));
        Set<String> variables = new HashSet<String>(Arrays.asList("V1", "V2", "V3", "V4"));

        options.setName("headerless");
        options.setDelimiter(",");
        options.hasHeader(false);

        DataSource source = new DelimitedDataSource(options);

        assertEquals(variables, source.getVariables());

        source.getRecords();

        assertEquals(2, source.getRecordCount());
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}
