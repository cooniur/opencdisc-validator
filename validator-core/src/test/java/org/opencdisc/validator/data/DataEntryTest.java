/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 *
 * @author Tim Stone
 */
public class DataEntryTest {
    /**
     * Ensures that partial date equality works when one date value is only a year, and
     * the other is the same year plus a month and a day.
     */
    @Test
    public void testLeadingYearPartialDateEqualitySuccess() {
        DataEntry lhs;
        DataEntry rhs;
        Object literal;

        // Confirm leading LHS (string-based) value matches
        lhs = new DataEntry("2011");
        rhs = new DataEntry("2011-11-01");

        assertEquals(0, lhs.compareTo(rhs));

        // Confirm leading LHS (number-based) value matches
        lhs = new DataEntry(2011);
        rhs = new DataEntry("2011-11-01");

        assertEquals(0, lhs.compareTo(rhs));

        // Confirm leading RHS value matches
        lhs = new DataEntry("2010-04-13");
        rhs = new DataEntry("2010");

        assertEquals(0, lhs.compareTo(rhs));

        // Confirm leading RHS (number-based) value matches
        lhs = new DataEntry("2010-04-13");
        rhs = new DataEntry(2010);

        assertEquals(0, lhs.compareTo(rhs));

        // Confirm leading literal (string-based) value matches
        lhs = new DataEntry("2009-07-21");
        literal = "2009";

        assertEquals(0, lhs.compareTo(literal));

        // Confirm leading literal (string-based) value matches
        lhs = new DataEntry("2008-09-24");
        literal = 2008;

        assertEquals(0, lhs.compareTo(literal));
    }

    @Test
    public void VeryLargeTextValuesShouldBeLeftAlone() {
        DataEntry entry = new DataEntry("9007199254740993");

        assertEquals("9007199254740993", entry.toString());
    }

    @Test
    public void WhatGoesInMustComeOut() {
        DataEntry entry = new DataEntry("12.00");

        assertEquals("12.00", entry.toString());
    }

    @Test
    public void UseMeaningfulAccuracy() {
        DataEntry entry = new DataEntry(2.2800000000000002);
        DataEntry expected = new DataEntry("2.28");

        assertEquals(expected, entry);
    }

    @Test
    public void ShouldAcceptMissingLeadingZeroes() {
        DataEntry entry = new DataEntry(".5");
        DataEntry expected = new DataEntry(0.5);

        assertEquals(expected, entry);
    }

    @Test
    public void ShouldAcceptNegativeMissingLeadingZeroes() {
        DataEntry entry = new DataEntry("-.5");
        DataEntry expected = new DataEntry(-0.5);

        assertEquals(expected, entry);
    }

    @Test
    public void RoundWithExpectedDecimalPlacesMatch() {
        DataEntry entry = new DataEntry(1.11112356);
        DataEntry expected = new DataEntry(1.11112354);

        assertEquals(expected, entry);
    }

    @Test
    public void RoundWithExpectedDecimalPlacesDontMatch() {
        DataEntry entry = new DataEntry(1.11112356);
        DataEntry expected = new DataEntry(1.1111233);

        assertNotEquals(expected, entry);
    }
}
