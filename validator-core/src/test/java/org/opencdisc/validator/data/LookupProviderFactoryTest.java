/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;
import org.opencdisc.validator.EventDispatcher;
import org.opencdisc.validator.util.ProcessToken;

/**
 * @author Tim Stone
 */
public class LookupProviderFactoryTest {
    /**
     * Ensures that the default LookupProvider implementation returned is a
     * ConcurrentLookupProvider instance
     */
    @Test
    public void ExpectedDefaultLookupProvider() {
        LookupProvider provider = LookupProviderFactory.getLookupProvider();

        assertTrue(provider instanceof ConcurrentLookupProvider);
    }

    /**
     * Ensures that passing in a class name as an identifier returns an
     * instance of that class as a LookupProvider
     */
    @Test
    public void ValidClassLookupProvider() {
        LookupProvider provider = LookupProviderFactory.getLookupProvider(
                "org.opencdisc.validator.data.LookupProviderFactoryTest$"
                + "LookupProviderImpl"
        );

        assertTrue(provider instanceof LookupProviderImpl);
    }

    /**
     * Ensures that passing in an invalid class name as an identifier throws an
     * exception
     */
    @Test(expected=IllegalArgumentException.class)
    public void InvalidClassLookupProvider() {
        LookupProviderFactory.getLookupProvider("missing.class");
    }

    /**
     * Concrete mock implementation of a LookupProvider
     *
     * @author Tim Stone
     */
    public static class LookupProviderImpl implements LookupProvider {
        public void clear(ProcessToken token) {}
        public Lookup get(String sourceName, Set<String> variables, ProcessToken token) {
            return null;
        }
        public void request(String sourceName, Set<String> variables,
                ProcessToken token) {
        }
        public boolean verifyExists(String sourceName, ProcessToken token) {
            return false;
        }
        public boolean verifyExists(String sourceName, Set<String> variables,
                ProcessToken token) {
            return false;
        }
        public void setDispatcher(EventDispatcher dispatcher) {}
    }
}
