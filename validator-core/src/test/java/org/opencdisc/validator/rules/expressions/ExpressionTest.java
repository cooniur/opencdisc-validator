/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.ValidationRuleTestHelper;

import java.util.List;

/**
 * @author Tim Stone
 */
public class ExpressionTest {
    @Test
    public void IllegalGroupReference() {
        Expression expression = Expression.createFrom(
            "AEORRES != '' @and AEORRES @re '[-+]?[0-9]*\\\\.?[0-9]+' @and AETEST @re '^((?!( Ratio)).)*'"
        );
    }

    @Test
    public void RepeatRegularExpressions() {
        DataRecord record;
        Expression expression = Expression.createFrom(
            "XY @re '[A-Z]+'"
        );

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(new DataEntry("ABC"));

        assertTrue(expression.evaluate(record));

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(new DataEntry("123"));

        assertFalse(expression.evaluate(record));

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(new DataEntry("LMN"));

        assertTrue(expression.evaluate(record));
    }

    /**
     * Assertion: Backslashes ("|") shouldn't be replaced during the constant
     *     resubstitute process.
     */
    @Test
    public void SlashesInLiteralsShouldRemainThere() {
        DataRecord record;
        Expression expression;

        expression = Expression.createFrom(
            "XY @re '[-+]?[0-9]*\\.?[0-9]+'"
        );

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(new DataEntry("<1"));

        assertFalse(expression.evaluate(record));

        expression = Expression.createFrom(
            "XY == '\\a'"
        );

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(new DataEntry("\\a"));

        assertTrue(expression.evaluate(record));

        expression = Expression.createFrom(
            "XY == '\\''"
        );

        record = mock(DataRecord.class);
        when(record.getValue("XY")).thenReturn(new DataEntry("'"));

        assertTrue(expression.evaluate(record));
    }

    /**
     * Assertion: Internal variable names of the form VAL:VARIABLE should be considered
     *     valid in an expression.
     */
    @Test
    public void InternalVariableNamesPermitted() {
        Expression expression = Expression.createFrom(
            "VAL:DATASET == 'FAEX'"
        );

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("VAL:DATASET",
            new DataEntry("FAEX"),
            new DataEntry("XX")
        );

        Boolean[] expected = new Boolean[] { true, false };
        Boolean[] actual = new Boolean[records.size()];
        int index = 0;

        for (DataRecord record : records) {
            actual[index++] = expression.evaluate(record);
        }

        assertArrayEquals(expected, actual);
    }
}
