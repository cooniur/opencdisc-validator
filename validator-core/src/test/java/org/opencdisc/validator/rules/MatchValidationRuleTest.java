/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.opencdisc.validator.rules.ValidationRuleTestHelper.*;

/**
 *
 * @author Tim Stone
 */
public class MatchValidationRuleTest {
    @Test
    public void CanMatchPairs() throws Exception {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("PairedVariable")).thenReturn(true);
        when(definition.getProperty("PairedVariable")).thenReturn("YY");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("A:B,C:D");

        ValidationRule rule = new MatchValidationRule(definition, token, null);

        List<DataRecord> records = createRecords(new String[] { "XX", "YY" },
            new DataEntry[] { new DataEntry("A"), new DataEntry("B") },
            new DataEntry[] { new DataEntry("C"), new DataEntry("D") },
            new DataEntry[] { new DataEntry("C"), new DataEntry("B") },
            new DataEntry[] { new DataEntry("C"), new DataEntry("R") }
        );

        Boolean[] expected = new Boolean[] {
            true, true, false, false
        };
        Boolean[] results = new Boolean[records.size()];
        int index = 0;

        for (DataRecord record : records) {
            results[index++] = rule.validate(record);
        }

        assertArrayEquals(expected, results);
    }
}
