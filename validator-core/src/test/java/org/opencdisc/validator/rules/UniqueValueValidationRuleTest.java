/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

import java.util.List;

/**
 * @author Tim Stone
 */
public class UniqueValueValidationRuleTest {
    /**
     * Assertion: If all values for a non-matching Unique rule are unique, the rule
     *     should pass.
     */
    @Test
    public void SingleColumnUniqueSuccess() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.getProperty("Matching")).thenReturn("No");

        ValidationRule rule = new UniqueValueValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("B"),
            new DataEntry("C")
        );

        boolean failure = false;

        for (DataRecord record : records) {
            if (!rule.validate(record)) {
                failure = true;
            }
        }

        assertFalse(failure);
    }

    /**
     * Assertion: If all values for a non-matching Unique rule aren't unique, the rule
     *     should fail on subsequent duplicates.
     */
    @Test
    public void SingleColumnUniqueFailure() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.getProperty("Matching")).thenReturn("No");

        ValidationRule rule = new UniqueValueValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("B"),
            new DataEntry("B"),
            new DataEntry("C"),
            new DataEntry("A")
        );

        // Boolean instead of boolean because junit lacks an assertArrayEquals(b[], b[])
        // for whatever reason
        Boolean[] expected = new Boolean[] {
            false, false, true, false, true
        };
        Boolean[] failures = new Boolean[records.size()];
        int index = 0;

        for (DataRecord record : records) {
            failures[index++] = !rule.validate(record);
        }

        assertArrayEquals(expected, failures);
    }

    /**
     * Assertion: If all values for a matching Unique rule are the same, the rule
     *     should pass.
     */
    @Test
    public void SingleColumnMatchingSuccess() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.getProperty("Matching")).thenReturn("Yes");

        ValidationRule rule = new UniqueValueValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("A"),
            new DataEntry("A")
        );

        boolean failure = false;

        for (DataRecord record : records) {
            if (!rule.validate(record)) {
                failure = true;
            }
        }

        assertFalse(failure);
    }

    /**
     * Assertion: If all values for a matching Unique rule are not the same, the rule
     *     should fail on subsequent non-matching values.
     */
    @Test
    public void SingleColumnMatchingFailure() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.getProperty("Matching")).thenReturn("Yes");

        ValidationRule rule = new UniqueValueValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("B"),
            new DataEntry("A"),
            new DataEntry("A"),
            new DataEntry("C")
        );

        Boolean[] expected = new Boolean[] {
            false, true, false, false, true
        };
        Boolean[] failures = new Boolean[records.size()];
        int index = 0;

        for (DataRecord record : records) {
            failures[index++] = !rule.validate(record);
        }

        assertArrayEquals(expected, failures);
    }
}
