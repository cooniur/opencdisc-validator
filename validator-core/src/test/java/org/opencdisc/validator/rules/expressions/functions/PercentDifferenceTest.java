/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.ValidationRuleTestHelper;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Tim Stone
 */
public class PercentDifferenceTest {
    @Test
    public void SubtractionIsEasy() {
        String[] names = new String[] { "AVAL", "BASE" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[]{
                new DataEntry(2), new DataEntry(1)
            }
        );
        Function function = new Difference("PCTDIFF", names);

        assertEquals(new DataEntry(1), function.compute(records.get(0)));
    }
}
