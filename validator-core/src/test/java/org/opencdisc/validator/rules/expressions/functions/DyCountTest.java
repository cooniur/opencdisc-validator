/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import static org.junit.Assert.*;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.ValidationRuleTestHelper;

import java.util.List;

/**
 * @author Tim Stone
 */
public class DyCountTest {
    /**
     * Assertion: A recorded date ahead of the reference date should have the expected
     *     DY value days + 1
     */
    @Test
    public void PositiveDateIsCorrect() {
        String[] names = new String[] { "REF", "REC" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] {
                new DataEntry("2013-03-01"), new DataEntry("2013-03-02")
            }
        );
        Function function = new DyCount("DY", names);

        assertEquals(new DataEntry(2), function.compute(records.get(0)));
    }

    /**
     * Assertion: A recorded date ahead of the reference date should have the expected
     *     DY value days + 1, irrespective of time
     */
    @Test
    public void PositiveDateIsCorrectWithDateTimes() {
        String[] names = new String[] { "REF", "REC" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] {
                new DataEntry("2013-03-01T01:24:57"), new DataEntry("2013-03-02T23:19:41")
            }
        );
        Function function = new DyCount("DY", names);

        assertEquals(new DataEntry(2), function.compute(records.get(0)));
    }

    /**
     * Assertion: A recorded date before the reference date should have the expected
     *     DY value -days
     */
    @Test
    public void NegativeDateIsCorrect() {
        String[] names = new String[] { "REF", "REC" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] {
               new DataEntry("2013-03-02"),  new DataEntry("2013-03-01")
            }
        );
        Function function = new DyCount("DY", names);

        assertEquals(new DataEntry(-1), function.compute(records.get(0)));
    }

    /**
     * Assertion: A recorded date that's the same as the reference day should have DY
     *     value 1
     */
    @Test
    public void SameDayIsCorrect() {
        String[] names = new String[] { "REF", "REC" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] {
                new DataEntry("2013-03-01"),  new DataEntry("2013-03-01")
            }
        );
        Function function = new DyCount("DY", names);

        assertEquals(new DataEntry(1), function.compute(records.get(0)));
    }
}
