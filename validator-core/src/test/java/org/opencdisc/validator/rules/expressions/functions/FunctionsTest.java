/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Tim Stone
 */
public class FunctionsTest {
    /**
     * Assertion: Functions can be created post-refactor.
     */
    @Test
    public void CanCreateFunction() {
        assertNotNull(Functions.create(":DY(AA, BB)"));
    }
}
