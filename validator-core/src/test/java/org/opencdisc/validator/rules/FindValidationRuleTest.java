/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

import java.util.Arrays;
import java.util.List;

/**
 * @author Tim Stone
 */
public class FindValidationRuleTest {
    /**
     * Assertion: You cannot specify both Terms and Test for a Find rule.
     *
     * @throws ConfigurationException
     */
    @Test(expected = ConfigurationException.class)
    public void TermsAndTestNotBothAllowed() throws ConfigurationException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.hasProperty("Test")).thenReturn(true);

        ValidationRule rule = new FindValidationRule(definition, token, null);
    }

    /**
     * Assertion: You must specify either Terms or Test for a Find rule.
     *
     * @throws ConfigurationException
     */
    @Test(expected = ConfigurationException.class)
    public void TermsOrTestMustBePresent() throws ConfigurationException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("Terms")).thenReturn(false);
        when(definition.hasProperty("Test")).thenReturn(false);

        ValidationRule rule = new FindValidationRule(definition, token, null);
    }

    /**
     * Assertion: If all Terms are present in the data, a Find rule should pass.
     *
     * @throws ConfigurationException
     * @throws CorruptRuleException
     */
    @Test
    public void AllTermsMatchSuccess() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("A,C");

        ValidationRule rule = new FindValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("B"),
            new DataEntry("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }

    /**
     * Assertion: If Match is specified and <code>n</code> Terms are present in the data,
     *     a Find rule should pass.
     *
     * @throws ConfigurationException
     * @throws CorruptRuleException
     */
    @Test
    public void TwoTermsMatchSuccess() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("A,B,C");
        when(definition.hasProperty("Match")).thenReturn(true);
        when(definition.getProperty("Match")).thenReturn("2");

        ValidationRule rule = new FindValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("B"),
            new DataEntry("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }

    /**
     * Assertion: If Match is specified and less than <code>n</code> Terms are present in
     *     the data, a Find rule should fail.
     *
     * @throws ConfigurationException
     * @throws CorruptRuleException
     */
    @Test
    public void TwoTermsMatchFailure() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("A,B,C");
        when(definition.hasProperty("Match")).thenReturn(true);
        when(definition.getProperty("Match")).thenReturn("2");

        ValidationRule rule = new FindValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("D"),
            new DataEntry("E")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertFalse(rule.validateDataset(null));
    }

    /**
     * Assertion: If Test matches somewhere in the dataset, a Find rule should pass.
     *
     * @throws ConfigurationException
     * @throws CorruptRuleException
     */
    @Test
    public void OneTestMatchSuccess() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Test")).thenReturn(true);
        when(definition.getProperty("Test")).thenReturn(".*");

        ValidationRule rule = new FindValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("B"),
            new DataEntry("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }

    /**
     * Assertion: The If clause should be able to check multiple independent conditions.
     *
     * @throws ConfigurationException
     * @throws CorruptRuleException
     */
    @Test
    public void MultipleIfMatchesSuccess() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("C");
        when(definition.hasProperty("If")).thenReturn(true);
        when(definition.getProperty("If")).thenReturn("XX == 'A' @iand XX == 'B'");

        ValidationRule rule = new FindValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("B"),
            new DataEntry("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }

    /**
     * Assertion: The If clause should be able to check multiple independent conditions.
     *
     * @throws ConfigurationException
     * @throws CorruptRuleException
     */
    @Test
    public void MultipleIfMatchesIgnore() throws ConfigurationException,
            CorruptRuleException {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("Q");
        when(definition.hasProperty("If")).thenReturn(true);
        when(definition.getProperty("If")).thenReturn("XX == 'A' @iand XX == 'D'");

        ValidationRule rule = new FindValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("A"),
            new DataEntry("B"),
            new DataEntry("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }
}
