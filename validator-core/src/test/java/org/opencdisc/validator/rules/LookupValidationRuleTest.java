/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.opencdisc.validator.EventDispatcher;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.data.Lookup;
import org.opencdisc.validator.data.LookupProvider;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public class LookupValidationRuleTest {
    /**
     * Ensures that a LookupValidationRule can perform checks using an external provider
     */
    @Test
    public void ExternalProviderLookup() throws Exception {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX == XX");
        when(definition.hasProperty("From")).thenReturn(true);
        when(definition.getProperty("From")).thenReturn("XX");
        when(definition.hasProperty("Provider")).thenReturn(true);
        when(definition.getProperty("Provider")).thenReturn(
            "org.opencdisc.validator.rules.LookupValidationRuleTest$LookupProviderImpl"
        );

        LookupValidationRule rule = new LookupValidationRule(definition, token, null);
        DataRecord record = new DataRecord(null, null);

        record.setValue("XX", new DataEntry("Y"));

        assertTrue(rule.validate(record));
    }

    @Test
    public void CaseInsensitiveClauseWorks() throws Exception {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Search")).thenReturn(true);
        when(definition.getProperty("Search")).thenReturn(
            "CDISCSubmissionValue @eqic [XX]"
        );
        when(definition.hasProperty("From")).thenReturn(true);
        when(definition.getProperty("From")).thenReturn(
            "FILE:CDISC:" + this.pathTo("/samples/rules/LookupValidationRule/ct.txt")
        );
        when(definition.hasProperty("Where")).thenReturn(true);
        when(definition.getProperty("Where")).thenReturn("CodeListCode == 'C65047'");

        ValidationRule rule;
        List<DataRecord> records;

        rule = new LookupValidationRule(definition, token, null);
        records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("aniso")
        );

        assertTrue(rule.validate(records.get(0)));

        when(definition.getProperty("Where")).thenReturn("CodeListCode == 'C67154'");

        rule = new LookupValidationRule(definition, token, null);
        records = ValidationRuleTestHelper.createRecords("XX",
            new DataEntry("ANISOCYTES"),
            new DataEntry("Anisocytes")
        );

        for (DataRecord record : records) {
            assertTrue(rule.validate(record));
        }
    }

    /**
     * Concrete mock implementation of a LookupProvider
     *
     * @author Tim Stone
     */
    public static class LookupProviderImpl implements LookupProvider {
        public void clear(ProcessToken token) {}
        @SuppressWarnings("unchecked")
        public Lookup get(String sourceName, Set<String> variables,
                ProcessToken token) {
            Lookup lookup = mock(Lookup.class);

            when(lookup.seek(anyList(), anyList(), anyBoolean())).thenReturn(true);

            return lookup;
        }
        public void request(String sourceName, Set<String> variables,
                ProcessToken token) {}
        public boolean verifyExists(String sourceName, ProcessToken token) {
            return true;
        }
        public boolean verifyExists(String sourceName, Set<String> variables,
                ProcessToken token) {
            return true;
        }
        public void setDispatcher(EventDispatcher dispatcher) {}
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}
