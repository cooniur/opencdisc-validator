/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.ValidationRuleTestHelper;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Tim Stone
 */
public class DifferenceTest {
    @Test
    public void SubtractionIsEasy() {
        String[] names = new String[] { "AVAL", "BASE" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[]{
                new DataEntry(6), new DataEntry(4)
            }
        );
        Function function = new Difference("DIFF", names);

        assertEquals(new DataEntry(2), function.compute(records.get(0)));
    }

    @Test
    public void FloatingPointSubtractionIsHard() {
        String[] names = new String[] { "AVAL", "BASE" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[]{
                new DataEntry(28.1), new DataEntry(19.7)
            },
            new DataEntry[] {
                new DataEntry(28.1), new DataEntry(34.5)
            },
            new DataEntry[] {
                new DataEntry(28.1), new DataEntry(13.1)
            }
        );
        Function function = new Difference("DIFF", names);

        assertEquals(new DataEntry(8.4), function.compute(records.get(0)));
        assertEquals(new DataEntry(-6.4), function.compute(records.get(1)));
        assertEquals(new DataEntry(15.0), function.compute(records.get(2)));
    }
}
