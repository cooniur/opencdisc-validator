/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions;

import static org.junit.Assert.*;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.ValidationRuleTestHelper;

import java.util.List;

/**
 * @author Tim Stone
 */
public class ComparisonTest {
    /**
     * Assertion: A fuzzy comparison against RHS value of 0 should not return false
     *     when the LHS value is also 0
     */
    @Test
    public void FuzzyEqualityZeroWorks() {
        Evaluable comparison = Comparison.createComparison("A %= 0");
        List<DataRecord> records = ValidationRuleTestHelper.createRecords("A", new DataEntry(0.0));

        assertTrue(comparison.evaluate(records.get(0)));
    }
}
