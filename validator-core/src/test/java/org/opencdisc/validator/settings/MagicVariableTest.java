/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.opencdisc.validator.settings.Definition.Target;
import org.opencdisc.validator.settings.MagicVariableParser.MagicProperty;

/**
 *
 * @author Tim Stone
 */
public class MagicVariableTest {
    /**
     *
     */
    @Test
    public void SimplePropertyDoesMatch() {
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Define.WithCodeList%",
                new ArrayList<List<String>>(),
                Arrays.asList("Define.WithCodeList")
        );
        Definition definition = new Definition(Target.Variable, "LBBLFL");

        definition.setProperty("Define.WithCodeList", "Y");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void SimplePropertyDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Define.WithCodeList%",
                new ArrayList<List<String>>(),
                Arrays.asList("Define.WithCodeList")
        );
        Definition definition = new Definition(Target.Variable, "LBBLFL");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void SimpleClauseDoesMatch() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY]%",
                Arrays.asList(Arrays.asList("*DY")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "AESTDY");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void SimpleClauseDoesNotMatch() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY]%",
                Arrays.asList(Arrays.asList("*DY")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "AESTDTC");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void ComplexPropertyDoesMatch() {
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Type:DateTime|Date|Time%",
                new ArrayList<List<String>>(),
                Arrays.asList("Type:DateTime|Date|Time")
        );
        Definition definition = new Definition(Target.Variable, "LBDTC");

        definition.setProperty("Type", "Time");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void ComplexPropertyDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Type:DateTime|Date|Time%",
                new ArrayList<List<String>>(),
                Arrays.asList("Type:DateTime|Date|Time")
        );
        Definition definition = new Definition(Target.Variable, "LBBLFL");

        definition.setProperty("Type", "Text");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiPropertyDoesMatch() {
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Type:Time$CodeList%",
                new ArrayList<List<String>>(),
                Arrays.asList("Type:Time", "CodeList")
        );
        Definition definition = new Definition(Target.Variable, "LBDTC");

        definition.setProperty("Type", "Time");
        definition.setProperty("CodeList", "Y");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiPropertyDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Type:Time$Define%",
                new ArrayList<List<String>>(),
                Arrays.asList("Type:Time", "Define")
        );
        Definition definition = new Definition(Target.Variable, "LBDTC");

        definition.setProperty("Type", "Time");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiSingleClauseDoesMatch() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY,*DTC]%",
                Arrays.asList(Arrays.asList("*DY", "*DTC")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "AESTDTC");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiSingleClauseDoesNotMatch() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY,*DTC]%",
                Arrays.asList(Arrays.asList("*DY", "*DTC")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "LBBLFL");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void NegativeClauseDoesMatch() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY]%",
                Arrays.asList(Arrays.asList("!*DY")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "AESTDTC");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void NegativeClauseDoesNotMatch() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY]%",
                Arrays.asList(Arrays.asList("!*DY")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "AESTDY");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiMatchClauseDoesMatch() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY,*DTC]%",
                Arrays.asList(Arrays.asList("*DTC"),
                Arrays.asList("AE*")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "AESTDTC");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiMatchClauseDoesNoMatch() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY,*DTC]%",
                Arrays.asList(Arrays.asList("*DTC"),
                Arrays.asList("AE*")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "LBSTDTC");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void ClauseReferences() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY,*DTC]%",
                Arrays.asList(Arrays.asList("TR##SDTM")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "TR01SDTM");

        assertTrue(variable.matches(definition));

        Map<Integer, String> references = variable.getReferences(definition);

        assertFalse(references.isEmpty());
        assertEquals("01", references.get(1));
    }

    /**
     *
     */
    @Test
    public void MultiClauseReferences() {
        @SuppressWarnings("unchecked")
        MagicVariable variable = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY,*DTC]%",
                Arrays.asList(Arrays.asList("TR##AG#N")),
                new ArrayList<String>()
        );
        Definition definition = new Definition(Target.Variable, "TR01AG1N");

        assertTrue(variable.matches(definition));

        Map<Integer, String> references = variable.getReferences(definition);

        assertFalse(references.isEmpty());
        assertEquals("01", references.get(1));
        assertEquals("1", references.get(2));
    }
}
