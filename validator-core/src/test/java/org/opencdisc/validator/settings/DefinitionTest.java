/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Tim Stone
 */
public class DefinitionTest {
    /**
     *
     */
    @Test
    public void SelectiveCopy() {
        Definition source = new Definition(Definition.Target.Domain, "domain");
        Definition destination = new Definition(Definition.Target.Domain, "domain");

        source.setProperty("a", "source");
        source.setProperty("b", "source");
        destination.setProperty("a", "destination");
        destination.setProperty("b", "destination");

        Definition.copyTo(source, destination, "a");

        assertEquals("source", destination.getProperty("a"));
        assertEquals("destination", destination.getProperty("b"));
    }
}
