/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;
import org.opencdisc.validator.settings.MagicVariableParser.MagicProperty;

/**
 *
 * @author Tim Stone
 */
public class MagicVariableParserTest {
    /**
     *
     */
    @Test
    public void OldConditionParse() {
        MagicVariableParser scope = new MagicVariableParser();
        MagicVariable expected = new MagicVariable(
                MagicProperty.Variables,
                "%Variables.Type:DateTime|Date|Time%",
                new ArrayList<List<String>>(),
                Arrays.asList("Type:DateTime|Date|Time", "Config")
        );
        List<MagicVariable> actual;

        actual = scope.parse(
                MagicProperty.Variables,
                "Testable",
                expected.getIdentifier()
        );

        // There should only be one result
        assertEquals(1, actual.size());
        // And that result should be the expected one
        assertEquals(expected, actual.get(0));
    }

    /**
     *
     */
    @Test
    public void NewConditionParse() {
        MagicVariableParser scope = new MagicVariableParser();
        MagicVariable expected = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Type:DateTime|Date|Time%",
                new ArrayList<List<String>>(),
                Arrays.asList("Type:DateTime|Date|Time", "Config")
        );
        List<MagicVariable> actual;

        actual = scope.parse(
                MagicProperty.Variables,
                "Testable",
                expected.getIdentifier()
        );

        // There should only be one result
        assertEquals(1, actual.size());
        // And that result should be the expected one
        assertEquals(expected, actual.get(0));
    }

    /**
     *
     */
    @Test
    public void MultiConditionParse() {
        MagicVariableParser scope = new MagicVariableParser();
        MagicVariable expected = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Define$Type:DateTime|Date|Time%",
                new ArrayList<List<String>>(),
                Arrays.asList("Define", "Type:DateTime|Date|Time")
        );
        List<MagicVariable> actual;

        actual = scope.parse(
                MagicProperty.Variables,
                "Testable",
                expected.getIdentifier()
        );

        // There should only be one result
        assertEquals(1, actual.size());
        // And that result should be the expected one
        assertEquals(expected, actual.get(0));
    }

    /**
     *
     */
    @Test
    public void NewConditionWithClauseParse() {
        MagicVariableParser scope = new MagicVariableParser();
        @SuppressWarnings("unchecked")
        MagicVariable expected = new MagicVariable(
                MagicProperty.Variables,
                "%Variables[*DY]$Type:DateTime|Date|Time%",
                Arrays.asList(Arrays.asList("*DY")),
                Arrays.asList("Type:DateTime|Date|Time", "Config")
        );
        List<MagicVariable> actual;

        actual = scope.parse(
                MagicProperty.Variables,
                "Testable",
                expected.getIdentifier()
        );

        // There should only be one result
        assertEquals(1, actual.size());
        // And that result should be the expected one
        assertEquals(expected, actual.get(0));
    }

    /**
     *
     */
    @Test(expected=MagicVariableSyntaxException.class)
    public void InvalidConditionParse() {
        MagicVariableParser scope = new MagicVariableParser();
        MagicVariable expected = new MagicVariable(
                MagicProperty.Variables,
                "%Variables$Unkown%",
                new ArrayList<List<String>>(),
                new ArrayList<String>()
        );

        scope.parse(
                MagicProperty.Variables,
                "Testable",
                expected.getIdentifier()
        );
    }

    /**
     *
     */
    @Test
    public void NonExpandedParse() {
        MagicVariableParser scope = new MagicVariableParser();
        List<MagicVariable> actual;

        actual = scope.parse(
                MagicProperty.Variables,
                "Testable",
                "%Variable%"
        );

        assertEquals(0, actual.size());
    }

    @Test
    public void ParsesDependencyExpansionSyntax() {
        MagicVariableParser scope = new MagicVariableParser();
        MagicVariable actual = scope.parse(
            MagicProperty.Variables,
            "Testable",
            "+%Variables.Config.@Clause%"
        ).get(0);

        assertTrue(actual.isReplicated());
        assertTrue(actual.isDependencyReplicated());
    }

    @Test
    public void ActuallyExpandsExpansionSyntax() {
        MagicVariableParser scope = new MagicVariableParser();
        MagicVariable parsed = scope.parse(
            MagicProperty.Variables,
            "Testable",
            "+%Variables.Config.@Clause%"
        ).get(0);
        Definition variable = new Definition(Definition.Target.Variable, "QSCAT");

        variable.setProperty("Config.@Clause", "Y");
        variable.addDependency(new Definition(Definition.Target.Variable, "QSCAT.SOMEVAL")
            .setProperty("Value", "SOMEVAL"));
        variable.addDependency(new Definition(Definition.Target.Variable, "QSCAT.SOMEOTHERVAL")
            .setProperty("Value", "SOMEOTHERVAL"));

        Definition rule = new Definition(Definition.Target.Rule, "Match")
            .setProperty("Message", "Test %Variable.@Clause.Value%");

        List<Definition> generated = scope.prepare(rule, parsed, Arrays.asList(variable),
            new HashMap<String, String>());

        assertEquals(2, generated.size());

        // We aren't guaranteed generated in a particular order, so...
        Set<String> expected = new HashSet<String>(Arrays.asList("Test SOMEVAL", "Test SOMEOTHERVAL"));
        Set<String> actual = new HashSet<String>();

        for (Definition definition : generated) {
            actual.add(definition.getProperty("Message"));
        }

        assertEquals(expected, actual);
    }

    @Test
    public void ActuallyExpandsExpansionSyntaxConditionally() {
        MagicVariableParser scope = new MagicVariableParser();
        MagicVariable parsed = scope.parse(
            MagicProperty.Variables,
            "Testable",
            "+%Variables.Config.@Clause.TestProp%"
        ).get(0);
        Definition variable = new Definition(Definition.Target.Variable, "QSCAT");

        variable.setProperty("Config.@Clause", "Y");
        variable.addDependency(new Definition(Definition.Target.Variable, "QSCAT.SOMEVAL")
            .setProperty("Value", "SOMEVAL")
            .setProperty("TestProp", "Y"));
        variable.addDependency(new Definition(Definition.Target.Variable, "QSCAT.SOMEOTHERVAL")
            .setProperty("Value", "SOMEOTHERVAL"));

        Definition rule = new Definition(Definition.Target.Rule, "Match")
            .setProperty("Message", "Test %Variable.@Clause.Value%");

        List<Definition> generated = scope.prepare(rule, parsed, Arrays.asList(variable),
            new HashMap<String, String>());

        assertEquals(1, generated.size());
    }
}
