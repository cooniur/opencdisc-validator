/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.Text;

import au.com.bytecode.opencsv.CSVReader;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.util.Settings;

/**
 *
 * @author Tim Stone
 */
public class DelimitedDataSource extends AbstractDataSource {
    public static final char DEFAULT_DELIMITER = '|';
    public static final char DEFAULT_QUALIFIER = '"';

    private final char delimiter;
    private final char qualifier;
    private CSVReader reader;
    private final File source;
    private String[] buffer;
    private int variableCount = -1;

    public DelimitedDataSource(SourceOptions options) throws InvalidDataException {
        super(options);

        File source = new File(super.options.getSource());

        if (!(source.isFile() && source.exists())) {
            // Someone didn't do a good job at checking, so...
            throw new InvalidDataException(
                    InvalidDataException.Codes.MissingSource,
                    Text.get("Exceptions.FileNotFound")
            );
        }

        this.source = source;
        this.entity.setProperty(EntityDetails.Property.FileSize, source.length());

        // Create the reader
        String delimiter = super.options.getDelimiter();
        String qualifier = super.options.getQualifier();

        this.delimiter = delimiter != null && delimiter.length() > 0 ?
                delimiter.charAt(0) : DEFAULT_DELIMITER;
        this.qualifier = qualifier != null && qualifier.length() > 0 ?
                qualifier.charAt(0) : DEFAULT_QUALIFIER;

        this.setReader();
    }

    public DataSource replicate() throws InvalidDataException {
        return new DelimitedDataSource(super.options);
    }

    public void close() {
        try {
            this.reader.close();
        } catch (IOException ex) {
            // Ignore
        }
    }

    public boolean test() {
        String[] line;
        boolean result = true;

        try {
            line = this.reader.readNext();

            if (line != null) {
                int count = line.length;
                line = this.reader.readNext();

                if (line != null && count != line.length) {
                    result = false;
                } else {
                    this.variableCount = count;
                }
            } else {
                result = false;
            }
        } catch (IOException ex) {
            result = false;
        }

        // Reset the reader
        if (result) {
            try {
                this.setReader();
            } catch (InvalidDataException ex) {
                result = false;
            }
        }

        return result;
    }

    protected void parseVariables() throws InvalidDataException {
        try {
            int count = 0;

            if (super.options.hasHeader() || this.variableCount < 0) {
                String[] variables = this.reader.readNext();

                if (variables == null || variables.length == 0) {
                    throw new InvalidDataException(InvalidDataException.Codes.NoVariables);
                }

                if (super.options.hasHeader()) {
                    for (String variable : variables) {
                        // Add the variable
                        super.metadata.add(variable.replaceAll("[^A-Za-z0-9]", ""));
                    }
                } else {
                    count = variables.length;
                    this.buffer = variables;
                }
            } else {
                count = this.variableCount;
            }

            if (!super.options.hasHeader()) {
                for (int i = 0; i < count; ++i) {
                    super.metadata.add("V" + (i + 1));
                }
            }
        } catch (IOException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }
    }

    protected List<DataRecord> parseRecords(int recordCount)
            throws InvalidDataException {
        List<DataRecord> records = new ArrayList<DataRecord>(recordCount);

        try {
            String[] line = null;
            boolean wasBuffered = false;
            Set<String> variables = super.getVariables();

            if (this.buffer != null) {
                line = this.buffer;
                wasBuffered = true;
            }

            while (this.hasRecords() && recordCount > 0) {
                if (!wasBuffered || (wasBuffered = false)) {
                    line = this.reader.readNext();
                }

                if (line != null) {
                    super.next();

                    DataRecord currentRecord = super.newRecord();

                    int index = 0;
                    for (String currentVariable : variables) {
                        currentRecord.setValue(currentVariable,
                                new DataEntry(line[index]));
                        ++index;
                    }

                    records.add(currentRecord);
                } else {
                    // We're done reading
                    super.markComplete();
                }

                --recordCount;
            }
        } catch (IOException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        return records;
    }

    private void setReader() throws InvalidDataException {
        Reader sourceFileReader;

        try {
            sourceFileReader = new InputStreamReader(new FileInputStream(this.source), Settings.get(
                "Engine.TextFileEncoding", "UTF-8"
            ));
        } catch (FileNotFoundException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.MissingSource,
                    "The source file is missing or is not a file");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }

        // Create the reader
        this.reader = new CSVReader(sourceFileReader, this.delimiter, this.qualifier);
    }
}
