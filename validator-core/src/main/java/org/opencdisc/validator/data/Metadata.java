/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.opencdisc.validator.model.DataDetails;
import org.opencdisc.validator.model.DataDetails.Info;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.model.EntityDetails.Reference;
import org.opencdisc.validator.util.KeyMap;

/**
 *
 * @author Tim Stone
 */
class Metadata implements DataSource {
    static final Set<String> VARIABLES = new LinkedHashSet<String>(Arrays.asList(
        "DOMAIN",
        "DATASET",
        "VARIABLE",
        "TYPE",
        "LENGTH",
        "LABEL",
        "ORDER",
        "FORMAT"
    ));

    private final InternalEntityDetails entity;
    private final InternalEntityDetails parent;
    private int recordCount = 0;
    private final List<DataRecord> records = new ArrayList<DataRecord>();
    private final Map<String, Integer> locations = new KeyMap<Integer>();

    Metadata(InternalEntityDetails entity) {
        this(null, entity);
    }

    private Metadata(InternalEntityDetails parent, InternalEntityDetails entity) {
        this.reset();
        this.parent = parent != null ? parent : entity;
        this.entity = new InternalEntityDetails(Reference.Metadata, entity, this.parent);
    }

    public void close() {
        // Do nothing
    }

    public InternalEntityDetails getDetails() {
        return this.entity;
    }

    public String getLocation() {
        return this.entity.getString(Property.Location);
    }

    public DataSource getMetadata() {
        throw new UnsupportedOperationException("cannot get metadata from metadata");
    }

    public String getName() {
        return this.entity.getString(Property.Name);
    }

    public int getRecordCount() {
        return this.recordCount;
    }

    public List<DataRecord> getRecords() throws InvalidDataException {
        return this.getRecords(this.records.size());
    }

    public List<DataRecord> getRecords(int recordCount) throws InvalidDataException {
        List<DataRecord> records = new ArrayList<DataRecord>();
        int current = this.recordCount;
        int bound = Math.min(current + recordCount, this.records.size());

        for ( ; current < bound; ++current) {
            records.add(this.records.get(current));
        }

        this.recordCount = current;

        return records;
    }

    public Set<String> getVariables() throws InvalidDataException {
        return VARIABLES;
    }

    public Object getVariableProperty(String variable, VariableProperty property) {
        throw new UnsupportedOperationException(
            "Can't get variable properties of a metadata source"
        );
    }

    public boolean hasRecords() {
        return this.recordCount < this.records.size();
    }

    public boolean isComposite() {
        return false;
    }

    public boolean isMetadata() {
        return true;
    }

    public DataSource replicate() throws InvalidDataException {
        Metadata duplicate = new Metadata(this.parent, this.entity);

        // Transfer the records
        duplicate.records.addAll(this.records);

        return duplicate;
    }

    public boolean test() {
        return true;
    }

    DataRecord getVariable(String name) {
        DataRecord variable = null;

        if (this.locations.containsKey(name)) {
            variable = this.records.get(this.locations.get(name));
        }

        return variable;
    }

    Set<String> getVariableNames() {
        return this.locations.keySet();
    }

    void add(String variable) {
        this.add(variable, null, null, null, null);
    }

    void add(String variable, String type, Integer length, String label, String format) {
        add(variable, type, length, label, format, format);
    }

    void add(String variable, String type, Integer length, String label, String format, String fullFormat) {
        int order = this.records.size() + 1;
        String domain = this.entity.getString(Property.Name).toUpperCase();
        String dataset = null;
        variable = variable.toUpperCase();

        if (this.entity.hasProperty(Property.Subname)) {
            dataset = this.entity.getString(Property.Subname);
        }

        DataRecord record = new DataRecord(
            new DataDetails(order, variable, Info.Variable),
            this.entity
        );

        record.setValue("DOMAIN", new DataEntry(domain));
        record.setValue("DATASET", new DataEntry(dataset));
        record.setValue("VARIABLE", new DataEntry(variable));
        record.setValue("TYPE", new DataEntry(type));
        record.setValue("LENGTH", new DataEntry(length));
        record.setValue("LABEL", new DataEntry(label));
        record.setValue("ORDER", new DataEntry((double)order));
        record.setValue("FORMAT", new DataEntry(format));

        this.records.add(record);
        this.locations.put(variable, this.records.size() - 1);
        this.entity.setProperty(Property.Variables, this.records.size());
        this.parent.addVariable(new InternalVariableDetails(
            variable, order, type, length, label, format, fullFormat
        ));
    }

    /**
     *
     */
    void reset() {
        this.recordCount = 0;
    }
}
