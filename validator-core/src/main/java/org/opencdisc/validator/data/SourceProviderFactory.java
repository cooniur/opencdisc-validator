/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.util.HashMap;
import java.util.Map;

import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public class SourceProviderFactory {
    private static final Map<ProcessToken, SourceProvider> PROVIDERS =
        new HashMap<ProcessToken, SourceProvider>();
    private SourceProviderFactory() {}

    /**
     *
     * @param token
     */
    public static synchronized void clear(ProcessToken token) {
        SourceProvider provider = PROVIDERS.remove(token);

        if (provider != null) {
            provider.close();
        }
    }

    /**
     *
     * @return
     */
    public static SourceProvider getSourceProvider() {
        return SourceProviderFactory.getSourceProvider(ProcessToken.DEFAULT_TOKEN);
    }

    /**
     *
     * @param token
     * @return
     */
    public static synchronized SourceProvider getSourceProvider(ProcessToken token) {
        if (PROVIDERS.containsKey(token) == false) {
            PROVIDERS.put(token, new SourceProvider());
        }

        return PROVIDERS.get(token);
    }
}
