/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.Text;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.util.Helpers;
import org.opencdisc.validator.util.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tim Stone
 */
public final class SasTransportDataSource extends AbstractDataSource {
    private static final Logger LOG = LoggerFactory.getLogger(SasTransportDataSource.class);
    private static final int BUFFER_MULTIPLIER;
    private static final int CHAR_COLUMN_TYPE = 2;
    private static final int DEFAULT_BUFFER_SIZE = 80;
    private static final int EXTENDED_BUFFER_SIZE = 140;
    private static final int INPUT_BUFFER_SIZE = 1024 * 24;
    private static final byte[] LIBRARY_HEADER_BYTES = {
        72, 69, 65, 68, 69, 82, 32, 82, 69, 67, 79, 82, 68, 42, 42, 42, 42, 42, 42,
        42, 76, 73, 66, 82, 65, 82, 89, 32, 72, 69, 65, 68, 69, 82, 32, 82, 69, 67,
        79, 82, 68, 33, 33, 33, 33, 33, 33, 33, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48
    };
    private static final byte[] DSCRPTR_HEADER_BYTES = {
        72, 69, 65, 68, 69, 82, 32, 82, 69, 67, 79, 82, 68, 42, 42, 42, 42, 42, 42,
        42, 68, 83, 67, 82, 80, 84, 82, 32, 72, 69, 65, 68, 69, 82, 32, 82, 69, 67,
        79, 82, 68, 33, 33, 33, 33, 33, 33, 33, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48, 32, 32
    };
    private static final byte[] NAMESTR_HEADER_BYTES = {
        72, 69, 65, 68, 69, 82, 32, 82, 69, 67, 79, 82, 68, 42, 42, 42, 42, 42, 42,
        42, 78, 65, 77, 69, 83, 84, 82, 32, 72, 69, 65, 68, 69, 82, 32, 82, 69, 67,
        79, 82, 68, 33, 33, 33, 33, 33, 33, 33, 48, 48, 48, 48, 48
    };
    private static final int NAMESTR_NUM_POSTITION = 54;
    private static final int DSCRPTR_LABEL_POSITION = 32;
    private static final int NUM_COLUMN_TYPE = 1;
    private static final byte[] OBS_HEADER_BYTES = {
        72, 69, 65, 68, 69, 82, 32, 82, 69, 67, 79, 82, 68, 42, 42, 42, 42, 42, 42,
        42, 79, 66, 83, 32, 32, 32, 32, 32, 72, 69, 65, 68, 69, 82, 32, 82, 69, 67,
        79, 82, 68, 33, 33, 33, 33, 33, 33, 33, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48
    };
    private static final DecimalFormat TRUNCATION_FORMAT =
        new DecimalFormat("#.00000000");
    private static final boolean USE_CLASSIC_ROUNDING;
    private final long fileSize;
    private FileInputStream inputStream = null;
    private FileChannel inputChannel = null;
    private ByteBuffer inputBuffer = ByteBuffer.allocateDirect(INPUT_BUFFER_SIZE);
    private int inputBufferSize = INPUT_BUFFER_SIZE;
    private int inputByteCount = 0;
    private boolean lastRecordWasNull = false;
    private int nullRecordCount = 0;
    private long totalBytesRead = 0;
    private int recordByteCount = 0;
    private List<Variable> variables;

    static {
        DecimalFormatSymbols symbols = TRUNCATION_FORMAT.getDecimalFormatSymbols();

        symbols.setDecimalSeparator('.');

        TRUNCATION_FORMAT.setDecimalFormatSymbols(symbols);

        // Determine the buffer multiplier
        int multiplier = 3;

        try {
            if (Settings.defines("Engine.InputBufferSize")) {
                multiplier = Integer.parseInt(Settings.get("Engine.InputBufferSize"));
            } else if (Settings.defines("SasInputBufferSize")) {
                multiplier = Integer.parseInt(Settings.get("SasInputBufferSize"));
            }
        } catch (NumberFormatException ignore) { }

        BUFFER_MULTIPLIER = multiplier;

        if (Settings.defines("Engine.SasRoundingMode")) {
            USE_CLASSIC_ROUNDING = Settings.get("Engine.SasRoundingMode")
                    .equalsIgnoreCase("Old");
        } else {
            USE_CLASSIC_ROUNDING = false;
        }

        LOG.debug("Using a buffer multiplier of: {}", BUFFER_MULTIPLIER);
        LOG.debug("Using classical rounding mode: {}", USE_CLASSIC_ROUNDING);
    }

    /**
     *
     * @param options
     *
     * @throws InvalidDataException
     */
    public SasTransportDataSource(SourceOptions options) throws InvalidDataException {
        super(options);

        File source = new File(super.options.getSource());

        if (!(source.isFile() && source.exists())) {
            LOG.debug("Throwing exception because {} wasn't found", source);

            // Someone didn't do a good job at checking, so...
            throw new InvalidDataException(
                    InvalidDataException.Codes.MissingSource,
                    Text.get("Exceptions.FileNotFound")
            );
        }

        this.fileSize = source.length();
        this.entity.setProperty(EntityDetails.Property.FileSize, this.fileSize);

        // Create the streams
        try {
            this.inputStream  = new FileInputStream(source);
            this.inputChannel = this.inputStream.getChannel();
        } catch (FileNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }

    public DataSource replicate() throws InvalidDataException {
        return new SasTransportDataSource(super.options);
    }

    protected List<DataRecord> parseRecords(int recordCount)
            throws InvalidDataException {
        List<DataRecord> records = new ArrayList<DataRecord>();
        int remainingRecordCount = recordCount;

        while (super.hasRecords() && remainingRecordCount > 0) {
            ByteBuffer buffer = this.getBuffer(this.recordByteCount);

            if (buffer != null && buffer.limit() == this.recordByteCount) {
                // Check if we're at the meaningful end of the dataset, in cases where
                // the records are really small and we're near the literal end
                if (this.recordByteCount <= DEFAULT_BUFFER_SIZE &&
                        this.totalBytesRead - this.recordByteCount >
                                this.fileSize - DEFAULT_BUFFER_SIZE) {
                    boolean looksNull = true;

                    for (int i = 0; i < this.recordByteCount; ++i) {
                        if (buffer.get() != 32) {
                            looksNull = false;
                            break;
                        }
                    }

                    buffer.rewind();

                    if (looksNull) {
                        if (this.lastRecordWasNull == false) {
                            this.lastRecordWasNull = true;
                        }

                        ++this.nullRecordCount;
                    } else {
                        this.lastRecordWasNull = false;
                        this.nullRecordCount   = 0;
                    }
                }

                super.next();

                DataRecord currentRecord = super.newRecord();
                int count = this.variables.size();
                // Iterate over the SAS variables and extract their values
                for (int i = 0; i < count; ++i) {
                    Variable variable = this.variables.get(i);
                    DataEntry recordVariableValue = null;

                    int observationByteSize = variable.getLength();
                    byte[] observationBytes = new byte[observationByteSize];

                    // Get the relevant observation bytes
                    buffer.get(observationBytes, 0, observationByteSize);

                    ByteBuffer observation = ByteBuffer.wrap(observationBytes);

                    // Check the type to determine the parsing method
                    if (variable.getType() == Variable.VariableTypes.Char) {
                        // The variable is character data, easy enough to parse
                        int bound   = observation.limit();
                        byte[] data = new byte[bound];

                        // Pull out the data
                        observation.get(data, 0, data.length);

                        String field = toString(data);

                        // Right-trim the data (preserve spaces to the left)
                        for (int n = field.length() - 1; n >= 0; --n) {
                            if (field.charAt(n) != (char)32) {
                                field = field.substring(0, n + 1);

                                break;
                            } else if (n == 0) {
                                // Empty (null) string
                                field = "";
                            }
                        }

                        // That's it, create a DataEntry for the value
                        recordVariableValue = new DataEntry(field.intern());
                    } else {
                        int adjust = 8 - observation.capacity();

                        if (adjust > 0) {
                            ByteBuffer tempBuffer = observation;
                            observation = ByteBuffer.allocate(8);

                            for (int j = 0; j < tempBuffer.capacity(); ++j) {
                                observation.put(tempBuffer.get());
                            }

                            tempBuffer.rewind();

                            for (int j = 0; j < adjust; ++j) {
                                observation.put((byte)0);
                            }
                        }

                        // Rewind the buffer, just in case we made modifications
                        observation.rewind();

                        boolean hasValue = false;
                        byte[] dataBytes = observation.array();

                        for (int j = 1; j < dataBytes.length; ++j) {
                            if (dataBytes[j] != 0) {
                                hasValue = true;
                                break;
                            }
                        }

                        if (hasValue == false) {
                            if (dataBytes[0] != 0x5F && dataBytes[0] != 0x2E &&
                                    (dataBytes[0] < 0x41 || dataBytes[0] > 0x5A)) {
                                hasValue = dataBytes[0] != 0;
                            }
                        }

                        if (hasValue) {
                            long xport1   = observation.getInt() & 0x00000000FFFFFFFFL;
                            long xport2   = observation.getInt() & 0x00000000FFFFFFFFL;

                            observation.rewind();

                            byte exponent = observation.get();
                            long ieee1    = xport1 & 0x00FFFFFF;
                            long ieee2    = xport2;
                            short shift;
                            long nib      = (int)xport1;

                            if ((nib & 0x00800000) != 0) {
                                shift = 3;
                            } else if ((nib & 0x00400000) != 0) {
                                shift = 2;
                            } else if ((nib & 0x00200000) != 0) {
                                shift = 1;
                            } else {
                                shift = 0;
                            }

                            if (shift > 0) {
                                ieee1 >>= shift;
                                ieee2 = (xport2 >>> shift) |
                                        ((xport1 & 0x00000007) << (29 + (3 - shift)));
                            }

                            ieee1 &= 0xFFEFFFFF;
                            ieee1 |= (((((long)(exponent & 0x7F) - 65) << 2) + shift +
                                    1023) << 20) | (xport1 & 0x80000000);

                            int ieee1conv = (int)ieee1;
                            int ieee2conv = (int)ieee2;
                            observation.rewind();
                            observation.putInt(ieee1conv);
                            observation.putInt(ieee2conv);
                            observation.rewind();

                            Double field;

                            if (USE_CLASSIC_ROUNDING) {
                                field = new Double(
                                        TRUNCATION_FORMAT.format(observation.getDouble())
                                );
                            } else {
                                field = Helpers.round(observation.getDouble());
                            }

                            // Now create the DataEntry wrapper
                            recordVariableValue = new DataEntry(field);
                        } else {
                            // The field doesn't have data in it
                            if (dataBytes[0] != 0) {
                                recordVariableValue = new DataEntry(null);
                            } else {
                                recordVariableValue = new DataEntry(0.0);
                            }
                        }
                    }

                    // Finally, set the value for this variable in the current record
                    currentRecord.setValue(variable.getName(), recordVariableValue);
                }

                // Add the record to the return list
                records.add(currentRecord);

                --remainingRecordCount;
            }
        }

        if (this.nullRecordCount > 0) {
            super.rewind(this.nullRecordCount);

            LOG.debug("Discarding {} records believed to be null", this.nullRecordCount);

            for (int i = 0; i < this.nullRecordCount && !records.isEmpty(); ++i) {
                records.remove(records.size() - 1);
            }

            this.nullRecordCount = 0;
        }

        return records;
    }

    protected void parseVariables() throws InvalidDataException {
        ByteBuffer buffer = null;
        boolean validFile = false;
        int variableCount = 0;
        int trailing = 0;

        // Loop until we find the NAMESTR header or run out of file
        while (super.hasRecords() && (buffer = this.getBuffer()) != null) {
            if (buffer.equals(ByteBuffer.wrap(DSCRPTR_HEADER_BYTES))) {
                trailing = 2;
            } else if (trailing > 0) {
                if (trailing == 1) {
                    byte[] string = new byte[40];

                    buffer.position(DSCRPTR_LABEL_POSITION);
                    buffer.get(string);

                    this.entity.setProperty(EntityDetails.Property.Label, new String(string).trim());
                    this.entity.setProperty(EntityDetails.Property.DatasetLabel, new String(string).trim());
                }

                --trailing;
            } else {
                byte[] comparableBytes = new byte[NAMESTR_NUM_POSTITION - 1];

                buffer.get(comparableBytes, 0, NAMESTR_NUM_POSTITION - 1);

                LOG.debug("Testing whether the buffer starts with NAMESTR_HEADER_BYTES");

                // Check to see if this is the NAMESTR header
                if (ByteBuffer.wrap(comparableBytes).equals(
                    ByteBuffer.wrap(NAMESTR_HEADER_BYTES)
                )) {
                    // It does, let's find out how many variables we have
                    char[] countString = new char[4];

                    buffer.position(NAMESTR_NUM_POSTITION);

                    for (int i = 0; i < 4; ++i) {
                        countString[i] = (char) buffer.get();
                    }

                    // Mark the fact we got this far and get the variable count
                    variableCount = Integer.parseInt(String.valueOf(countString).trim());
                    validFile = true;

                    // We're done looping
                    break;
                }
            }
        }

        if (!validFile) {
            LOG.debug("Throwing exception because the file is considered invalid");

            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        if (variableCount == 0) {
            LOG.debug("Throwing exception because the file doesn't have any variables");

            throw new InvalidDataException(InvalidDataException.Codes.NoVariables);
        }

        this.variables = new ArrayList <Variable>(variableCount);

        for (int i = 0; i < variableCount; ++i) {
            buffer = this.getBuffer(EXTENDED_BUFFER_SIZE);

            // Get the type number
            short type = buffer.getShort();
            // Skip the next two bytes
            buffer.position(buffer.position() + 2);
            // Get the variable observation length
            short length = buffer.getShort();
            // Skip the next two bytes
            buffer.position(buffer.position() + 2);
            // Get the variable name
            byte[] name = new byte[8];
            buffer.get(name);
            String nameString = toString(name).trim();
            // Get the variable label
            byte[] label = new byte[40];
            buffer.get(label);
            String labelString = toString(label).trim();

            byte[] format = new byte[8];
            buffer.get(format);
            String formatString = toString(format).trim();

            Variable variable = new Variable(nameString, type, length);

            // TODO: Hack..
            if (labelString.length() == 0) {
                labelString = "null";
            }

            // Add the variable
            super.metadata.add(
                    variable.getName(),
                    variable.getType().toString(),
                    new Integer(length),
                    labelString,
                    formatString
            );

            // Store the additional variable information
            this.variables.add(variable);

            // Increase the record size
            this.recordByteCount = this.recordByteCount + variable.getLength();
        }

        // Discard any remaining pad-bytes
        if ((this.totalBytesRead % DEFAULT_BUFFER_SIZE) > 0) {
            this.getBuffer(DEFAULT_BUFFER_SIZE -
                    (int)(this.totalBytesRead % DEFAULT_BUFFER_SIZE));
        }

        LOG.debug("Testing whether the next buffer starts with OBS_HEADER_BYTES");

        // Finally, verify that the next record is the OBS record
        if (this.bufferStartsWith(this.getBuffer(),
                ByteBuffer.wrap(OBS_HEADER_BYTES)) == false) {
            LOG.debug("Throwing exception because test failed");

            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }
    }

    public boolean test() {
        boolean result = true;

        try {
            // Check to see if the file begins with the LIBRARY header
            ByteBuffer testBuffer = this.getBuffer();

            if (testBuffer == null || this.bufferStartsWith(testBuffer,
                    ByteBuffer.wrap(LIBRARY_HEADER_BYTES)) == false) {
                result = false;
            }
        } catch (InvalidDataException ex) {
            result = false;
        }

        LOG.debug("Testing whether the file {} starts with LIBRARY_HEADER_BYTES: {}",
            this.getName(), result);

        return result;
    }

    public void close() {
        try {
            this.inputChannel.close();
        } catch (IOException ex) {

        } finally {
            try {
                this.inputStream.close();
            } catch (IOException ex) {}
        }
    }

    /**
     *
     * @return
     * @throws InvalidDataException
     */
    private ByteBuffer getBuffer() throws InvalidDataException {
        return this.getBuffer(DEFAULT_BUFFER_SIZE);
    }

    /**
     *
     * @param bufferSize
     * @return
     * @throws IllegalArgumentException
     * @throws InvalidDataException
     */
    private ByteBuffer getBuffer(int bufferSize) throws IllegalArgumentException,
            InvalidDataException {
        if (bufferSize <= 0) {
            LOG.debug("Throwing exception because the passed buffer size was zero");

            throw new IllegalArgumentException("bufferSize must be greater than zero");
        }

        ByteBuffer bytes = ByteBuffer.allocate(bufferSize);
        long bytesRead   = 0;

        try {
            if (bufferSize >= this.inputBufferSize ||
                    (this.inputByteCount < bufferSize && this.inputByteCount != 0)) {
                // Read out the rest of the buffer
                while (this.inputByteCount > 0) {
                    // Transfer the byte
                    bytes.put(this.inputBuffer.get());

                    // Decrement the remaining byte count
                    --this.inputByteCount;
                }

                if (bufferSize >= this.inputBufferSize) {
                    // Change the buffer size
                    this.inputBufferSize = bufferSize * BUFFER_MULTIPLIER *
                            DEFAULT_RECORD_COUNT;
                    // Reset the buffer
                    this.inputBuffer = ByteBuffer.allocateDirect(this.inputBufferSize);

                    LOG.debug("Changing buffer size to {}", this.inputBufferSize);
                }

                // Make sure to rewind the buffer
                this.inputBuffer.rewind();
                // Read into the current buffer, and the new buffer
                this.inputChannel.read(new ByteBuffer[] { bytes, this.inputBuffer });

                // Update the counts
                bytesRead = bytes.position();
                this.inputByteCount = this.inputBuffer.position();

                // Rewind both buffers
                bytes.rewind();
                this.inputBuffer.rewind();
            } else {
                if (this.inputByteCount == 0) {
                    // Rewind the buffer
                    this.inputBuffer.rewind();

                    // Read into the buffer
                    this.inputChannel.read(this.inputBuffer);

                    // Update the count
                    this.inputByteCount = this.inputBuffer.position();

                    // Rewind the buffer
                    this.inputBuffer.rewind();
                }

                bytesRead = Math.min(this.inputByteCount, bufferSize);

                for (int i = 0; i < bytesRead; ++i) {
                    bytes.put(this.inputBuffer.get());
                }

                this.inputByteCount = this.inputByteCount - (int)bytesRead;
            }

            this.totalBytesRead = this.totalBytesRead + bytesRead;

            if (bytesRead < bufferSize || (this.fileSize - this.totalBytesRead) == 0) {
                // We're done reading
                super.markComplete();
            }
        } catch (IOException ex) {
            LOG.debug("Throwing exception because a parse error occured: {}", ex);

            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        // Be sure to rewind the buffer
        bytes.rewind();

        return (bytesRead == bufferSize ? bytes : null);
    }

    /**
     *
     * @param buffer
     * @param startsWith
     * @return
     */
    private boolean bufferStartsWith(ByteBuffer buffer, ByteBuffer startsWith) {
        if (buffer == null) {
            LOG.debug("Throwing exception because buffer was null");

            throw new IllegalArgumentException("buffer cannot be null");
        }

        boolean result = true;

        if (startsWith != null) {
            if (buffer.capacity() < startsWith.capacity()) {
                LOG.debug("Throwing exception because buffer wasn't large enough to " +
                        "perform comparison");

                throw new IllegalArgumentException("buffer must be at least as large "
                        + "as startsWith");
            }

            int bufPosition = buffer.position();
            int stwPosition = startsWith.position();

            buffer.rewind();
            startsWith.rewind();

            StringBuilder debugger = null;

            if (LOG.isDebugEnabled()) {
                debugger = new StringBuilder();
            }

            for (int i = 0; i < startsWith.capacity() && result; ++i) {
                byte got = buffer.get();

                if (debugger != null) {
                    if (debugger.length() > 0) {
                        debugger.append(" ");
                    }

                    debugger.append(got);
                }

                result = startsWith.get() == got;
            }

            if (!result && debugger != null) {
                LOG.debug("Failed buffer comparison after input of {}",
                        debugger.toString());
            }

            buffer.position(bufPosition);
            startsWith.position(stwPosition);
        } else {
            result = false;
        }

        return result;
    }

    private static String toString(byte[] characters) {
        try {
            return new String(characters, Settings.get("Engine.SasFileEncoding", "ISO-8859-1"));
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     *
     * @author Tim Stone
     */
    private static class Variable {
        enum VariableTypes {
            Num, Char
        }

        private final VariableTypes variableType;

        private final String variableName;

        private final short length;

        /**
         *
         * @param name
         * @param type
         * @param length
         */
        Variable(String name, short type, short length) {
            this.variableName = name;
            this.length = length;

            switch (type) {
            case SasTransportDataSource.NUM_COLUMN_TYPE:
                this.variableType = VariableTypes.Num;
                break;
            case SasTransportDataSource.CHAR_COLUMN_TYPE:
                // Fall-through
            default:
                this.variableType = VariableTypes.Char;
                break;
            }
        }

        /**
         *
         * @return
         */
        short getLength() {
            return this.length;
        }

        /**
         *
         * @return
         */
        String getName() {
            return this.variableName;
        }

        /**
         *
         * @return
         */
        VariableTypes getType() {
            return this.variableType;
        }
    }
}
