/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.SourceOptions.Type;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.Settings;

/**
 *
 * @author Tim Stone
 */
public class SourceProvider {
    private final Map<String, DataSource> sources = new KeyMap<DataSource>();
    private final Set<String> unavailable = new HashSet<String>();

    /**
     *
     * @param source
     */
    public void addSource(SourceOptions source) {
        if (this.isFileBasedSource(source)) {
            File input = new File(source.getSource());

            if (input.exists()) {
                File[] resolvedFileList;
                String rootDirectoryPath;

                if (input.isDirectory()) {
                    resolvedFileList = input.listFiles();
                    rootDirectoryPath = input.getPath();
                } else {
                    resolvedFileList = new File[] { input };
                    rootDirectoryPath = input.getParent();
                }

                for (File resolvedFile : resolvedFileList) {
                    // Skip directories and "hidden" files
                    if (resolvedFile.isDirectory() ||
                            resolvedFile.isHidden()) {
                        continue;
                    }

                    File sourceFile = new File(
                        rootDirectoryPath + File.separator + resolvedFile.getName()
                    );
                    String sourcePath = sourceFile.getAbsolutePath();
                    source = new SourceOptions(sourcePath, source);
                    String sourceName = source.getName();
                    boolean needsName = sourceName == null;

                    sourceName = sourceFile.getName();
                    int index = sourceName.lastIndexOf('.');

                    if (index > -1) {
                        sourceName = sourceName.substring(0, index);
                    }

                    if (needsName) {
                        source.setName(sourceName);
                    }

                    // Just always set subname, because the XX, XXYY case of split
                    // datasets has a tendency to mess up in this case
                    if (StringUtils.isEmpty(source.getSubname())) {
                        source.setSubname(sourceName);
                    }

                    DataSource generatedSource = this.createFileBasedSource(source);

                    // Check if a source was created, and test it for usability
                    if (generatedSource != null && generatedSource.test()) {
                        // Check if the source might be part of a composite source
                        DataSource existing = this.sources.get(source.getName());

                        if (existing != null) {
                            if (!existing.isComposite()) {
                                existing.getDetails().setProperty(Property.Split, true);
                                existing = new CombinedDataSource(existing);

                                // Replace the existing normal source with the new
                                // composite one
                                this.sources.put(source.getName(), existing);
                            }

                            if (!(existing instanceof CombinedDataSource)) {
                                throw new RuntimeException("existing was inexplicably "
                                        + "not a CombinedDataSource");
                            }

                            generatedSource.getDetails().setProperty(Property.Split,
                                    true);

                            ((CombinedDataSource)existing).add(generatedSource);
                        } else {
                            this.sources.put(source.getName(), generatedSource);
                        }
                    } else {
                        this.unavailable.add(sourcePath);
                    }
                }
            } else {
                String name = input.getAbsolutePath();

                // TODO: This may have unintended consequences, test
                if (source.getName() != null) {
                    name = source.getName();
                }

                this.unavailable.add(name);
            }
        } else {
            throw new UnsupportedOperationException("Sources can currently only be "
                    + "file-based");
        }
    }

    /**
     *
     */
    public void close() {
        this.sources.clear();
    }

    /**
     *
     * @param sourceName
     * @return
     */
    public boolean containsSource(String sourceName) {
        return this.sources.containsKey(sourceName);
    }

    /**
     *
     * @return
     */
    public Set<String> getInvalidSourceNames() {
        return this.unavailable;
    }

    /**
     *
     * @param sourceName
     * @return
     */
    public DataSource getSource(String sourceName) {
        return this.getSource(sourceName, false);
    }

    /**
     *
     * @param sourceName
     * @param replicated
     * @return
     */
    public DataSource getSource(String sourceName, boolean replicated) {
        if (this.sources.containsKey(sourceName) == false) {
            throw new IllegalArgumentException(String.format("Unknown source %s",
                    sourceName));
        }

        DataSource source = this.sources.get(sourceName);

        if (source != null && replicated) {
            try {
                source = source.replicate();
            } catch (InvalidDataException ex) {}
        }

        return source;
    }

    /**
     *
     * @return
     */
    public Set<String> getSourceNames() {
        return this.sources.keySet();
    }

    private boolean isFileBasedSource(SourceOptions source) {
        return source.getType() != Type.Database;
    }

    private DataSource createFileBasedSource(SourceOptions options) {
        DataSource source = null;
        SourceOptions.Type type = options.getType();

        try {
            if (type == SourceOptions.Type.SasTransport) {
                if (Settings.defines("Engine.UseEnhancedSas")) {
                    source = new EnhancedSasTransportDataSource(options);
                } else {
                    source = new SasTransportDataSource(options);
                }
            } else if (type == SourceOptions.Type.Delimited) {
                source = new DelimitedDataSource(options);
            } else if (type == Type.DatasetXml) {
                source = new DatasetXmlDataSource(options);
            } else if (type == SourceOptions.Type.Xml) {
                source = new XmlDataSource(options);
            } else if (type == Type.UseProtocol) {
                // TODO: We're not actually checking the protocol here yet
                //   In the future we want to allow dynamically registered data sources,
                //   at which point they'll need to be created via reflection based on
                //   the provided protocol information. For now though, we know this is
                //   all we're getting.
                source = new ControlledTerminologyDataSource(options);
            }
        } catch (InvalidDataException ex) {
            ex.printStackTrace();
        }

        return source;
    }
}
