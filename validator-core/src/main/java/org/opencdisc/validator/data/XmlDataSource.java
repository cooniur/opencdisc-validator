/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.util.List;

import org.opencdisc.validator.SourceOptions;

/**
 *
 * @author Tim Stone
 */
public class XmlDataSource extends AbstractDataSource {
    public XmlDataSource(SourceOptions options) throws InvalidDataException {
        super(options);
    }

    @Override
    protected List<DataRecord> parseRecords(int recordCount)
            throws InvalidDataException {
        throw new UnsupportedOperationException("Cannot read records from an "
                + "XmlDataSource");
    }

    @Override
    protected void parseVariables() throws InvalidDataException {
        throw new UnsupportedOperationException("Cannot read variables from an "
                + "XmlDataSource");
    }

    public void close() {
        // Do nothing
    }

    public DataSource replicate() throws InvalidDataException {
        return this;
    }

    public boolean test() {
        return true;
    }
}
