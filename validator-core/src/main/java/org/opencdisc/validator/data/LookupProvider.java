/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.lang.ref.Reference;
import java.util.Set;

import org.opencdisc.validator.EventDispatcher;
import org.opencdisc.validator.util.ProcessToken;

/**
 * Allows for metadata verification and lookup access to a variety of sources, loaded
 * from external data, or provided from local runtime data via calls to
 * <code>{@link #provideReference(Reference)}</code>.
 *
 * @author Tim Stone
 */
public interface LookupProvider {
    /**
     *
     * @param token
     */
    public void clear(ProcessToken token);

    /**
     *
     * @param sourceName
     * @param variables
     * @param token
     *
     * @return
     */
    public Lookup get(String sourceName, Set<String> variables, ProcessToken token);

    /**
     *
     * @param sourceName
     * @param variables
     * @param token
     */
    public void request(String sourceName, Set<String> variables, ProcessToken token);

    /**
     *
     * @param sourceName
     * @param token
     *
     * @return
     */
    public boolean verifyExists(String sourceName, ProcessToken token);

    /**
     *
     * @param sourceName
     * @param variables
     * @param token
     *
     * @return
     */
    public boolean verifyExists(String sourceName, Set<String> variables,
        ProcessToken token);

    /**
     *
     * @param dispatcher
     */
    public void setDispatcher(EventDispatcher dispatcher);
}
