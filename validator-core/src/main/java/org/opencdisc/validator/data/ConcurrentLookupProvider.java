/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.EventDispatcher;
import org.opencdisc.validator.util.ConcurrentCacheMap;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public class ConcurrentLookupProvider implements LookupProvider {
    private final ConcurrentMap<ProcessToken, Partition> partitions =
        new ConcurrentHashMap<ProcessToken, Partition>();
    private EventDispatcher dispatcher = null;

    public void clear(ProcessToken token) {
        synchronized (token) {
            if (this.partitions.containsKey(token)) {
                this.partitions.get(token).clear();
            }
        }
    }

    public Lookup get(String sourceName, Set<String> variables, ProcessToken token) {
        Partition partition = this.getPartition(token);

        while (true) {
            DataCache cache = partition.getCaches().get(sourceName);

            if (cache != null) {
                if (cache.contains(variables)) {
                    // The cache contains all of the variables we need
                    synchronized (cache) {
                        if (!cache.isComplete()) {
                            try {
                                cache.wait();
                            } catch (InterruptedException ex) {
                                throw new RuntimeException(ex);
                            }
                        }

                        if (!cache.isCorrupt()) {
                            return cache;
                        } else {
                            return null;
                        }
                    }
                } else if (!this.verifyExists(sourceName, variables, token)) {
                    // The cache can't be created
                    return null;
                }
            }

            // We're going to need to parse some sort of source
            SourceOptions  options  = this.parseSourceName(sourceName);
            SourceProvider provider = SourceProviderFactory.getSourceProvider(token);

            if (cache == null) {
                // The cache doesn't exist at all
                if ((options.getSource() == null &&
                        !this.verifyExists(sourceName, variables, token)) ||
                        provider.getInvalidSourceNames().contains(sourceName)) {
                    // We can't generate this source, because it's a missing dataset or
                    // invalid source of some type
                    return null;
                }

                cache = new DataCache(variables);

                if (partition.getCaches().putIfAbsent(sourceName, cache) == null) {
                    // This will be the thread responsible for filling the cache
                    synchronized (cache) {
                        // Determine if the cache can be made
                        if (!provider.containsSource(sourceName)) {
                            provider.addSource(options);
                        }

                        if (!provider.containsSource(sourceName)) {
                            // Mark cache as corrupted
                            cache.setCorrupt(true);
                            // Remove the cache
                            partition.getCaches().remove(sourceName, cache);
                            // Mark the cache as complete
                            cache.setComplete(true);
                            // Notify all waiters
                            cache.notifyAll();
                            // The source still doesn't exist
                            return null;
                        }

                        DataSource source = provider.getSource(sourceName, true);

                        try {
                            if (!source.getVariables().containsAll(variables)) {
                                // Mark cache as corrupted
                                cache.setCorrupt(true);
                                // Remove the cache
                                partition.getCaches().remove(sourceName, cache);
                                // The source won't work to generate a cache from
                                return null;
                            }

                            while (source.hasRecords()) {
                                cache.store(source.getRecords());

                                if (this.dispatcher != null) {
                                    this.dispatcher.fireSubtaskIncrement(
                                            source.getName(),
                                            source.getRecordCount()
                                    );
                                }
                            }

                            return cache;
                        } catch (InvalidDataException ex) {
                            // Mark cache as corrupted
                            cache.setCorrupt(true);
                            // Remove the cache
                            partition.getCaches().remove(sourceName, cache);
                            // The source won't work to generate a cache from
                            return null;
                        } finally {
                            // Be sure to close the source
                            source.close();
                            // Mark the cache as complete
                            cache.setComplete(true);
                            // Notify all waiters
                            cache.notifyAll();
                        }
                    }
                } else {
                    // Someone's added a cache while we were doing this, so just repeat
                    // the whole process
                    continue;
                }
            } else {
                // The cache needs to be partially regenerated
                if (!this.verifyExists(sourceName, variables, token)) {
                    // We already have a cache, so we know this is an obtainable source,
                    // but does it have the right variables?
                    return null;
                }

                synchronized (cache) {
                    DataCache current = partition.getCaches().get(sourceName);

                    if (current != cache && current.contains(variables)) {
                        // The cache has already been updated by a previous thread, so
                        // just use this
                        return current;
                    }

                    DataCache updated = new DataCache(variables, cache);
                    DataSource source = provider.getSource(sourceName, true);

                    try {
                        source.getVariables();

                        while (source.hasRecords()) {
                            updated.store(source.getRecords());

                            if (this.dispatcher != null) {
                                this.dispatcher.fireSubtaskIncrement(source.getName(),
                                        source.getRecordCount());
                            }
                        }

                        // Replace the existing cache
                        partition.getCaches().replace(sourceName, cache, updated);

                        return updated;
                    } catch (InvalidDataException ex) {
                        // The source won't work to generate a cache from
                        return null;
                    } finally {
                        // Be sure to close the source
                        source.close();
                        // Mark the cache as complete
                        updated.setComplete(true);
                    }
                }
            }
        }
    }

    public synchronized void request(String sourceName, Set<String> variables,
            ProcessToken token) {
        synchronized (token) {
            Map<String, Integer> requestedVariables;

            Partition partition = this.getPartition(token);

            if (partition.getRequests().containsKey(sourceName)) {
                requestedVariables = partition.getRequests().get(sourceName);
            } else {
                requestedVariables = new KeyMap<Integer>();
            }

            for (String variable : variables) {
                if (requestedVariables.containsKey(variable)) {
                    requestedVariables.put(variable,
                            requestedVariables.get(variable) + 1);
                } else {
                    requestedVariables.put(variable, 1);
                }
            }

            partition.getRequests().put(sourceName, requestedVariables);
        }
    }

    public void setDispatcher(EventDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public boolean verifyExists(String sourceName, ProcessToken token) {
        return SourceProviderFactory.getSourceProvider(token).containsSource(sourceName);
    }

    public boolean verifyExists(String sourceName, Set<String> variables,
            ProcessToken token) {
        boolean result = false;

        if (this.verifyExists(sourceName, token)) {
            DataSource source = SourceProviderFactory.getSourceProvider(token)
                    .getSource(sourceName);

            try {
                result = source.getVariables().containsAll(variables);
            } catch (InvalidDataException ex) {
                result = false;
            }
        }

        return result;
    }

    private Partition getPartition(ProcessToken token) {
        synchronized (this.partitions) {
            if (!this.partitions.containsKey(token)) {
                this.partitions.put(token, new Partition());
            }
        }

        return this.partitions.get(token);
    }

    private SourceOptions parseSourceName(String sourceName) {
        if (sourceName == null || sourceName.length() == 0) {
            throw new IllegalArgumentException("sourceName must not be null and must "
                    + "have length > 0");
        }

        SourceOptions source = null;

        if (sourceName.matches("[A-Za-z][A-Za-z0-9]*")) {
            source = new SourceOptions(null);
        } else if (sourceName.contains(":")) {
            String[] pieces = sourceName.split(":", 3);

            if (pieces.length == 3) {
                String protocol = pieces[0];
                String typeName = pieces[1];
                String location = pieces[2];

                if (protocol.equalsIgnoreCase("File")) {
                    source = new SourceOptions(location);

                    source.setName(sourceName);

                    if (typeName.equalsIgnoreCase("XPT")) {
                        source.setType(SourceOptions.Type.SasTransport);
                    } else {
                        source.setType(SourceOptions.Type.Delimited);
                        pieces = typeName.split("-");
                        typeName = pieces[0];

                        if (typeName.equalsIgnoreCase("TAB")) {
                            source.setDelimiter("\t");
                        } else if (typeName.equalsIgnoreCase("CSV")) {
                            source.setDelimiter(",");
                        } else if (typeName.equalsIgnoreCase("PIPE")) {
                            source.setDelimiter("|");
                        } else if (typeName.equalsIgnoreCase("DOLLAR")) {
                            source.setDelimiter("$");
                        } else if (typeName.equalsIgnoreCase("CDISC")) {
                            source.setType(SourceOptions.Type.UseProtocol);
                        } else {
                            throw new IllegalArgumentException(String.format(
                                    "Unknown type name %s in sourceName",
                                    typeName
                            ));
                        }

                        if (pieces.length == 2 && pieces[1].equalsIgnoreCase("HEADERLESS")) {
                            source.hasHeader(false);
                        }
                    }
                } else {
                    throw new IllegalArgumentException(String.format(
                            "Unknown protocol %s in sourceName",
                            protocol
                    ));
                }
            } else {
                throw new IllegalArgumentException("sourceName in this format should  "
                        + "have three components");
            }
        } else {
            throw new IllegalArgumentException("sourceName is in some unknown format");
        }

        return source;
    }

    private static class Partition {
        private final Map<String, Map<String, Integer>> requests
                = new KeyMap<Map<String, Integer>>();
        private final ConcurrentMap<String, DataCache> caches
                = new ConcurrentCacheMap<DataCache>();

        public void clear() {
            this.requests.clear();
            this.caches.clear();
        }

        public synchronized ConcurrentMap<String, DataCache> getCaches() {
            return this.caches;
        }

        public synchronized Map<String, Map<String, Integer>> getRequests() {
            return this.requests;
        }
    }
}
