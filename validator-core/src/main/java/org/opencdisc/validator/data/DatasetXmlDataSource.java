/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.Text;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author Tim Stone
 */
public class DatasetXmlDataSource extends AbstractDataSource {
    private final Map<String, String> oids = new LinkedHashMap<String, String>();
    private XMLStreamReader reader;
    private final File define;
    private final File source;
    private String metadataOID;
    private String itemgroupOID;
    private boolean isTested;

    public DatasetXmlDataSource(SourceOptions options) throws InvalidDataException {
        super(options);

        File source = new File(options.getSource());
        File define;

        if (!StringUtils.isEmpty(options.getDefine())) {
            define = new File(options.getDefine());
        } else {
            define = new File(source.getParentFile(), "define.xml");
        }

        if (!source.isFile() || !define.isFile()) {
            // Someone didn't do a good job at checking, so...
            throw new InvalidDataException(
                InvalidDataException.Codes.MissingSource,
                Text.get("Exceptions.FileNotFound")
            );
        }

        this.source = source;
        this.define = define;
    }

    public DataSource replicate() throws InvalidDataException {
        return new DatasetXmlDataSource(super.options);
    }

    public void close() {
        if (this.reader != null) {
            try {
                this.reader.close();
            } catch (XMLStreamException ignore) {}
        }
    }

    public boolean test() {
        boolean result = true;

        try {
            this.reader = XMLInputFactory.newInstance().createXMLStreamReader(
                new FileInputStream(this.source)
            );

            int event, depth = 0;

            while (this.reader.hasNext() && result) {
                event = this.reader.next();

                if (event == XMLStreamReader.START_ELEMENT) {
                    String tag = this.reader.getLocalName();

                    if (depth == 0 && !tag.equals("ODM")) {
                        result = false;
                    } else if (depth == 1) {
                        if (tag.equals("ReferenceData") || tag.equals("ClinicalData")) {
                            this.metadataOID = this.reader.getAttributeValue(
                                null, "MetaDataVersionOID"
                            );

                            if (this.metadataOID == null) {
                                result = false;
                            }
                        } else {
                            result = false;
                        }
                    } else if (depth == 2) {
                        if (tag.equals("ItemGroupData")) {
                            this.itemgroupOID = this.reader.getAttributeValue(
                                null, "ItemGroupOID"
                            );

                            if (this.itemgroupOID == null) {
                                result = false;
                            }

                            break;
                        } else {
                            result = false;
                        }
                    }

                    ++depth;
                }
            }
        } catch (XMLStreamException ex) {
            result = false;
        } catch (IOException ex) {
            result = false;
        }

        this.isTested = true;

        return result;
    }

    // TODO: We should cache the define.xml data across instances so we can just read it once
    // But this has the added problem of needing to be cleared between runs
    protected void parseVariables() throws InvalidDataException {
        if (!this.isTested) {
            this.test();
        }

        try {
            XMLStreamReader define = XMLInputFactory.newInstance().createXMLStreamReader(
                new FileInputStream(this.define)
            );

            int event, depth = 0;
            boolean inItemGroupDef = false;
            boolean confirmedMetadataOID = false;

            while (define.hasNext()) {
                event = define.next();

                if (event == XMLStreamReader.START_ELEMENT) {
                    String tag = define.getLocalName();

                    if (depth == 0 && !tag.equals("ODM")) {
                        throw new InvalidDataException(
                            InvalidDataException.Codes.CannotParseSource
                        );
                    } else if (depth == 1 && !tag.equals("Study")) {
                        throw new InvalidDataException(
                            InvalidDataException.Codes.CannotParseSource
                        );
                    } else if (depth == 2) {
                        if (tag.equals("MetaDataVersion")) {
                            String oid = define.getAttributeValue(null, "OID");

                            if (!this.metadataOID.equals(oid)) {
                                throw new InvalidDataException(
                                    InvalidDataException.Codes.CannotParseSource
                                );
                            }

                            confirmedMetadataOID = true;
                        } else if (!tag.equals("GlobalVariables")) {
                            throw new InvalidDataException(
                                InvalidDataException.Codes.CannotParseSource
                            );
                        }
                    } else if (depth > 2 && confirmedMetadataOID) {
                        if (depth == 3) {
                            if ( tag.equals("ItemGroupDef")) {
                                String oid = define.getAttributeValue(null, "OID");

                                inItemGroupDef = this.itemgroupOID.equals(oid);
                            } else if (tag.equals("ItemDef")) {
                                String oid = define.getAttributeValue(
                                    null, "OID"
                                );

                                if (oid == null) {
                                    throw new InvalidDataException(
                                        InvalidDataException.Codes.CannotParseSource
                                    );
                                }

                                if (this.oids.containsKey(oid)) {
                                    String name = define.getAttributeValue(
                                        null, "Name"
                                    );

                                    if (name == null) {
                                        throw new InvalidDataException(
                                            InvalidDataException.Codes.CannotParseSource
                                        );
                                    }

                                    this.oids.put(oid, name);
                                }
                            }
                        } else if (depth == 4) {
                            if (inItemGroupDef && tag.equals("ItemRef")) {
                                String oid = define.getAttributeValue(
                                    null, "ItemOID"
                                );

                                if (oid == null) {
                                    throw new InvalidDataException(
                                        InvalidDataException.Codes.CannotParseSource
                                    );
                                }

                                this.oids.put(oid, null);
                            }
                        }
                    }

                    ++depth;
                } else if (depth > 2 && event == XMLStreamReader.END_ELEMENT) {
                    --depth;

                    if (depth == 3 && inItemGroupDef) {
                        inItemGroupDef = false;
                    }
                }
            }

            define.close();

            if (!confirmedMetadataOID) {
                throw new InvalidDataException(
                    InvalidDataException.Codes.CannotParseSource
                );
            }
        } catch (XMLStreamException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        } catch (IOException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        for (Map.Entry<String, String> mapping : this.oids.entrySet()) {
            if (mapping.getValue() == null) {
                throw new InvalidDataException(
                    InvalidDataException.Codes.CannotParseSource,
                    String.format("No name found for ItemOID %s", mapping.getKey())
                );
            }

            super.metadata.add(mapping.getValue());
        }
    }

    protected List<DataRecord> parseRecords(int recordCount)
            throws InvalidDataException {
        List<DataRecord> records = new ArrayList<DataRecord>(recordCount);

        try {
            // This is where we left off in test()
            int event = this.reader.getEventType(), depth = 0;
            Set<String> variables = super.getVariables();
            DataRecord record = null;

            do {
                if (event == XMLStreamReader.START_ELEMENT) {
                    String tag = this.reader.getLocalName();

                    if (depth == 0) {
                        if (!tag.equals("ItemGroupData")) {
                            throw new InvalidDataException(
                                InvalidDataException.Codes.CannotParseSource
                            );
                        }

                        if (record != null) {
                            Set<String> unpopulated = new HashSet<String>(variables);

                            unpopulated.removeAll(record.getVariables());

                            for (String variable : unpopulated) {
                                record.setValue(variable, DataEntry.NULL_ENTRY);
                            }

                            records.add(record);
                        }

                        super.next();
                        record = super.newRecord();
                    } else if (depth == 1 && tag.equals("ItemData")) {
                        String oid = this.reader.getAttributeValue(
                            null, "ItemOID"
                        );

                        if (!this.oids.containsKey(oid)) {
                            throw new InvalidDataException(
                                InvalidDataException.Codes.CannotParseSource
                            );
                        }

                        String value = this.reader.getAttributeValue(
                            null, "Value"
                        );

                        record.setValue(this.oids.get(oid), new DataEntry(value));
                    }

                    ++depth;
                } else if (event == XMLStreamReader.END_ELEMENT) {
                    --depth;

                    if (depth == 0) {
                        --recordCount;
                    }
                }
            } while (this.reader.hasNext()
                && (event = this.reader.next()) > -1
                &&  depth >= 0
                && recordCount > 0
            );

            if (record != null) {
                Set<String> unpopulated = new HashSet<String>(variables);

                unpopulated.removeAll(record.getVariables());

                for (String variable : unpopulated) {
                    record.setValue(variable, DataEntry.NULL_ENTRY);
                }

                records.add(record);
            }

            if (depth < 0 || event == XMLStreamReader.END_ELEMENT) {
                super.markComplete();
            } else if (!this.reader.hasNext()) {
                throw new InvalidDataException(
                    InvalidDataException.Codes.CannotParseSource
                );
            }
        } catch (XMLStreamException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        return records;
    }
}
