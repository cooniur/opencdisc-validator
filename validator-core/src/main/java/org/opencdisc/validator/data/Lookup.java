/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.util.List;
import java.util.Set;

import org.opencdisc.validator.rules.expressions.PreparedQuery;

/**
 *
 * @author Tim Stone
 */
public interface Lookup {
    /**
     *
     * @param variable
     *
     * @return
     */
    public boolean contains(String variable);

    /**
     *
     * @param variables
     *
     * @return
     */
    public boolean contains(Set<String> variables);

    /**
     *
     * @param variables
     */
    public void release(Set<String> variables);

    /**
     *
     * @param values
     * @param where
     * @param ignoreWhereFailure
     *
     * @return
     */
    public boolean seek(List<PreparedQuery.Mapping> search,
            List<PreparedQuery.Mapping> where, boolean ignoreWhereFailure);
}
