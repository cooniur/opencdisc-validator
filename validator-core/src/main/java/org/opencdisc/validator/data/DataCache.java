/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.util.*;

import org.opencdisc.validator.rules.expressions.PreparedQuery;
import org.opencdisc.validator.util.BitSet;
import org.opencdisc.validator.util.Settings;

/**
 *
 * @author Tim Stone
 */
public class DataCache implements Lookup {
    private static final DataEntryComparator CACHE_COMPARATOR =
        new DataEntryComparator();
    private static final boolean UPPERCASE_KEYS = Settings.get(
        "Engine.CacheCasingConversion", "uppercase"
    ).equalsIgnoreCase("uppercase");

    private final Map<String, SortedMap<DataEntry, BitSet>> cache =
        new HashMap<String, SortedMap<DataEntry, BitSet>>();
    private final Map<String, Map<String, Set<DataEntry>>> casings =
        new HashMap<String, Map<String, Set<DataEntry>>>();
    private final Set<String> ignorable = new HashSet<String>();

    private volatile boolean isCorrupt  = false;
    private volatile boolean isComplete = false;

    public DataCache(Set<String> variables) {
        this.init(variables);
    }

    public DataCache(Set<String> variables, DataCache existingCache) {
        variables.removeAll(existingCache.cache.keySet());

        this.init(variables);

        for (String key : existingCache.cache.keySet()) {
            this.ignorable.add(key);
            this.cache.put(key, existingCache.cache.get(key));
            this.casings.put(key, existingCache.casings.get(key));
        }
    }

    private void init(Set<String> variables) {
        for (String variable : variables) {
            this.cache.put(variable, new TreeMap<DataEntry, BitSet>(CACHE_COMPARATOR));
        }
    }

    public synchronized void setCorrupt(boolean corrupt) {
        this.isCorrupt = corrupt;
    }

    public synchronized void setComplete(boolean complete) {
        this.isComplete = complete;
    }

    public synchronized boolean isCorrupt() {
        return this.isCorrupt;
    }

    public synchronized boolean isComplete() {
        return this.isComplete;
    }

    public List<DataRecord> store(final List<DataRecord> dataRecords) {
        Set<String> variables = this.cache.keySet();

        for (DataRecord dataRecord : dataRecords) {
            for (String variable : variables) {
                if (dataRecord.definesVariable(variable) &&
                        !this.ignorable.contains(variable)) {
                    DataEntry entry = dataRecord.getValue(variable);
                    SortedMap<DataEntry, BitSet> values = this.cache.get(variable);
                    BitSet records = values.get(entry);

                    if (records == null) {
                        values.put(entry, records = new BitSet());

                        if (entry.getDataType() == DataEntry.DataType.Text) {
                            String original = entry.toString();
                            String keyed = toCasingKey(original);

                            if (!original.equals(keyed)) {
                                Map<String, Set<DataEntry>> casings =
                                    this.casings.get(variable);

                                if (casings == null) {
                                    this.casings.put(
                                        variable,
                                        casings = new HashMap<String, Set<DataEntry>>()
                                    );
                                }

                                Set<DataEntry> conversions = casings.get(keyed);

                                if (conversions == null) {
                                    casings.put(
                                        keyed, conversions = new HashSet<DataEntry>()
                                    );
                                }

                                conversions.add(entry);
                            }
                        }
                    }

                    records.add(dataRecord.getID());
                }
            }
        }

        return dataRecords;
    }

    public boolean contains(String variable) {
        return this.cache.containsKey(variable);
    }

    public boolean contains(Set<String> variables) {
        return this.cache.keySet().containsAll(variables);
    }

    public void release(Set<String> variables) {

    }

    public boolean seek(List<PreparedQuery.Mapping> search,
            List<PreparedQuery.Mapping> where, boolean ignoreWhereFailure) {
        boolean result = true;
        BitSet matches = new BitSet();

        if (where.size() > 0) {
            this.search(matches, where);
            result = !matches.isEmpty();
        }

        if (result) {
            // TODO: Add support for or in search when where clause present
            //    Currently attempting to use an or in search when where is
            //    present will break, because the first lookup will be anded
            //    with the results from the where. We'd have to generate the
            //    search results separately and then intersect them with the
            //    where results, but this could be more memory intensive...
            //    Or it might not be an issue in practice, so we'll need to
            //    test and find out.
            this.search(matches, search);
            result = !matches.isEmpty();
        } else {
            result = ignoreWhereFailure;
        }

        return result;
    }

    private void search(BitSet matches, List<PreparedQuery.Mapping> mappings) {
        int shortCircuitIndex = 0;

        // TODO: Implement operator precedence. It's just go from left to right now
        //   This is good enough for our purposes right now, but also inconsistent.
        //   This manner of determining when to short circuit is a bit kludgy as
        //   well.
        int current = 0;

        for (PreparedQuery.Mapping mapping : mappings) {
            PreparedQuery.Mapping.Operator operator = mapping.getOperator();

            if (operator == PreparedQuery.Mapping.Operator.OR) {
                shortCircuitIndex = current;
            }

            ++current;
        }

        // Traverse the where list again, now that we have the short circuit point
        current = 0;

        for (PreparedQuery.Mapping mapping : mappings) {
            PreparedQuery.Mapping.Operator operator = mapping.getOperator();
            PreparedQuery.Mapping.Comparator comparator = mapping.getComparator();
            DataEntry value = mapping.getValue();
            String variable = mapping.getRemote();
            BitSet currentMatches = null;

            SortedMap<DataEntry, BitSet> cache = this.cache.get(variable);

            if (comparator == PreparedQuery.Mapping.Comparator.EQ) {
                currentMatches = cache.get(value);
            } else if (comparator == PreparedQuery.Mapping.Comparator.EIQ) {
                if (value.getDataType() == DataEntry.DataType.Text) {
                    String keyed = toCasingKey(value.toString());
                    Map<String, Set<DataEntry>> casings = this.casings.get(variable);

                    if (casings != null) {
                        Set<DataEntry> conversions = casings.get(keyed);

                        if (conversions != null) {
                            BitSet.Unioner unioner = null;

                            for (DataEntry conversion : conversions) {
                                if (unioner != null) {
                                    unioner.union(cache.get(conversion));
                                } else {
                                    unioner = BitSet.union(
                                        new BitSet(), cache.get(conversion)
                                    );
                                }
                            }

                            if (unioner != null) {
                                currentMatches = unioner.get();
                            }
                        }
                    }

                    if (currentMatches == null) {
                        // If the value came in key-case, it won't have a casings
                        // map to use above. We might do this in cases where it's
                        // not necessary, but it shouldn't be a big deal...
                        currentMatches = cache.get(new DataEntry(keyed));
                    }
                } else {
                    currentMatches = cache.get(value);
                }
            } else if (comparator == PreparedQuery.Mapping.Comparator.NEQ) {
                currentMatches = this.convertMapToSet(cache, value);
            } else if (comparator == PreparedQuery.Mapping.Comparator.LT ||
                    comparator == PreparedQuery.Mapping.Comparator.LTE) {
                currentMatches = this.convertMapToSet(cache.headMap(value), null);

                if (comparator == PreparedQuery.Mapping.Comparator.LTE &&
                        cache.containsKey(value)) {
                    currentMatches.union(cache.get(value));
                }
            } else if (comparator == PreparedQuery.Mapping.Comparator.GT ||
                    comparator == PreparedQuery.Mapping.Comparator.GTE) {
                currentMatches = this.convertMapToSet(
                    cache.tailMap(value),
                    comparator == PreparedQuery.Mapping.Comparator.GT ? value : null
                );
            }

            if (currentMatches == null) {
                currentMatches = new BitSet();
            }

            if ((operator == null && matches.isEmpty()) ||
                    operator == PreparedQuery.Mapping.Operator.OR) {
                matches.union(currentMatches);
            } else {
                matches.intersect(currentMatches);
            }

            // Check if we can short circuit due to failure
            if (matches.isEmpty() && current >= shortCircuitIndex) {
                break;
            }

            ++current;
        }
    }

    private BitSet convertMapToSet(Map<DataEntry, BitSet> selection,
            DataEntry excluding) {
        BitSet.Unioner unioner = null;

        for (Map.Entry<DataEntry, BitSet> entry : selection.entrySet()) {
            if (excluding == null || !excluding.equals(entry.getKey())) {
                if (unioner != null) {
                    unioner.union(entry.getValue());
                } else {
                    unioner = BitSet.union(new BitSet(), entry.getValue());
                }
            }
        }

        return unioner != null ? unioner.get() : new BitSet();
    }

    private String toCasingKey(String key) {
        return UPPERCASE_KEYS ? key.toUpperCase() : key.toLowerCase();
    }

    private static class DataEntryComparator implements Comparator<DataEntry> {
        public int compare(DataEntry o1, DataEntry o2) throws NullPointerException {
            if (o1 == null || o2 == null) {
                throw new NullPointerException();
            }

            return o1.compareTo(o2);
        }
    }
}
