/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.model.DataDetails;
import org.opencdisc.validator.model.DataDetails.Info;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.model.EntityDetails.Reference;

/**
 * Provides the essential common functionality for a <code>DataSource</code> to operate
 * reliably. Implementations of <code>DataSource</code> are strongly suggested to
 * subclass <code>AbstractDataSource</code> to keep the logic behind storing variables
 * and setting statuses consistent.
 *
 * @author Tim Stone
 */
public abstract class AbstractDataSource implements DataSource {
    static final int DEFAULT_RECORD_COUNT = 10;

    private boolean isFinished = false;
    private int recordCount = 0;
    private Set<String> variables = null;

    protected InternalEntityDetails entity;
    protected final SourceOptions options;
    protected final Metadata metadata;

    protected AbstractDataSource(SourceOptions options) {
        this.options = options;
        this.entity = new InternalEntityDetails(
            Reference.Data, options.getName(), options.getSubname(), options.getSource()
        );
        this.entity.setProperty(Property.Records, 0);
        this.metadata = new Metadata(this.entity);
    }

    public InternalEntityDetails getDetails() {
        return this.entity;
    }

    public String getLocation() {
        return this.entity.getString(Property.Location);
    }

    public DataSource getMetadata() {
        return this.metadata;
    }

    public String getName() {
        return this.entity.getString(Property.Name);
    }

    public int getRecordCount() {
        return this.recordCount;
    }

    public List<DataRecord> getRecords() throws InvalidDataException {
        return this.getRecords(DEFAULT_RECORD_COUNT);
    }

    public List<DataRecord> getRecords(int recordCount) throws InvalidDataException {
        if (this.variables == null) {
            this.getVariables();
        }

        return this.parseRecords(recordCount);
    }

    public synchronized Set<String> getVariables() throws InvalidDataException {
        if (this.variables == null) {
            this.parseVariables();

            this.variables = new LinkedHashSet<String>();

            if (this.metadata.hasRecords()) {
                for (DataRecord record : this.metadata.getRecords()) {
                    String variable = record.getValue("VARIABLE").toString();

                    if (this.variables.contains(variable)) {
                        throw new InvalidDataException(
                            InvalidDataException.Codes.DuplicateVariable,
                            String.format(
                                "The variable %s must only appear once",
                                variable
                            )
                        );
                    }

                    this.variables.add(variable);
                }

                this.metadata.reset();
            }

            this.variables = Collections.unmodifiableSet(this.variables);
        }

        if (this.variables.size() == 0) {
            throw new InvalidDataException(
                    InvalidDataException.Codes.NoVariables,
                    "This source does not appear to contain variable definitions."
            );
        }

        return this.variables;
    }

    public Object getVariableProperty(String variable, VariableProperty property)
            throws InvalidDataException {
        if (this.variables == null) {
            this.getVariables();
        }

        DataRecord record = this.metadata.getVariable(variable);

        if (record == null) {
            throw new IllegalArgumentException(String.format(
                "The variable %s is not defined for this source", variable
            ));
        }

        // TODO: Why aren't we getting VariableDetails here instead?
        Object value = record.getValue(property.toString().toUpperCase()).getValue();

        // Backwards compatibility
        return value instanceof BigDecimal ? ((BigDecimal)value).doubleValue() : value;
    }

    public boolean hasRecords() {
        return !this.isFinished;
    }

    public boolean isComposite() {
        return false;
    }

    public boolean isMetadata() {
        return false;
    }

    protected abstract List<DataRecord> parseRecords(int recordCount)
            throws InvalidDataException;

    protected abstract void parseVariables() throws InvalidDataException;

    protected void markComplete() {
        this.isFinished = true;
        this.close();
    }

    protected DataRecord newRecord() {
        return new DataRecord(
            new DataDetails(
                this.getRecordCount(),
                Info.Data
            ),
            this.entity
        );
    }

    protected void next() {
        ++this.recordCount;

        this.entity.setProperty(Property.Records, this.recordCount);
    }

    protected void rewind(int offset) {
        this.recordCount -= offset;

        this.entity.setProperty(Property.Records, this.recordCount);
    }
}
