/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.Text;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.util.Helpers;
import org.opencdisc.validator.util.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.Cleaner;
import sun.nio.ch.DirectBuffer;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tim Stone
 */
public class EnhancedSasTransportDataSource extends AbstractDataSource {
    private static final Logger LOG = LoggerFactory.getLogger(
        EnhancedSasTransportDataSource.class
    );

    private static final int DEFAULT_BUFFER_SIZE = 80;
    private static final int EXTENDED_BUFFER_SIZE = 140;
    private static final int MAPPABLE_REGION_SIZE = 1024 * 1024 * 100;
    private static final int EXPECTED_NUMERIC_WIDTH = 8;
    private static final int NAMESTR_NUM_POSTITION = 54;
    private static final int DSCRPTR_LABEL_POSITION = 32;
    private static final byte[] LIBRARY_HEADER_BYTES = {
        72, 69, 65, 68, 69, 82, 32, 82, 69, 67, 79, 82, 68, 42, 42, 42, 42, 42, 42,
        42, 76, 73, 66, 82, 65, 82, 89, 32, 72, 69, 65, 68, 69, 82, 32, 82, 69, 67,
        79, 82, 68, 33, 33, 33, 33, 33, 33, 33, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48
    };
    private static final byte[] DSCRPTR_HEADER_BYTES = {
        72, 69, 65, 68, 69, 82, 32, 82, 69, 67, 79, 82, 68, 42, 42, 42, 42, 42, 42,
        42, 68, 83, 67, 82, 80, 84, 82, 32, 72, 69, 65, 68, 69, 82, 32, 82, 69, 67,
        79, 82, 68, 33, 33, 33, 33, 33, 33, 33, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48, 32, 32
    };
    private static final byte[] NAMESTR_HEADER_BYTES = {
        72, 69, 65, 68, 69, 82, 32, 82, 69, 67, 79, 82, 68, 42, 42, 42, 42, 42, 42,
        42, 78, 65, 77, 69, 83, 84, 82, 32, 72, 69, 65, 68, 69, 82, 32, 82, 69, 67,
        79, 82, 68, 33, 33, 33, 33, 33, 33, 33, 48, 48, 48, 48, 48
    };
    private static final byte[] OBS_HEADER_BYTES = {
        72, 69, 65, 68, 69, 82, 32, 82, 69, 67, 79, 82, 68, 42, 42, 42, 42, 42, 42,
        42, 79, 66, 83, 32, 32, 32, 32, 32, 72, 69, 65, 68, 69, 82, 32, 82, 69, 67,
        79, 82, 68, 33, 33, 33, 33, 33, 33, 33, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,
        48, 48
    };
    private static final Charset ENCODING_CHARSET = Charset.forName(
        Settings.get("Engine.SasFileEncoding", "ISO-8859-1")
    );

    private final FileChannel channel;
    private final FileInputStream stream;
    private final List<Variable> variables = new ArrayList<Variable>();
    private final long bytesTotal;
    private MappedByteBuffer buffer = null;
    private int recordSize = 0;
    private long bytesRead = 0;

    private int previouslyNull = 0;

    public EnhancedSasTransportDataSource(SourceOptions options)
            throws InvalidDataException {
        super(options);

        File source = new File(options.getSource());

        if (!source.isFile()) {
            LOG.debug("Failed to create source because {} does not exist", source);

            throw new InvalidDataException(
                InvalidDataException.Codes.MissingSource,
                Text.get("Exceptions.FileNotFound")
            );
        }

        this.bytesTotal = source.length();
        this.entity.setProperty(EntityDetails.Property.FileSize, this.bytesTotal);

        try {
            this.stream  = new FileInputStream(source);
            this.channel = this.stream.getChannel();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void close() {
        this.clean();

        try {
            this.channel.close();
        } catch (IOException ex) {
            LOG.error("Unexpected IOException {}", ex);
        }

        try {
            this.stream.close();
        } catch (IOException ex) {
            LOG.error("Unexpected IOException {}", ex);
        }
    }

    public DataSource replicate() throws InvalidDataException {
        return new EnhancedSasTransportDataSource(super.options);
    }

    public boolean test() {
        boolean result;

        try {
            ByteBuffer test = this.getBuffer();

            LOG.debug("Testing to see if the file starts with LIBRARY_HEADER_BYTES");

            result = test != null && this.bufferStartsWith(
                test,
                ByteBuffer.wrap(LIBRARY_HEADER_BYTES)
            );
        } catch (InvalidDataException ex) {
            result = false;
        }

        return result;
    }

    protected List<DataRecord> parseRecords(int batchsize) throws InvalidDataException {
        List<DataRecord> records = new ArrayList<DataRecord>(batchsize);
        int pending = batchsize;

        while (super.hasRecords() && pending > 0) {
            ByteBuffer observation = this.getBuffer(this.recordSize);

            if (observation == null) {
                break;
            }

            // TODO: Ugh...we should just look ahead to identify this case
            // We can probably peek into the last segment of the file at
            // the beginning, and determine in advance where the logical
            // end will be.
            long previous = this.bytesRead - this.recordSize;
            long remaining = this.bytesTotal - DEFAULT_BUFFER_SIZE;

            if (this.recordSize <= DEFAULT_BUFFER_SIZE && previous > remaining) {
                boolean looksNull = true;

                for (int i = 0; i < this.recordSize && looksNull; ++i) {
                    looksNull = observation.get() == 32;
                }

                observation.rewind();

                if (looksNull) {
                    ++this.previouslyNull;
                } else {
                    this.previouslyNull = 0;
                }
            }

            super.next();
            DataRecord record = super.newRecord();

            for (Variable variable : this.variables) {
                DataEntry entry;
                byte[] copy = new byte[variable.length];

                observation.get(copy);

                if (variable.type == Variable.PhysicalFormat.Character) {
                    // DataEntry now does rtrim() for us. We also don't intern for
                    // the moment, since I'm kind of concerned that that was doing
                    // more harm than good before.
                    entry = new DataEntry(toString(copy));
                } else {
                    entry = new DataEntry(convert(ByteBuffer.wrap(copy)));
                }

                record.setValue(variable.name, entry);
            }

            records.add(record);

            --pending;
        }

        if (this.previouslyNull > 0) {
            super.rewind(this.previouslyNull);

            LOG.debug("Discarding {} records believed to be null", this.previouslyNull);

            for (int i = 0; i < this.previouslyNull && !records.isEmpty(); ++i) {
                records.remove(records.size() - 1);
            }

            this.previouslyNull = 0;
        }

        return records;
    }

    protected void parseVariables() throws InvalidDataException {
        ByteBuffer namestrHeader = ByteBuffer.wrap(NAMESTR_HEADER_BYTES);
        ByteBuffer dscrptrHeader = ByteBuffer.wrap(DSCRPTR_HEADER_BYTES);
        ByteBuffer buffer;
        boolean isValid = false;
        int variables = 0;
        int trailing = 0;

        while (!isValid && super.hasRecords() && (buffer = this.getBuffer()) != null) {
            if (buffer.equals(dscrptrHeader)) {
                trailing = 2;
            } else if (trailing > 0) {
                if (trailing == 1) {
                    byte[] string = new byte[40];

                    buffer.position(DSCRPTR_LABEL_POSITION);
                    buffer.get(string);

                    this.entity.setProperty(EntityDetails.Property.Label, new String(string).trim());
                    this.entity.setProperty(EntityDetails.Property.DatasetLabel, new String(string).trim());
                }

                --trailing;
            } else {
                byte[] comparable = new byte[NAMESTR_NUM_POSTITION - 1];

                buffer.position(0);
                buffer.get(comparable, 0, comparable.length);

                LOG.debug("Testing to see if buffer starts with NAMESTR_HEADER_BYTES");

                // Check to see if this is the NAMESTR header
                if (ByteBuffer.wrap(comparable).equals(namestrHeader)) {
                    // It is, figure out how many variables we have
                    byte[] string = new byte[4];

                    buffer.position(NAMESTR_NUM_POSTITION);
                    buffer.get(string);

                    variables = Integer.parseInt(new String(string).trim());
                    isValid = true;
                }
            }
        }

        if (!isValid) {
            LOG.debug("Throwing exception because the file is considered invalid");

            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        if (variables == 0) {
            LOG.debug("Throwing exception because the file doesn't have any variables");

            throw new InvalidDataException(InvalidDataException.Codes.NoVariables);
        }

        byte[] temp;

        for (int i = 0; i < variables; ++i) {
            buffer = this.getBuffer(EXTENDED_BUFFER_SIZE);

            // Get the type number
            short type = buffer.getShort();
            // Skip the next two bytes
            buffer.position(buffer.position() + 2);
            // Get the variable observation length
            short length = buffer.getShort();
            // Skip the next two bytes
            buffer.position(buffer.position() + 2);
            // Get the variable name
            buffer.get((temp = new byte[8]));
            String name = toString(temp).trim().toUpperCase();
            // Get the variable label
            buffer.get((temp = new byte[40]));
            String label = toString(temp).trim();
            // Get the variable format string
            buffer.get((temp = new byte[8]));
            String format = toString(temp).trim();

            // TODO: Why are we doing this...?
            if (label.length() == 0) {
                label = "null";
            }

            short formatLength = buffer.getShort();

            String fullFormat = format;

            if (format.length() > 0 && formatLength != 0) {
                fullFormat += formatLength;
            }

            Variable variable = new Variable(name, type, length);

            this.variables.add(variable);
            super.metadata.add(
                name,
                variable.type.name,
                (int)length,
                label,
                format,
                fullFormat
            );

            this.recordSize += length;
        }

        // Discard any remaining padding bytes
        int remainder = (int)(this.bytesRead % DEFAULT_BUFFER_SIZE);

        if (remainder > 0) {
            this.getBuffer(DEFAULT_BUFFER_SIZE - remainder);
        }

        // Finally, verify that the next record is the OBS record
        namestrHeader = ByteBuffer.wrap(OBS_HEADER_BYTES);

        if (!this.bufferStartsWith(this.getBuffer(), namestrHeader)) {
            LOG.debug("Next buffer was unexpectedly not OBS_HEADER_BYTES");

            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }
    }

    private ByteBuffer getBuffer() throws InvalidDataException {
        return this.getBuffer(DEFAULT_BUFFER_SIZE);
    }

    private ByteBuffer getBuffer(int size) throws InvalidDataException {
        byte[] copy = new byte[size];
        int copied;

        try {
            if (this.buffer == null) {
                if (!this.reallocate()) {
                    LOG.debug("Looks like the source is an empty file");

                    throw new InvalidDataException(
                        InvalidDataException.Codes.CannotParseSource
                    );
                }
            }

            int readable = copied = Math.min(copy.length, this.buffer.remaining());

            if (readable > 0) {
                this.buffer.get(copy, 0, readable);
            }

            this.bytesRead += copied;

            if (readable != copy.length && this.reallocate()) {
                readable = Math.min(copy.length - copied, this.buffer.remaining());

                this.buffer.get(copy, copied, copy.length - copied);

                this.bytesRead += readable;
                copied += readable;
            }
        } catch (IOException ex) {
            LOG.debug("Unexpected IOException while reading buffer: {}", ex);

            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        return copied == size ? ByteBuffer.wrap(copy) : null;
    }

    private boolean bufferStartsWith(ByteBuffer buffer, ByteBuffer prefix) {
        if (buffer == null || prefix == null) {
            LOG.debug("buffer or prefix was unexpectedly null");

            throw new IllegalArgumentException("comparison buffers cannot be null");
        }

        int capacity = prefix.capacity();
        byte[] debug = LOG.isDebugEnabled() ? new byte[capacity] : null;
        boolean result = true;

        for (int i = 0; i < capacity && result; ++i) {
            byte got = buffer.get();

            if (debug != null) {
                debug[i] = got;
            }

            result = prefix.get() == got;
        }

        if (!result && debug != null) {
            LOG.debug("Failed buffer prefix check, expected {} but failed with {}",
                Helpers.toHexString(prefix.array()), Helpers.toHexString(debug));
        }

        return result;
    }

    private static Double convert(ByteBuffer cell) {
        Double result = null;
        int capacity = cell.capacity();
        int adjust = EXPECTED_NUMERIC_WIDTH - capacity;

        if (adjust > 0) {
            ByteBuffer temp = cell;
            cell = ByteBuffer.allocate(8);

            for (int i = 0; i < EXPECTED_NUMERIC_WIDTH; ++i) {
                if (i < capacity) {
                    cell.put(temp.get());
                } else {
                    cell.put((byte)0);
                }
            }

            cell.rewind();
        }

        boolean hasValue = false;
        byte[] raw = cell.array();

        for (int i = 1; i < raw.length && !hasValue; ++i) {
            hasValue = raw[i] != 0;
        }

        if (!hasValue) {
            if (raw[0] != 0x5F && raw[0] != 0x2E && (raw[0] < 0x41 || raw[0] > 0x5A)) {
                hasValue = raw[0] != 0;
            }
        }

        if (hasValue) {
            long xport1   = cell.getInt() & 0x00000000FFFFFFFFL;
            long xport2   = cell.getInt() & 0x00000000FFFFFFFFL;

            cell.rewind();

            byte exponent = cell.get();
            long ieee1    = xport1 & 0x00FFFFFF;
            long ieee2    = xport2;
            short shift;
            long nib      = (int)xport1;

            if ((nib & 0x00800000) != 0) {
                shift = 3;
            } else if ((nib & 0x00400000) != 0) {
                shift = 2;
            } else if ((nib & 0x00200000) != 0) {
                shift = 1;
            } else {
                shift = 0;
            }

            if (shift > 0) {
                ieee1 >>= shift;
                ieee2 = (xport2 >>> shift) |
                        ((xport1 & 0x00000007) << (29 + (3 - shift)));
            }

            ieee1 &= 0xFFEFFFFF;
            ieee1 |= (((((long)(exponent & 0x7F) - 65) << 2) + shift +
                    1023) << 20) | (xport1 & 0x80000000);

            int ieee1conv = (int)ieee1;
            int ieee2conv = (int)ieee2;

            cell.rewind();
            cell.putInt(ieee1conv);
            cell.putInt(ieee2conv);
            cell.rewind();

            result = Helpers.round(cell.getDouble());
        } else if (raw[0] == 0) {
            result = 0.0;
        }

        return result;
    }

    private boolean reallocate() throws IOException {
        if (this.bytesRead == this.bytesTotal) {
            super.markComplete();
            return false;
        }

        this.clean();

        this.buffer = this.channel.map(
            FileChannel.MapMode.READ_ONLY,
            this.bytesRead,
            Math.min(this.bytesTotal - this.bytesRead, MAPPABLE_REGION_SIZE)
        );

        return true;
    }

    private void clean() {
        if (this.buffer != null) {
            Cleaner cleaner = ((DirectBuffer)this.buffer).cleaner();

            if (cleaner != null) {
                cleaner.clean();
            }
        }
    }

    private static String toString(byte[] characters) {
        return new String(characters, ENCODING_CHARSET);
    }

    private static class Variable {
        enum PhysicalFormat {
            Numeric(1, "Num"),
            Character(2, "Char");

            PhysicalFormat(int code, String name) {
                this.code = (short)code;
                this.name = name;
            }

            private final short code;
            private final String name;
        }

        private final PhysicalFormat type;
        private final String name;
        private final short length;

        Variable(String name, short type, short length) {
            this.name = name;
            this.length = length;
            this.type = type == PhysicalFormat.Numeric.code ?
                PhysicalFormat.Numeric : PhysicalFormat.Character;
        }
    }
}
