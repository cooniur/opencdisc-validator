/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import org.opencdisc.validator.model.VariableDetails;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tim Stone
 */
public class InternalVariableDetails implements VariableDetails {
    private final Map<Property, Object> properties = new HashMap<Property, Object>();

    InternalVariableDetails(String name, Integer order) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }

        if (order == null) {
            throw new IllegalArgumentException("order cannot be null");
        }

        this.setProperty(Property.Name, name);
        this.setProperty(Property.Order, order);
    }

    InternalVariableDetails(String name, Integer order, String type, Integer length,
            String label, String format, String fullFormat) {
        this(name, order);

        this.setProperty(Property.Type, type);
        this.setProperty(Property.Length, length);
        this.setProperty(Property.Label, label);
        this.setProperty(Property.Format, format);
        this.setProperty(Property.FullFormat, fullFormat);
    }

    public boolean getBoolean(Property property) {
        return this.convert(property) != 0;
    }

    public int getInteger(Property property) {
        return this.convert(property);
    }

    public String getString(Property property) {
        return this.getProperty(property).toString();
    }

    public boolean hasProperty(Property property) {
        return this.properties.containsKey(property);
    }

    public void setProperty(Property property, Object value) {
        if (this.hasProperty(property) && !property.isUpdatable()) {
            throw new IllegalArgumentException(String.format(
                "Cannot reassign the property %s", property.toString()
            ));
        }

        if (value == null) {
            return;
        }

        this.properties.put(property, value);
    }

    private Object getProperty(Property property) {
        if (!this.hasProperty(property)) {
            throw new IllegalArgumentException(String.format(
                "The property %s is not set", property.toString()
            ));
        }

        return this.properties.get(property);
    }

    private Integer convert(Property property) {
        Object value = this.getProperty(property);

        if (!property.isConvertible()) {
            throw new IllegalArgumentException(String.format(
                    "The property %s cannot be converted to a non-String type", property
            ));
        }

        if (value instanceof Integer) {
            return (Integer)value;
        }

        if (value instanceof Boolean) {
            return (Boolean)value ? 1 : 0;
        }

        // This should be made unnecessary by code elsewhere, but just in case
        return Integer.valueOf(value.toString());
    }
}
