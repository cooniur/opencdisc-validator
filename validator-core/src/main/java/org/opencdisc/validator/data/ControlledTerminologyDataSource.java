/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.util.Settings;

import java.util.*;

/**
 * @author Tim Stone
 */
public class ControlledTerminologyDataSource extends AbstractDataSource {
    private static final String VALUE_COLUMN;
    private static final String CODE_COLUMN;
    public static final String DECODE_COLUMN = "VAL:DECODE";

    static {
        VALUE_COLUMN = Settings.get(
            "Engine.ControlledTerminology.ValueColumn", "CDISCSUBMISSIONVALUE"
        );
        CODE_COLUMN = Settings.get("Engine.ControlledTerminology.CodeColumn", "CODE");
    }

    private final Map<String, Integer> terms = new HashMap<String, Integer>();
    private final DataSource backing;
    private List<DataRecord> records = null;

    public ControlledTerminologyDataSource(SourceOptions options) throws InvalidDataException {
        super(options);
        options.setDelimiter("\t");

        this.backing = new DelimitedDataSource(options);
    }

    public boolean test() {
        return this.backing.test();
    }

    public void close() {
        this.backing.close();
    }

    public DataSource replicate() throws InvalidDataException {
        return new ControlledTerminologyDataSource(super.options);
    }

    protected void parseVariables() throws InvalidDataException {
        for (String variable : this.backing.getVariables()) {
            super.metadata.add(variable);
        }

        super.metadata.add(DECODE_COLUMN);
    }

    protected List<DataRecord> parseRecords(int count) throws InvalidDataException{
        if (this.records == null) {
            this.records = new ArrayList<DataRecord>();

            while (this.backing.hasRecords()) {
                for (DataRecord record : this.backing.getRecords()) {
                    int index = this.records.size();
                    DataEntry code = record.getValue(CODE_COLUMN);
                    Integer previous = this.terms.put(code.toString(), index);

                    if (previous != null) {
                        DataRecord existing = this.records.get(previous);

                        if (!existing.definesVariable(DECODE_COLUMN)) {
                            record.setValue(DECODE_COLUMN, existing.getValue(VALUE_COLUMN));
                            existing.setValue(DECODE_COLUMN, record.getValue(VALUE_COLUMN));
                        }
                    }

                    this.records.add(record);
                }
            }

            Collections.reverse(this.records);
        }

        List<DataRecord> selected = new LinkedList<DataRecord>();

        while (this.records.size() > 0 && count-- > 0) {
            selected.add(this.records.remove(this.records.size() - 1));
        }

        if (this.records.size() == 0) {
            super.markComplete();
        }

        return selected;
    }
}
