/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import org.opencdisc.validator.util.Helpers;
import org.opencdisc.validator.util.Settings;
import org.opencdisc.validator.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Acts as a wrapper around the value of a cell in a dataset.
 *
 * @author Tim Stone
 */
public final class DataEntry {
    public static final DataEntry NULL_ENTRY = new DataEntry(null);

    private static final int COMPARISON_DECIMALS;

    private static final ThreadLocal<DecimalFormat> DECIMAL_PATTERN = new ThreadLocal<DecimalFormat>() {
        @Override
        protected DecimalFormat initialValue() {
            DecimalFormat format = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

            format.setMaximumFractionDigits(Helpers.ROUNDING_CUTOFF);

            return format;
        }
    };
    private static final ThreadLocal<Pattern> DATE_PATTERN = new ThreadLocal<Pattern>() {
        @Override
        protected Pattern initialValue() {
            return Pattern.compile(
                "^(?:-|[0-9]{4})" +
                    "(?:-" +
                        "(?:-|0[0-9]|1[0-2])" +
                        "(?:-" +
                            "(?:-|[0-2][0-9]|3[0-1])" +
                            "(?:T" +
                                "(?:-|[0-1][0-9]|2[0-4])" +
                                "(?::" +
                                    "(?:-|[0-5][0-9])" +
                                    "(?::[0-5][0-9])?" +
                                ")?" +
                            ")?" +
                        ")?" +
                ")?$"
            );
        }
    };

    static {
        int digits = 6;

        if (Settings.defines("Engine.ComparisonRoundingDigits")) {
            try {
                digits = Integer.parseInt(Settings.get("Engine.ComparisonRoundingDigits"));
            } catch (NumberFormatException ignore) {}
        }

        COMPARISON_DECIMALS = digits;
    }

    public enum DataType {
        Date,
        DateTime,
        Duration,
        Float,
        Integer,
        Text,
        Time,
        Null;

        public boolean isNumeric() {
            return this == Float || this == Integer;
        }

        public boolean isDate() {
            return this == Date || this == DateTime;
        }
    }

    private final Object value;
    private final DataType datatype;

    public DataEntry(Object value) {
        if (value instanceof BigDecimal) {
            // TODO: Technically this may be an integer but since this isn't used anywhere we're avoiding the overhead
            this.datatype = DataType.Float;
            this.value = ((BigDecimal)value).setScale(COMPARISON_DECIMALS, RoundingMode.HALF_UP);

            return;
        }

        if (value != null && value instanceof String) {
            value = StringUtils.rtrim((String)value);

            if (((String)value).length() == 0) {
                value = null;
            }
        }

        this.datatype = determineImplicitType(value);

        if (this.datatype.isNumeric()) {
            if (value instanceof Float) {
                value = ((Float)value).doubleValue();
            }

            if (value instanceof Double) {
                double converted = (Double)value;

                if (converted == Math.rint(converted)) {
                    value = (long)converted;
                }
            }

            BigDecimal converted;

            if (value instanceof String || value instanceof Double) {
                if (value instanceof Double) {
                    value = DECIMAL_PATTERN.get().format((double)(Double)value);
                }

                converted = new BigDecimal((String)value);
                converted = converted.setScale(COMPARISON_DECIMALS, RoundingMode.HALF_UP);
            } else if (value instanceof Long) {
                converted = new BigDecimal((Long)value);
            } else if (value instanceof Integer) {
                converted = new BigDecimal((Integer)value);
            } else {
                // The assertion below is bogus since that will never happen, but the IDE gets mad we might cause a NPE (we can't)
                assert value != null;

                throw new IllegalArgumentException("Unable to handle provided value of type " + value.getClass().getName());
            }

            value = new NumericValue(converted, value.toString());
        }

        this.value = value;
    }

    public int compareTo(Object o) {
        return this.compareTo(o, true);
    }

    public int compareTo(Object o, boolean caseSensitive) {
        DataEntry rhsEntry = o instanceof DataEntry ? (DataEntry)o : new DataEntry(o);

        int result;
        Object lhs = this.getValue();
        Object rhs = rhsEntry.getValue();
        DataType lhsType = this.getDataType();
        DataType rhsType = rhsEntry.getDataType();

        // The implicit type is theoretically the most "natural" comparison we're going
        // to get, so we attempt to compare on those grounds first. If not, we'll default
        // to comparing strings.
        if (lhs == null && rhs == null) {
            result = 0;
        } else if (lhs == null) {
            result = -1;
        } else if (rhs == null) {
            result = 1;
        } else {
            if (lhsType.isNumeric() && rhsType.isNumeric()) {
                result = ((BigDecimal)lhs).compareTo((BigDecimal)rhs);
            } else {
                if (!(lhs instanceof String)) {
                    lhs = this.toString();
                }

                if (!(rhs instanceof String)) {
                    rhs = rhsEntry.toString();
                }

                String lhsValue = (String)lhs;
                String rhsValue = (String)rhs;
                boolean lhsDate = lhsType == DataType.DateTime ||
                        (lhsType == DataType.Integer && lhsValue.length() == 4);
                boolean rhsDate = rhsType == DataType.DateTime ||
                        (rhsType == DataType.Integer && rhsValue.length() == 4);

                // Perform fuzzy equality for things that look like dates
                if (lhsDate && rhsDate && (lhsValue.startsWith(rhsValue) ||
                        rhsValue.startsWith(lhsValue))) {
                    result = 0;
                } else {
                    if (!caseSensitive) {
                        lhsValue = lhsValue.toUpperCase();
                        rhsValue = rhsValue.toUpperCase();
                    }

                    result = lhsValue.compareTo(rhsValue);
                }
            }
        }

        return result;
    }

    public boolean equals(Object o) {
        return this.compareTo(o) == 0;
    }

    public DataType getDataType() {
        return this.datatype;
    }

    public Object getValue() {
        return this.value instanceof NumericValue ?
            ((NumericValue)this.value).getValue() : this.value;
    }

    public boolean isNumeric() {
        return this.datatype.isNumeric();
    }

    public boolean isDate() {
        return this.datatype.isDate();
    }

    public int hashCode() {
        String value = this.toString();

        if (value.length() == 0) {
            return 0;
        }

        return value.hashCode();
    }

    public boolean hasValue() {
        return this.value != null;
    }

    public String toString() {
        return this.value != null ? this.value.toString() : "";
    }

    private static DataType determineImplicitType(Object value) {
        DataType type = null;

        if (value == null) {
            type = DataType.Null;
        } else if (value instanceof String) {
            String asString = (String)value;
            int length = asString.length();
            char firstChar = length > 0 ? asString.charAt(0) : '\0';

            if (length == 0 || !((firstChar == '-' && length > 1) ||
                    (firstChar == '.' && length > 1) ||
                    (firstChar >= '0' && firstChar <= '9'))) {
                // Can't be anything else
                type = DataType.Text;
            } else {
                boolean couldBeDouble  = firstChar == '.';
                boolean determinedType = true;
                boolean possibleNumber = firstChar != '0' || (length == 1 || asString.charAt(1) == '.');

                if (possibleNumber) {
                    for (int i = 1; i < length; ++i) {
                        char currentChar = asString.charAt(i);
                        if (!(currentChar >= '0' && currentChar <= '9')) {
                            if (currentChar == '.' && !couldBeDouble && i < length - 1) {
                                couldBeDouble = true;
                            } else {
                                determinedType = false;
                                break;
                            }
                        }
                    }
                } else {
                    determinedType = false;
                }

                if (determinedType) {
                    if (couldBeDouble) {
                        type = DataType.Float;
                    } else {
                        type = DataType.Integer;
                    }
                } else {
                    if (DATE_PATTERN.get().matcher(asString).matches()) {
                        type = DataType.DateTime;
                    } else {
                        type = DataType.Text;
                    }
                }
            }
        } else {
            if (value instanceof Integer || value instanceof Long) {
                type = DataType.Integer;
            } else if (value instanceof Float || value instanceof Double) {
                type = DataType.Float;
            }
        }

        return type;
    }

    private static class NumericValue {
        private final BigDecimal value;
        private final String string;

        NumericValue(BigDecimal value, String string) {
            this.value = value;
            this.string = string;
        }

        BigDecimal getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return this.string;
        }
    }
}
