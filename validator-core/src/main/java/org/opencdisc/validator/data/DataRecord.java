/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.DataDetails;
import org.opencdisc.validator.util.Helpers;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.Settings;

/**
 * Contains a single "row" of data from a given <code>DataSource</code>. A row is
 * comprised of <code>DataEntry</code> objects index by their given variable name.
 *
 * @author Tim Stone
 */
public class DataRecord {
    private static final ThreadLocal<MessageDigest> DIGEST = new ThreadLocal<MessageDigest>();

    private InternalEntityDetails entity;
    private Map<String, DataEntry> entries = new HashMap<String, DataEntry>();
    private int id = -1;
    private boolean keyState;
    private boolean allowUnqualifiedReferences;
    private final DataDetails record;
    private final Set<String> transientEntries = new HashSet<String>();

    /**
     *
     * @param record  the <code>DataDetails</code> identifying this record
     * @param details  the <code>InternalEntityDetails</code> representing the source
     *    of this record
     */
    public DataRecord(DataDetails record, InternalEntityDetails details) {
        this.entity = details;
        this.record = record;
        this.keyState = this.entity != null && this.entity.getKeys().size() > 0 &&
                Settings.defines("Engine.GenerateKeys");

        if (this.keyState && DIGEST.get() == null) {
            try {
                DIGEST.set(MessageDigest.getInstance("SHA-1"));
            } catch (NoSuchAlgorithmException ignore) {}
        }
    }

    /**
     *
     * @param variable  the name of the variable to check for
     * @return <code>true</code> if the variable is defined for this record, <code>false</code> otherwise
     */
    public boolean definesVariable(String variable) {
        return this.entries.containsKey(variable) ||
            (this.allowUnqualifiedReferences && this.entries.containsKey(DataSupplement.VARIABLE_PREFIX + variable));
    }

    public boolean isTransient(String variable) {
        return this.transientEntries.contains(variable);
    }

    public void allowUnqualifiedReferences() {
        this.allowUnqualifiedReferences = true;
    }

    public void disallowUnqualifiedReferences() {
        this.allowUnqualifiedReferences = false;
    }

    /**
     *
     * @return the <code>DataDetails</code> identifying this record
     */
    public DataDetails getDataDetails() {
        if (this.keyState) {
            this.keyState = false;

            Set<String> variables = this.entity.getKeys();
            MessageDigest digest = DIGEST.get();
            boolean first = true;
            DataEntry entry;

            for (String variable : variables) {
                if (!first || (first = false)) {
                    digest.update((byte)'\037');
                }

                entry = this.entries.get(variable);

                if (entry != null) {
                    digest.update(entry.toString().getBytes());
                }
            }

            this.record.setKey(Helpers.toHexString(digest.digest()));
        }

        return this.record;
    }

    /**
     *
     * @return the <code>EntityDetails</code> representing the source of this record
     */
    public EntityDetails getEntityDetails() {
        return this.entity;
    }

    /**
     *
     * @param variable  the name of the variable to retrieve
     * @return the <code>DataEntry</code> assigned to the provided variable
     * @throws IllegalArgumentException  if the variable is not defined for this record
     */
    public DataEntry getValue(String variable) {
        DataEntry value = this.entries.get(variable);

        if (value == null) {
            if (this.allowUnqualifiedReferences &&
                    (value = this.entries.get(DataSupplement.VARIABLE_PREFIX + variable)) != null) {
                return value;
            }

            throw new IllegalArgumentException(String.format(
                "The variable %s does not exist in this DataRecord",
                variable
            ));
        }

        return value;
    }

    /**
     *
     * @return the <code>Set</code> of variables defined for this record
     */
    public Set<String> getVariables() {
        return this.entries.keySet();
    }

    /**
     *
     * @param variable  the name of the variable to set for this record
     * @param value  the <code>DataEntry</code> that encases the value for the given variable
     * @throws IllegalArgumentException
     */
    public void setValue(String variable, DataEntry value) {
        if (variable == null || value == null) {
            throw new IllegalArgumentException("Variable name and value must not be "
                    + "null");
        } else if (this.definesVariable(variable)) {
            throw new IllegalArgumentException(String.format(
                    "Cannot modify already-defined variable %s",
                    variable
            ));
        }

        this.entries.put(variable, value);
    }

    /**
     *
     * @param variable  the name of the transient variable to set for this record
     * @param value  the <code>DataEntry</code> that encases the value for the given variable
     */
    public void setTransientValue(String variable, DataEntry value) {
        // TODO: Do we even need to call setValue here, or could we just keep a separate map?
        //       There doesn't seem to be a good reason for adding transient entries to our
        //       normal values map
        this.setValue(variable, value);
        this.transientEntries.add(variable);
    }

    public void clear() {
        this.entries.keySet().removeAll(this.transientEntries);
        this.transientEntries.clear();
    }

    /**
     *
     * @return the per-source ID of this record
     */
    public int getID() {
        return this.id > 0 ? this.id : this.record.getID();
    }

    void setID(int id) {
        if (this.id > 0) {
            throw new IllegalArgumentException("Record ID already set");
        }

        this.id = id;
    }
}
