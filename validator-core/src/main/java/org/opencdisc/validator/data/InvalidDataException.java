/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

/**
 *
 * @author Tim Stone
 */
public class InvalidDataException extends Exception {
    private static final long serialVersionUID = -1542200475977752791L;

    /**
     *
     */
    public enum Codes {
        CannotParseSource,
        NoVariables,
        NoRecords,
        InvalidRequestState,
        DataDimensionMismatch,
        MissingSource,
        DuplicateVariable
    }

    private final Codes code;

    /**
     *
     * @param errorCode
     */
    public InvalidDataException(Codes errorCode) {
        this(errorCode, null);
    }

    /**
     *
     * @param errorCode
     * @param message
     */
    public InvalidDataException(Codes errorCode, String message) {
        super(message);

        this.code = errorCode;
    }

    /**
     *
     * @return
     */
    public Codes getCode() {
        return this.code;
    }
}
