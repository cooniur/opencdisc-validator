/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.VariableDetails;

import java.util.*;

/**
 * @author Tim Stone
 */
public final class InternalEntityDetails implements EntityDetails {
    private final Map<EntityDetails.Property, Object> properties =
        new HashMap<EntityDetails.Property, Object>();
    private final EntityDetails.Reference reference;
    private final EntityDetails parent;
    private final List<EntityDetails> subentities = new LinkedList<EntityDetails>();
    private final List<VariableDetails> variables = new ArrayList<VariableDetails>();
    private final Map<String, Integer> lookup = new HashMap<String, Integer>();
    private List<String> examined;
    private Set<String> keys;

    public InternalEntityDetails(EntityDetails.Reference reference, String name,
            String location) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }

        if (location == null) {
            throw new IllegalArgumentException("location cannot be null");
        }

        this.reference = reference;
        this.properties.put(EntityDetails.Property.Name, name.toUpperCase());
        this.properties.put(EntityDetails.Property.Location, location);
        this.parent = null;
    }

    public InternalEntityDetails(EntityDetails.Reference reference, String name,
            String subname, String location) {
        this(reference, name, location);

        if (StringUtils.isNotEmpty(subname)) {
            this.properties.put(EntityDetails.Property.Subname, subname.toUpperCase());
        }
    }

    public InternalEntityDetails(EntityDetails.Reference reference,
            Map<EntityDetails.Property, Object> properties,
            InternalEntityDetails parent) {
        if (properties == null) {
            throw new IllegalArgumentException("properties map cannot be null");
        }

        if (!properties.containsKey(EntityDetails.Property.Name)) {
            throw new IllegalArgumentException("the Name property must always be "
                    + "specified");
        }

        this.reference = reference;
        this.properties.putAll(properties);
        this.parent = parent;
    }

    public InternalEntityDetails(EntityDetails.Reference reference,
            InternalEntityDetails template) {
        this(reference, template.properties, null);
    }

    public InternalEntityDetails(EntityDetails.Reference reference,
            InternalEntityDetails template, InternalEntityDetails parent) {
        this(reference, template.properties, parent);
    }

    public boolean getBoolean(EntityDetails.Property property) {
        return this.convert(property) != 0;
    }

    public int getInteger(EntityDetails.Property property) {
        return this.convert(property);
    }

    public EntityDetails getParent() {
        return this.parent;
    }

    public EntityDetails.Reference getReference() {
        return this.reference;
    }

    public String getString(EntityDetails.Property property) {
        return this.getProperty(property).toString();
    }

    public boolean hasVariable(String name) {
        return this.getVariable(name) != null;
    }

    public VariableDetails getVariable(String name) {
        Integer position = this.lookup.get(name);

        if (position == null) {
            return null;
        }

        return this.variables.get(position);
    }

    public List<VariableDetails> getVariables() {
        return Collections.unmodifiableList(this.variables);
    }

    public boolean hasProperty(EntityDetails.Property property) {
        return this.properties.containsKey(property);
    }

    private Object getProperty(EntityDetails.Property property) {
        if (!this.hasProperty(property)) {
            throw new IllegalArgumentException(String.format(
                "The property %s is not set", property.toString()
            ));
        }

        return this.properties.get(property);
    }

    public void setProperty(EntityDetails.Property property, Object value) {
        if (value == null || (value instanceof String && StringUtils.isEmpty((String)value))) {
            return;
        }

        if (this.hasProperty(property) && !property.isUpdatable()) {
            throw new IllegalArgumentException(String.format(
                "Cannot reassign the property %s", property.toString()
            ));
        }

        if (property == EntityDetails.Property.Keys) {
            Set<String> keys = new TreeSet<String>();

            for (String key : value.toString().split(",")) {
                keys.add(key.toUpperCase().trim());
            }

            this.keys = Collections.unmodifiableSet(keys);
        }

        this.properties.put(property, value);
    }

    public void addVariable(VariableDetails variable) {
        this.lookup.put(
            variable.getString(VariableDetails.Property.Name), this.variables.size()
        );
        this.variables.add(variable);
    }

    public Set<String> getKeys() {
        return this.keys == null ? Collections.<String>emptySet() : this.keys;
    }

    public List<String> getExamined() {
        return this.examined == null ? Collections.<String>emptyList() : this.examined;
    }

    public void addSubentity(InternalEntityDetails subentity) {
        this.subentities.add(subentity);
    }

    public List<EntityDetails> getSubentities() {
        return Collections.unmodifiableList(this.subentities);
    }

    public void setExamined(List<String> keys) {
        this.examined = Collections.unmodifiableList(keys);
    }

    private Integer convert(EntityDetails.Property property) {
        Object value = this.getProperty(property);

        if (!property.isConvertible()) {
            throw new IllegalArgumentException(String.format(
                "The property %s cannot be converted to a non-String type", property
            ));
        }

        if (value instanceof Integer) {
            return (Integer)value;
        }

        if (value instanceof Boolean) {
            return (Boolean)value ? 1 : 0;
        }

        // This should be made unnecessary by code elsewhere, but just in case
        return Integer.valueOf(value.toString());
    }
}
