/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.model.EntityDetails.Reference;

/**
 *
 * @author Tim Stone
 */
public class CombinedDataSource implements DataSource {
    private static final DataEntry NULL_ENTRY = new DataEntry(null);
    private static final String SPLIT_VARIABLE = "VAL:DATASET";

    private final Set<String> variables = new LinkedHashSet<String>();
    private final boolean identifyAsMetadata;
    private final List<DataSource> sources = new ArrayList<DataSource>();
    private final CombinedDataSource metadata;
    private final InternalEntityDetails entity;

    private Set<String> augment = null;
    private boolean isMetadata = false;
    private int recordCount = 0;
    private DataSource source = null;
    private int sourcePosition = 0;

    /**
     *
     * @param sources
     */
    public CombinedDataSource(DataSource... sources) {
        this(
            sources.length > 0 ? sources[0].getDetails().getString(Property.Name) : null,
            combineLocations(sources)
        );

        for (DataSource source : sources) {
            this.add(source);
        }
    }

    /**
     *
     * @param name
     * @param location
     */
    public CombinedDataSource(String name, String location) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }

        this.entity = new InternalEntityDetails(Reference.Data, name, location);
        this.entity.setProperty(Property.Subname, name);
        this.entity.setProperty(Property.Combined, true);
        this.metadata = new CombinedDataSource(this.entity);
        this.identifyAsMetadata = false;
    }

    private CombinedDataSource(InternalEntityDetails entity) {
        this.entity = new InternalEntityDetails(Reference.Metadata, entity);
        this.metadata = null;
        this.isMetadata = true;
        this.identifyAsMetadata = true;
    }

    /**
     *
     * @param source
     */
    public void add(DataSource source) {
        try {
            if (this.sources.isEmpty()) {
                this.isMetadata = source.isMetadata();
                this.source = source;
                this.confirmAndUpdateMetadata(source);
            } else if (!this.confirmAndUpdateMetadata(source)) {
                throw new IllegalArgumentException("sources had incompatible metadata");
            }
        } catch (InvalidDataException ex) {
            throw new IllegalArgumentException("invalid source");
        }

        this.sources.add(source);
        this.entity.addSubentity(source.getDetails());
        this.entity.setProperty(Property.Location, combineLocations(
            this.sources.toArray(new DataSource[this.sources.size()]))
        );
    }

    public void close() {
        for (DataSource source : this.sources) {
            source.close();
        }
    }

    public InternalEntityDetails getDetails() {
        return this.entity;
    }

    public String getLocation() {
        return this.entity.getString(Property.Location);
    }

    public DataSource getMetadata() {
        return this.metadata;
    }

    public String getName() {
        return this.entity.getString(Property.Name);
    }

    public int getRecordCount() {
        return this.recordCount;
    }

    public List<DataRecord> getRecords() throws InvalidDataException {
        return this.getRecords(AbstractDataSource.DEFAULT_RECORD_COUNT);
    }

    public List<DataRecord> getRecords(int recordCount) throws InvalidDataException {
        List<DataRecord> records = new ArrayList<DataRecord>();

        if (this.augment == null) {
            this.determineAugment();
        }

        while (this.hasRecords() && records.size() < recordCount) {
            if (this.source.hasRecords()) {
                records.addAll(this.augment(this.source.getRecords(recordCount)));
            }

            if (records.size() < recordCount || !this.source.hasRecords()) {
                ++this.sourcePosition;

                if (this.sourcePosition < this.sources.size()) {
                    this.source = this.sources.get(this.sourcePosition);
                    this.determineAugment();
                }
            }
        }

        this.entity.setProperty(Property.Records, this.recordCount);

        return records;
    }

    public Set<String> getVariables() throws InvalidDataException {
        // TODO: Are we supposed to be guaranteeing order here?
        //   If so, the interface should actually enforce that
        return new LinkedHashSet<String>(
            this.isMetadata ? Metadata.VARIABLES : this.variables
        );
    }

    public Object getVariableProperty(String variable, VariableProperty property)
            throws InvalidDataException {
        throw new UnsupportedOperationException(
            "cannot call getVariableProperty on a combined data source"
        );
    }

    public boolean hasRecords() {
        return this.source != null && this.source.hasRecords();
    }

    public boolean isComposite() {
        return true;
    }

    public boolean isMetadata() {
        return this.identifyAsMetadata && this.isMetadata;
    }

    public DataSource replicate() throws InvalidDataException {
        CombinedDataSource duplicate;

        // TODO: This is not the ideal way to replicate this source
        if (!this.identifyAsMetadata) {
            duplicate = new CombinedDataSource(
                this.entity.getString(Property.Name),
                this.entity.getString(Property.Location)
            );
        } else {
            duplicate = new CombinedDataSource(
                this.entity
            );
        }

        for (DataSource source : this.sources) {
            duplicate.add(source.replicate());
        }

        return duplicate;
    }

    public boolean test() {
        // All of the individual sources should have been tested by now, so we can just
        // say that the test succeeds (I don't think this will ever be called, actually)
        return true;
    }

    /**
     *
     * @param records
     * @return
     */
    private List<DataRecord> augment(List<DataRecord> records) {
        // We never need to augment metadata
        if (!this.isMetadata) {
            DataEntry split = new DataEntry(
                this.source.getDetails().getString(Property.Subname)
            );

            for (DataRecord record : records) {
                record.setID(++this.recordCount);
                record.setValue(SPLIT_VARIABLE, split);

                for (String variable : this.augment) {
                    record.setValue(variable, NULL_ENTRY);
                }
            }
        } else {
            this.recordCount += records.size();
        }

        return records;
    }

    /**
     *
     */
    private void determineAugment() {
        if (this.isMetadata) {
            return;
        }

        try {
            Set<String> defined = this.source.getVariables();

            this.augment = new HashSet<String>(this.variables);
            this.augment.removeAll(defined);
        } catch (InvalidDataException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     *
     * @param source
     * @return
     */
    private boolean confirmAndUpdateMetadata(DataSource source)
            throws InvalidDataException {
        boolean matches = true;

        if (source.isMetadata() != this.isMetadata) {
            throw new IllegalArgumentException("The two sources are not the same "
                    + "reference type");
        }

        if (this.isMetadata) {
            matches = source.getVariables().equals(Metadata.VARIABLES);
        } else {
            this.variables.addAll(source.getVariables());
            this.metadata.add(source.getMetadata().replicate());
        }

        return matches;
    }

    /**
     *
     * @param sources
     * @return
     */
    private static String combineLocations(DataSource[] sources) {
        StringBuilder builder = new StringBuilder();

        if (sources.length == 0) {
            return "unknown";
        }

        for (int i = 0; i < sources.length; ++i) {
            if (i > 0) {
                builder.append(File.pathSeparatorChar);
            }

            builder.append(sources[i].getLocation());
        }

        return builder.toString();
    }
}
