/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.*;

/**
 * @author Tim Stone
 */
public class DataSupplement {
    public static final String VARIABLE_PREFIX = "SUB";

    // TODO: This is sort of experimental at the moment, so the key value is hard-coded
    //    Ideally we can support this via external configuration, like some attribute on
    //    the ItemGroupDef in the configuration file.
    private static final String KEY_VARIABLE = "USUBJID";
    private final Map<String, Integer> variables = new HashMap<String, Integer>();
    private final Map<DataEntry, DataEntry> interns =
        new HashMap<DataEntry, DataEntry>();
    private final Map<DataEntry, DataEntry[]> groupings =
        new HashMap<DataEntry, DataEntry[]>();

    public DataSupplement(DataSource source) throws InvalidDataException {
        source = source.replicate();
        Set<String> variables = new HashSet<String>(source.getVariables());

        variables.remove(KEY_VARIABLE);
        // TODO: These variables are hardcoded, but probably shouldn't be
        variables.remove("DOMAIN");
        variables.remove("STUDYID");

        int count = 0;

        for (String variable : variables) {
            this.variables.put(VARIABLE_PREFIX + ":" + variable, count++);
        }

        while (source.hasRecords()) {
            for (DataRecord record : source.getRecords()) {
                DataEntry key = this.intern(record.getValue(KEY_VARIABLE));
                DataEntry[] values = new DataEntry[count];

                for (Map.Entry<String, Integer> entry : this.variables.entrySet()) {
                    values[entry.getValue()] = this.intern(
                        record.getValue(
                            entry.getKey().substring((VARIABLE_PREFIX + ":").length())
                        )
                    );
                }

                this.groupings.put(key, values);
            }
        }
    }

    public DataRecord augment(DataRecord record) {
        // TODO: Technically we should defer augmentation until it's actually needed
        //    We need to see if there's any significant performance implications
        //    that come with doing this regardless of need
        // TODO: We should probably pull this check to a higher level
        //    The calling code should know not to try and augment sources that it
        //    has no reason to be attempting to augment, most likely
        if (record.definesVariable(KEY_VARIABLE)) {
            DataEntry key = record.getValue(KEY_VARIABLE);
            DataEntry[] values = this.groupings.get(key);

            for (Map.Entry<String, Integer> variable : this.variables.entrySet()) {
                record.setValue(
                    variable.getKey(),
                    values != null ? values[variable.getValue()] : DataEntry.NULL_ENTRY
                );
            }
        }

        return record;
    }

    private DataEntry intern(DataEntry entry) {
        DataEntry interned = this.interns.get(entry);

        if (interned == null) {
            this.interns.put(entry, interned = entry);
        }

        return interned;
    }
}
