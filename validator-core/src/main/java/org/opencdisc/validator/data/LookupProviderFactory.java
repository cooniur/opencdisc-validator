/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.data;

import java.util.Map;

import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public final class LookupProviderFactory {
    private static final Map<String, LookupProvider> PROVIDERS =
        new KeyMap<LookupProvider>();
    // TODO: Add ability to register adapters from calling code
    private static final Map<String, Class<? extends LookupProvider>> REGISTERED =
        new KeyMap<Class<? extends LookupProvider>>();
    private static final String DEFAULT_INSTANCE = "_default";

    private LookupProviderFactory() {}

    /**
     *
     * @return
     */
    public synchronized static LookupProvider getLookupProvider() {
        return getLookupProvider(DEFAULT_INSTANCE);
    }

    /**
     *
     * @param identifier
     * @return
     */
    public synchronized static LookupProvider getLookupProvider(String identifier) {
        if (identifier == null) {
            throw new IllegalArgumentException("identifier cannot be null");
        }

        if (!PROVIDERS.containsKey(identifier)) {
            if (identifier.equalsIgnoreCase(DEFAULT_INSTANCE)) {
                PROVIDERS.put(identifier, new ConcurrentLookupProvider());
            } else {
                Class<?> impl = REGISTERED.get(identifier);

                // The identifier might be a raw class name
                if (impl == null) {
                    try {
                        impl = Class.forName(identifier);
                    } catch (ClassNotFoundException ex) {
                        // Do nothing, because we only care that we didn't get a valid
                        // result, not that this specific step failed
                    }
                }

                if (impl == null) {
                    throw new IllegalArgumentException(String.format(
                            "The identifier %s does not match an available "
                            + "LookupProvider implementation",
                            identifier
                    ));
                }

                boolean failed = true;

                try {
                    PROVIDERS.put(identifier, (LookupProvider)impl.newInstance());

                    failed = false;
                } catch (ClassCastException ex) {
                    // Do nothing, because we only care that we didn't get a valid
                    // result, not that this specific step failed
                } catch (InstantiationException ex) {
                    // Do nothing, because we only care that we didn't get a valid
                    // result, not that this specific step failed
                } catch (IllegalAccessException ex) {
                    // Do nothing, because we only care that we didn't get a valid
                    // result, not that this specific step failed
                }

                if (failed) {
                    throw new IllegalArgumentException(String.format(
                            "Could not successfully create an instance of the "
                            + "LookupProvider implementation identified by %s",
                            identifier
                    ));
                }
            }
        }

        return PROVIDERS.get(identifier);
    }

    public synchronized static void clear(ProcessToken token) {
        for (LookupProvider provider : PROVIDERS.values()) {
            provider.clear(token);
        }
    }
}
