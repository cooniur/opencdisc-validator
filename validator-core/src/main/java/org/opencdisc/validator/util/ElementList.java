/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Iterator;

/**
 * @author Tim Stone
 */
public class ElementList implements Iterable<Element> {
    private final NodeList nodes;

    public ElementList(NodeList nodes) {
        this.nodes = nodes;
    }

    public boolean hasElements() {
        return this.nodes.getLength() > 0;
    }

    public Element get(int index) {
        if (index < 0 || index > this.nodes.getLength()) {
            throw new ArrayIndexOutOfBoundsException();
        }

        return (Element)this.nodes.item(index);
    }

    public int size() {
        return this.nodes.getLength();
    }

    public Iterator<Element> iterator() {
        return new NodeListIterator();
    }

    private class NodeListIterator implements Iterator<Element> {
        private final int length = ElementList.this.nodes.getLength();
        private int index = -1;

        public boolean hasNext() {
            return this.index < this.length - 1;
        }

        public Element next() {
            ++this.index;

            return (Element)ElementList.this.nodes.item(this.index);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
