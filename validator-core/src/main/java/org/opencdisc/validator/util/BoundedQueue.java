/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author Tim Stone
 */
public class BoundedQueue<T> implements Queue<T> {
    private final boolean autoShift;
    private Queue<T> queue = new LinkedList<T>();
    private final int queueSize;

    public BoundedQueue(int queueSize) {
        this(queueSize, true);
    }

    public BoundedQueue(int queueSize, boolean autoShift) {
        this.queueSize = queueSize;
        this.autoShift = autoShift;
    }

    public boolean offer(T value) {
        boolean result = true;

        if (this.size() < this.queueSize) {
            this.queue.add(value);
        } else {
            if (this.autoShift) {
                this.poll();
                this.queue.add(value);
            } else {
                result = false;
            }
        }

        return result;
    }

    public boolean addAll(Collection<? extends T> collection) {
        boolean result = true;

        for (T item : collection) {
            if (!this.offer(item)) {
                result = false;
            }
        }

        return result;
    }

    public boolean add(T value) {
        return this.offer(value);
    }

    public T element() {
        return this.queue.element();
    }

    public T peek() {
        return this.queue.peek();
    }

    public T poll() {
        return this.queue.poll();
    }

    public T remove() {
        return this.queue.remove();
    }

    public void clear() {
        this.queue.clear();
    }

    public boolean contains(Object value) {
        return this.queue.contains(value);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.queue.containsAll(collection);
    }

    public boolean isEmpty() {
        return this.queue.isEmpty();
    }

    public Iterator<T> iterator() {
        return this.queue.iterator();
    }

    public boolean remove(Object value) {
        return this.queue.remove(value);
    }

    public boolean removeAll(Collection<?> collection) {
        return this.queue.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        return this.queue.retainAll(collection);
    }

    public int size() {
        return this.queue.size();
    }

    public Object[] toArray() {
        return this.queue.toArray();
    }

    public <E> E[] toArray(E[] array) {
        return this.queue.toArray(array);
    }

}
