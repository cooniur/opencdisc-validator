/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringEscapeUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

/**
 *
 * @author Tim Stone
 */
public class XmlWriter {
    public static final String SCHEMA_NAMESPACE_URI =
        "http://www.w3.org/2001/XMLSchema-instance";
    public static final String XLINK_NAMESPACE_URI =
        "http://www.w3.org/1999/xlink";
    public static final String XML_NAMESPACE_URI =
        "http://www.w3.org/XML/1998/namespace";

    private static final String BLANK_PREFIX = "";
    private static final String SCHEMA_PREFIX = "xsi";
    private static final String XLINK_PREFIX = "xlink";
    private static final String XML_PREFIX = "xml";

    private final TransformerHandler contentHandler;
    private final String defaultNamespace;
    private final Map<String, String> prefixes = new HashMap<String, String>();
    private final OutputStream targetStream;

    /**
     *
     * @param target
     * @param defaultNamespace
     * @throws IOException
     * @throws SAXException
     * @throws TransformerConfigurationException
     */
    public XmlWriter(File target, String defaultNamespace) throws IOException,
            SAXException, TransformerConfigurationException {
        this(target, defaultNamespace, (Instruction[])null);
    }

    /**
     *
     * @param target
     * @param defaultNamespace
     * @param instructions
     * @throws IOException
     * @throws SAXException
     * @throws TransformerConfigurationException
     */
    public XmlWriter(File target, String defaultNamespace, Instruction... instructions)
            throws IOException, SAXException, TransformerConfigurationException {
        this.defaultNamespace = defaultNamespace;
        this.targetStream = new FileOutputStream(target);
        Result result = new StreamResult(this.targetStream);
        SAXTransformerFactory factory =
                (SAXTransformerFactory)SAXTransformerFactory.newInstance();
        this.contentHandler = factory.newTransformerHandler();
        Transformer transformer = this.contentHandler.getTransformer();

        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        this.contentHandler.setResult(result);
        this.contentHandler.startDocument();

        if (instructions != null) {
            for (Instruction instruction : instructions) {
                this.contentHandler.processingInstruction(instruction.type,
                        instruction.instruction);
                this.contentHandler.characters(new char[] { '\n' }, 0, 1);
            }
        }

        this.addMapping(XML_NAMESPACE_URI, XML_PREFIX);
        this.addMapping(SCHEMA_NAMESPACE_URI, SCHEMA_PREFIX);
        this.addMapping(XLINK_NAMESPACE_URI, XLINK_PREFIX);
        this.addMapping(this.defaultNamespace, BLANK_PREFIX);
    }

    /**
     *
     * @param uri
     * @param prefix
     * @throws SAXException
     */
    public void addMapping(String uri, String prefix) throws SAXException {
        this.prefixes.put(uri, prefix);
        this.contentHandler.startPrefixMapping(prefix, uri);
    }

    /**
     *
     * @throws SAXException
     * @throws IOException
     */
    public void close() throws SAXException, IOException {
        try {
            for (String prefix : this.prefixes.keySet()) {
                this.contentHandler.endPrefixMapping(prefix);
            }

            this.contentHandler.endDocument();
        } finally {
            this.targetStream.close();
        }
    }

    /**
     *
     * @param characterData
     * @throws SAXException
     */
    public void characters(String characterData) throws SAXException {
        characterData = StringEscapeUtils.escapeXml(characterData);

        this.contentHandler.characters(characterData.toCharArray(), 0,
                characterData.length());
    }

    /**
     *
     * @param comment
     * @throws SAXException
     */
    public void comment(String...comments) throws SAXException {
        for (String comment : comments) {
            this.contentHandler.comment(comment.toCharArray(), 0, comment.length());
            this.contentHandler.characters(new char[] { '\n' }, 0, 1);
        }
    }

    /**
     *
     * @param localName
     * @throws SAXException
     */
    public void startElement(String localName) throws SAXException {
        this.startElement(this.defaultNamespace, localName);
    }

    /**
     *
     * @param localName
     * @param attributes
     * @throws SAXException
     */
    public void startElement(String localName, Attributes attributes)
            throws SAXException {
        this.startElement(this.defaultNamespace, localName, attributes);
    }

    /**
     *
     * @param uri
     * @param localName
     * @throws SAXException
     */
    public void startElement(String uri, String localName) throws SAXException {
        this.startElement(uri, localName, new AttributesImpl());
    }

    /**
     *
     * @param uri
     * @param localName
     * @param attributes
     * @throws SAXException
     */
    public void startElement(String uri, String localName, Attributes attributes)
            throws SAXException {
        this.contentHandler.startElement(uri, localName, prefix(localName, uri),
                attributes);
    }

    /**
     *
     * @param localName
     * @param characterData
     * @throws SAXException
     */
    public void simpleElement(String localName, String characterData)
            throws SAXException {
        this.simpleElement(this.defaultNamespace, localName, characterData);
    }

    /**
     *
     * @param localName
     * @param attributes
     * @throws SAXException
     */
    public void simpleElement(String localName, Attributes attributes)
            throws SAXException {
        this.simpleElement(this.defaultNamespace, localName, attributes, null);
    }

    /**
     *
     * @param localName
     * @param attributes
     * @param characterData
     * @throws SAXException
     */
    public void simpleElement(String localName, Attributes attributes,
            String characterData) throws SAXException {
        this.simpleElement(this.defaultNamespace, localName, attributes, characterData);
    }

    /**
     *
     * @param uri
     * @param localName
     * @param characterData
     * @throws SAXException
     */
    public void simpleElement(String uri, String localName, String characterData)
            throws SAXException {
        this.simpleElement(uri, localName, new AttributesImpl(), characterData);
    }

    /**
     *
     * @param uri
     * @param localName
     * @param attributes
     * @param characterData
     * @throws SAXException
     */
    public void simpleElement(String uri, String localName, Attributes attributes,
            String characterData) throws SAXException {
        this.startElement(uri, localName, attributes);

        if (characterData != null) {
            this.characters(characterData);
        }

        this.endElement(uri, localName);
    }

    /**
     *
     * @param localName
     * @throws SAXException
     */
    public void endElement(String localName) throws SAXException {
        this.endElement(this.defaultNamespace, localName);
    }

    /**
     *
     * @param uri
     * @param localName
     * @throws SAXException
     */
    public void endElement(String uri, String localName) throws SAXException {
        this.contentHandler.endElement(uri, localName, prefix(localName, uri));
    }

    /**
     *
     * @param localName
     * @throws SAXException
     */
    public void emptyElement(String localName) throws SAXException {
        this.emptyElement(this.defaultNamespace, localName);
    }

    /**
     *
     * @param uri
     * @param localName
     * @throws SAXException
     */
    public void emptyElement(String uri, String localName) throws SAXException {
        this.startElement(uri, localName);
        this.contentHandler.characters(new char[] { ' ' }, 0, 1);
        this.endElement(uri, localName);
    }

    /**
     *
     * @return
     */
    public SimpleAttributes newAttributes() {
        return new SimpleAttributes();
    }

    /**
     *
     * @param name
     * @return
     */
    public SimpleAttributes newAttributes(String name) {
        return new SimpleAttributes(name);
    }

    /**
     *
     * @param localName
     * @param uri
     * @return
     */
    private String prefix(String localName, String uri) {
        String prefix = BLANK_PREFIX;

        if (this.prefixes.containsKey(uri)) {
            prefix = this.prefixes.get(uri) + ":";
        }

        return prefix + localName;
    }

    /**
     *
     * @author Tim Stone
     */
    public static class Instruction {
        final String type;
        final String instruction;

        /**
         *
         * @param type
         * @param instruction
         */
        public Instruction(String type, String instruction) {
            this.type = type;
            this.instruction = instruction;
        }
    }

    /**
     *
     * @author Tim Stone
     */
    public class SimpleAttributes extends AttributesImpl
            implements Comparable<SimpleAttributes> {
        private final String name;

        /**
         *
         */
        SimpleAttributes() {
            this("");
        }

        /**
         *
         * @param name
         */
        SimpleAttributes(String name) {
            this.name = name;
        }

        /**
         *
         * @param localName
         */
        public SimpleAttributes addEmptyAttribute(String localName) {
            return this.addAttribute(localName, "");
        }

        /**
         *
         * @param uri
         * @param localName
         */
        public SimpleAttributes addEmptyAttribute(String uri, String localName) {
            return this.addAttribute(uri, localName, "");
        }

        /**
         *
         * @param localName
         * @param value
         */
        public SimpleAttributes addAttribute(String localName, String value) {
            return this.addAttribute(XmlWriter.this.defaultNamespace, localName, value);
        }

        /**
         *
         * @param uri
         * @param localName
         * @param value
         */
        public SimpleAttributes addAttribute(String uri, String localName,
                String value) {
            if (value == null) {
                value = "";
            }

            super.addAttribute(uri, localName, prefix(localName, uri), "CDATA",
                    StringEscapeUtils.escapeXml(value));

            return this;
        }

        public int compareTo(SimpleAttributes attributes) {
            return this.name.compareTo(attributes.name);
        }
    }
}
