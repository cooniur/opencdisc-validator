/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import it.unimi.dsi.fastutil.ints.Int2LongAVLTreeMap;
import it.unimi.dsi.fastutil.ints.Int2LongMap;
import it.unimi.dsi.fastutil.ints.Int2LongSortedMap;

import java.util.Iterator;

/**
 *
 * @author Tim Stone
 */
public class BitSet {
    private int lower = Integer.MAX_VALUE;
    private final Int2LongSortedMap offsets = new Int2LongAVLTreeMap();
    private int upper = -1;

    /**
     *
     * @param i
     */
    public void add(int i) {
        int position = i % Long.SIZE;
        int offset = i / Long.SIZE;

        if (i < this.lower) {
            this.lower = i;
        }

        if ( i > this.upper) {
            this.upper = i;
        }

        this.offsets.put(offset, this.offsets.get(offset) | 1L << position);
    }

    public void clear() {
        this.offsets.clear();
        this.determineBounds();
    }

    public boolean isEmpty() {
        return this.offsets.isEmpty();
    }

    /**
     *
     * @param set
     */
    public void intersect(BitSet set) {
        if (set.upper < this.lower || set.lower > this.upper) {
            this.offsets.clear();

            return;
        }

        int lower = set.lower < this.lower ? this.lower : set.lower;
        int upper = set.upper > this.upper ? this.upper : set.upper;

        lower = lower / Long.SIZE;
        upper = upper / Long.SIZE;

        this.offsets.headMap(lower).clear();
        this.offsets.tailMap(upper + 1).clear();

        Iterator<Int2LongMap.Entry> offsets = this.offsets.int2LongEntrySet().iterator();

        while (offsets.hasNext()) {
            Int2LongMap.Entry offset = offsets.next();
            int key = offset.getIntKey();
            long remote = set.offsets.get(key);

            if (remote == 0) {
                offsets.remove();
            } else {
                long store = offset.getLongValue() & remote;

                if (store != 0) {
                    offset.setValue(store);
                } else {
                    offsets.remove();
                }
            }
        }

        this.determineBounds();
    }

    public void union(BitSet set) {
        this.dirtyUnion(set);
        this.determineBounds();
    }

    public static BitSet copy(BitSet set) {
        BitSet copy = new BitSet();

        for (Int2LongMap.Entry offset : set.offsets.int2LongEntrySet()) {
            copy.offsets.put(offset.getIntKey(), offset.getLongValue());
        }

        copy.lower = set.lower;
        copy.upper = set.upper;

        return copy;
    }

    public static Unioner union(BitSet set, BitSet union) {
        return new Unioner(set).union(union);
    }

    public static class Unioner {
        private final BitSet unioned;

        Unioner(BitSet set) {
            this.unioned = set;
        }

        public BitSet get() {
            this.unioned.determineBounds();

            return this.unioned;
        }

        public Unioner union(BitSet set) {
            this.unioned.dirtyUnion(set);

            return this;
        }
    }

    private void determineBounds() {
        if (this.offsets.isEmpty()) {
            this.lower = Integer.MAX_VALUE;
            this.upper = -1;
        } else {
            int offset;

            offset = this.offsets.firstIntKey();
            this.lower = (offset * Long.SIZE) +
                Long.numberOfTrailingZeros(this.offsets.get(offset));

            offset = this.offsets.lastIntKey();
            this.upper = (offset * Long.SIZE) +
                (Long.SIZE - (Long.numberOfLeadingZeros(this.offsets.get(offset)) + 1));
        }
    }

    private void dirtyUnion(BitSet set) {
        if (set.isEmpty()) {
            return;
        }

        for (Int2LongMap.Entry entry : set.offsets.int2LongEntrySet()) {
            int offset = entry.getIntKey();

            this.offsets.put(offset, entry.getLongValue() | this.offsets.get(offset));
        }
    }
}
