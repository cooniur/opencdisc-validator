/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Tim Stone
 */
public class Codelist implements Iterable<Codelist.Code> {
    private final Map<String, Code> codes = new LinkedHashMap<String, Code>();
    private final String dictionary;
    private final String name;
    private final String oid;
    private final String type;
    private final String version;
    private final boolean isExtensible;

    public Codelist(String name, String type) {
        this(name, type, name.replaceAll("[^A-Za-z\\-_0-9]+", ""), null, null);
    }

    public Codelist(String name, String oid, String type) {
        this(name, type, oid, null, null);
    }

    public Codelist(String name, String oid, String type, boolean isExtensible) {
        this(name, type, oid, isExtensible, null, null);
    }

    public Codelist(String name, String type, String oid, String dictionary,
            String version) {
        this(name, type, oid, false, dictionary, version);
    }

    private Codelist(String name, String type, String oid, boolean isExtensible, String dictionary,
            String version) {
        this.name = name;
        this.type = type;
        this.oid = oid;
        this.dictionary = dictionary;
        this.version = version;
        this.isExtensible = isExtensible;
    }

    public boolean hasCodes() {
        return !this.codes.isEmpty();
    }

    public boolean isExtensible() {
        return this.isExtensible;
    }

    public boolean isExternal() {
        return this.dictionary != null;
    }

    public String getDataType() {
        return this.type;
    }

    public String getDictionary() {
        return this.dictionary;
    }

    public String getName() {
        return this.name;
    }

    public String getOID() {
        return this.oid;
    }

    public String getVersion() {
        return this.version;
    }

    public boolean hasCode(String codedValue) {
        return this.codes.containsKey(codedValue);
    }

    public Code getCode(String codedValue) {
        if (this.isExternal()) {
            throw new UnsupportedOperationException("Can't get codes from an external "
                    + "codelist");
        }

        return this.codes.get(codedValue);
    }

    public void addCode(Code code) {
        if (this.isExternal()) {
            throw new UnsupportedOperationException("Can't add to an external codelist");
        }

        this.codes.put(code.getValue(), code);
    }

    public Iterator<Code> iterator() {
        return this.codes.values().iterator();
    }

    public static class Code {
        private final String value;
        private Map<String, String> decodes = new KeyMap<String>();

        public Code(String codedValue) {
            this.value = codedValue;
        }

        public Set<String> getDecodeLangs() {
            return this.decodes.keySet();
        }

        public String getDecode(String lang) {
            return this.decodes.get(lang);
        }

        public boolean hasDecode() {
            return this.decodes.size() > 0;
        }

        public String getDecode() {
            // Makes the assumption that there's only one decode, which is typical
            if (this.decodes.size() != 1) {
                throw new IllegalArgumentException(String.format("Number of decodes is %d, not 1",
                    this.decodes.size()));
            }

            return this.decodes.values().iterator().next();
        }

        public String getValue() {
            return this.value;
        }

        public void setDecode(String decode, String lang) {
            this.decodes.put(lang, decode);
        }
    }
}
