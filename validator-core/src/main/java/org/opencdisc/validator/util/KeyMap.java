/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Tim Stone
 */
public class KeyMap<V> implements Map<String, V> {
    private static final long serialVersionUID = -3647507535078292801L;

    private final Map<String, V> table = new HashMap<String, V>();

    public int size() {
        return this.table.size();
    }

    public boolean isEmpty() {
        return this.table.isEmpty();
    }

    public boolean containsKey(Object key) {
        return this.table.containsKey(formatKey(key));
    }

    public boolean containsValue(Object value) {
        return this.table.containsValue(value);
    }

    public V get(Object key) {
        return this.table.get(formatKey(key));
    }

    public V put(String key, V value) {
        return this.table.put(formatKey(key), value);
    }

    public V remove(Object key) {
        return this.table.remove(formatKey(key));
    }

    public void putAll(Map<? extends String, ? extends V> t) {
        for (String key : t.keySet()) {
            this.put(key, t.get(key));
        }
    }

    public void clear() {
        this.table.clear();
    }

    public Set<String> keySet() {
        return this.table.keySet();
    }

    public Collection<V> values() {
        return this.table.values();
    }

    public Set<java.util.Map.Entry<String, V>> entrySet() {
        return this.table.entrySet();
    }

    public boolean equals(Object o) {
        return this.table.equals(o);
    }

    public int hashCode() {
        return this.table.hashCode();
    }

    private static String formatKey(Object key) {
        if (key == null) {
            return null;
        }

        return key.toString().toUpperCase();
    }
}
