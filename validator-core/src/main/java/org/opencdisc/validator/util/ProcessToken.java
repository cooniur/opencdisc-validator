/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author Tim Stone
 */
public class ProcessToken {
    public static final ProcessToken DEFAULT_TOKEN = new ProcessToken();
    private static final Set<String> USED_TOKENS = new HashSet<String>();

    private final String tokenID;

    public ProcessToken(String tokenID) {
        synchronized(DEFAULT_TOKEN) {
            if (tokenID == null || tokenID.length() == 0) {
                throw new IllegalArgumentException("Token ID must not be null and must "
                        + "have length > 0");
            }

            if (USED_TOKENS.contains(tokenID)) {
                throw new IllegalArgumentException(
                        String.format("Attempting to use already-in-use token ID %s",
                                tokenID)
                );
            }

            USED_TOKENS.add(this.tokenID = tokenID);
        }
    }

    protected ProcessToken() {
        this.tokenID = "";
    }

    public static void free(ProcessToken token) {
        synchronized(DEFAULT_TOKEN) {
            USED_TOKENS.remove(token.getID());
        }
    }

    public static ProcessToken getRandomToken() {
        while (true) {
            String tokenID = UUID.randomUUID().toString();

            synchronized (DEFAULT_TOKEN) {
                if (!USED_TOKENS.contains(tokenID)) {
                    return new ProcessToken(tokenID);
                }
            }
        }
    }

    public boolean equals(Object o) {
        boolean result = true;

        if (o == null || !(o instanceof ProcessToken)) {
            result = false;
        } else if (o == this) {
            result = true;
        } else if (!((ProcessToken)o).tokenID.equals(this.tokenID)) {
            result = false;
        }

        return result;
    }

    public int hashCode() {
        return this.tokenID.hashCode();
    }

    public String getID() {
        return this.tokenID;
    }
}
