/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.opencdisc.validator.data.DataEntry;

public class DataGrouping {
    private final DataEntry[] group;
    private final int hashCode;

    public DataGrouping(DataEntry[] group) {
        this.group = group;

        HashCodeBuilder builder = new HashCodeBuilder(27, 131);

        for (DataEntry entry : group) {
            builder.append(entry);
        }

        this.hashCode = builder.toHashCode();
    }

    protected DataGrouping(DataGrouping grouping) {
        this(grouping.group);
    }

    @Override
    public int hashCode() {
        return this.hashCode;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof DataGrouping)) {
            return false;
        }

        if (o == this) {
            return true;
        }

        return new EqualsBuilder().append(
            this.group, ((DataGrouping)o).group
        ).isEquals();
    }

    public boolean accepts(DataEntry entry) {
        return false;
    }

    public DataEntry get(int index) {
        return this.group[index];
    }
}
