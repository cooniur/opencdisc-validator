/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * @author Tim Stone
 */
public class ErrorAccumulator {
    private final Deque<Context> context = new LinkedList<Context>();
    private final List<ErrorMessage> errors = new LinkedList<ErrorMessage>();

    public ErrorAccumulator(String type, String name) {
        this.startContext(type, name);
    }

    public void record(String message) {
        this.errors.add(new ErrorMessage(this.context.toArray(new Context[this.context.size()]), message));
    }

    public void startContext(String type, String name) {
        this.context.push(new Context(type, name));
    }

    public void endContext() {
        this.context.pop();
    }

    public boolean hasErrors() {
        return this.errors.isEmpty();
    }

    public String toString() {
        return StringUtils.join(this.errors, "\n");
    }

    private static class Context {
        private final String type;
        private final String name;

        Context(String type, String name) {
            this.type = type;
            this.name = name;
        }

        public String toString() {
            return String.format("%s[%s]", this.type, this.name);
        }
    }

    private static class ErrorMessage {
        private final Context[] context;
        private final String message;

        ErrorMessage(Context[] context, String message) {
            this.context = context;
            this.message = message;
        }

        public String toString() {
            return String.format("%s: %s", StringUtils.join(this.context, " -> "), this.message);
        }
    }
}
