/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author Tim Stone
 */
public class Helpers {
    public static final int ROUNDING_CUTOFF;
    private static final char[] HEX_TABLE = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    static {
        int digits = 8;

        if (Settings.defines("Engine.RoundingDigits")) {
            try {
                digits = Integer.parseInt(Settings.get("Engine.RoundingDigits"));
            } catch (NumberFormatException ignore) {}
        }

        ROUNDING_CUTOFF = digits;
    }

    /**
     *
     * @param bytes
     * @return
     */
    public static String toHexString(byte[] bytes) {
        int size = bytes.length * 2;
        char[] render = new char[size];
        byte b;

        for (int i = 0; i < size; i = i + 2) {
            b = bytes[i / 2];
            render[i] = HEX_TABLE[(b >> 4) & 0x0F];
            render[i + 1] = HEX_TABLE[b & 0x0F];
        }

        return new String(render);
    }

    /**
     * Rounds a double value to a given number of significant digits past the decimal
     * place. This is done to generate the "right" numbers, due to conversion issues
     * that occur from different number storage formats and the frailness of floating
     * point arithmetic.
     * <p>
     * <em>Adapted from http://stackoverflow.com/a/1581007</em>
     *
     * @param input  the value to round off
     * @return the corrected number
     */
    public static double round(double input) {
        double truncated = (long)input;
        double remainder = input - truncated;

        if (remainder == 0) {
            return input;
        }

        double magnitude = Math.pow(10,
            ROUNDING_CUTOFF - (int)Math.ceil(
                Math.log10(remainder < 0 ? -remainder : remainder)
            )
        );

        return truncated + (Math.round(remainder * magnitude) / magnitude);
    }

    /**
     *
     * @param string
     * @param delimiter
     * @return
     */
    public static String[] trimSplit(String string, String delimiter) {
        String[] split = string.split(Pattern.quote(delimiter));

        for (int i = 0; i < split.length; ++i) {
            split[i] = split[i].trim();
        }

        return split;
    }

    /**
     *
     * @param definition
     * @param <I>
     * @return
     */
    public static <I> List<Class<I>> find(Class<I> definition) {
        return find(definition, true);
    }

    /**
     *
     * @param input
     * @return
     */
    public static int[] determineLargestIncreasingSubsequence(int[] input) {
        return determineLargestIncreasingSubsequence(input, false);
    }

    /**
     *
     * @param input
     * @param returnIndexes
     * @return
     */
    public static int[] determineLargestIncreasingSubsequence(int[] input, boolean returnIndexes) {
        int length = 0, count = input.length + 1;
        int[] current  = new int[count];
        int[] previous = new int[count];

        System.arraycopy(input, 0, current, 1, input.length);

        input = current;
        current = new int[count];

        for (int i = 1, j = 0; i < count; ++i) {
            if (length == 0 || input[current[1]] >= input[i]) {
                j = 0;
            } else {
                int low = 1, high = length + 1, mid;

                while (low < high - 1) {
                    mid = (low + high) / 2;

                    if (input[current[mid]] < input[i]) {
                        low = mid;
                    } else {
                        high = mid;
                    }
                }

                j = low;
            }

            previous[i] = current[j];

            if (j == length || input[i] < input[current[j + 1]]) {
                current[j + 1] = i;
                length = Math.max(length, j + 1);
            }
        }

        int[] subsequence = new int[length];
        int position = current[length];

        while (length > 0) {
            subsequence[length - 1] = returnIndexes? position - 1 : input[position];
            position = previous[position];

            --length;
        }

        return subsequence;
    }

    /**
     * <em>Adapted from http://stackoverflow.com/a/251670</em>
     *
     * @param definition
     * @param useCache
     * @param <I>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <I> List<Class<I>> find(Class<I> definition, boolean useCache) {
        List<Class<I>> implementations = new ArrayList<Class<I>>();

        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Enumeration<URL> resources = loader.getResources(
                "META-INF/services/" + definition.getName()
            );

            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();
                InputStream stream = url.openStream();

                try {
                    BufferedReader reader = new BufferedReader(
                        new InputStreamReader(stream, "UTF-8")
                    );

                    String line;

                    while ((line = reader.readLine()) != null) {
                        int comment = line.indexOf('#');

                        if (comment == 0) {
                            continue;
                        }

                        if (comment > 0) {
                            line = line.substring(0, comment);
                        }

                        line = line.trim();

                        if (line.length() == 0) {
                            continue;
                        }

                        try {
                            Class<?> implementation = Class.forName(line, true, loader);

                            if (definition.isAssignableFrom(implementation)) {
                                implementations.add((Class<I>)implementation);
                            }
                        } catch (ClassNotFoundException ignore) {}
                    }
                } finally {
                    stream.close();
                }
            }
        } catch (IOException ignore) {}

        return implementations;
    }
}
