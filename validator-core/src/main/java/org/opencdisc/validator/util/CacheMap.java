/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 *
 * @author Tim Stone
 */
public class CacheMap<T> implements Map<String, T> {
    private Map<String, CacheMap<T>.SoftKeyedReference> cacheMap =
        new KeyMap<CacheMap<T>.SoftKeyedReference>();
    private ReferenceQueue<T> collectedReferenceQueue = new ReferenceQueue<T>();
    private Queue<T> lastHardReferences;

    public CacheMap() {
        this(1);
    }

    public CacheMap(int retainerSize) {
        this.lastHardReferences = new BoundedQueue<T>(retainerSize);
    }

    public T put(String key, T value) {
        SoftKeyedReference keyedReference = new SoftKeyedReference(key, value);

        this.lastHardReferences.offer(value);
        this.cacheMap.put(key, keyedReference);
        this.clean();

        return value;
    }

    @SuppressWarnings("unchecked")
    private void clean() {
        Reference<? extends T> reference = null;

        while ((reference = this.collectedReferenceQueue.poll()) != null) {
            if (reference instanceof CacheMap.SoftKeyedReference) {
                SoftKeyedReference keyedReference = (SoftKeyedReference)reference;

                this.cacheMap.remove(keyedReference.getKey());
            }
        }
    }

    public T get(Object key) {
        T value = null;

        if (this.cacheMap.containsKey(key)) {
            SoftKeyedReference keyedReference = this.cacheMap.get(key);
            value = keyedReference.get();
        }

        this.clean();

        return value;
    }

    public void putAll(Map<? extends String, ? extends T> valueMap) {
        for (Map.Entry<? extends String, ? extends T> entry : valueMap.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }

    private class SoftKeyedReference extends SoftReference<T> {
        private final String key;

        public SoftKeyedReference(String key, T value) {
            super(value, collectedReferenceQueue);

            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }

    public void clear() {
        this.lastHardReferences.clear();
        this.cacheMap.clear();
    }

    public T remove(Object key) {
        T value = null;

        if (this.containsKey(key)) {
            SoftKeyedReference keyedReference = this.cacheMap.remove(key);
            value = keyedReference.get();

            this.lastHardReferences.remove(key);
        }

        return value;
    }

    public boolean containsKey(Object key) {
        return this.cacheMap.containsKey(key);
    }

    public boolean isEmpty() {
        return this.cacheMap.isEmpty();
    }

    public Set<String> keySet() {
        return this.cacheMap.keySet();
    }

    public int size() {
        return this.cacheMap.size();
    }

    public boolean containsValue(Object value) throws UnsupportedOperationException {
        // This kind of breaks the Map interface contract, but we can't allow it
        throw new UnsupportedOperationException();
    }

    public Set<java.util.Map.Entry<String, T>> entrySet()
            throws UnsupportedOperationException {
        // This kind of breaks the Map interface contract, but we can't allow it
        throw new UnsupportedOperationException();
    }

    public Collection<T> values() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
