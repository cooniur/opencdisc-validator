/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.engine;

import java.io.File;

import javax.xml.transform.*;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.opencdisc.validator.EventDispatcher;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.SimpleMetrics;
import org.opencdisc.validator.report.Reporter;
import org.opencdisc.validator.report.SchematronResultTransformer;
import org.opencdisc.validator.settings.Configuration;

/**
 *
 * @author Tim Stone
 */
public class HierarchicalValidator extends AbstractEngine implements ValidationEngine {
    private final EventDispatcher dispatcher;
    private final SimpleMetrics metrics;
    private final Reporter reporter;

    /**
     *
     * @param reporter
     */
    public HierarchicalValidator(EventDispatcher dispatcher, SimpleMetrics metrics,
            Reporter reporter) {
        this.dispatcher = dispatcher;
        this.metrics = metrics;
        this.reporter = reporter;
    }

    public void validate() {
        for (EngineEntity entity : super.entities) {
            DataSource source = entity.getSource();
            Configuration configuration = entity.getConfiguration();

            if (configuration == null || !configuration.hasProperty("Schematron")) {
                continue;
            }

            File input = new File(source.getLocation());
            File schematron = new File(configuration.getProperty("Schematron"));

            this.dispatcher.fireTaskStart(input.getName(), -1, -1);

            if (!input.isFile() || !schematron.isFile()) {
                // TODO: Improve validation error handling
                throw new RuntimeException("input xml or schematron file does not exist "
                        + "or isn't accessible");
            }

            EntityDetails details = entity.getDetails();

            this.reporter.declare(details);
            this.metrics.addEntity(details);

            try {
                Source inputSource = new StreamSource(input);
                Source schematronSource = new StreamSource(schematron);
                TransformerFactory factory = TransformerFactory.newInstance();

                factory.setErrorListener(new ErrorListener() {
                    public void warning(TransformerException exception) throws TransformerException {
                        // Swallow the warnings
                    }

                    public void error(TransformerException exception) throws TransformerException {
                        throw exception;
                    }

                    public void fatalError(TransformerException exception) throws TransformerException {
                        throw exception;
                    }
                });

                Transformer validator = factory.newTransformer(schematronSource);
                SchematronResultTransformer proxy = new SchematronResultTransformer(
                        details,
                        this.reporter,
                        this.metrics
                );
                Result result = new SAXResult(proxy);

                validator.setErrorListener(proxy);
                validator.transform(inputSource, result);
            } catch (TransformerConfigurationException ex) {
                // TODO: Improve validation error handling
                throw new RuntimeException(ex);
            } catch (TransformerException ex) {
                // Do nothing
            }
        }
    }
}
