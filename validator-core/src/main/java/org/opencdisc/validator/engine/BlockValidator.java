/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.opencdisc.validator.EventDispatcher;
import org.opencdisc.validator.Text;
import org.opencdisc.validator.data.*;
import org.opencdisc.validator.data.InvalidDataException.Codes;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.SimpleMetrics;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.report.Reporter;
import org.opencdisc.validator.rules.CorruptRuleException;
import org.opencdisc.validator.rules.CorruptRuleException.State;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.rules.ValidationRule.Target;
import org.opencdisc.validator.settings.Configuration;
import org.opencdisc.validator.util.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tim Stone
 */
public class BlockValidator extends AbstractEngine implements ValidationEngine {
    private final static Logger LOG = LoggerFactory.getLogger(BlockValidator.class);

    private final EventDispatcher dispatcher;
    private final ExecutorService executor;
    private final Configuration global;
    private final SimpleMetrics metrics;
    private final Reporter reporter;

    private DataSupplement supplement;

    /**
     *
     * @param dispatcher
     * @param metrics
     * @param reporter
     * @param global
     */
    public BlockValidator(EventDispatcher dispatcher, SimpleMetrics metrics,
            Reporter reporter, Configuration global) {
        this.dispatcher = dispatcher;
        // TODO: This is kind of hacky, specifying the global configuration like this..
        this.global = global;
        this.metrics = metrics;
        this.reporter = reporter;

        int threads = 0;
        int processors = Runtime.getRuntime().availableProcessors();

        if (Settings.defines("Engine.ThreadCount")) {
            String countSetting = Settings.get("Engine.ThreadCount");

            if (countSetting.equalsIgnoreCase("auto")) {
                threads = processors;
            } else {
                threads = Integer.parseInt(countSetting);
            }
        }

        threads = Math.max(Math.min(threads, processors), 1);
        this.executor = threads > 1 ? Executors.newFixedThreadPool(threads) : null;

        LOG.debug("Block engine will be invoked with {} threads, max memory: {}", threads,
            Runtime.getRuntime().maxMemory());
    }

    public void cancel() {
        super.cancel();

        if (this.executor != null) {
            this.executor.shutdownNow();
        }
    }

    public void prepare(EngineEntity entity) {
        EntityDetails details = entity.getDetails();

        if (entity.getConfiguration() == null) {
            this.metrics.updateMessageCount(
                this.reporter.write(new Message(
                    "MISSING_CONFIG",
                    "System",
                    Text.get("Messages.MissingConfig"),
                    String.format(
                        Text.get("Descriptions.MissingConfig"),
                        details.getString(Property.Name)
                    ),
                    Message.Type.Warning,
                    details
                )) ? 1 : 0
            );
        } else {
            this.reporter.declare(details);
            this.metrics.addEntity(details);
            super.prepare(entity);

            // TODO: This is a very deliberate hack to get the basic functionality in place
            //    This should absolutely be externalized somehow to the configuration
            if (details.getString(Property.Name).equals("DM")) {
                try {
                    this.supplement = new DataSupplement(entity.getSource());
                } catch (InvalidDataException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
    }

    public void validate() {
        List<Callable<Void>> tasks = new LinkedList<Callable<Void>>();
        CombinedDataSource global = new CombinedDataSource("GLOBAL", "System");

        // Set the total validation item count
        this.metrics.setTotalItems(this.entities.size());
        int itemCount = 0;

        for (EngineEntity entity : this.entities) {
            ++itemCount;

            // Append a task for the data
            tasks.add(new BlockTask(entity, itemCount));

            // Prepend a task for the metadata
            tasks.add(0, new BlockTask(
                new EngineEntity(
                    entity.getSource().getMetadata(),
                    entity.getConfiguration()
                )
            ));

            // Add the metadata to the global data source
            try {
                global.add(entity.getSource().getMetadata().replicate());
            } catch (InvalidDataException ex) {
                // Won't happen, because metadata sources don't throw this exception
            }
        }

        // There isn't anything to process
        if (tasks.isEmpty()) {
            return;
        }

        tasks.add(0, new BlockTask(new EngineEntity(global, this.global)));

        if (this.executor != null) {
            for (Callable<Void> task : tasks) {
                ///////////////////////
                //    Abort Point    //
                ///////////////////////
                if (this.abort) {
                    return;
                }

                // Submit the task to the ExecutorService for processing
                this.executor.submit(task);
            }

            try {
                this.executor.shutdown();

                // Wait ten days... (we don't want to wait for a set period, but we need
                // to wait until this is done, which invokeAll() is supposed to do, but
                // doesn't (at least on JRE 1.5)
                this.executor.awaitTermination(60 * 60 * 24 * 10, TimeUnit.SECONDS);
            } catch (InterruptedException ex) {
                // If we were interrupted, we're done anyway
            }
        } else {
            // We're not bothering with an ExecutorService (only one thread)
            for (Callable<Void> task : tasks) {
                ///////////////////////
                //    Abort Point    //
                ///////////////////////
                if (this.abort) {
                    return;
                }

                try {
                    task.call();
                } catch (CancellationException ex) {
                    // This is OK
                    return;
                } catch (Exception ex) {
                    // TODO: Determine what circumstances this might happen in
                    throw new RuntimeException(ex);
                }
            }
        }

        LOG.info("All validations completed normally");
    }

    /**
     *
     * @author Tim Stone
     */
    private class BlockTask implements Callable<Void> {
        private final int id;
        private final EngineEntity entity;

        BlockTask(EngineEntity entity) {
            this(entity, 0);
        }

        BlockTask(EngineEntity entity, int taskNumber) {
            this.id = taskNumber;
            this.entity = entity;
        }

        public Void call() {
            List<Message> messages = new ArrayList<Message>();
            InternalEntityDetails details = this.entity.getDetails();
            DataSource source = this.entity.getSource();
            String sourceName = details.getString(Property.Name);
            int checks = 0;
            int totalRecords = 0;
            boolean isGlobal = sourceName.equalsIgnoreCase("GLOBAL");
            boolean isTracking = Settings.defines("Engine.CaptureFilters");
            boolean isFiltered = false;

            BlockValidator.this.dispatcher.fireTaskStart(
                isGlobal ? "metadata" : sourceName,
                this.id,
                BlockValidator.this.metrics.getTotalItems()
            );

            String display = sourceName;

            if (details.getReference() == EntityDetails.Reference.Metadata) {
                display += " (metadata)";
            }

            LOG.info("Beginning validation of {}", display);

            try {
                Set<ValidationRule> filters = this.entity.getFilters();
                Set<ValidationRule> rules = this.entity.getRules();
                List<String> examined = new LinkedList<String>();

                // Perform setup activities on all filters/rules
                if (filters != null) {
                    for (ValidationRule filter : filters) {
                        filter.setup(this.entity.getDetails());
                    }
                }

                for (ValidationRule rule : rules) {
                    rule.setup(this.entity.getDetails());
                }

                // Process all of the records in the dataset
                while (source.hasRecords()) {
                    ///////////////////////
                    //    Abort Point    //
                    ///////////////////////
                    this.isAlive();

                    int records = 0;

                    // Get the next batch of records
                    for (DataRecord record : source.getRecords()) {
                        ///////////////////////
                        //    Abort Point    //
                        ///////////////////////
                        this.isAlive();

                        if (supplement != null &&
                                !details.getString(Property.Name).equals("DM") &&
                                details.getReference() != EntityDetails.Reference.Metadata) {
                            record = supplement.augment(record);
                        }

                        if (filters != null && filters.size() > 0) {
                            Iterator<ValidationRule> filterIterator = filters.iterator();
                            boolean filtered = false;

                            while (!filtered && filterIterator.hasNext()) {
                                ///////////////////////
                                //    Abort Point    //
                                ///////////////////////
                                this.isAlive();

                                ValidationRule filter = filterIterator.next();

                                try {
                                    filtered = !filter.validate(record);
                                } catch (CorruptRuleException ex) {
                                    if (ex.getState() == State.Unrecoverable) {
                                        filterIterator.remove();
                                    }
                                } finally {
                                    record.clear();
                                }
                            }

                            if (filtered) {
                                isFiltered = true;

                                continue;
                            }

                            if (isTracking) {
                                examined.add(record.getDataDetails().getKey());
                            }
                        }

                        ///////////////////////
                        //    Abort Point    //
                        ///////////////////////
                        this.isAlive();

                        Iterator<ValidationRule> ruleIterator = rules.iterator();

                        while (ruleIterator.hasNext()) {
                            ///////////////////////
                            //    Abort Point    //
                            ///////////////////////
                            this.isAlive();

                            ValidationRule rule = ruleIterator.next();

                            try {
                                // Perform the validation against this record
                                if (!rule.validate(record)) {
                                    messages.addAll(rule.getMessages());
                                }

                                // Update metrics information
                                if (rule.getTarget().equals(Target.Record)) {
                                    ++checks;
                                }
                            } catch (CorruptRuleException ex) {
                                State corruptionState = ex.getState();

                                LOG.debug("Validation rule {} in {} threw {}",
                                        new Object[] {ex.getID(), sourceName, ex});

                                if (corruptionState == State.Unrecoverable) {
                                    LOG.debug("Exception caused validation rule {} in " +
                                            "{} to be removed", ex.getID(), sourceName);

                                    // Remove the rule, since we can't use it anymore
                                    ruleIterator.remove();
                                } else {
                                    // TODO: Check for smarter ways to deal with this
                                    //   Not everything is only temporary. Some things
                                    //   are actually unrecoverable, but we just don't
                                    //   know enough at the time to realize that.
                                    messages.add(new Message(
                                        String.format("SKIP_%s", ex.getID()),
                                        "System",
                                        ex.getMessage(),
                                        String.format(
                                            Text.get("Descriptions.RuleSkipped"),
                                            ex.getID(),
                                            ex.getDescription()
                                        ),
                                        Message.Type.Information,
                                        this.entity.getDetails()
                                    ));
                                }
                            } finally {
                                record.clear();
                            }
                        }

                        ++records;
                        ++totalRecords;
                    }

                    BlockValidator.this.dispatcher.fireTaskIncrement(
                        sourceName,
                        this.id,
                        BlockValidator.this.metrics.getTotalItems(),
                        totalRecords
                    );

                    if (!source.isMetadata() && !isGlobal) {
                        BlockValidator.this.metrics.updateSubitemCount(records);
                    }

                    // Write out the messages from this group of records
                    synchronized (BlockValidator.this.reporter) {
                        BlockValidator.this.metrics.updateMessageCount(
                            BlockValidator.this.reporter.write(messages)
                        );
                    }

                    messages.clear();
                }

                ///////////////////////
                //    Abort Point    //
                ///////////////////////
                this.isAlive();

                // Finally, check all of the dataset validation rules
                for (ValidationRule rule : this.entity.getRules()) {
                    ///////////////////////
                    //    Abort Point    //
                    ///////////////////////
                    this.isAlive();

                    // Perform the validation
                    if (!rule.validateDataset(this.entity.getDetails())) {
                        messages.addAll(rule.getMessages());
                    }

                    // Update metrics information
                    if (rule.getTarget().equals(Target.Dataset)) {
                        ++checks;
                    }
                }

                details.setProperty(Property.Validated, 1);
                details.setProperty(Property.Filtered, isFiltered);

                if (isFiltered && isTracking) {
                    details.setExamined(examined);
                }
            } catch (InvalidDataException ex) {
                Codes errorCode = ex.getCode();

                if (errorCode == Codes.CannotParseSource) {
                    messages.add(new Message(
                        "PARSE_ERROR",
                        "System",
                        Text.get("Messages.CorruptSource"),
                        Text.get("Descriptions.CorruptSource"),
                        Message.Type.Error,
                        this.entity.getDetails()
                    ));
                } else {
                    // Shouldn't happen, so bail immediately
                    throw new RuntimeException(ex);
                }
            } catch (InterruptedException ex) {
                throw new CancellationException();
            } catch (Exception ex) {
                LOG.error("Unexpected exception while validating {}: {}", display, ex);

                throw new RuntimeException(ex);
            } finally {
                if (!source.isMetadata() && !isGlobal) {
                    BlockValidator.this.metrics.updateItemCount();
                }

                BlockValidator.this.dispatcher.fireTaskCompleted(
                    sourceName,
                    this.id,
                    BlockValidator.this.metrics.getTotalItems()
                );

                synchronized (BlockValidator.this.reporter) {
                    BlockValidator.this.metrics.updateMessageCount(
                        BlockValidator.this.reporter.write(messages)
                    );
                }

                BlockValidator.this.metrics.updateChecksPerformed(checks);

                LOG.info("Completed validation of {}, {} rows checked", display,
                    totalRecords);
            }

            return null;
        }

        private void isAlive() throws InterruptedException {
            if (Thread.currentThread().isInterrupted() || BlockValidator.this.abort) {
                throw new InterruptedException();
            }
        }
    }
}
