/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.engine;

import static org.apache.commons.lang.time.DateFormatUtils.ISO_DATETIME_FORMAT;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Pattern;

import javax.xml.transform.TransformerConfigurationException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.data.InvalidDataException;
import org.opencdisc.validator.model.DataDetails;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.settings.Configuration;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.Codelist;
import org.opencdisc.validator.util.Codelist.Code;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.XmlWriter;
import org.opencdisc.validator.util.XmlWriter.SimpleAttributes;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *
 * @author Tim Stone
 */
public class DefineGenerator extends AbstractEngine implements GenerationEngine {
    private static final String ODM_NAMESPACE =
        "http://www.cdisc.org/ns/odm/v1.2";
    private static final String DEFINE_NAMESPACE_V1 =
        "http://www.cdisc.org/ns/def/v1.0";
    private static final String SCHEMA_LOCATION =
        "define1-0-0.xsd";
    private static final String DEFINE_PREFIX = "def";
    private static final String ODM_VERSION = "1.2";
    private static final String DEFINE_VERSION = "1.0.0";
    private static final String DEFINE_STYLE =
        "type=\"text/xsl\" href=\"define1_0_0.xsl\"";
    private static final String DEFAULT_LANG = "en";

    private final Map<String, Codelist> codelists = new KeyMap<Codelist>();
    private final Set<SimpleAttributes> definitions = new TreeSet<SimpleAttributes>();
    private final XmlWriter writer;
    private final DefineMetadata metadata;

    /**
     *
     * @param target
     * @param metadata
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws SAXException
     */
    public DefineGenerator(File target, DefineMetadata metadata) throws IOException,
            TransformerConfigurationException, SAXException {
        super(metadata.comparator);

        this.metadata = metadata;
        this.writer = new XmlWriter(target, ODM_NAMESPACE,
                new XmlWriter.Instruction("xml-stylesheet", DEFINE_STYLE));
        this.writer.addMapping(DEFINE_NAMESPACE_V1, DEFINE_PREFIX);
    }

    public void cancel() {
        // TODO: Add ability to cancel generation
    }

    public void close() {
        try {
            this.writer.close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } catch (SAXException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void generate() {
        try {
            this.parseExternalMetadata();
            this.printComments();

            SimpleAttributes attributes;

            attributes = this.writer.newAttributes()
                .addAttribute(XmlWriter.SCHEMA_NAMESPACE_URI, "schemaLocation",
                        String.format("%s %s", ODM_NAMESPACE, SCHEMA_LOCATION)
                )
                .addEmptyAttribute("FileOID")
                .addAttribute("ODMVersion", ODM_VERSION)
                .addAttribute("FileType", "Snapshot")
                .addAttribute("CreationDateTime",
                        ISO_DATETIME_FORMAT.format(System.currentTimeMillis()));

            this.writer.startElement("ODM", attributes);

            attributes = this.writer.newAttributes().addEmptyAttribute("OID");

            this.writer.startElement("Study", attributes);
            this.writer.startElement("GlobalVariables");
            this.writer.emptyElement("StudyName");
            this.writer.emptyElement("StudyDescription");
            this.writer.emptyElement("ProtocolName");
            this.writer.endElement("GlobalVariables");

            attributes = this.writer.newAttributes()
                .addEmptyAttribute("OID")
                .addEmptyAttribute("Name")
                .addEmptyAttribute("Description")
                .addAttribute(DEFINE_NAMESPACE_V1, "DefineVersion", DEFINE_VERSION)
                .addAttribute(DEFINE_NAMESPACE_V1, "StandardName",
                    this.metadata.standardName)
                .addAttribute(DEFINE_NAMESPACE_V1, "StandardVersion",
                    this.metadata.standardVersion);

            // Begin the main part of the document
            this.writer.startElement("MetaDataVersion", attributes);

            for (EngineEntity entity : super.entities) {
                try {
                    this.printItemGroupDef(entity);
                } catch (InvalidDataException ex) {
                    // TODO: Improve generation error handling
                }
            }

            // Print out the ItemDefs
            this.printItemDefs();

            // Print out the code lists
            this.printCodeLists();

            // Close the main tags
            this.writer.endElement("MetaDataVersion");
            this.writer.endElement("Study");
            this.writer.endElement("ODM");
        } catch (SAXException ex) {
            // TODO: Improve generation error handling
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            this.close();
        }
    }

    private void parseExternalMetadata() throws IOException {
        if (this.metadata.externalMetadata != null &&
                this.metadata.externalMetadata.canRead()) {
            InputStream inputStream = null;

            try {
                inputStream = new FileInputStream(this.metadata.externalMetadata);
                Workbook workbook = new HSSFWorkbook(inputStream);
                Sheet assignmentSheet = workbook.getSheet("CodeList Assignments");
                Sheet definitionSheet = workbook.getSheet("CodeList Definitions");
                Sheet studySheet = workbook.getSheet("Study Metadata");
                Sheet domainSheet = workbook.getSheet("Domain Metadata");
                DataFormat formats = workbook.createDataFormat();

                if (assignmentSheet != null && definitionSheet != null) {
                    Map<String, Set<String>> assignments = new KeyMap<Set<String>>();
                    int rowCount;

                    rowCount = assignmentSheet.getLastRowNum() + 1;

                    for (int i = 0; i < rowCount; ++i) {
                        Row row = assignmentSheet.getRow(i);

                        if (row == null) {
                            continue;
                        }

                        String domain = row.getCell(0,
                                Row.CREATE_NULL_AS_BLANK).toString();
                        String variable = row.getCell(1,
                                Row.CREATE_NULL_AS_BLANK).toString();
                        String codelist = row.getCell(2,
                                Row.CREATE_NULL_AS_BLANK).toString();

                        if (variable.length() == 0 || codelist.length() == 0) {
                            continue;
                        }

                        if (domain.length() > 0) {
                            variable = domain + "." + variable;
                        }

                        if (!assignments.containsKey(codelist)) {
                            assignments.put(codelist, new HashSet<String>());
                        }

                        assignments.get(codelist).add(variable.toUpperCase());
                    }

                    if (!assignments.isEmpty()) {
                        Codelist current = null;
                        Map<String, Codelist> codelists = new KeyMap<Codelist>();

                        rowCount = definitionSheet.getLastRowNum() + 1;

                        for (int i = 0; i < rowCount; ++i) {
                            Row row = definitionSheet.getRow(i);

                            if (row == null) {
                                continue;
                            }

                            String codelist = row.getCell(0,
                                    Row.CREATE_NULL_AS_BLANK).toString();
                            String type = row.getCell(1,
                                    Row.CREATE_NULL_AS_BLANK).toString();
                            Cell value = row.getCell(2, Row.CREATE_NULL_AS_BLANK);
                            Cell decode = row.getCell(3, Row.CREATE_NULL_AS_BLANK);
                            String lang = row.getCell(4,
                                    Row.CREATE_NULL_AS_BLANK).toString();

                            if (current == null || (codelist.length() > 0 &&
                                    !current.getOID().equalsIgnoreCase(codelist))) {
                                // Currently there's no protection against the user
                                // inserting invalid types. Right now we're just letting
                                // the define.xml validator catch them, which I think is
                                // good enough. If not, we might want to do additional
                                // checking here.
                                if (type.length() == 0) {
                                    type = "text";
                                }

                                if (!codelists.containsKey(codelist)) {
                                    codelists.put(codelist,
                                            new Codelist(codelist, type));
                                }

                                current = codelists.get(codelist);
                            }

                            if (current != null) {
                                Code code = null;
                                String translatedValue = convertCellFormat(formats,
                                        value);
                                String translatedDecode = convertCellFormat(formats,
                                        decode);

                                if (!current.hasCode(translatedValue)) {
                                    current.addCode(new Code(translatedValue));
                                }

                                code = current.getCode(translatedValue);

                                if (code != null) {
                                    code.setDecode(translatedDecode,
                                            lang.length() == 0 ? DEFAULT_LANG : lang);
                                }
                            }
                        }

                        // We have to do this after the fact since it's possible that
                        // someone broke their codelist into two or more chunks in the
                        // Excel file
                        for (String codelist : codelists.keySet()) {
                            if (!assignments.containsKey(codelist)) {
                                continue;
                            }

                            current = codelists.get(codelist);

                            for (String variable : assignments.get(codelist)) {
                                this.codelists.put(variable, current);
                            }
                        }
                    }
                } else if (assignmentSheet != null || definitionSheet != null) {
                    // TODO: Handle user errors in a better way
                    throw new IllegalArgumentException(
                            "You are missing either the CodeList Assignment or CodeList "
                            + "Definitions sheet in the Excel file you provided"
                    );
                }

                if (studySheet != null) {
                    // TODO: Parse study-level metadata
                }

                if (domainSheet != null) {
                    // TODO: Parse domain-level metadata
                }
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        }
    }

    /**
     *
     * @throws SAXException
     */
    private void printComments() throws SAXException {
        this.writer.comment(
                " ***************************************************************** ",
                " This define.xml document was produced by the OpenCDISC Validator  ",
                " Visit http://www.opencdisc.org/ for more details                  ",
                "                                                                   ",
                " Please note that not all information can be generated solely from ",
                " source datasets, so be sure to fill in supplemental data as it    ",
                " applies to your organization and study.                           ",
                " ***************************************************************** "
        );
    }

    /**
     *
     * @param entity
     */
    private void printItemGroupDef(EngineEntity entity) throws SAXException,
            InvalidDataException {
        EntityDetails details = entity.getDetails();
        String name = details.getString(Property.Name);
        String file = (new File(details.getString(Property.Location))).getName();
        String archive = String.format("Location.%s", name);

        SimpleAttributes attributes;
        Configuration configuration = entity.getConfiguration();

        attributes = this.writer.newAttributes()
            .addAttribute("OID", name)
            .addAttribute("Name", name)
            .addAttribute("Repeating", configuration != null ?
                    configuration.getProperty("Repeating") : "")
            .addAttribute("IsReferenceData", configuration != null ?
                    configuration.getProperty("IsReferenceData") : "")
            .addAttribute("Purpose", configuration != null ?
                    configuration.getProperty("Purpose") : "Tabulation")
            .addAttribute(DEFINE_NAMESPACE_V1, "Label",
                    details.hasProperty(Property.Label) ?
                            details.getString(Property.Label) : "")
            .addAttribute(DEFINE_NAMESPACE_V1, "Structure",
                    configuration != null ?
                            configuration.getProperty("Structure") : "")
            .addEmptyAttribute(DEFINE_NAMESPACE_V1, "DomainKeys")
            .addAttribute(DEFINE_NAMESPACE_V1, "Class",
                    details.hasProperty(Property.Class) ?
                            details.getString(Property.Class) : "")
            .addAttribute(DEFINE_NAMESPACE_V1, "ArchiveLocationID", archive);

        this.writer.startElement("ItemGroupDef", attributes);

        // Print out ItemRef and define ItemDef elements
        DataSource metadata = entity.getSource().getMetadata();

        while (metadata.hasRecords()) {
            for (DataRecord variable : metadata.getRecords()) {
                this.printItemRef(name, variable, configuration);
            }
        }

        // Take care of the leaf that links the define.xml to the source file
        attributes = this.writer.newAttributes()
            .addAttribute("ID", archive)
            .addAttribute(XmlWriter.XLINK_NAMESPACE_URI, "href", file);

        this.writer.startElement(DEFINE_NAMESPACE_V1, "leaf", attributes);
        this.writer.startElement(DEFINE_NAMESPACE_V1, "title");
        this.writer.characters(file);
        this.writer.endElement(DEFINE_NAMESPACE_V1, "title");
        this.writer.endElement(DEFINE_NAMESPACE_V1, "leaf");

        this.writer.endElement("ItemGroupDef");
    }

    /**
     *
     * @param variable
     */
    private void printItemRef(String domain, DataRecord variable,
            Configuration configuration) throws SAXException {
        DataDetails details = variable.getDataDetails();
        String name = details.getName();
        Definition variableTemplate = null;
        SimpleAttributes attributes;

        if (configuration != null) {
            variableTemplate = configuration.getVariable(name);
        }

        // Prepare the ItemDef
        String OID = variableTemplate != null ?
                variableTemplate.getProperty("OID") : String.format("%s.%s", domain,
                        name);
        String type = variableTemplate != null ?
                variableTemplate.getProperty("DataType") : "";
        String label = variableTemplate != null ?
                variableTemplate.getProperty("Label") :
                        variable.getValue("LABEL").toString();

        attributes = this.writer.newAttributes(OID)
            .addAttribute("OID", OID)
            .addAttribute("Name", name)
            .addAttribute("DataType", type)
            .addEmptyAttribute("Origin")
            .addEmptyAttribute("Comment")
            .addAttribute(DEFINE_NAMESPACE_V1, "Label", label);

        if (!(type.equalsIgnoreCase("Date") || type.equalsIgnoreCase("DateTime") ||
                type.equalsIgnoreCase("Time"))) {
            // TODO: Does it always make more sense to take this from the data?
            String length = variableTemplate != null ?
                    variableTemplate.getProperty("Length") :
                            variable.getValue("LENGTH").toString();

            attributes.addAttribute("Length", length);
        }

        if (variableTemplate != null &&
                variableTemplate.hasProperty("SignificantDigits")) {
            attributes.addAttribute("SignificantDigits",
                    variableTemplate.getProperty("SignificantDigits"));
        }

        this.definitions.add(attributes);

        // Prepare the ItemRef
        attributes = this.writer.newAttributes()
            .addAttribute("ItemOID", OID)
            .addAttribute("OrderNumber", Integer.toString(details.getID()))
            .addAttribute("Mandatory",  variableTemplate != null ?
                    variableTemplate.getProperty("Mandatory") : "")
            .addAttribute("Role",  variableTemplate != null ?
                    variableTemplate.getProperty("Role") : "");

        this.writer.startElement("ItemRef", attributes);
        this.writer.endElement("ItemRef");
    }

    /**
     *
     * @throws SAXException
     */
    private void printItemDefs() throws SAXException {
        Set<String> variables = new HashSet<String>();

        for (Attributes definition : this.definitions) {
            this.writer.startElement("ItemDef", definition);

            String itemOID = definition.getValue(ODM_NAMESPACE, "OID");
            boolean hasCodelist = this.codelists.containsKey(itemOID);


            if (!hasCodelist) {
                // This is pretty hacky, but since we rely on config OIDs sometimes
                // we need to make sure they match our expected structure here...
                String[] bits = itemOID.split(Pattern.quote("."));

                if (bits.length == 2) {
                    itemOID = String.format("%s.%s%s", bits[0], bits[0], bits[1]);
                    hasCodelist = this.codelists.containsKey(itemOID);
                }
            }

            if (hasCodelist) {
                Codelist codelist = this.codelists.get(itemOID);

                this.writer.simpleElement("CodeListRef",
                        this.writer.newAttributes()
                            .addAttribute("CodeListOID", codelist.getOID())
                );
            }

            variables.add(itemOID);

            this.writer.endElement("ItemDef");
        }

        // Remove the unused codelists
        this.codelists.keySet().retainAll(variables);
    }

    /**
     *
     * @throws SAXException
     */
    private void printCodeLists() throws SAXException {
        Map<String, Codelist> definitions = new HashMap<String, Codelist>();

        for (Codelist codelist : this.codelists.values()) {
            if (!definitions.containsKey(codelist.getOID())) {
                definitions.put(codelist.getOID(), codelist);
            }
        }

        for (Codelist codelist : definitions.values()) {
            if (codelist == null || !codelist.hasCodes()) {
                continue;
            }

            SimpleAttributes attributes;

            attributes = this.writer.newAttributes()
                .addAttribute("OID", codelist.getOID())
                .addAttribute("Name", codelist.getName())
                .addAttribute("DataType", codelist.getDataType());

            this.writer.startElement("CodeList", attributes);

            int rank = 1;

            for (Code code : codelist) {
                attributes = this.writer.newAttributes()
                    .addAttribute("CodedValue", code.getValue())
                    .addAttribute(DEFINE_NAMESPACE_V1, "Rank",
                            Integer.toString(rank));

                this.writer.startElement("CodeListItem", attributes);
                this.writer.startElement("Decode");

                for (String lang : code.getDecodeLangs()) {
                    attributes = this.writer.newAttributes()
                        .addAttribute(XmlWriter.XML_NAMESPACE_URI, "lang",
                                lang.toLowerCase());

                    this.writer.startElement("TranslatedText", attributes);
                    this.writer.characters(code.getDecode(lang));
                    this.writer.endElement("TranslatedText");
                }

                this.writer.endElement("Decode");
                this.writer.endElement("CodeListItem");

                ++rank;
            }

            this.writer.endElement("CodeList");
        }
    }

    private static String convertCellFormat(DataFormat formats, Cell cell) {
        String convertedValue = "";

        if (cell.getCellType() == Cell.CELL_TYPE_STRING ||
                cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
            convertedValue = cell.toString();
        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            String format = formats.getFormat(cell.getCellStyle().getDataFormat());

            System.out.println(format);
        }

        return convertedValue;
    }

    /**
     *
     * @author Tim Stone
     */
    public static class DefineMetadata {
        final String standardName;
        final String standardVersion;
        final Comparator<EngineEntity> comparator;
        final File externalMetadata;

        public DefineMetadata(String standardName, String standardVersion,
                Comparator<EngineEntity> comparator, File externalMetadata) {
            this.standardName = standardName;
            this.standardVersion = standardVersion;
            this.comparator = comparator;
            this.externalMetadata = externalMetadata;
        }
    }
}
