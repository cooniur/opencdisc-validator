/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.engine;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Tim Stone
 */
public class AbstractEngine implements Engine {
    protected volatile boolean abort = false;
    protected final Set<EngineEntity> entities;

    /**
     *
     */
    protected AbstractEngine() {
        this(null);
    }

    /**
     *
     * @param comparator
     */
    protected AbstractEngine(Comparator<EngineEntity> comparator) {
        if (comparator == null) {
            // If a comparator is provided, store the entities based on the sort criteria
            this.entities = new LinkedHashSet<EngineEntity>();
        } else {
            // If no comparator is provided, just store the entities in insertion order
            this.entities = new TreeSet<EngineEntity>(comparator);
        }
    }


    public void cancel() {
        this.abort = true;
    }

    public void prepare(EngineEntity entity) {
        this.entities.add(entity);
    }
}
