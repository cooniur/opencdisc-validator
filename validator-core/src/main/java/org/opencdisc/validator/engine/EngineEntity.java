/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.engine;

import java.util.Collections;
import java.util.Set;

import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.data.InternalEntityDetails;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.settings.Configuration;

/**
 *
 *
 * @author Tim Stone
 */
public class EngineEntity {
    private final Configuration configuration;
    private final DataSource source;
    private final EntityDetails.Reference reference;

    /**
     *
     * @param source
     * @param configuration
     */
    public EngineEntity(DataSource source, Configuration configuration) {
        this.source = source;
        this.configuration = configuration;

        if (this.configuration != null) {
            InternalEntityDetails details = this.source.getDetails();

            details.setProperty(Property.Keys, configuration.getProperty("DomainKeys"));
            details.setProperty(Property.Class, configuration.getProperty("Class"));
            details.setProperty(Property.Configuration, configuration.getProperty("Configuration"));

            if (!details.hasProperty(Property.Label)) {
                details.setProperty(Property.Label, configuration.getProperty("Label"));
            }

            this.reference = details.getReference();
        } else {
            this.reference = null;
        }
    }

    /**
     *
     * @return
     */
    public Configuration getConfiguration() {
        return this.configuration;
    }

    /**
     *
     * @return
     */
    public InternalEntityDetails getDetails() {
        return this.source.getDetails();
    }

    /**
     *
     * @return
     */
    public Set<ValidationRule> getFilters() {
        if (this.configuration == null) {
            return null;
        }

        if (this.reference == EntityDetails.Reference.Metadata) {
            return null;
        }

        return this.configuration.getFilters();
    }

    /**
     *
     * @return
     */
    public Set<ValidationRule> getRules() {
        if (this.configuration == null) {
            return Collections.emptySet();
        }

        return this.configuration.getRules(this.reference);
    }

    /**
     *
     * @return
     */
    public DataSource getSource() {
        return this.source;
    }
}
