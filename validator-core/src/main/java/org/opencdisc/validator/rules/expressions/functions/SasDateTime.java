/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.opencdisc.validator.Text;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.expressions.EvaluationException;

import java.math.BigDecimal;

/**
 * @author Tim Stone
 */
public class SasDateTime extends AbstractFunction {
    private final static BigDecimal TIME_FACTOR = new BigDecimal(60 * 60 * 24);

    SasDateTime(String name, String[] arguments) {
        super(name, arguments, 1);
    }

    /**
     *
     * @param record  the source data record
     * @return
     */
    public DataEntry compute(DataRecord record) {
        DataEntry timestamp = record.getValue(this.arguments[0]);

        if (!timestamp.hasValue() || !timestamp.isNumeric()) {
            throw new EvaluationException(
                Text.get("Messages.TimestampNaN"),
                String.format(
                    Text.get("Descriptions.TimestampNaN"),
                    timestamp.getDataType().toString()
                )
            );
        }

        BigDecimal computed = (BigDecimal)timestamp.getValue();

        if (this.name.equals("TIME")) {
            computed = computed.remainder(TIME_FACTOR);
        } else {
            computed = computed.divideToIntegralValue(TIME_FACTOR);
        }

        return new DataEntry(computed);
    }
}
