/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public class MatchValidationRule extends AbstractScriptableValidationRule {
    private static final String DEFAULT_DELIMITER = ",";
    private static final String DEFAULT_PAIR_DELIMITER = ":";
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "Variable", "Terms"
    };

    private final String variable;
    private final String pairedVariable;
    private final Map<String, String> acceptableValues = new HashMap<String, String>();
    private boolean isCaseSensitive = true;

    /**
     *
     * @param ruleDefinition
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    public MatchValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Record, REQUIRED_VARIABLES, token, metrics);

        this.variable = ruleDefinition.getProperty("Variable").toUpperCase();
        String delimiter = DEFAULT_DELIMITER;
        String pairDelimiter = DEFAULT_PAIR_DELIMITER;

        super.addVariable(this.variable);

        if (ruleDefinition.hasProperty("PairedVariable")) {
            this.pairedVariable = ruleDefinition.getProperty("PairedVariable").toUpperCase();
            super.addVariable(this.pairedVariable);
        } else {
            this.pairedVariable = null;
        }

        if (ruleDefinition.hasProperty("Delimiter")) {
            delimiter = ruleDefinition.getProperty("Delimiter");
        }

        if (ruleDefinition.hasProperty("PairDelimiter")) {
            pairDelimiter = ruleDefinition.getProperty("PairDelimiter");
        }

        if (ruleDefinition.hasProperty("CaseSensitive")) {
            if (ruleDefinition.getProperty("CaseSensitive").equalsIgnoreCase("No")) {
                this.isCaseSensitive = false;
            }
        }

        if (ruleDefinition.hasProperty("When")) {
            super.prepareExpression(ruleDefinition.getProperty("When"));
        }

        String[] matchValues = ruleDefinition.getProperty("Terms").split(
            Pattern.quote(delimiter)
        );

        for (String value : matchValues) {
            if (!this.isCaseSensitive) {
                value = value.toUpperCase();
            }

            String paired = value;

            if (this.pairedVariable != null) {
                String[] pairs = value.split(Pattern.quote(pairDelimiter));

                value = pairs[0];
                paired = pairs[1];
            }

            this.acceptableValues.put(value.trim(), paired.trim());
        }
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        boolean result = true;
        DataEntry entry = dataRecord.getValue(this.variable);

        if (entry.hasValue()) {
            String value = entry.toString();

            if (!this.isCaseSensitive) {
                value = value.toUpperCase();
            }

            if (this.pairedVariable == null) {
                result = this.acceptableValues.containsKey(value);
            } else {
                String expected = this.acceptableValues.get(value);

                entry = dataRecord.getValue(this.pairedVariable);

                if (entry.hasValue()) {
                    value = entry.toString();

                    if (!this.isCaseSensitive) {
                        value = value.toUpperCase();
                    }

                    if (this.acceptableValues.values().contains(value) || expected != null) {
                        result = value.equals(expected);
                    }
                }
            }
        }

        return (byte)(result ? 1 : 0);
    }
}
