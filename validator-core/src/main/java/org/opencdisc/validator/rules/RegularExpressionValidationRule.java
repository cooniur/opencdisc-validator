/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tim Stone
 *
 */
public class RegularExpressionValidationRule extends AbstractValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "Variable", "Test"
    };

    private final Pattern expression;
    private final String variable;
    private Matcher matcher;

    /**
     *
     * @param ruleDefinition
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    public RegularExpressionValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Record, REQUIRED_VARIABLES, token, metrics);

        this.expression = Pattern.compile(ruleDefinition.getProperty("Test"));
        this.variable = ruleDefinition.getProperty("Variable").toUpperCase();

        super.addVariable(this.variable);
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        DataEntry entry = dataRecord.getValue(this.variable);

        if (entry.hasValue()) {
            String value = entry.toString();

            if (this.matcher == null) {
                this.matcher = this.expression.matcher(value);
            } else {
                this.matcher.reset(value);
            }

            if (!this.matcher.matches()) {
                return 0;
            }
        }

        return 1;
    }
}
