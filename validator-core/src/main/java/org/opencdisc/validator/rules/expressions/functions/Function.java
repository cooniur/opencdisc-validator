/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;

import java.util.Set;

/**
 * @author Tim Stone
 */
public interface Function {
    public static final int DIVISION_SCALE = 8;

    /**
     * Computes a comparable result based on the function-specific logic.
     *
     * @param record  the source data record
     * @return  the computed value of the function
     */
    public DataEntry compute(DataRecord record);

    /**
     * Gets a set of variables used by this function.
     *
     * @return  the set of variables used by this function
     */
    public Set<String> getVariables();
}
