/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import java.util.*;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.Text;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.ProcessToken;

/**
 * Provides the framework necessary for all basic <code>{@link ValidationRule}</code>
 * implementations.
 *
 * @author Tim Stone
 */
public abstract class AbstractValidationRule implements ValidationRule {
    private final String category;
    private final String context;
    private final String description;
    private final String message;
    private final List<Message> messages = new ArrayList<Message>();
    private final List<Message> immutable = Collections.unmodifiableList(this.messages);
    private final String ruleID;
    private final Message.Severity severity;
    private final Target target;
    private final Message.Type type;
    private final RuleMetrics metrics;
    private final byte metricsLevel;
    private final boolean generate;
    private final byte negation;
    private final boolean allowUnqualifiedVariables;

    protected final Set<String> excludes;
    protected final Set<String> includes;
    protected final ProcessToken token;
    protected final Set<String> variables = new HashSet<String>();

    /**
     *
     * @param ruleDefinition
     * @param target
     * @param required
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    protected AbstractValidationRule(Definition ruleDefinition, Target target,
            String[] required, ProcessToken token, RuleMetrics metrics)
            throws ConfigurationException {
        // Verify that the rule has an ID
        if (!ruleDefinition.hasProperty("ID")) {
            throw new ConfigurationException(ConfigurationException.Type.RuleDefinition,
                String.format(
                    "ID was undefined for this %s rule.",
                    ruleDefinition.getTargetName()
                )
            );
        }

        String context = ruleDefinition.getProperty("VariableContext");

        this.token  = token;
        this.target = target;
        this.ruleID = ruleDefinition.getProperty("ID");
        this.metrics = metrics;
        this.generate = !"true".equalsIgnoreCase(
            ruleDefinition.getProperty("DisableMessaging")
        );
        this.context = StringUtils.isNotEmpty(context) ? context : null;
        this.negation = (byte)("false".equalsIgnoreCase(
            ruleDefinition.getProperty("Expect")
        ) ? 1 : 0);
        this.allowUnqualifiedVariables = "true".equalsIgnoreCase(
            ruleDefinition.getProperty("AllowUnqualifiedVariables")
        );

        if (metrics == null) {
            this.metricsLevel = 0;
        } else {
            this.metricsLevel = (byte)(target == Target.Record ? 1 : 2);
        }

        // Determine error message information
        this.message = ruleDefinition.getProperty("Message");
        this.description = ruleDefinition.getProperty("Description");
        this.category = ruleDefinition.getProperty("Category");
        this.type = Message.Type.fromString(ruleDefinition.getProperty("Type"));

        // We're phasing out severity, so this will be removed soon
        if (ruleDefinition.hasProperty("Severity")) {
            this.severity = Message.severityFromString(
                ruleDefinition.getProperty("Severity")
            );
        } else {
            this.severity = null;
        }

        // Extra variables to display in the report messages
        if (ruleDefinition.hasProperty("Display")) {
            String[] variables = ruleDefinition.getProperty("Display")
                    .split(Pattern.quote(","));
            Set<String> includes = new HashSet<String>();
            Set<String> excludes = new HashSet<String>();

            for (String variable : variables) {
                variable = variable.trim().toUpperCase();

                if (variable.startsWith("-")) {
                    excludes.add(variable.substring(1));
                } else {
                    if (variable.startsWith("+")) {
                        variable = variable.substring(1);
                    }

                    includes.add(variable);
                }
            }

            this.includes = includes.size() > 0 ? includes : null;
            this.excludes = excludes.size() > 0 ? excludes : null;
        } else {
            this.includes = null;
            this.excludes = null;
        }

        if (required != null) {
            String missingProperties = null;

            for (String requiredProperty : required) {
                if (!ruleDefinition.hasProperty(requiredProperty)) {
                    if (missingProperties == null) {
                        missingProperties = requiredProperty;
                    } else {
                        missingProperties = missingProperties + ", " + requiredProperty;
                    }
                }
            }

            if (missingProperties != null) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    String.format(
                        Text.get("Exceptions.MissingAttribute"),
                        ruleDefinition.getTargetName(),
                        this.getID(),
                        missingProperties
                    )
                );
            }
        }
    }

    public List<Message> getMessages() {
        return this.immutable;
    }

    public Target getTarget() {
        return this.target;
    }

    @SuppressWarnings("unchecked")
    public final boolean validate(DataRecord dataRecord)
            throws CorruptRuleException {
        EntityDetails entity = dataRecord.getEntityDetails();

        if (this.allowUnqualifiedVariables) {
            dataRecord.allowUnqualifiedReferences();
        }

        if (this.metricsLevel == 1) {
            this.metrics.processRuleStart(
                entity,
                this.ruleID,
                this.context,
                this.message
            );
        }

        this.messages.clear();
        byte result;

        // TODO: It's probably sufficient to perform this check only once, instead of per-record...
        for (String variable : this.variables) {
            // Check if the variable is not defined
            if (!dataRecord.definesVariable(variable)) {
                // We can't perform validations with rules that don't have all their data
                // Consequently, mark that this rule is in an invalid state by throwing
                // an exception
                throw new CorruptRuleException(
                    CorruptRuleException.State.Unrecoverable,
                    this.getID(),
                    String.format(
                        Text.get("Messages.MissingVariables"),
                        this.getID(),
                        entity.getString(Property.Name)
                    ),
                    Text.get("Descriptions.MissingVariables") + ": '" + variable + "'"
                );
            }
        }

        result = (byte)(this.performValidation(dataRecord) ^ this.negation);

        // Check if the rule failed and prepare the message if it did
        if (result == 0 && this.generate) {
            this.messages.add(new Message(
                this.ruleID,
                this.category,
                this.message,
                this.description,
                this.context,
                this.type,
                this.severity,
                entity,
                dataRecord.getDataDetails()
            ));
        }

        // Populate the message objects with the data
        for (Message message : this.messages) {
            this.populateMessage(
                message,
                dataRecord,
                this.variables
            );
        }

        if (this.metricsLevel == 1) {
            this.metrics.processRuleStop(
                dataRecord.getEntityDetails(),
                this.ruleID,
                this.context,
                result > -1,
                result == 0
            );
        }

        if (this.allowUnqualifiedVariables) {
            dataRecord.disallowUnqualifiedReferences();
        }

        return result != 0;
    }

    public final boolean validateDataset(EntityDetails entity) {
        this.messages.clear();
        List<Outcome> results = this.performDatasetValidation(entity);

        boolean failed = false;

        for (Outcome result : results) {
            if (this.metricsLevel == 2) {
                this.metrics.processRuleStart(
                    entity,
                    this.ruleID,
                    this.context,
                    this.message
                );
            }

            if (result.result == 0) {
                failed = true;

                if (this.generate) {
                    Message message = new Message(
                        this.ruleID,
                        this.category,
                        this.message,
                        this.description,
                        this.context,
                        this.type,
                        this.severity,
                        result.entity != null ? result.entity : entity,
                        null
                    );

                    for (Map.Entry<String, String> pair : result.display.entrySet()) {
                        message.setValueOf(pair.getKey(), pair.getValue());
                    }

                    this.messages.add(message);
                }
            }

            if (this.metricsLevel == 2) {
                this.metrics.processRuleStop(
                    entity,
                    this.ruleID,
                    this.context,
                    result.result > -1,
                    result.result == 0
                );
            }
        }

        return !failed;
    }

    public String getID() {
        return this.ruleID;
    }

    public void setup(EntityDetails entity) {}

    /**
     *
     * @param thisRecord
     * @return
     * @throws CorruptRuleException
     */
    protected byte performValidation(DataRecord thisRecord) throws CorruptRuleException {
        return -1;
    }

    /**
     *
     * @return
     */
    protected List<Outcome> performDatasetValidation(EntityDetails entity) {
        return Arrays.asList(new Outcome((byte)-1));
    }

    /**
     *
     * @param variable
     */
    protected void addVariable(String variable) {
        this.variables.add(variable.toUpperCase());
    }

    /**
     *
     * @return
     */
    protected Set<String> getVariables() {
        return this.variables;
    }

    protected void populateMessage(Message message, DataRecord record,
            Set<String>... variables) {
        if (this.includes != null) {
            this.populateMessageInternal(message, record, this.includes);
        }

        for (Set<String> set : variables) {
            this.populateMessageInternal(message, record, set);
        }
    }

    private void populateMessageInternal(Message message, DataRecord record,
            Set<String> variables) {
        for (String variable : variables) {
            String value = null;

            if (this.excludes != null && this.excludes.contains(variable)) {
                continue;
            }

            if (record.definesVariable(variable) && !record.isTransient(variable)) {
                DataEntry entry = record.getValue(variable);

                if (entry.hasValue()) {
                    value = entry.toString();
                }

                message.setValueOf(variable, value);
            }
        }
    }

    protected static class Outcome {
        public final Map<String, String> display = new HashMap<String, String>();
        public final byte result;
        private final EntityDetails entity;

        public Outcome(byte result) {
            this(result, null);
        }

        public Outcome(byte result, EntityDetails entity) {
            this.result = result;
            this.entity = entity;
        }
    }
}
