/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Tim Stone
 */
public class PercentDifference extends Difference {
    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    PercentDifference(String name, String[] arguments) {
        super(name, arguments);
    }

    public DataEntry compute(DataRecord record) {
        DataEntry difference = super.compute(record);
        DataEntry rhs = record.getValue(this.arguments[1]);

        return new DataEntry(
            ((BigDecimal)difference.getValue()).divide(
                (BigDecimal)rhs.getValue(), Function.DIVISION_SCALE, RoundingMode.HALF_UP
            ).multiply(ONE_HUNDRED)
        );
    }
}
