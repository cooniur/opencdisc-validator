/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.opencdisc.validator.Text;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.data.Lookup;
import org.opencdisc.validator.data.LookupProvider;
import org.opencdisc.validator.data.LookupProviderFactory;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.rules.expressions.PreparedQuery;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public class LookupValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "From"
    };

    private final boolean ignoreWhereFailure;
    private final Map<String, Lookup> lookups = new KeyMap<Lookup>();
    private final LookupProvider provider;
    private final PreparedQuery query;

    /**
     *
     * @param ruleDefinition
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    public LookupValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Record, REQUIRED_VARIABLES, token, metrics);

        if (!ruleDefinition.hasProperty("Variable") &&
                !ruleDefinition.hasProperty("Search")) {
            throw new ConfigurationException(
                ConfigurationException.Type.RuleDefinition,
                "Lookup rules must have one of (Variable, Search)"
            );
        } else if (ruleDefinition.hasProperty("Variable") &&
                ruleDefinition.hasProperty("Search")) {
            throw new ConfigurationException(
                ConfigurationException.Type.RuleDefinition,
                "Lookup rules can only have one of (Variable, Search)"
            );
        }

        if (ruleDefinition.hasProperty("When")) {
            super.prepareExpression(ruleDefinition.getProperty("When"));
        }

        this.ignoreWhereFailure = ruleDefinition.hasProperty("WhereFailure") &&
                ruleDefinition.getProperty("WhereFailure").equalsIgnoreCase("Ignore");

        String adapter = ruleDefinition.hasProperty("Provider") ?
                ruleDefinition.getProperty("Provider") : null;

        if (adapter == null) {
            this.provider = LookupProviderFactory.getLookupProvider();
        } else {
            try {
                this.provider = LookupProviderFactory.getLookupProvider(adapter);
            } catch (IllegalArgumentException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    ex
                );
            }
        }

        String search;
        String target = ruleDefinition.getProperty("From");
        String where = ruleDefinition.getProperty("Where");

        if (ruleDefinition.hasProperty("Variable")) {
            // Upgrade legacy Variable syntax to new form (like where clause)
            search = ruleDefinition.getProperty("Variable").replace(
                ",", " @and "
            ).replaceAll("(==\\s*)([A-Za-z+][A-Za-z0-9]+)", "$1 [$2]");
        } else {
            search = ruleDefinition.getProperty("Search");
        }

        this.query = new PreparedQuery(target, search, where, adapter == null);

        for (String variable : this.query.getLocal()) {
            super.addVariable(variable);
        }

        if(this.query.isRequestable()) {
            this.provider.request(
                this.query.getTarget(),
                this.query.getRemote(),
                this.token
            );
        }
    }

    protected List<Outcome> performDatasetValidation(EntityDetails entity) {
        Set<String> lookupNames = new HashSet<String>(this.lookups.keySet());

        for (String lookupName : lookupNames) {
            this.lookups.remove(lookupName);
        }

        return super.performDatasetValidation(entity);
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        boolean result = true;
        Set<String> remote = new HashSet<String>();
        String target = this.query.getTarget(dataRecord);

        if (!this.lookups.containsKey(target) || this.lookups.get(target) != null) {
            Lookup lookup;
            List<PreparedQuery.Mapping> search = this.query.getSearch(dataRecord);
            List<PreparedQuery.Mapping> where = this.query.getWhere(dataRecord);

            // TODO: Iterating over these lists for this information seems annoying
            for (PreparedQuery.Mapping mapping : search) {
                remote.add(mapping.getRemote());
            }

            for (PreparedQuery.Mapping mapping : where) {
                remote.add(mapping.getRemote());
            }

            if (this.lookups.containsKey(target)) {
                lookup = this.lookups.get(target);

                if (!lookup.contains(remote)) {
                    lookup = this.provider.get(target, remote, this.token);

                    if (lookup == null) {
                        throw new CorruptRuleException(
                            CorruptRuleException.State.Temporary,
                            super.getID(),
                            String.format(
                                Text.get("Messages.MissingLookup"),
                                target.replaceFirst("^FILE:([A-Z-]{2,}:)?", "")
                            ),
                            String.format(
                                Text.get("Descriptions.MissingLookup"),
                                super.getID(),
                                target.replaceFirst("^FILE:([A-Z-]{2,}:)?", "")
                            )
                        );
                    } else {
                        this.lookups.put(target, lookup);
                    }
                }
            } else {
                lookup = this.provider.get(target, remote, this.token);

                this.lookups.put(target, lookup);

                if (lookup == null) {
                    throw new CorruptRuleException(
                        CorruptRuleException.State.Temporary,
                        super.getID(),
                        String.format(
                            Text.get("Messages.MissingLookup"),
                            target.replaceFirst("^FILE:([A-Z-]{2,}:)?", "")
                        ),
                        String.format(
                            Text.get("Descriptions.MissingLookup"),
                            super.getID(),
                            target.replaceFirst("^FILE:([A-Z-]{2,}:)?", "")
                        )
                    );
                }
            }

            result = lookup.seek(search, where, this.ignoreWhereFailure);
        }

        return (byte)(result ? 1 : 0);
    }
}
