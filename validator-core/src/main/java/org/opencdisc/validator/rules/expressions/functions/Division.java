/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.opencdisc.validator.Text;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.expressions.EvaluationException;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Tim Stone
 */
public class Division extends AbstractFunction {
    Division(String name, String[] arguments) {
        super(name, arguments, 2);
    }

    /**
     *
     * @param record  the source data record
     * @return
     */
    public DataEntry compute(DataRecord record) {
        DataEntry numerator = record.getValue(this.arguments[0]);
        DataEntry denominator = record.getValue(this.arguments[1]);

        if (!numerator.hasValue() || !numerator.isNumeric()) {
            throw new EvaluationException(
                Text.get("Messages.NumeratorNaN"),
                String.format(
                    Text.get("Descriptions.NumeratorNaN"),
                    numerator.getDataType().toString()
                )
            );
        }

        if (!denominator.hasValue() || !denominator.isNumeric()) {
            throw new EvaluationException(
                Text.get("Messages.DenominatorNaN"),
                String.format(
                    Text.get("Descriptions.DenominatorNaN"),
                    numerator.getDataType().toString()
                )
            );
        }

        return new DataEntry(
            ((BigDecimal)numerator.getValue()).divide(
                (BigDecimal)denominator.getValue(), Function.DIVISION_SCALE, RoundingMode.HALF_UP
            )
        );
    }
}
