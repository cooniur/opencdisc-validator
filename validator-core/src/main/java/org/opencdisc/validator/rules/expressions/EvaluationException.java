/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions;

/**
 *
 * @author Tim Stone
 */
public class EvaluationException extends RuntimeException {
    private final String description;

    /**
     *
     * @param message
     */
    public EvaluationException(String message, String description) {
        super(message);

        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }
}
