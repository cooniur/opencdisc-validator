/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import java.util.HashSet;
import java.util.Set;

import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.data.LookupProvider;
import org.opencdisc.validator.data.LookupProviderFactory;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.rules.expressions.PreparedQuery;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public class MetadataValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "From"
    };

    private final boolean ignoreFromFailure;
    private final LookupProvider provider;
    private final PreparedQuery query;

    /**
     *
     * @param ruleDefinition
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    public MetadataValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Record, REQUIRED_VARIABLES, token, metrics);

        if (ruleDefinition.hasProperty("When")) {
            super.prepareExpression(ruleDefinition.getProperty("When"));
        }

        this.ignoreFromFailure = ruleDefinition.hasProperty("Variable") &&
                ruleDefinition.getProperty("Variable").length() > 0;
        this.provider = LookupProviderFactory.getLookupProvider();
        String target = ruleDefinition.hasProperty("From") ?
                ruleDefinition.getProperty("From") : null;
        String search = ruleDefinition.hasProperty("Variable") ?
                ruleDefinition.getProperty("Variable") : null;
        this.query = new PreparedQuery(target, search);

        for (String variable : this.query.getLocal()) {
            super.addVariable(variable);
        }
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        boolean result = true;
        String target = this.query.getTarget(dataRecord);

        if (this.provider.verifyExists(target, this.token)) {
            Set<String> remote = new HashSet<String>();

            for (PreparedQuery.Mapping mapping : this.query.getSearch(dataRecord)) {
                remote.add(mapping.getRemote());
            }

            result = this.provider.verifyExists(target, remote, this.token);
        } else {
            result = this.ignoreFromFailure;
        }

        return (byte)(result ? 1 : 0);
    }
}
