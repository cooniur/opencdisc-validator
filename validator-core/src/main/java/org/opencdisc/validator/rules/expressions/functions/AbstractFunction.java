/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.rules.expressions.SyntaxException;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tim Stone
 */
public abstract class AbstractFunction implements Function {
    protected final String name;
    protected final String[] arguments;
    protected final Set<String> variables;

    public AbstractFunction(String name, String[] arguments, int expected) {
        this(name, arguments, expected, false);
    }

    public AbstractFunction(String name, String[] arguments, int expected,
            boolean unbounded) {
        if (arguments.length != expected &&
                (!unbounded || arguments.length < expected)) {
            throw new SyntaxException(String.format(
                "The %s function requires %s%d arguments; %d given",
                name, (unbounded ? "at least " : ""), expected, arguments.length
            ));
        }

        this.name = name;
        this.arguments = arguments;
        this.variables = Collections.unmodifiableSet(
            new HashSet<String>(Arrays.asList(arguments))
        );

        for (int i = 0; i < this.arguments.length; ++i) {
            this.arguments[i] = this.arguments[i].trim();
        }
    }

    public Set<String> getVariables() {
        return this.variables;
    }

    public String toString() {
        return String.format(
            ":%s(%s)", this.name, StringUtils.join(this.arguments, ',')
        );
    }
}
