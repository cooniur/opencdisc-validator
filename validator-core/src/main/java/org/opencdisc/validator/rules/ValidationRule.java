/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.Message;

/**
 *
 * @author Tim Stone
 */
public interface ValidationRule {
    /**
     *
     * @author Tim Stone
     */
    @Retention(RetentionPolicy.RUNTIME)
    @java.lang.annotation.Target({ ElementType.TYPE })
    public @interface RuleAssociation {
        String value();
    }

    /**
     *
     * @author Tim Stone
     */
    public enum Target {
        Record,
        Dataset
    }

    /**
     *
     * @return
     */
    public String getID();

    /**
     *
     * @return
     */
    public List<Message> getMessages();

    /**
     *
     * @return
     */
    public Target getTarget();

    /**
     * @param entity
     */
    public void setup(EntityDetails entity);

    /**
     *
     *
     * @param dataRecord  the record containing data to be validated
     * @return <code>true</code> if the data passes the validation, <code>false</code>
     *     otherwise
     * @throws CorruptRuleException  if this rule determines that it cannot
     *     run with the information provided to it
     */
    public boolean validate(DataRecord dataRecord)
            throws CorruptRuleException;

    /**
     * Performs validations against the entirety of data for logic that can only be
     * applied once there's guaranteed not to be any more data to process. Consequently,
     * this method should only be called after no more calls to a rule's
     * {@link #validate(DataRecord)} method are intended.
     *
     * @param entity
     * @return <code>true</code> if the data passes the post-validation step or does not
     *     require post-validation, <code>false</code> otherwise
     */
    public boolean validateDataset(EntityDetails entity);
}
