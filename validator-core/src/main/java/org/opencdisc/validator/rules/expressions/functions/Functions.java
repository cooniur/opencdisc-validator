/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import java.lang.reflect.Constructor;
import java.util.*;

import org.opencdisc.validator.Text;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.expressions.EvaluationException;
import org.opencdisc.validator.rules.expressions.SyntaxException;

/**
 *
 * @author Tim Stone
 */
public class Functions {
    private static Map<String, Class<? extends Function>> functions =
        new HashMap<String, Class<? extends Function>>();

    static {
        functions.put("DATE", SasDateTime.class);
        functions.put("TIME", SasDateTime.class);
        functions.put("MAX", Maximum.class);
        functions.put("DIV", Division.class);
        functions.put("DY", DyCount.class);
        functions.put("DIFF", Difference.class);
        functions.put("PCTDIFF", PercentDifference.class);
    }

    public static Function create(String function) {
        if (!function.startsWith(":")) {
            throw new SyntaxException(String.format("'%s' isn't a function", function));
        }

        // Remove the colon
        function = function.substring(1);

        // Now get the name
        int start = function.indexOf('(');

        if (start == -1 || !function.endsWith(")")) {
            throw new SyntaxException(String.format(
                "'%s' doesn't have the correct parentheses", function
            ));
        }

        String name = function.substring(0, start);
        String args = function.substring(start + 1, function.length() - 1);

        Class<? extends Function> definition = functions.get(name);

        if (definition == null) {
            throw new SyntaxException(String.format("Unknown function '%s'", name));
        }

        try {
            Constructor<? extends Function> constructor = definition.getDeclaredConstructor(
                new Class[] { String.class, String[].class }
            );

            return constructor.newInstance(name, args.split(","));
        } catch (Exception ex) {
            Throwable cause = ex.getCause();

            if (cause instanceof SyntaxException) {
                throw (SyntaxException)cause;
            }

            throw new RuntimeException(String.format(
                "Unable to create function %s", name
            ), ex);
        }
    }

    private Functions() {}
}
