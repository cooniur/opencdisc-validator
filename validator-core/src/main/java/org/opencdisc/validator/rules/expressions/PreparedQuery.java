/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;

/**
 *
 * @author Tim Stone
 */
public class PreparedQuery {
    private final static String COMPLEX_VARIABLE =
        "(" +                              //
          "[A-Za-z]?" +                    // Do we start with an alpha char?
          "(?:" +                          //
            "(?<=[A-Za-z])" +              // If we did, we might have some
            "[A-Za-z0-9]*" +               // alphanumeric stuff following
          ")?" +                           //
          "(?:" +                          //
            "\\[" +                        // There's optionally a local variable
            "([A-Za-z][A-Za-z0-9]*)" +     // It starts with an alpha char, then
            "(?:" +                        // some optional alphanumerics
              "\\:" +                      // There might be a formatting option
              "([0-9](?:L|R)[^\\]])" +     // (times)(left or right)(pad char)
            ")?" +                         //
            "\\]" +                        //
          ")?" +                           //
          "(?<!\\A)" +                     // Make sure there was stuff before this
          "[A-Za-z0-9]*" +                 // There might be some more at the end
          "|VAL:[A-Za-z0-9]+" +            // Or it's a system variable
        ")";                               //
    private final static String LITERAL =
        "(" +                              // A literal is either
          "'(?:[^']|\\\\')+'" +            // A quoted string
          "|" +                            // OR
          "(?:-?[0-9]+(?:\\.[0-9]+)?)" +   // A number
          "|" +                            // OR
          "null" +                         // null
        ")";
    private final static String OPERATORS = "([<>]|[=<>!^]=)";
    private final static String SIMPLE_VARIABLE =
        "(" +                              //
          "[A-Za-z]" +                     // Begins with a letter
          "[A-Za-z0-9]+" +                 // Followed by alphanumerics
         ")";                              //
    private final static String CLAUSE_PATTERN =
        "(\\b|(?<=[^\\w\\s]))\\s*(?=\\&{2}|\\|{2})";


    private boolean requestable = true;
    private final Pattern clause = Pattern.compile(
        "(\\&{2}|\\|{2})?" + // Get the (optional) operator
        "\\s*" +             // Followed by optional space
        COMPLEX_VARIABLE +   // Then a complex variable
        "\\s*" +             // Followed by more optional space
        OPERATORS +          // Followed by an allowed operator
        "\\s*" +             // Then more optional space
        "(?:" +              // Then the right hand side,
          "\\[" +            //
          SIMPLE_VARIABLE +  // A local variable
          "\\]" +            //
          "|" +              // OR
          LITERAL +          // A literal value
        ")"
    );
    private final List<Mapping> searches = new ArrayList<Mapping>();
    private final List<Mapping> where = new ArrayList<Mapping>();
    private final ComplexVariable target;

    /**
     *
     * @param target
     * @param search
     */
    public PreparedQuery(String target, String search) {
        Pattern pattern;
        Matcher matcher;

        pattern = Pattern.compile(COMPLEX_VARIABLE);
        matcher = pattern.matcher(target);

        if (matcher.matches()) {
            this.target = new ComplexVariable(
                    matcher.group(2),
                    matcher.group(1),
                    matcher.group(3)
            );
        } else {
            throw new IllegalArgumentException("target is not properly formatted");
        }

        // TODO: This is all special-cased, which needs to be fixed somehow
        //   We use this for both the lookup and metadata rule, but they aren't exactly
        //   the same...so this constructor handles the metadata, while the other one
        //   does the parsing for lookups. The whole thing needs to be reorganized a bit,
        //   but I guess this is sufficient for now. Should probably take another look at
        //   it in the future when trying to implement execution plans for lookups in the
        //   DataCache class
        if (search != null && search.length() > 0) {
            String[] associations = search.split(",");

            for (String association : associations) {
                matcher = pattern.matcher(association.trim());

                if (!matcher.matches()) {
                    throw new IllegalArgumentException(String.format(
                            "The remote variable '%s' is not properly formatted",
                            association
                    ));
                }

                this.searches.add(new Mapping(
                        new ComplexVariable(
                                matcher.group(2),
                                matcher.group(1),
                                matcher.group(3)
                        ),
                        null,
                        null,
                        null
                ));
            }
        }
    }

    /**
     *
     * @param target
     * @param search
     * @param where
     */
    public PreparedQuery(String target, String search, String where, boolean strict) {
        Pattern pattern;
        Matcher matcher;

        pattern = Pattern.compile(COMPLEX_VARIABLE);
        matcher = pattern.matcher(target);

        if (matcher.matches()) {
            this.target = new ComplexVariable(
                    matcher.group(2),
                    matcher.group(1),
                    matcher.group(3)
            );
        } else if (target.startsWith("FILE:") || !strict) {
            // TODO: This feels pretty hacky
            this.target = new ComplexVariable(
                    null,
                    target,
                    null
            );
        } else {
            throw new IllegalArgumentException("target is not properly formatted");
        }

        if (search != null && search.length() > 0) {
            parseInto(search, this.searches);

            if (where != null && where.length() > 0) {
                parseInto(where, this.where);
            }
        }
    }

    private void parseInto(String query, List<Mapping> mappings) {
        query = query.replace("''",     "null")
                     .replace("@and",   "&&")
                     .replace("@gteq",  ">=")
                     .replace("@lteq",  "<=")
                     .replace("@lt",    "<")
                     .replace("@gt",    ">")
                     .replace("@or",    "||")
                     .replace("@eqic",  "^=")
                     .replace("@neqic", "!="); // TODO: No case insensitivity for not equals in lookup
                                               //    This case is more difficult than equals, so it's
                                               //    deferred until we need it

        String[] clauses = query.split(CLAUSE_PATTERN);

        for (String clause : clauses) {
            Matcher matcher = this.clause.matcher(clause);

            if (!matcher.matches()) {
                throw new IllegalArgumentException(String.format(
                    "The clause '%s' is not properly formatted", clause
                ));
            }

            Mapping.Operator operator = null;
            Mapping.Comparator comparator = null;
            String operatorGroup = matcher.group(1);
            String comparatorGroup = matcher.group(5);

            if (operatorGroup != null) {
                operator = operatorGroup.equals("&&") ?
                    Mapping.Operator.AND : Mapping.Operator.OR;
            }

            if (comparatorGroup.equals("==")) {
                comparator = Mapping.Comparator.EQ;
            } else if (comparatorGroup.equals("^=")) {
                comparator = Mapping.Comparator.EIQ;
            } else if (comparatorGroup.equals("!=")) {
                comparator = Mapping.Comparator.NEQ;
            } else if (comparatorGroup.equals("<")) {
                comparator = Mapping.Comparator.LT;
            } else if (comparatorGroup.equals("<=")) {
                comparator = Mapping.Comparator.LTE;
            } else if (comparatorGroup.equals(">")) {
                comparator = Mapping.Comparator.GT;
            } else if (comparatorGroup.equals(">=")) {
                comparator = Mapping.Comparator.GTE;
            }

            Mapping mapping = new Mapping(
                new ComplexVariable(
                    matcher.group(3),
                    matcher.group(2),
                    matcher.group(4)
                ),
                comparator,
                matcher.group(6),
                operator
            );

            String literal = matcher.group(7);

            if (literal != null) {
                mapping.setValue(new DataEntry(
                    literal.equals("null") ? null : StringUtils.strip(literal, "'")
                ));
            }

            mappings.add(mapping);
        }
    }

    /**
     *
     * @param record
     * @return
     */
    public List<Mapping> getSearch(DataRecord record) {
        return this.getMappings(this.searches, record);
    }

    /**
     *
     * @return
     */
    public String getTarget() {
        return this.getTarget(null);
    }

    /**
     *
     * @param record
     * @return
     */
    public String getTarget(DataRecord record) {
        return this.target.getVariable(record);
    }

    /**
     *
     * @return
     */
    public Set<String> getLocal() {
        Set<String> variables = new HashSet<String>();

        if (this.target.local != null) {
            variables.add(this.target.local);

            this.requestable = false;
        }

        for (Mapping mapping : this.searches) {
            if (mapping.local != null) {
                variables.add(mapping.local);
            }

            if (mapping.remote.local != null) {
                variables.add(mapping.remote.local);

                this.requestable = false;
            }
        }

        for (Mapping mapping : this.where) {
            if (mapping.local != null) {
                variables.add(mapping.local);
            }

            if (mapping.remote.local != null) {
                variables.add(mapping.remote.local);

                this.requestable = false;
            }
        }

        return variables;
    }

    /**
     *
     * @return
     */
    public Set<String> getRemote() {
        Set<String> variables = new HashSet<String>();

        // TODO: Uppercasing here seems like it should be unnecessary
        for (Mapping mapping : this.searches) {
            variables.add(mapping.remote.template.toUpperCase());
        }

        for (Mapping mapping : this.where) {
            variables.add(mapping.remote.template.toUpperCase());
        }

        return variables;
    }

    /**
     *
     * @param record
     * @return
     */
    public List<Mapping> getWhere(DataRecord record) {
        return this.getMappings(this.where, record);
    }

    public boolean isRequestable() {
        return this.requestable;
    }

    /**
     *
     * @return
     */
    List<Mapping> getMappings(List<Mapping> mappings, DataRecord record) {
        String local;
        for (Mapping mapping : mappings) {
            local = mapping.getLocal();
            mapping.getRemote(record);

            if (local != null) {
                mapping.setValue(record.getValue(local));
            }
        }

        return mappings;
    }

    /**
     *
     * @author Tim Stone
     */
    public static class Mapping {
        /**
         *
         * @author Tim Stone
         */
        public enum Comparator {
            EQ,
            EIQ,
            NEQ,
            LT,
            GT,
            LTE,
            GTE
        }

        /**
         *
         * @author Tim Stone
         */
        public enum Operator {
            AND,
            OR
        }

        private final Comparator comparator;
        private String last;
        private final String local;
        private final Operator operator;
        private final ComplexVariable remote;
        private DataEntry value;

        /**
         *
         * @param remote
         * @param comparator
         * @param local
         */
        Mapping(ComplexVariable remote, Comparator comparator, String local,
                Operator operator) {
            this.remote = remote;
            this.comparator = comparator;
            this.local = local;
            this.operator = operator;
        }

        /**
         *
         * @return
         */
        public Comparator getComparator() {
            return this.comparator;
        }

        /**
         *
         * @return
         */
        public String getLocal() {
            return this.local;
        }

        /**
         *
         * @return
         */
        public Operator getOperator() {
            return this.operator;
        }

        /**
         *
         * @return
         */
        public String getRemote() {
            return this.last;
        }

        /**
         *
         * @param record
         * @return
         */
        String getRemote(DataRecord record) {
            // TODO: Uppercasing here seems like it should be unnecessary
            return this.last = this.remote.getVariable(record).toUpperCase();
        }

        /**
         *
         * @return
         */
        public DataEntry getValue() {
            return this.value;
        }

        /**
         *
         * @param value
         */
        void setValue(DataEntry value) {
            this.value = value;
        }
    }

    /**
     *
     * @author Tim Stone
     */
    private static class ComplexVariable {
        final String local;
        final Padding padding;
        final String template;
        private String cache;
        private String last;

        /**
         *
         * @param local
         * @param template
         * @param padding
         */
        ComplexVariable(String local, String template, String padding) {
            this.local = local;
            this.template = local != null ? template.replaceFirst("\\[[^\\]]+\\]", "%s%s") : template;
            this.padding = padding != null ? new Padding(padding) : null;
        }

        /**
         *
         * @param record
         * @return
         */
        String getVariable(DataRecord record) {
            if (this.local == null) {
                return this.template;
            }

            String insert = record.getValue(this.local).toString();

            if (insert.equals(this.last)) {
                return this.cache;
            }

            boolean left = true;
            int difference = 0;
            String padding = "";

            if (this.padding != null) {
                left = this.padding.left;
                difference = this.padding.times - insert.length();

                if (difference > 0) {
                    padding = StringUtils.repeat(this.padding.padding, difference);
                }
            }

            this.cache = String.format(
                    this.template,
                    left ? padding : insert,
                    left ? insert : padding
            );
            this.last = insert;

            return this.cache;
        }
    }

    /**
     *
     * @author Tim Stone
     */
    private static class Padding {
        /**
         * Indication of whether the padding is on the left or right
         */
        final boolean left;

        /**
         * The content to use for padding
         */
        final String padding;

        /**
         * The minimum width of the resulting string
         */
        final byte times;

        /**
         *
         * @param padding
         */
        Padding(String padding) {
            if (padding == null || padding.length() != 3) {
                throw new IllegalArgumentException("Invalid padding string");
            }

            this.times = Byte.parseByte(padding.substring(0, 1));
            this.left = !padding.substring(1, 2).equals("R");
            this.padding = padding.substring(2, 3);
        }
    }
}
