/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.rules.CorruptRuleException.State;
import org.opencdisc.validator.rules.expressions.EvaluationException;
import org.opencdisc.validator.rules.expressions.Expression;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public abstract class AbstractScriptableValidationRule extends AbstractValidationRule {
    protected static final String DEFAULT_EXPRESSION_NAME = "DEFAULT_EXPRESSION";

    private static final DataEntry NULL_ENTRY = new DataEntry(null);

    private final Set<String> nullableVariables = new HashSet<String>();
    private final Map<String, Expression> expressions = new HashMap<String, Expression>();
    private boolean hasExpressions = false;

    /**
     *
     * @param ruleDefinition
     * @param type
     * @param requiredProperties
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    protected AbstractScriptableValidationRule(Definition ruleDefinition, Target type,
            String[] requiredProperties, ProcessToken token, RuleMetrics metrics)
            throws ConfigurationException {
        super(ruleDefinition, type, requiredProperties, token, metrics);

        if (ruleDefinition.hasProperty("Optional")) {
            String[] nullableVariables = ruleDefinition.getProperty("Optional")
                    .split(Pattern.quote(","));

            for (String nullableVariable : nullableVariables) {
                nullableVariable = nullableVariable.trim().toUpperCase();

                this.nullableVariables.add(nullableVariable);
            }
        }
    }

    protected void addVariable(String variable) {
        variable = variable.toUpperCase();

        if (!this.nullableVariables.contains(variable)) {
            super.addVariable(variable);
        }
    }

    protected boolean checkExpression(DataRecord dataRecord)
            throws CorruptRuleException {
        return this.checkExpression(dataRecord, DEFAULT_EXPRESSION_NAME);
    }

    protected boolean checkExpression(DataRecord dataRecord, String name)
            throws CorruptRuleException {
        Expression expression = this.expressions.get(name);

        if (expression == null) {
            return true;
        }

        for (String nullableVariable : this.nullableVariables) {
            if (!dataRecord.definesVariable(nullableVariable)) {
                dataRecord.setTransientValue(nullableVariable, NULL_ENTRY);
            }
        }

        try {
            return expression.evaluate(dataRecord);
        } catch (EvaluationException ex) {
            throw new CorruptRuleException(
                State.Temporary,
                super.getID(),
                ex.getMessage(),
                ex.getDescription()
            );
        }
    }

    protected boolean hasExpression() {
        return this.hasExpression(DEFAULT_EXPRESSION_NAME);
    }

    protected boolean hasExpression(String name) {
        return this.hasExpressions && this.expressions.containsKey(name);
    }

    protected void prepareExpression(String condition) {
        this.prepareExpression(DEFAULT_EXPRESSION_NAME, condition);
    }

    protected void prepareExpression(String condition, boolean registerVariables) {
        this.prepareExpression(DEFAULT_EXPRESSION_NAME, condition, registerVariables);
    }

    protected void prepareExpression(String name, String testCondition) {
        this.prepareExpression(name, testCondition, true);
    }

    protected void prepareExpression(String name, String testCondition, boolean registerVariables) {
        if (testCondition == null) {
            throw new IllegalArgumentException("testCondition cannot be null");
        }

        Expression compiled = Expression.createFrom(testCondition);

        if (registerVariables) {
            for (String variable : compiled.getVariables()) {
                this.addVariable(variable);
            }
        }

        this.hasExpressions = true;
        this.expressions.put(name, compiled);
    }

    @SuppressWarnings("unchecked")
    protected void populateMessage(Message message, DataRecord record,
            Set<String>... variables) {
        Set<String>[] copy = new Set[variables.length + 1];
        copy[copy.length - 1] = this.nullableVariables;

        System.arraycopy(variables, 0, copy, 0, variables.length);

        super.populateMessage(message, record, copy);
    }
}
