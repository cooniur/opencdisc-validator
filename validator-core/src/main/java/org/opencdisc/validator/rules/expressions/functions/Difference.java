/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.opencdisc.validator.Text;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.expressions.EvaluationException;

import java.math.BigDecimal;

/**
 * @author Tim Stone
 */
public class Difference extends AbstractFunction {
    Difference(String name, String[] arguments) {
        super(name, arguments, 2);
    }

    public DataEntry compute(DataRecord record) {
        DataEntry lhs = record.getValue(this.arguments[0]);
        DataEntry rhs = record.getValue(this.arguments[1]);

        if (!lhs.hasValue() || !lhs.isNumeric()) {
            throw new EvaluationException(
                Text.get("Messages.LhsNaN"),
                String.format(
                    Text.get("Descriptions.LhsNaN"),
                    lhs.getDataType().toString()
                )
            );
        }

        if (!rhs.hasValue() || !rhs.isNumeric()) {
            throw new EvaluationException(
                Text.get("Messages.RhsNaN"),
                String.format(
                    Text.get("Descriptions.RhsNaN"),
                    rhs.getDataType().toString()
                )
            );
        }

        return new DataEntry(((BigDecimal)lhs.getValue()).subtract((BigDecimal)rhs.getValue()));
    }
}
