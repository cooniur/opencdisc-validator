/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.util.StringUtils;

/**
 *
 * @author Tim Stone
 */
public class Expression implements Evaluable {
    private enum Operator {
        AND,
        OR
    }

    private final List<Evaluable> evaluationStack = new ArrayList<Evaluable>();
    private final List<Operator>  operatorStack   = new ArrayList<Operator>();
    private final boolean negated;
    private Set<String> variables = null;

    /**
     *
     * @param raw
     * @return
     */
    public static Expression createFrom(String raw) {
        raw = raw.replaceAll("(?<!\\\\)''", "null")
                 .replace("@and",   "&&")
                 .replace("@gteq",  ">=")
                 .replace("@lteq",  "<=")
                 .replace("@lt",    "<")
                 .replace("@gt",    ">")
                 .replace("@or",    "||")
                 .replace("@eqic",  "^=")
                 .replace("@neqic", ".=")
                 .replace("@re",    "~=")
                 .replace("@feq",   "%=");

        return new Expression(raw);
    }

    public Expression(String expression) {
        this(expression, false);

        // Cache the variable list for the head Expression only
        this.variables = this.getVariables();
    }

    protected Expression(String expression, boolean neg) {
        if (expression == null) {
            throw new SyntaxException("A null expression cannot be evaluated");
        }

        expression = expression.trim();

        if (expression.length() == 0) {
            throw new SyntaxException("An empty expression cannot be evaluated");
        }

        this.negated = neg;

        //  First, extract all of the functions and put in placeholders
        List<String> functions = new ArrayList<String>();
        Pattern functionPattern = Pattern.compile("\\:[A-Z]+\\([^)]+\\)");
        Matcher functionMatcher = functionPattern.matcher(expression);

        // Match all of the functions
        while (functionMatcher.find()) {
            String function = functionMatcher.group();

            // Add the function to the function list
            functions.add(function);

            // Replace the function with the placeholder
            expression = expression.replace(function, StringUtils.combine("~",
                    Integer.toString(functions.size() - 1)));
        }


        // Next, extract all constant values and put in placeholders
        List<String> constants  = new ArrayList<String>();
        Pattern constantPattern = Pattern.compile(
                "(?:" +                        // Look for the operator first (we only
                  "(?:" +                      // want the right hand side literals)
                    "[<>]" +                   // It's just < or >
                    "|" +                      // OR
                    "[.^=<>!~]=" +             // .=, ^=, <=, >=, !=, or ~=
                  ")" +                        //
                  "\\s*" +                     // Followed by optional whitespace
                ")" +                          //
                "(" +                          //
                  "'(?:[^']|\\\\')+'" +        // Literals are enclosed in single quotes
                  "|" +                        // OR
                  "(?:" +                      //
                    "-?[0-9]+(?:\\.[0-9]+)?" + // They just look like numbers
                  ")" +                        //
                  "|" +                        // OR
                  "null" +                     // They're null
                ")"                            //
        );
        Matcher constantMatcher = constantPattern.matcher(expression);

        // Match all of the constants (this actually will typically run for more times
        // than necessary, but that's probably better than resetting?)
        while (constantMatcher.find()) {
            String constant = constantMatcher.group(1);

            // Add the constant to the constant list
            constants.add(constant);

            // Replace the constant with the placeholder
            expression = expression.replace(constant, StringUtils.combine("$",
                    Integer.toString(constants.size() - 1)));
        }

        int start  = -1;
        int finish = -1;
        int opened = 0;
        boolean tracking = false;

        String originalExpression = expression;
        List<String> expressions  = new ArrayList<String>();
        Pattern enclosurePattern  = Pattern.compile("\\!?\\(|\\)");
        Matcher enclosureMatcher  = enclosurePattern.matcher(expression);

        while (enclosureMatcher.find()) {
            String match = enclosureMatcher.group();

            if (tracking) {
                // Keep track of parenthesis
                if (!match.equals(")")) {
                    ++opened;
                } else {
                    --opened;
                }

                if (opened == 0) {
                    finish = enclosureMatcher.end();

                    // Get the subexpression
                    String subexpression = originalExpression.substring(start, finish);

                    // Add the subexpression to the expression list
                    expressions.add(subexpression);

                    // Replace the subexpression with the placeholder
                    expression = expression.replaceFirst(Pattern.quote(subexpression),
                            StringUtils.combine("%",
                                    Integer.toString(expressions.size() - 1)));

                    tracking = false;
                } else if (opened < 0) {
                    throw new SyntaxException("Misplaced closing parenthesis");
                }
            } else {
                if (!match.equals(")")) {
                    ++opened;
                    start  = enclosureMatcher.start();
                    tracking = true;
                } else {
                    throw new SyntaxException("Misplaced closing parenthesis");
                }
            }
        }

        Pattern operatorPattern = Pattern.compile("\\|{2}|\\&{2}");
        Matcher operatorMatcher = operatorPattern.matcher(expression);

        while (operatorMatcher.find()) {
            String operator = operatorMatcher.group();

            if (operator.equals("&&")) {
                this.operatorStack.add(Operator.AND);
            } else {
                this.operatorStack.add(Operator.OR);
            }
        }

        String[] components = expression.split("\\|{2}|\\&{2}");

        for (String component : components) {
            boolean isExpression = false;
            component = component.trim();

            if (component.startsWith("%")) {
                isExpression = true;

                int index = Integer.parseInt(component.substring(1));
                component = expressions.get(index);
            }

            // Replace all of the function placeholders (work backwards so $1 doesn't
            // replace $10, etc.)
            for (int j = functions.size() - 1; j > -1; --j) {
                component = component.replaceAll(Pattern.quote(StringUtils.combine("~",
                        Integer.toString(j))), functions.get(j));
            }

            // Replace all of the constant placeholders (work backwards so $1 doesn't
            // replace $10, etc.)
            for (int j = constants.size() - 1; j > -1; --j) {
                component = component.replaceAll(
                    Pattern.quote(StringUtils.combine("$", Integer.toString(j))),
                    Matcher.quoteReplacement(constants.get(j))
                );
            }

            if (isExpression) {
                boolean isNegated = component.startsWith("!");

                // Remove the expression enclosure
                component = component.substring(component.indexOf('(') + 1,
                        component.lastIndexOf(')'));

                this.evaluationStack.add(new Expression(component, isNegated));
            } else {
                this.evaluationStack.add(Comparison.createComparison(component));
            }
        }
    }

    public boolean evaluate(DataRecord record) {
        boolean result = true;
        int stackSize  = this.evaluationStack.size();

        for (int i = 0; i < stackSize; ++i) {
            Evaluable subresult = this.evaluationStack.get(i);

            if (i == 0) {
                result = subresult.evaluate(record);
            } else {
                if (this.operatorStack.get(i - 1) == Operator.AND) {
                    if (result) {
                        result = subresult.evaluate(record);
                    }
                } else {
                    if (result) {
                        // Anything to the right of a logical OR with left-hand true can
                        // be short-circuited
                        break;
                    } else {
                        if (i < this.operatorStack.size() &&
                                this.operatorStack.get(i) == Operator.AND) {
                            boolean combinedResult = false;
                            int startPosition = i;

                            for (int j = startPosition; j < this.operatorStack.size() &&
                                    this.operatorStack.get(j) == Operator.AND; ++j,
                                    i = j + 1) {
                                Evaluable subsubresult = this.evaluationStack.get(j + 1);

                                if (j == startPosition) {
                                    combinedResult = subresult.evaluate(record) &&
                                            subsubresult.evaluate(record);
                                } else {
                                    if (combinedResult) {
                                        combinedResult = subsubresult.evaluate(record);
                                    }
                                }
                            }

                            result = combinedResult;
                        } else {
                            result = subresult.evaluate(record);
                        }
                    }
                }
            }
        }

        return this.negated ? !result : result;
    }

    public String toString() {
        String result = "";
        int stackSize = this.evaluationStack.size();

        for (int i = 0; i < stackSize; ++i) {
            if (i == 0) {
                result += this.evaluationStack.get(i);
            } else {
                result += " " + this.operatorStack.get(i - 1) + " " +
                        this.evaluationStack.get(i);
            }
        }

        return (this.negated ? "!" : "") + "(" + result + ")";
    }

    public Set<String> getVariables() {
        // Return the cache, if we have it
        if (this.variables != null) {
            return this.variables;
        }

        Set<String> variables = new HashSet<String>();

        for (Evaluable level : this.evaluationStack) {
            variables.addAll(level.getVariables());
        }

        return variables;
    }
}
