/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import java.util.*;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.BitSet;
import org.opencdisc.validator.util.DataGrouping;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public class UniqueValueValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {"Variable"};

    private final Map<DataEntry, DataEntry> interns =
        new HashMap<DataEntry, DataEntry>();
    private final Map<DataGrouping, DataGrouping> groupings =
        new HashMap<DataGrouping, DataGrouping>();
    private final boolean matching;
    private final String variable;
    private final String[] groups;

    /**
     *
     * @param ruleDefinition
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    public UniqueValueValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Record, REQUIRED_VARIABLES, token, metrics);

        this.matching = ruleDefinition.getProperty("Matching").equalsIgnoreCase("Yes");

        if (ruleDefinition.hasProperty("When")) {
            super.prepareExpression(ruleDefinition.getProperty("When"));
        }

        this.variable = ruleDefinition.getProperty("Variable").toUpperCase();

        super.addVariable(this.variable);

        if (ruleDefinition.hasProperty("GroupBy")) {
            String[] groups = ruleDefinition.getProperty("GroupBy").split(",");
            this.groups = new String[groups.length];

            for (int i = 0; i < groups.length; ++i) {
                this.groups[i] = groups[i].trim().toUpperCase();

                super.addVariable(this.groups[i]);
            }
        } else {
            this.groups = new String[0];
        }
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        int count = this.groups.length;
        DataEntry[] group = new DataEntry[Math.max(count, 1)];
        DataEntry value = this.intern(dataRecord.getValue(this.variable));

        if (count != 0) {
            for (int i = 0; i < count; ++i) {
                group[i] = this.intern(dataRecord.getValue(this.groups[i]));
            }
        } else {
            group[0] = value;
        }

        DataGrouping search = new DataGrouping(group);
        DataGrouping result = this.groupings.get(search);

        if (result == null && (!this.matching || count > 0 || this.groupings.size() == 0)) {
            if (this.matching) {
                result = new MatchingDataGrouping(search);
            } else {
                result = new UniqueDataGrouping(search);
            }

            this.groupings.put(result, result);
        }

        return (byte)(result != null && result.accepts(value) ? 1 : 0);
    }

    protected List<Outcome> performDatasetValidation(EntityDetails entity) {
        this.interns.clear();
        this.groupings.clear();

        return super.performDatasetValidation(entity);
    }

    private DataEntry intern(DataEntry entry) {
        DataEntry intern = this.interns.get(entry);

        if (intern == null) {
            this.interns.put(entry, intern = entry);
        }

        return intern;
    }

    private static class UniqueDataGrouping extends DataGrouping {
        private final Set<DataEntry> values = new HashSet<DataEntry>();

        UniqueDataGrouping(DataGrouping grouping) {
            super(grouping);
        }

        public boolean accepts(DataEntry entry) {
            return this.values.add(entry);
        }
    }

    private static class MatchingDataGrouping extends DataGrouping {
        private DataEntry value;

        MatchingDataGrouping(DataGrouping grouping) {
            super(grouping);
        }

        public boolean accepts(DataEntry entry) {
            if (this.value == null) {
                this.value = entry;

                return true;
            }

            return this.value.equals(entry);
        }
    }
}
