/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.expressions.functions.Function;
import org.opencdisc.validator.rules.expressions.functions.Functions;
import org.opencdisc.validator.util.Settings;

/**
 *
 * @author Tim Stone
 */
class Comparison implements Evaluable {
    private static final BigDecimal FUZZY_TOLERANCE = new BigDecimal(
        Settings.get("Engine.FuzzyTolerance", "0.0025")
    );
    // TODO: This seems rather arbitrary...? Should check the sanity in this
    private static final BigDecimal FUZZY_ZERO_TOLERANCE = FUZZY_TOLERANCE.pow(2);
    private static final String VARIABLE_PATTERN = "[A-Za-z][A-Za-z0-9]*|(?:VAL|SUB):[A-Za-z0-9]+";

    private final String lhs;
    private final String rhs;
    private final DataEntry constant;
    private final String operator;
    private final Function function;
    private final boolean equal;
    private final boolean greater;
    private final boolean less;

    static Evaluable createComparison(String expression) {
        Evaluable comparison = null;

        expression = expression.trim();

        Pattern comparisonPattern = Pattern.compile(
            "(" + VARIABLE_PATTERN + ")" +
            "(?:" +
              "\\s*" +
              "([<>]|[%\\.\\^=<>!~]=)" +
              "\\s*" +
            ")" +
            "(" +
              "(?:" + VARIABLE_PATTERN + ")" +
              "|" +
              "'(?:[^']|\\\\')+'" +
              "|" +
              "(?:-?[0-9]+(?:\\.[0-9]+)?)" +
              "|" +
              "null" +
              "|" +
              "\\:[A-Z]+\\([^)]+\\)" +
            ")"
        );
        Matcher comparisonMatcher = comparisonPattern.matcher(expression);

        // TODO: Provide more useful information about the location/type of syntax error
        if (!comparisonMatcher.matches() || comparisonMatcher.groupCount() != 3) {
            throw new SyntaxException(String.format(
                "The comparison clause %s not properly formatted",
                expression
            ));
        }

        String lhs = comparisonMatcher.group(1);
        String rhs = comparisonMatcher.group(3);
        String operator = comparisonMatcher.group(2);

        // Replace escaped single quotes
        rhs = rhs.replace("\\'", "'");

        if (rhs.equals("null")) {
            boolean isNegated = false;

            // This is a NullComparison
            if (operator.equals("!=")) {
                isNegated = true;
            } else if (operator.equals("==")) {
                isNegated = false;
            } else {
                throw new SyntaxException(String.format(
                    "The comparison clause %s attempts to compare the variable %s "
                    + "to null via an incompatible operator (%s)",
                    expression,
                    lhs,
                    operator
                ));
            }

            comparison = new NullComparison(lhs, isNegated);
        } else if (operator.equals("~=")) {
            comparison = new RegexComparison(lhs, rhs.substring(1, rhs.length() - 1));
        } else {
            comparison = new Comparison(lhs, rhs, operator);
        }

        return comparison;
    }

    private Comparison(String lhs, String rhs, String operator) {
        this.lhs = lhs;
        this.operator = operator;
        this.equal =
            operator.equals("<=") || operator.equals(">=") || operator.equals("==");
        this.greater =
            operator.equals(">=") || operator.equals(">") || operator.equals("!=");
        this.less =
            operator.equals("<=") || operator.equals("<") || operator.equals("!=");

        if (rhs.matches(VARIABLE_PATTERN)) {
            // The right hand side is a variable
            this.rhs = rhs;
            this.constant = null;
            this.function = null;
        } else if (rhs.startsWith(":")) {
            // The right hand side is a function
            this.rhs = null;
            this.constant = null;
            this.function = Functions.create(rhs);
        } else {
            // The right hand side is a constant
            this.rhs = null;

            if (rhs.startsWith("'")) {
                rhs = rhs.substring(1, rhs.length() - 1);
            }

            this.constant = new DataEntry(rhs);
            this.function = null;
        }
    }

    public boolean evaluate(DataRecord record) {
        boolean result;
        DataEntry lhs = record.getValue(this.lhs);
        DataEntry rhs;
        String operator = this.operator;
        int comparison;

        if (this.constant == null) {
            if (this.function == null) {
                rhs = record.getValue(this.rhs);
            } else {
                rhs = this.function.compute(record);
            }
        } else {
            rhs = this.constant;
        }

        if(operator.equals("^=") || operator.equals(".=")) {
            comparison = lhs.compareTo(rhs, false);
            result = operator.equals("^=") ? comparison == 0 : comparison != 0;
        } else if (operator.equals("%=")) {
            if (lhs.isNumeric() && rhs.isNumeric()) {
                BigDecimal lhsValue = (BigDecimal)lhs.getValue();
                BigDecimal rhsValue = (BigDecimal)rhs.getValue();

                if (rhsValue.compareTo(BigDecimal.ZERO) != 0) {
                    result = BigDecimal.ONE.subtract(
                        lhsValue.divide(rhsValue, Function.DIVISION_SCALE, RoundingMode.HALF_UP)
                    ).abs().compareTo(FUZZY_TOLERANCE) < 0;

                } else {
                    result = lhsValue.abs().compareTo(FUZZY_ZERO_TOLERANCE) < 0;
                }
            } else {
                // Fall back to normal equality if the two sides aren't numbers
                result = lhs.compareTo(rhs, true) == 0;
            }
        } else {
            comparison = lhs.compareTo(rhs, true);

            if (comparison == 0) {
                result = this.equal;
            } else if (comparison > 0) {
                result = this.greater;
            } else {
                result = this.less;
            }
        }

        return result;
    }

    public Set<String> getVariables() {
        Set<String> variables = new HashSet<String>();

        variables.add(this.lhs);

        if (this.rhs != null) {
            variables.add(this.rhs);
        } else if (this.function != null) {
            variables.addAll(this.function.getVariables());
        }

        return variables;
    }

    public String toString() {
        String rhs = this.rhs;

        if (this.constant != null) {
            rhs = "'" + this.constant.toString() + "'";
        } else if (this.function != null) {
            rhs = this.function.toString();
        }

        return String.format(
                "%s %s %s",
                this.lhs,
                this.operator,
                rhs
        );
    }

    private static class RegexComparison implements Evaluable {
        private final String lhs;
        private final Pattern pattern;
        private Matcher matcher = null;

        public RegexComparison(String lhs, String rhs) {
            this.lhs = lhs;
            this.pattern = Pattern.compile(rhs);
        }

        public boolean evaluate(DataRecord record) {
            DataEntry entry = record.getValue(this.lhs);
            String value = entry.toString();

            if (this.matcher == null) {
                this.matcher = this.pattern.matcher(value);
            } else {
                this.matcher.reset(value);
            }

            return this.matcher.matches();
        }

        public Set<String> getVariables() {
            return new HashSet<String>(Arrays.asList(this.lhs));
        }

        public String toString() {
            return String.format("%s ~= '%s'", this.lhs, this.pattern.pattern());
        }
    }

    private static class NullComparison implements Evaluable {
        private final String lhs;
        private final boolean isNegated;

        public NullComparison(String lhs, boolean isNegated) {
            this.lhs = lhs;
            this.isNegated = isNegated;
        }

        public boolean evaluate(DataRecord record) {
            DataEntry entry = record.getValue(this.lhs);

            return this.isNegated ? entry.hasValue() : !entry.hasValue();
        }

        public Set<String> getVariables() {
            return new HashSet<String>(Arrays.asList(this.lhs));
        }

        public String toString() {
            return String.format("%s %s null", this.lhs, this.isNegated ? "!=" : "==");
        }
    }
}
