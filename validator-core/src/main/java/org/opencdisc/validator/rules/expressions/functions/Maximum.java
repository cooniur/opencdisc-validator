/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;

/**
 * @author Tim Stone
 */
public class Maximum extends AbstractFunction {
    Maximum(String name, String[] arguments) {
        super(name, arguments, 1, true);
    }

    /**
     *
     * @param record  the source data record
     * @return
     */
    public DataEntry compute(DataRecord record) {
        DataEntry maximum = null;

        for (String variable : this.variables) {
            DataEntry current = record.getValue(variable);

            if (maximum == null || maximum.compareTo(current) < 0) {
                maximum = current;
            }
        }

        return maximum;
    }
}
