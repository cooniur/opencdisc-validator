/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules.expressions.functions;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.opencdisc.validator.Text;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.rules.expressions.EvaluationException;

/**
 * @author Tim Stone
 */
public class DyCount extends AbstractFunction {
    DyCount(String name, String[] arguments) {
        super(name, arguments, 2);
    }

    /**
     *
     * @param record  the source data record
     * @return
     */
    public DataEntry compute(DataRecord record) {
        DataEntry reference = record.getValue(this.arguments[0]);
        DataEntry recorded = record.getValue(this.arguments[1]);

        if (!reference.hasValue() || !reference.isDate()) {
            throw new EvaluationException(
                Text.get("Messages.InvalidDate"),
                String.format(
                    Text.get("Descriptions.InvalidDate"),
                    this.arguments[0], reference.getDataType().toString()
                )
            );
        }

        if (!recorded.hasValue() || !recorded.isDate()) {
            throw new EvaluationException(
                Text.get("Messages.InvalidDate"),
                String.format(
                    Text.get("Descriptions.InvalidDate"),
                    this.arguments[1], recorded.getDataType().toString()
                )
            );
        }

        int days = Days.daysBetween(
            DateTime.parse(reference.toString().substring(0, "YYYY-MM-DD".length())),
            DateTime.parse(recorded.toString().substring(0, "YYYY-MM-DD".length()))
        ).getDays();

        return new DataEntry(days < 0 ? days : days + 1);
    }
}
