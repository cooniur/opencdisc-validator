/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.rules;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

/**
 *
 * @author Tim Stone
 */
public class FindValidationRule extends AbstractScriptableValidationRule {
    private static final String DEFAULT_DELIMITER = ",";
    private static final String[] REQUIRED_VARIABLES = new String[] { "Variable" };

    private Set<String> terms = new HashSet<String>();
    private boolean isCaseSensitive = true;
    private final int matchCount;
    private final int ifStatements;
    private final boolean matchExact;
    private int counter = 0;
    private final String variable;
    private final String value;
    private final Pattern test;
    private final String[] groups;
    private final Map<DataGrouping, DataGrouping> groupings =
        new HashMap<DataGrouping, DataGrouping>();

    /**
     *
     * @param ruleDefinition
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    public FindValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Dataset, REQUIRED_VARIABLES, token, metrics);

        this.variable = ruleDefinition.getProperty("Variable").toUpperCase();

        super.addVariable(this.variable);

        if (ruleDefinition.hasProperty("Terms") == ruleDefinition.hasProperty("Test")) {
            throw new ConfigurationException(
                ConfigurationException.Type.RuleDefinition,
                "Find rules must have one of (Terms, Test), and cannot have both."
            );
        }

        String storeValue = "";
        String delimiter = DEFAULT_DELIMITER;
        int matchCount = -1;

        if (ruleDefinition.hasProperty("Match")) {
            String value = ruleDefinition.getProperty("Match");

            if (value.equalsIgnoreCase("One")) {
                matchCount = 1;
            } else {
                try {
                    matchCount = Integer.parseInt(value);
                } catch (NumberFormatException ex) {
                    throw new ConfigurationException(
                        ConfigurationException.Type.RuleDefinition,
                        "Invalid value for Match attribute, expected a number or 'one'",
                        ex
                    );
                }
            }
        }

        this.matchExact = ruleDefinition.getProperty("MatchExact").equalsIgnoreCase("Yes");

        if (ruleDefinition.hasProperty("CaseSensitive")) {
            if (ruleDefinition.getProperty("CaseSensitive").equalsIgnoreCase("No")) {
                this.isCaseSensitive = false;
            }
        }

        if (ruleDefinition.hasProperty("Terms")) {
            storeValue = ruleDefinition.getProperty("Terms");

            if (ruleDefinition.hasProperty("Delimiter")) {
                delimiter = ruleDefinition.getProperty("Delimiter");
            }

            String[] matchValues = ruleDefinition.getProperty("Terms").split(
                Pattern.quote(delimiter)
            );

            for (String value : matchValues) {
                if (!this.isCaseSensitive) {
                    value = value.toUpperCase();
                }

                this.terms.add(value.trim());
            }

            this.counter = this.terms.size();

            if (matchCount < 0) {
                matchCount = this.counter;
            }

            this.test = null;
        } else {
            try {
                int mask = this.isCaseSensitive ? 0 : Pattern.CASE_INSENSITIVE;

                this.test = Pattern.compile(ruleDefinition.getProperty("Test"), mask);
            } catch (PatternSyntaxException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    "Value for Test attribute is an invalid regular expression",
                    ex
                );
            }

            if (matchCount < 0) {
                matchCount = 1;
            }
        }

        if (ruleDefinition.hasProperty("If")) {
            String[] tests = ruleDefinition.getProperty("If").split("@iand");

            for (int i = 0; i < tests.length; ++i) {
                super.prepareExpression("IF" + i, tests[i]);
            }

            this.ifStatements = tests.length;
        } else {
            this.ifStatements = 0;
        }

        if (ruleDefinition.hasProperty("GroupBy")) {
            String[] groups = ruleDefinition.getProperty("GroupBy").split(",");
            this.groups = new String[groups.length];

            for (int i = 0; i < groups.length; ++i) {
                this.groups[i] = groups[i].trim().toUpperCase();

                super.addVariable(this.groups[i]);
            }
        } else {
            this.groups = new String[0];

            DataGrouping group = new DataGrouping(
                new DataEntry[0], this.counter, this.ifStatements, this.terms
            );

            this.groupings.put(group, group);
        }

        this.matchCount = matchCount;

        if (ruleDefinition.hasProperty("When")) {
            super.prepareExpression(ruleDefinition.getProperty("When"));
        }

        this.value = storeValue;
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        int count = this.groups.length;
        DataEntry[] group = new DataEntry[count];

        for (int i = 0; i < count; ++i) {
            group[i] = dataRecord.getValue(this.groups[i]);
        }

        DataGrouping search = new DataGrouping(
            group, this.counter, this.ifStatements, this.terms
        );
        DataGrouping result = this.groupings.get(search);

        if (result == null) {
            this.groupings.put(search, result = search);
        }

        if (super.hasExpression("IF0") && !result.isActivated()) {
            for (int i = 0; i < this.ifStatements; ++i) {
                if (!result.isActivated(i) && super.checkExpression(dataRecord, "IF" + i)) {
                    result.activate(i);
                }
            }
        }

        DataEntry entry = dataRecord.getValue(this.variable);

        if (super.checkExpression(dataRecord) && entry.hasValue()) {
            String value = entry.toString();

            if (this.test == null) {
                if (!this.isCaseSensitive) {
                    value = value.toUpperCase();
                }

                if (result.terms.contains(value)) {
                    result.terms.remove(value);
                }
            } else {
                if (this.test.matcher(value).matches()) {
                    ++result.counter;
                }
            }
        }

        return -1;
    }

    protected List<Outcome> performDatasetValidation(EntityDetails entity) {
        List<Outcome> results = new ArrayList<Outcome>();

        for (DataGrouping grouping : this.groupings.keySet()) {
            if (!super.hasExpression("IF0") || grouping.isActivated()) {
                boolean result = true;

                if (this.test == null) {
                    if (!this.matchExact) {
                        if (grouping.counter - this.matchCount < grouping.terms.size()) {
                            result = false;
                        }
                    } else {
                        if (grouping.counter - this.matchCount != grouping.terms.size()) {
                            result = false;
                        }
                    }
                } else {
                    if (!this.matchExact) {
                        if (grouping.counter < this.matchCount) {
                            result = false;
                        }
                    } else {
                        if (grouping.counter != this.matchCount) {
                            result = false;
                        }
                    }
                }

                Outcome outcome = new Outcome((byte)(result ? 1 : 0));

                outcome.display.put(this.variable, this.value);

                for (int i = 0; i < this.groups.length; ++i) {
                    outcome.display.put(this.groups[i], grouping.group[i].toString());
                }

                results.add(outcome);
            }
        }

        if (results.isEmpty()) {
            return super.performDatasetValidation(entity);
        }

        return results;
    }

    private static class DataGrouping {
        private final DataEntry[] group;
        private final int hashCode;
        private final boolean[] activated;
        private boolean isActivated;
        final Set<String> terms;
        int counter;

        DataGrouping(DataEntry[] group, int counter, int conditions, Set<String> terms) {
            this.group = group;
            this.activated = new boolean[conditions];

            HashCodeBuilder builder = new HashCodeBuilder(15, 97);

            for (DataEntry entry : group) {
                builder.append(entry);
            }

            this.hashCode = builder.toHashCode();
            this.counter = counter;
            this.terms = new HashSet<String>(terms);
        }

        @Override
        public int hashCode() {
            return this.hashCode;
        }

        @Override
        public boolean equals(Object o) {
            if (o == null || !(o instanceof DataGrouping)) {
                return false;
            }

            if (o == this) {
                return true;
            }

            return new EqualsBuilder().append(
                this.group, ((DataGrouping)o).group
            ).isEquals();
        }

        void activate(int i) {
            this.activated[i] = true;

            for (boolean isActive : this.activated) {
                if (!isActive) {
                    return;
                }
            }

            this.isActivated = true;
        }

        boolean isActivated() {
            return this.isActivated;
        }

        boolean isActivated(int i) {
            return this.activated[i];
        }
    }
}
