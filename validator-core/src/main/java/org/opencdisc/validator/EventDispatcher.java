/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

import java.util.LinkedHashSet;
import java.util.Set;

import org.opencdisc.validator.ValidatorEvent.State;

/**
 *
 * @author Tim Stone
 */
public class EventDispatcher {
    private final Set<ValidatorListener> listeners =
        new LinkedHashSet<ValidatorListener>();

    public synchronized void add(ValidatorListener listener) {
        this.listeners.add(listener);
    }

    public synchronized void clear() {
        this.listeners.clear();
    }

    public void fireParsing(String configurationPath) {
        this.dispatchUpdated(new ValidatorEvent(State.Parsing, configurationPath));
    }

    public void fireTaskStart(String taskName, long currentTask, long totalTasks) {
        this.dispatchUpdated(new ValidatorEvent(State.Processing, currentTask,
                totalTasks, taskName));
    }

    public void fireTaskIncrement(String taskName, long currentTask, long totalTasks,
            long currentIncrement) {
        ValidatorEvent event = new ValidatorEvent(State.Processing, currentTask,
                totalTasks, taskName);

        event.setSubevent(new ValidatorEvent(State.Processing, currentIncrement));

        this.dispatchUpdated(event);
    }

    public void fireTaskCompleted(String taskName, long currentTask, long totalTasks) {
        this.dispatchUpdated(new ValidatorEvent(State.Completed, currentTask, totalTasks,
                taskName));
    }

    public void fireSubtaskIncrement(String subtaskName, int currentIncrement) {
        this.dispatchUpdated(new ValidatorEvent(State.Subprocessing, currentIncrement,
                subtaskName));
    }

    public void fireGenerating(String resultPath) {
        this.dispatchUpdated(new ValidatorEvent(State.Generating, resultPath));
    }

    public void fireStarted() {
        this.dispatchStarted(new ValidatorEvent());
    }

    public void fireStopped() {
        this.dispatchStopped(new ValidatorEvent());
    }

    public synchronized boolean remove(ValidatorListener listener) {
        return this.listeners.remove(listener);
    }

    private synchronized Set<ValidatorListener> copyListeners() {
        return new LinkedHashSet<ValidatorListener>(this.listeners);
    }

    private void dispatchUpdated(ValidatorEvent event) {
        Set<ValidatorListener> listeners = this.copyListeners();

        for (ValidatorListener listener : listeners) {
            synchronized (listener) {
                listener.validatorProgressUpdated(event);
            }
        }
    }

    private void dispatchStarted(ValidatorEvent event) {
        Set<ValidatorListener> listeners = this.copyListeners();

        for (ValidatorListener listener : listeners) {
            synchronized (listener) {
                listener.validatorStarted(event);
            }
        }
    }

    private void dispatchStopped(ValidatorEvent event) {
        Set<ValidatorListener> listeners = this.copyListeners();

        for (ValidatorListener listener : listeners) {
            synchronized (listener) {
                listener.validatorStopped(event);
            }
        }
    }
}
