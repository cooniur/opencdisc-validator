/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import java.lang.reflect.Constructor;
import java.util.*;

import org.opencdisc.validator.model.EntityDetails.Reference;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.report.MessageTemplate;
import org.opencdisc.validator.rules.*;
import org.opencdisc.validator.settings.ConfigurationException.Type;
import org.opencdisc.validator.settings.Definition.Target;
import org.opencdisc.validator.settings.MagicVariableParser.MagicProperty;
import org.opencdisc.validator.util.Helpers;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.ProcessToken;
import org.opencdisc.validator.util.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tim Stone
 */
public class ConfigurationManager {
    private final static Logger LOG = LoggerFactory.getLogger(
        ConfigurationManager.class
	);

    static final Map<String, Class<? extends ValidationRule>> RULE_TYPES =
        new KeyMap<Class<? extends ValidationRule>>();

    static {
        RULE_TYPES.put("Condition", ConditionalValidationRule.class);
        RULE_TYPES.put("Find",      FindValidationRule.class);
        RULE_TYPES.put("Lookup",    LookupValidationRule.class);
        RULE_TYPES.put("Match",     MatchValidationRule.class);
        RULE_TYPES.put("Metadata",  MetadataValidationRule.class);
        RULE_TYPES.put("Unique",    UniqueValueValidationRule.class);
        RULE_TYPES.put("Regex",     RegularExpressionValidationRule.class);
        RULE_TYPES.put("Required",  ConditionalRequiredValidationRule.class);

        List<Class<ValidationRule>> external = Helpers.find(ValidationRule.class);

        for (Class<ValidationRule> type : external) {
            if (type.isAnnotationPresent(ValidationRule.RuleAssociation.class)) {
                ValidationRule.RuleAssociation annotation = type.getAnnotation(
                    ValidationRule.RuleAssociation.class
                );

                String association = annotation.value();

                if (!RULE_TYPES.containsKey(association)) {
                    RULE_TYPES.put(association, type);

                    LOG.debug(
                        "Registred {} as rule association {}",
                        type,
                        association
                    );
                } else {
                    LOG.error(
                        "Cannot register {} as rule association {} because "
                        + "the association is already defined",
                        type,
                        association
                    );
                }
            }
        }
    }

    private final Set<ConfigurationMetadata> metadata = new HashSet<ConfigurationMetadata>();
    private final Map<String, Template> configurations = new KeyMap<Template>();
    private final List<MessageTemplate> messages = new ArrayList<MessageTemplate>();
    private final Map<String, Template> prototypes = new KeyMap<Template>();
    private final ProcessToken token;
    private final RuleMetrics metrics;
    private final Map<String, String> defaults;

    public ConfigurationManager(ProcessToken token, Map<String, String> defaults) {
        this.token = token;
        this.metrics = Settings.defines("Engine.GenerateRuleMetrics") ?
            new RuleMetrics() : null;
        this.defaults = defaults;
    }

    void complete() {
        for(Template configuration : this.configurations.values()) {
            configuration.complete();
        }
    }

    void define(Template configuration) {
        this.configurations.put(configuration.getTargetName(), configuration);
    }

    public boolean defines(String name) {
        return this.configurations.containsKey(name);
    }

    Template getConfiguration(String name) {
        return this.configurations.get(name);
    }

    public List<MessageTemplate> getMessages() {
        return this.messages;
    }

    public Set<ConfigurationMetadata> getMetadata() {
        return this.metadata;
    }

    public RuleMetrics getMetrics() {
        return this.metrics;
    }

    Template getPrototype(String name) {
        return this.prototypes.get(name);
    }

    void register(ConfigurationMetadata metadata) {
        this.metadata.add(metadata);
    }

    void store(Definition rule) {
        Message.Type type = null;
        Message.Severity severity = null;

        if (rule.hasProperty("Type")) {
            type = Message.Type.fromString(rule.getProperty("Type"));
        }

        if (rule.hasProperty("Severity")) {
            severity = Message.severityFromString(rule.getProperty("Severity"));
        }

        this.messages.add(new MessageTemplate(
            rule.getProperty("ID"),
            rule.getProperty("PublisherID"),
            humanize(rule.getProperty("Message")),
            humanize(rule.getProperty("Description")),
            rule.getProperty("Category"),
            type,
            severity
        ));
    }

    public void declare(Set<String> available) {
        for (String name : available) {
            Template configuration = this.configurations.get(name);

            if (configuration != null) {
                configuration.setProperty("Present", "Y");
            }
        }
    }

    public Configuration prepare(String name, Set<String> variables,
            boolean createRules) {
        Template template = null;
        String ruleTemplateName = name;

        if (this.configurations.containsKey(name)) {
            // The configuration is named
            template = this.configurations.get(name);
        }

        if ((template == null || !template.isConfiguration()) && variables != null) {
            Template prototype = null;
            int matches = 0;

            // The configuration needs prototyping, for one reason or another
            for (Template candidate : this.prototypes.values()) {
                int count = candidate.matches(name, variables);

                if (count != 0 && (matches == 0 || count > matches)) {
                    matches = count;
                    prototype = candidate;
                }
            }

            if (prototype != null) {
                if (template == null) {
                    template = prototype;
                } else {
                    // Copy over the rules
                    for (Reference target : Reference.values()) {
                        for (Definition rule : prototype.getRules(target)) {
                            template.defineRule(target, rule);
                        }
                    }

                    // Supplement the variable information
                    prototype.updateVariables(template);

                    // Mark it as configured now so that we avoid the risk of
                    // repeating this step on a future iteration
                    template.setConfigured(true);
                    template.complete();
                }

                ruleTemplateName = prototype.getProperty("Name").toUpperCase();
            }
        }

        Configuration configuration = null;

        // It's possible that we weren't able to create anything from the prototype either,
        // so make sure we only create a Configuration for a configured Template
        if (template != null && template.isConfiguration()) {
            configuration = template.createFrom(name, variables);

            if (createRules) {
                // Now go through the process of creating the rules
                for (Reference target : Reference.values()) {
                    Set<Definition> definitions = template.getRules(target);

                    for (Definition definition : definitions) {
                        for (ValidationRule rule : this.prepareRule(definition,
                                configuration, false)) {
                            configuration.defineRule(target, rule);
                        }
                    }
                }

                for (Definition definition : template.getFilters()) {
                    for (ValidationRule filter : this.prepareRule(definition,
                            configuration, true)) {
                        configuration.defineFilter(filter);
                    }
                }
            }
        }

        if (configuration != null) {
            configuration.setProperty("Configuration", ruleTemplateName);
        }

        return configuration;
    }

    void prototype(Template prototype) {
        this.prototypes.put(prototype.getTargetName(), prototype);
    }

    public boolean prototypes(String name) {
        return this.prototypes.containsKey(name);
    }

    private static String humanize(String text) {
        if (text == null || text.length() == 0) {
            return text;
        }

        return text.replaceAll("%[^%\\s]+%", "--");
    }

    private ValidationRule instantiateRule(Definition rule, boolean isFilter) {
        ValidationRule instance = null;
        String ruleType = rule.getTargetName();

        if (!RULE_TYPES.containsKey(ruleType)) {
            LOG.error("Rule {} creation failed because no type {} is defined",
                rule.getProperty("ID"), ruleType);

            return instance;
        }

        try {
            Constructor<? extends ValidationRule> constructor;
            Class<?>[] constructorSignature = {
                Definition.class, ProcessToken.class, RuleMetrics.class
            };
            Class<? extends ValidationRule> ruleClass = RULE_TYPES.get(ruleType);

            if (isFilter) {
                rule.setProperty("DisableMessaging", "true");
            }

            constructor = ruleClass.getConstructor(constructorSignature);
            instance = constructor.newInstance(
                rule,
                this.token,
                !isFilter ? this.metrics : null
            );
        } catch (Exception ex) {
            Throwable cause = ex.getCause();

            // TODO: Improve configuration error handling
            LOG.error(
                "Rule {} creation failed because of an exception: {}",
                rule.getProperty("ID"),
                cause != null ? cause.getMessage() : "(unknown reason)"
            );

            throw new RuntimeException(
                String.format(
                    "Creation of rule %s failed due to exception",
                    rule.getProperty("ID")
                ),
                ex
            );
        }

        return instance;
    }

    private List<Definition> performMagic(Definition definition,
            Configuration configuration) throws ConfigurationException {
        List<Definition> definitions = new ArrayList<Definition>();

        definitions.add(definition);

        for (MagicProperty magicProperty : MagicProperty.values()) {
            List<Definition> templates = new ArrayList<Definition>(definitions);

            definitions.clear();

            for (Definition template : templates) {
                MagicVariableParser scope = new MagicVariableParser();
                MagicVariable magicVariable = null;

                // We actually only accept a single magic variable right now, but
                // theoretically we could allow multiple ones in the future...so set
                // ourselves up for that scenario so we can warn about lack of support if
                // someone tries to do it right now.
                for (String property : magicProperty.getProperties()) {
                    if (template.hasProperty(property)) {
                        List<MagicVariable> magicVariables = scope.parse(magicProperty,
                                String.format("%s.%s", template.getProperty("ID"),
                                        property),
                                template.getProperty(property));

                        if (magicVariables.size() > 1 || (magicVariables.size() != 0
                            && magicVariable != null)) {
                            throw new ConfigurationException(Type.RuleDefinition,
                                    "Only one magic variable per rule definition is"
                                    + " allowed");
                        }

                        if (magicVariables.size() > 0) {
                            magicVariable = magicVariables.get(0);
                        }
                    }
                }

                if (magicVariable != null) {
                    List<Definition> accepted = new ArrayList<Definition>();

                    if (magicProperty == MagicProperty.Variables) {
                        for (Definition candidate : configuration.getVariables()) {
                            if (magicVariable.matches(candidate)) {
                                accepted.add(candidate);
                            }
                        }
                    } else if (magicProperty == MagicProperty.Domains) {
                        for (String configurationName : this.configurations.keySet()) {
                            Definition candidate = this.configurations
                                    .get(configurationName);

                            // This is a bit of a hack to support %Domain% and
                            // %Domain.From% as they were originally designed
                            Definition proxy = new Definition(Target.Domain,
                                    candidate.getProperty("Name"));

                            proxy.setProperty("From", candidate.getProperty("Name"));
                            proxy.setProperty("Name", configuration.getTargetName());

                            candidate = Definition.createFrom(proxy, candidate);

                            if (magicVariable.matches(candidate)) {
                                accepted.add(candidate);
                            }
                        }
                    }

                    if (!accepted.isEmpty()) {
                        definitions.addAll(scope.prepare(template, magicVariable,
                                accepted, this.defaults));
                    }
                } else {
                    definitions.add(Definition.createFrom(template));
                }
            }
        }

        return definitions;
    }

    private List<ValidationRule> prepareRule(Definition rule,
			Configuration configuration, boolean isFilter) {
        // Replicate the rule right away
        rule = Definition.createFrom(rule);

        List<ValidationRule> rules = new ArrayList<ValidationRule>();

        if (rule.hasProperty("ActiveUnless")) {
            // TODO: This is still super hacky, we just use it for this special case
            String activeUnless = rule.getProperty("ActiveUnless");

            if (activeUnless.startsWith("%Variables[") &&
                    activeUnless.endsWith("].Define.WithCodeList%")) {
                // TODO: Figure out why we need to replace Domain here
                String variableName = activeUnless.substring("%Variables[".length(),
                        activeUnless.indexOf(']')).replace("%Domain%",
                                configuration.getTargetName());

                if (configuration.hasVariable(variableName)) {
                    Definition variable = configuration.getVariable(variableName);

                    if (variable.hasProperty("Define") &&
                            variable.hasProperty("CodeList")) {

                        return rules;
                    }
                }
            }
        }

        if (RULE_TYPES.containsKey(rule.getTargetName())) {
            String name = configuration.getTargetName();

            for (String property : rule.getProperties()) {
                rule.setProperty(property,
                        rule.getProperty(property).replace("%Domain%", name));
            }
        }

        try {
            for (Definition finalized : this.performMagic(rule, configuration)) {
                LOG.debug("Creating {} for {}: {}", new Object[] {
                    isFilter ? "filter" : "rule", configuration.getTargetName(), finalized
                });

                if (!isFilter && Settings.defines("Parser.AutoDisplayDomainKeys")) {
                    // TODO: We don't want this to happen for terminology, but we need a better way
                    if (!finalized.hasProperty("Display") &&
                            !finalized.getProperty("Category").equalsIgnoreCase("Terminology")) {
                        finalized.setProperty("Display", configuration.getProperty("DomainKeys"));
                    }
                }

                ValidationRule instance = this.instantiateRule(finalized, isFilter);

                if (instance != null) {
                    rules.add(instance);
                }
            }
        } catch (ConfigurationException ex) {
            LOG.debug("Rule creation failed: {}", ex.getMessage());

            // TODO: Improve configuration error handling
            throw new RuntimeException(ex);
        }

        return rules;
    }

    public static class ConfigurationMetadata {
        public final String standardName;
        public final String standardVersion;

        public ConfigurationMetadata(String standardName, String standardVersion) {
            this.standardName = standardName;
            this.standardVersion = standardVersion;
        }
    }
}
