/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.opencdisc.validator.model.EntityDetails.Reference;
import org.opencdisc.validator.util.KeyMap;

/**
 * @author Tim Stone
 */
class Template extends Definition {
    private boolean configured = false;
    private boolean defined = false;
    private final Set<Definition> filters = new HashSet<Definition>();
    private final Map<Reference, Set<Definition>> rules =
        new HashMap<Reference, Set<Definition>>();
    private final Set<Definition> templateVariables = new HashSet<Definition>();
    private final Map<String, Definition> variables = new KeyMap<Definition>();
    private String[] keys;
    private boolean isPrototype;

    Template(String name, String[] keys) {
        super(Definition.Target.Domain, name);

        this.keys = keys;
        this.isPrototype = keys != null;

        for (Reference target : Reference.values()) {
            this.rules.put(target, new LinkedHashSet<Definition>());
        }
    }

    Configuration createFrom(String name, Set<String> existing) {
        Configuration configuration = new Configuration(name);
        String prototypeName = this.getProperty("Name").toUpperCase();

        // If this configuration came from define.xml, only rely on that
        if (existing != null) {
            // Prepare the incoming variables
            Set<String> variables = new HashSet<String>();

            for (String variable : existing) {
                variables.add(variable.toUpperCase());
            }

            // Otherwise, we have to retain anything that's expected or required,
            // as well as anything that matches
            for (Definition variable : this.variables.values()) {
                String variableName = this.prepare(variable.getTargetName(), name);

                // Create a copy of the existing variable so we don't mess up the
                // template's version
                Definition replacementVariable = Definition.createFrom(
                    variableName,
                    variable
                );

                if (variables.contains(variableName)) {
                    replacementVariable.setProperty("Present", "Y");
                }

                // TODO: This seems very deliberate (we do it for the generator)
                if (this.isPrototype) {
                    if (replacementVariable.hasProperty("OID")) {
                        String OID = replacementVariable.getProperty("OID");

                        replacementVariable.setProperty("OID",
                                OID.replace(prototypeName, name));
                    }
                }

                configuration.defineVariable(replacementVariable);

                variables.remove(variableName);
            }

            // Then go ahead and match the template variables, if there are any
            if (!this.templateVariables.isEmpty()) {
                for (String variable : variables) {
                    VariableTemplate template = this.findVariableMatch(variable);

                    if (template != null) {
                        Definition replacementVariable = Definition.createFrom(
                            variable, template.template
                        );

                        // TODO: I'm not sure if I like intentionally doing this here...
                        if (replacementVariable.hasProperty("Name")) {
                            replacementVariable.setProperty("Name", variable);
                        }

                        replacementVariable.setProperty("Present", "Y");

                        if (replacementVariable.hasProperty("Label")) {
                            String label = replacementVariable.getProperty("Label");

                            for (int i = 1; i <= template.captures.length; ++i) {
                                label = label.replaceAll(
                                    Pattern.quote("$" + i), template.captures[i - 1]
                                );
                            }

                            replacementVariable.setProperty("Label", label);
                        }

                        if (this.isPrototype) {
                            if (replacementVariable.hasProperty("OID")) {
                                String OID = replacementVariable.getProperty("OID");

                                replacementVariable.setProperty("OID",
                                        OID.replace(prototypeName, name));
                            }
                        }

                        configuration.defineVariable(replacementVariable);
                    }
                }
            }
        }

        // Copy over all of the properties
        configuration.properties.putAll(super.properties);

        // TODO: Is this appropriate in all cases?
        // No, it's not, but we should expand on this cleanup at some point...
        if (this.isPrototype) {
            String label = this.hasProperty("Label") ? this.getProperty("Label") : name;
            String keys = this.getProperty("DomainKeys");

            configuration.setProperty("Name", name);
            configuration.setProperty("Label", label.replaceAll("%Domain%", name));
            configuration.setProperty("DomainKeys", keys.replaceAll("__", name));
        }

        return configuration;
    }

    void complete() {
        if (this.templateVariables.isEmpty()) {
            return;
        }

        for (Definition variable : this.variables.values()) {
            if (!variable.hasProperty("Config")) {
                VariableTemplate template = this.findVariableMatch(variable.getTargetName());

                if (template != null) {
                    variable.setProperty("Config", "Y");

                    for (String property : template.template.getProperties()) {
                        if (!variable.hasProperty(property)) {
                            variable.setProperty(property, template.template.getProperty(property));
                        }
                    }

                    if (variable.hasProperty("Label")) {
                        String label = variable.getProperty("Label");

                        for (int i = 1; i <= template.captures.length; ++i) {
                            label = label.replaceAll(
                                Pattern.quote("$" + i), template.captures[i - 1]
                            );
                        }

                        variable.setProperty("Label", label);
                    }
                }
            }
        }
    }

    void defineRule(Reference target, Definition rule) {
        this.rules.get(target).add(rule);
    }

    void defineFilter(Definition filter) {
        this.filters.add(filter);
    }

    void defineKeys(String[] keys) {
        this.keys = keys;
        this.isPrototype = true;
    }

    void defineVariable(Definition variable) {
        String variableName = variable.getTargetName();

        if (variableName.indexOf('*') == -1) {
            this.variables.put(variableName, variable);
        } else {
            this.templateVariables.add(variable);
        }
    }

    Set<Definition> getRules(Reference target) {
        return this.rules.get(target);
    }

    Set<Definition> getFilters() {
        return this.filters;
    }

    Definition getVariable(String name) {
        return this.variables.get(name);
    }

    boolean hasVariable(String name) {
        return this.variables.containsKey(name);
    }

    boolean isConfiguration() {
        return this.configured;
    }

    boolean isDefinition() {
        return this.defined;
    }

    int matches(String name, Set<String> variables) {
        int matches = 0;

        if (this.keys.length == 1 && this.keys[0].equals("*")) {
            matches = -1;
        } else {
            Set<String> allVariables = new HashSet<String>();

            for (String variable : variables) {
                variable = variable.trim().toUpperCase();

                allVariables.add(variable);
            }

            for (String variable : this.keys) {
                if (allVariables.contains(this.prepare(variable, name))) {
                    ++matches;
                }
            }
        }

        return matches;
    }

    void setConfigured(boolean configured) {
        this.configured = configured;
    }

    void setDefined(boolean defined) {
        this.defined = defined;
    }

    void updateVariables(Template target) {
        String targetName = target.getProperty("Name").toUpperCase();

        for (Definition variable : this.variables.values()) {
            String name = this.prepare(variable.getTargetName(), targetName);

            if (target.hasVariable(name)) {
                Definition.copyTo(variable, target.getVariable(name));
            } else {
                target.defineVariable(Definition.createFrom(name, variable));
            }
        }

        for (Definition variable : this.templateVariables) {
            String name = this.prepare(variable.getTargetName(), targetName);

            target.defineVariable(Definition.createFrom(name, variable));
        }
    }

    private String prepare(String variable, String name) {
        return variable.trim().replace("__", name).toUpperCase();
    }

    private VariableTemplate findVariableMatch(String variable) {
        int matches = 0;
        int captured = Integer.MAX_VALUE;
        Definition current = null;
        String[] captures = null;

        for (Definition templateVariable : this.templateVariables) {
            Pattern template = Pattern.compile(
                templateVariable.getTargetName().replace("*", "([A-Za-z0-9]+)")
            );
            Matcher matcher = template.matcher(variable);

            if (matcher.matches()) {
                int count = matcher.groupCount();
                int length = 0;
                String[] groups = new String[count];

                for (int i = 1; i <= count; ++i) {
                    length += (groups[i -1] = matcher.group(i)).length();
                }

                if (count > matches || (count == matches && length < captured)) {
                    matches = count;
                    captured = length;
                    current = templateVariable;
                    captures = groups;
                }
            }
        }

        return current == null ? null : new VariableTemplate(current, captures);
    }

    private static class VariableTemplate {
        final Definition template;
        final String[] captures;

        VariableTemplate(Definition template, String[] captures) {
            this.template = template;
            this.captures = captures;
        }
    }
}
