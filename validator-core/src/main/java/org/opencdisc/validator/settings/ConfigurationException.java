/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

/**
 *
 * @author Tim Stone
 */
public class ConfigurationException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     *
     * @author Tim Stone
     */
    public enum Type {
        GeneralStructure,
        ElementDefinition,
        RuleDefinition,
        GroupDefinition
    }

    private final Type type;

    /**
     *
     * @param type
     * @param message
     */
    public ConfigurationException(Type type, String message) {
        super(message);

        this.type = type;
    }

    /**
     *
     * @param type
     * @param cause
     */
    public ConfigurationException(Type type, Throwable cause) {
        super(cause);

        this.type = type;
    }

    /**
     *
     * @param type
     * @param message
     * @param cause
     */
    public ConfigurationException(Type type, String message, Throwable cause) {
        super(message, cause);

        this.type = type;
    }

    /**
     *
     * @return
     */
    public Type getType() {
        return this.type;
    }
}
