/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Tim Stone
 */
class MagicVariableParser {
    enum MagicProperty {
        Variables(
            new String[] {
                "If",
                "Terms",
                "Test",
                "Variable",
                "When",
                "GroupBy"
            },
            new HashSet<String>(
                Arrays.asList(
                    "CodeList",
                    "CodeList.Extensible",
                    "CodeList.Values",
                    "CodeList.Name",
                    "CodeList.Delimiter",
                    "CodeList.Dictionary",
                    "CodeList.External",
                    "CodeList.Version",
                    "CodeList.PairedValues",
                    "Config.CodeList",
                    "Config.CodeList.Extensible",
                    "Config.CodeList.Values",
                    "Config.CodeList.Name",
                    "Config.CodeList.Delimiter",
                    "Config.CodeList.Dictionary",
                    "Config.CodeList.External",
                    "Config.CodeList.Version",
                    "Config.CodeList.PairedValues",
                    "Config.@Clause",
                    "Core",
                    "@Clause",
                    "DecodedVariable",
                    "Define",
                    "Define.DecodedVariable",
                    "Define.Length",
                    "Define.Type",
                    "Define.Type.Basic",
                    "Define.Type.Regex",
                    "Define.WithCodeList",
                    "Label",
                    "Length",
                    "Present",
                    "Type",
                    "Type.Basic",
                    "Type.Regex"
                )
            )
        ),
        Domains(
            new String[] {
                "From",
                "Terms"
            },
            new HashSet<String>(
                Arrays.asList(
                    "Class",
                    "Define",
                    "From",
                    "Present",
                    "Purpose",
                    "Define.DomainKeys"
                )
            )
        );

        private final String[] properties;
        private final Set<String> values;

        MagicProperty(String[] properties, Set<String> values) {
            this.properties = properties;
            this.values = values;
        }

        String[] getProperties() {
            return this.properties;
        }

        String getSingular() {
            String singular = this.toString();

            return singular.substring(0, singular.length() - 1);
        }

        Set<String> getValues() {
            return this.values;
        }
    }

    private final static String CONTEXT_PROPERTY = "VariableContext";

    private final Pattern clause = Pattern.compile("[A-Za-z0-9!,;*#@_]+");
    private final Pattern token = Pattern.compile("(?:^| |'|\\()([=+]?%[^%]+%)");

    List<MagicVariable> parse(MagicProperty property, String name, String parsable) {
        Matcher tokenizer = this.token.matcher(parsable);
        List<MagicVariable> magicVariables = new ArrayList<MagicVariable>();

        while (tokenizer.find()) {
            String magicName = property.toString();
            String original = tokenizer.group(1);
            String token = original.substring(original.indexOf('%') + 1,
                    original.length() - 1);

            if (token.startsWith(magicName)) {
                token = token.substring(magicName.length());
                String clause;
                String[] tokens = token.split("\\$");
                List<String> conditions = new ArrayList<String>();
                List<List<String>> clauses = new ArrayList<List<String>>();

                // The first token is a special case for two reasons:
                //
                //   1. Backward compatibility : The original format was based on
                //          %Variables[*].Property.Subproperty%, which we want to
                //          preserve support for.
                //   2. Variable clause ([*])  : This can appear at the beginning,
                //          so it appears in the first token here after the split.
                //

                if (tokens[0].startsWith("[")) {
                    int end = tokens[0].indexOf(']');

                    if (end != -1) {
                        clause = tokens[0].substring(1, end);

                        if (this.clause.matcher(clause).matches()) {
                            tokens[0] = tokens[0].substring(end + 1);
                            String[] pieces = clause.split(";");

                            for (String piece : pieces) {
                                clauses.add(Arrays.asList(piece.split(",")));
                            }
                        } else {
                            throw new MagicVariableSyntaxException(name, original,
                                    "Invalid variable clause syntax");
                        }
                    } else {
                        throw new MagicVariableSyntaxException(name, original,
                                "Missing closing ]");
                    }
                }

                if (tokens[0].startsWith(".")) {
                    tokens[0] = tokens[0].substring(1);
                }

                boolean hasDefineIdentifier = false;

                for (String identifier : tokens) {
                    String[] pieces = identifier.split(":", 2);
                    int clauseIndex = pieces[0].indexOf("@Clause"),
                        dotIndex = pieces[0].indexOf('.', clauseIndex);

                    if (clauseIndex > -1 && dotIndex > -1) {
                        pieces[0] = pieces[0].substring(0, dotIndex);
                    }

                    if (identifier.length() > 0 &&
                            !property.getValues().contains(pieces[0])) {
                        throw new MagicVariableSyntaxException(name, original,
                                String.format("Invalid identifier '%s'", identifier));
                    } else if (identifier.length() > 0) {
                        // TODO: We should determine this case in a less flimsy way
                        if (identifier.contains("Define")) {
                            hasDefineIdentifier = true;
                        }

                        conditions.add(identifier);
                    }
                }

                if (!hasDefineIdentifier) {
                    conditions.add("Config");
                }

                magicVariables.add(new MagicVariable(property, original, clauses,
                        conditions));
            }
        }

        return magicVariables;
    }

    List<Definition> prepare(Definition template, MagicVariable magicVariable,
            List<Definition> candidates, Map<String, String> defaults) {
        List<Definition> generated = new ArrayList<Definition>();

        if (magicVariable.isReplicated()) {
            for (Definition candidate : candidates) {
                List<Definition> normalizedCandidates = new LinkedList<Definition>();

                if (magicVariable.isDependencyReplicated()) {
                    for (Definition dependency : candidate.getDependencies()) {
                        if (magicVariable.matchesClause(dependency)) {
                            String prefix = dependency.getProperty("Prefix");
                            String context = DigestUtils.sha1Hex(dependency.getProperty("Expression")).substring(0, 6).toUpperCase();

                            if (StringUtils.isNotEmpty(prefix)) {
                                prefix += ".";
                            }

                            normalizedCandidates.add(candidate.with(prefix + "@Clause", dependency)
                                .setProperty(CONTEXT_PROPERTY, context));
                        }
                    }
                } else {
                    normalizedCandidates.add(candidate);
                }

                for (Definition normalizedCandidate : normalizedCandidates) {
                    Definition copy = Definition.createFrom(template).addDependency(
                        normalizedCandidate
                    );
                    String name = normalizedCandidate.getTargetName().toUpperCase();
                    String context = name;

                    if (normalizedCandidate.hasProperty(CONTEXT_PROPERTY)) {
                        context += "." + normalizedCandidate.getProperty(CONTEXT_PROPERTY);
                    }

                    generated.add(
                        this.replaceProperties(
                            this.replaceVariable(
                                copy,
                                magicVariable,
                                name,
                                context
                            ),
                            magicVariable,
                            normalizedCandidate,
                            defaults
                        )
                    );
                }
            }
        } else {
            Definition copy = Definition.createFrom(template);
            StringBuilder builder = new StringBuilder();
            Definition previous = null;
            String name;

            for (Definition candidate : candidates) {
                if (previous != null) {
                    name = previous.getTargetName().toUpperCase();
                    builder.append(name).append(",");
                }

                previous = candidate;

                copy.addDependency(candidate);
            }

            if (previous != null) {
                builder.append(previous.getTargetName().toUpperCase());
            }

            generated.add(
                this.replaceVariable(
                    copy,
                    magicVariable,
                    builder.toString(),
                    null
                )
            );
        }

        return generated;
    }

    private Definition replaceProperties(Definition definition,
            MagicVariable magicVariable, Definition candidate,
            Map<String, String> defaults) {
        MagicProperty magicProperty = magicVariable.getMagicProperty();

        Pattern token = Pattern.compile(
            "%" + magicProperty.getSingular() + "\\.([^|]+)(?:\\|(.+))?%"
        );

        for (String property : definition.getProperties()) {
            String value = definition.getProperty(property);
            Matcher tokenizer = token.matcher(value);

            while (tokenizer.find()) {
                String expected = tokenizer.group(1);
                String fallback = tokenizer.group(2);

                if (candidate.hasProperty(expected)) {
                    value = value.replace(
                        tokenizer.group(0), candidate.getProperty(expected)
                    );

                    continue;
                }

                if (fallback != null) {
                    String replacement = null;

                    if (fallback.startsWith("System.")) {
                        replacement = defaults.get(
                            fallback.substring("System.".length()).toLowerCase()
                        );
                    } else if (candidate.hasProperty(fallback)) {
                        replacement = candidate.getProperty(fallback);
                    }

                    if (replacement != null) {
                        value = value.replace(tokenizer.group(0), replacement);
                    }
                }
            }

            Map<Integer, String> references = magicVariable.getReferences(candidate);

            if (references != null) {
                for (int i : references.keySet()) {
                    value = value.replace(
                        "%" + magicProperty.getSingular() + "." + i + "%",
                        references.get(i)
                    );
                }
            }

            value = value.replace(
                "%" + magicProperty.getSingular() + "%",
                candidate.getTargetName()
            );

            definition.setProperty(property, value);
        }

        return definition;
    }

    private Definition replaceVariable(Definition definition,
            MagicVariable magicVariable, String replacement, String context) {
        for (String property : magicVariable.getMagicProperty().getProperties()) {
            if (definition.hasProperty(property)) {
                definition.setProperty(property,
                    definition.getProperty(property).replace(
                        magicVariable.getIdentifier(), replacement
                    )
                );
            }
        }

        if (context != null) {
            definition.setProperty(CONTEXT_PROPERTY, context);
        }

        return definition;
    }
}
