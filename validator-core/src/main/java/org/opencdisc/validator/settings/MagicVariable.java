/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.opencdisc.validator.settings.MagicVariableParser.MagicProperty;

/**
 *
 * @author Tim Stone
 */
class MagicVariable {
    private final List<List<String>> clauses;
    private final List<String> conditions = new LinkedList<String>();
    private final List<String> clauseConditions = new LinkedList<String>();
    private final Pattern grouping = Pattern.compile("#+|@+|_+");
    private final String identifier;
    private final boolean isReplicated;
    private final boolean isDependencyReplicated;
    private final MagicProperty magicProperty;
    private final Map<String, Map<Integer, String>> references;

    MagicVariable(MagicProperty magicProperty, String identifier,
            List<List<String>> clauses, List<String> conditions) {
        this.clauses = clauses;
        this.identifier = identifier;
        this.isReplicated = !this.identifier.startsWith("=");
        this.isDependencyReplicated = this.identifier.startsWith("+");
        this.magicProperty = magicProperty;
        this.references = new HashMap<String, Map<Integer, String>>();

        for (String condition: conditions) {
            // TODO: Might not want to hardcode this here but eh...
            String[] pieces = condition.split(":", 2);
            int clauseIndex = pieces[0].indexOf("@Clause"),
                    dotIndex = pieces[0].indexOf('.', clauseIndex);

            if (clauseIndex > -1 && dotIndex > -1) {
                this.clauseConditions.add(condition.substring(dotIndex + 1));

                condition = condition.substring(0, dotIndex);
            }

            this.conditions.add(condition);
        }
    }

    String getIdentifier() {
        return this.identifier;
    }

    MagicProperty getMagicProperty() {
        return this.magicProperty;
    }

    Map<Integer, String> getReferences(Definition definition) {
        return this.references.get(definition.getTargetName());
    }

    boolean isReplicated() {
        return this.isReplicated;
    }

    boolean isDependencyReplicated() {
        return this.isDependencyReplicated;
    }

    boolean matches(Definition candidate) {
        String name = candidate.getTargetName();

        for(List<String> subclauses : this.clauses) {
            boolean matches = false;

            for (String clause : subclauses) {
                boolean negated = clause.startsWith("!");

                if (negated) {
                    clause = clause.substring(1);
                }

                Pattern tester = Pattern.compile(this.regexify(clause));
                Matcher matcher = tester.matcher(name);

                matches = matcher.matches() ^ negated;

                if (matches) {
                    if (!negated && matcher.groupCount() > 0) {
                        for (int i = 1; i <= matcher.groupCount(); ++i) {
                            if (!this.references.containsKey(name)) {
                                this.references.put(name,
                                        new HashMap<Integer, String>());
                            }

                            Map<Integer, String> references = this.references.get(name);

                            if (!references.containsKey(i)) {
                                references.put(i, matcher.group(i));
                            }
                        }
                    }

                    break;
                }
            }

            if (!matches) {
                return false;
            }
        }

        return this.checkConditions(this.conditions, candidate);
    }

    boolean matchesClause(Definition clause) {
        return this.checkConditions(this.clauseConditions, clause);
    }

    private boolean checkConditions(List<String> conditions, Definition candidate) {
        for (String condition : conditions) {
            String[] pieces = condition.split(":", 2);
            String property = pieces[0];
            String valueSet = pieces.length == 2 ? pieces[1] : null;

            if (!candidate.hasProperty(property)) {
                return false;
            } else if (valueSet != null) {
                Set<String> values = new HashSet<String>(
                        Arrays.asList(valueSet.split("\\|"))
                );

                String propertyValue = candidate.getProperty(property);
                boolean matches = false;

                for (String value : values) {
                    if (value.equalsIgnoreCase(propertyValue)) {
                        matches = true;

                        break;
                    }
                }

                if (!matches) {
                    return false;
                }
            }
        }

        return true;
    }

    private String regexify(String simplePattern) {
        String regexifiedPattern = simplePattern
            .replace("@*", "([A-Za-z]+)")
            .replace("#*", "([0-9]+)")
            .replace("_*", "([A-Za-z0-9]+)")
            .replace("*", "([A-Za-z0-9]+)");

        Matcher grouper = this.grouping.matcher(regexifiedPattern);

        while (grouper.find()) {
            String group = grouper.group();
            String replacement = "([%s]{%d})";

            if (group.startsWith("@")) {
                replacement = String.format(replacement, "A-Za-z", group.length());
            } else if (group.startsWith("#")) {
                replacement = String.format(replacement, "0-9", group.length());
            } else {
                replacement = String.format(replacement, "A-Za-z0-9", group.length());
            }

            regexifiedPattern = regexifiedPattern.replaceFirst(group, replacement);
        }

        return regexifiedPattern;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (o.getClass() != this.getClass()) {
            return false;
        }

        MagicVariable r = (MagicVariable)o;

        return new EqualsBuilder()
            .append(this.identifier, r.identifier)
            .append(this.clauses, r.clauses)
            .append(this.conditions, r.conditions)
            .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder()
            .append(this.identifier)
            .append(this.clauses)
            .append(this.conditions)
            .toHashCode();
    }

    public String toString() {
        return String.format("MagicVariable %s [Clauses: %s, Conditions: %s]",
            this.identifier, this.clauses, this.conditions);
    }
}
