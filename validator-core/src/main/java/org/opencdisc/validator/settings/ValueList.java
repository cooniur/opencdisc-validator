/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import org.apache.commons.lang.StringUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

class ValueList {
    private final String oid;
    private final List<Clause> clauses = new LinkedList<Clause>();

    ValueList(String oid) {
        this.oid = oid;
    }

    Clause addClause(String whereItemOid) {
        Clause clause = new Clause(whereItemOid);

        this.clauses.add(clause);

        return clause;
    }

    String getOid() {
        return this.oid;
    }

    Resolver resolverFor(Definition variable) {
        return new Resolver(this, variable, variable.getPrefix());
    }

    static class Resolver {
        private final ValueList valueList;
        private final Definition variable;
        private final String prefix;

        private Resolver(ValueList valueList, Definition variable, String prefix) {
            this.valueList = valueList;
            this.variable = variable;
            this.prefix = prefix;
        }

        ValueList getValueList() {
            return this.valueList;
        }

        Definition getVariable() {
            return this.variable;
        }

        boolean resolve(Map<String, Definition> variables) {
            // TODO: We're making an assumption about the prefix here which is true...
            //       but requires implementation knowledge outside of this code, which
            //       is bad. Need to change how that works to make this less problematic.
            boolean result = true;

            this.variable.setPrefix(this.prefix);

            for (Clause clause : this.valueList.clauses) {
                Definition target = variables.get(clause.oid);
                String expression = clause.getExpression(variables);

                if (target == null || expression == null) {
                    result = false;

                    break;
                }

                String name = target.getTargetName();

                target.setPrefix(this.prefix);

                // TODO: Should we just copy over everything from target instead?
                this.variable.addDependency(new Definition(Definition.Target.Variable, clause.oid)
                    .setProperty("Prefix", this.prefix)
                    .setProperty("Expression", expression)
                    .setProperty("Variable", name)
                    .setProperty("CodeList.Name", target.getProperty("CodeList.Name"))
                    .setProperty("CodeList.Values", target.getProperty("CodeList.Values"))
                    .setProperty("CodeList.PairedValues", target.getProperty("CodeList.PairedValues"))
                    .setProperty("CodeList.Delimiter", target.getProperty("CodeList.Delimiter"))
                    .setProperty("CodeList.PairDelimiter", target.getProperty("CodeList.PairDelimiter"))
                    .setProperty("CodeList.Extensible", target.getProperty("CodeList.Extensible"))
                ).setProperty("@Clause", "Y");

                target.clearPrefix();
            }

            this.variable.clearPrefix();

            return result;
        }
    }

    static class Clause {
        private final String oid;
        private final List<Check> checks = new LinkedList<Check>();

        private Clause(String whereItemOid) {
            this.oid = whereItemOid;
        }

        public Clause addCheck(String checkItemOid, String comparator, Set<String> values) {
            this.checks.add(new Check(checkItemOid, comparator, values));

            return this;
        }

        String getExpression(Map<String, Definition> variables) {
            StringBuilder buffer = new StringBuilder();
            boolean shouldWrap = this.checks.size() > 1;
            boolean isFirst = true;

            for (Check check : this.checks) {
                if (!isFirst || (isFirst = false)) {
                    buffer.append(" @and ");
                }

                if (shouldWrap) {
                    buffer.append("(");
                }

                String resolved = check.resolve(variables);

                if (resolved == null) {
                    // TODO: Better error handling...
                    return null;
                }

                buffer.append(resolved);

                if (shouldWrap) {
                    buffer.append(")");
                }
            }

            return buffer.toString();
        }

        private static class Check {
            private final String oid;
            private final List<String> expressions = new LinkedList<String>();

            Check(String oid, String comparator, Set<String> values) {
                this.oid = oid;

                if (values.size() > 1 && !comparator.equalsIgnoreCase("IN")) {
                    throw new IllegalArgumentException(String.format(
                        "Operator %s cannot have multiple values", comparator
                    ));
                }

                // TODO: We're only handling EQ and IN at the immediate moment
                for (String value : values) {
                    this.expressions.add(" == '" + value.replace("'", "\\'")  + "'");
                }
            }

            String resolve(Map<String, Definition> variables) {
                Definition variable = variables.get(this.oid);

                if (variable == null) {
                    // TODO: Better error handling...
                    return null;
                }

                boolean isFirst = true;
                StringBuilder buffer = new StringBuilder();

                for (String expression : this.expressions) {
                    // TODO: We could just have an IN operator in the expressions...
                    //       but we don't for right now, so ors it is
                    if (!isFirst || (isFirst = false)) {
                        buffer.append(" @or ");
                    }

                    buffer.append(variable.getTargetName()).append(expression);
                }

                return buffer.toString();
            }
        }
    }
}
