/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

/**
 *
 *
 * @author Tim Stone
 */
public class ElementDefinition extends Definition {
    /**
     *
     * @param target
     * @param element
     */
    public ElementDefinition(Target target, Element element) {
        this(target, element, element.getLocalName());
    }

    /**
     *
     * @param target
     * @param element
     * @param name
     */
    public ElementDefinition(Target target, Element element, String name) {
        this(target, element, name, "");
    }

    /**
     *
     * @param target
     * @param element
     * @param name
     * @param prefix
     */
    public ElementDefinition(Target target, Element element, String name, String prefix) {
        super(target, name, prefix);

        NamedNodeMap attributes = element.getAttributes();
        int bound = attributes.getLength();

        for (int i = 0; i < bound; ++i) {
            Attr attribute = (Attr)attributes.item(i);

            this.setProperty(attribute.getLocalName(), attribute.getNodeValue());
        }
    }
}
