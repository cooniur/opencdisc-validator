/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.model.EntityDetails.Reference;
import org.opencdisc.validator.settings.Definition.Target;
import org.opencdisc.validator.util.*;
import org.opencdisc.validator.util.Codelist.Code;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author Tim Stone
 */
public class XmlConfigurationParser implements ConfigurationParser {
    private enum DefineVersion {
        V1("http://www.cdisc.org/ns/def/v1.0"),
        V2("http://www.cdisc.org/ns/def/v2.0");

        public final String uri;

        private DefineVersion(String uri) {
            this.uri = uri;
        }
    }

    private static final String CONFIG_NAMESPACE_URI =
        "http://www.opencdisc.org/schema/validator";
    private static final String NCI_NAMESPACE_URI = "http://ncicb.nci.nih.gov/xml/odm/EVS/CDISC";
    private static final String GROUPING_SEPARATOR = Character.toString((char)0x1C);
    private static final String PAIR_GROUPING_SEPARATOR = Character.toString((char)0x1F);

    private final File[] configurationFiles;
    private String cwd = "";
    private final File defineFile;
    private final Map<String, String> defaults;
    private final List<ErrorAccumulator> errors = new ArrayList<ErrorAccumulator>();
    private final DocumentBuilder builder;

    public XmlConfigurationParser(File configuration, File define,
            Map<String, String> defaults) {
        this(new File[] { configuration }, define, defaults);
    }

    public XmlConfigurationParser(File[] configurations, File define,
            Map<String, String> defaults) {
        this.configurationFiles = configurations;
        this.defineFile = define;
        this.defaults = defaults != null ? defaults : new HashMap<String, String>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        factory.setValidating(false);
        factory.setIgnoringComments(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(true);

        try {
            this.builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            // Shouldn't ever happen
            throw new RuntimeException(ex);
        }
    }

    public List<ErrorAccumulator> getErrors() {
        return Collections.unmodifiableList(this.errors);
    }

    public boolean parse(ConfigurationManager manager) {
        boolean result = true;

        try {


            // Take care of the define.xml document, if we have one
            if (this.defineFile != null) {
                ErrorAccumulator errors = new ErrorAccumulator("define",
                    this.defineFile.getName());

                if (this.defineFile.isFile() && this.defineFile.canRead()) {
                    Document document = this.builder.parse(this.defineFile);

                    // Parse the define.xml document
                    DefineVersion version = this.determineDefineVersion(document);

                    // We know what version this define.xml document is
                    if (version != null) {
                        // Parse the define.xml document
                        this.parseDefineLevelContent(document, true, manager, version,
                            errors);
                    } else {
                        errors.record("Unable to determine the define.xml document version");
                    }
                } else {
                    // Check if the define.xml file provided was actually invalid
                    if (!this.defineFile.canRead()) {
                        errors.record("Unable to read file");
                    } else if (!this.defineFile.isFile()) {
                        errors.record("The provided path is not a file");
                    }
                }

                if (errors.hasErrors()) {
                    this.errors.add(errors);
                }
            }

            // Now take care of the configuration documents
            for (File configuration : this.configurationFiles) {
                if (configuration != null) {
                    ErrorAccumulator errors = new ErrorAccumulator("config",
                        configuration.getName());

                    if(configuration.isFile() && configuration.canRead()) {
                        Document document = this.builder.parse(configuration);

                        // Set the current working directory
                        this.cwd = configuration.getParentFile().getAbsolutePath();

                        // Make sure this is a configuration
                        boolean isConfiguration = this.verifyNamespace(document,
                            CONFIG_NAMESPACE_URI);

                        if (isConfiguration) {
                            // Parse the define.xml content from the configuration
                            DefineVersion version = this.determineDefineVersion(document);

                            if (version != null) {
                                boolean parseResult = this.parseDefineLevelContent(document,
                                    false, manager, version, errors);

                                if (!parseResult) {
                                    // TODO: Improve configuration error handling
                                    System.out.println("Issues parsing");
                                }
                            } else {
                                // TODO: Improve configuration error handling
                                System.out.println("We can't determine the version");
                            }

                            // Parse the configuration content from the configuration
                            boolean parseResult =
                                this.parseConfigurationLevelContent(document, manager);

                            if (!parseResult) {
                                // TODO: Improve configuration error handling
                                System.out.println("Issues parsing");
                            }
                        } else {
                            // TODO: Improve configuration error handling
                            System.out.println("Not a configuration");
                        }
                    } else {
                        // Check if the configuration file provided was actually invalid
                        if (configuration.isFile() && !configuration.canRead()) {
                            // TODO: Improve configuration error handling
                            System.out.println("File is not readable");
                        } else if (!configuration.isFile()) {
                            // TODO: Improve configuration error handling
                            System.out.println("This isn't a file / doesn't exist");

                            throw new RuntimeException(String.format(
                                "The configuration file %s does not exist!",
                                configuration.getAbsolutePath()
                            ));
                        }
                    }

                    if (errors.hasErrors()) {
                        this.errors.add(errors);
                    }
                } else {
                    // TODO: Improve configuration error handling
                    System.out.println("Configuration was null");
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }

        if (result) {
            manager.complete();
        }

        return result;
    }

    private boolean parseDefineLevelContent(Document document, boolean isDefineDocument,
            ConfigurationManager manager, DefineVersion version, ErrorAccumulator errors) {
        boolean result = true;

        Element metadata = only("MetaDataVersion", document);

        // TODO: We could have more than one MetaDataVersion section
        //   In what cases is this practical / likely to happen?
        if (metadata == null) {
            errors.record("The number of MetaDataVersion sections was not one");
            // This is an error condition
            return false;
        }

        if (!isDefineDocument) {
            manager.register(new ConfigurationManager.ConfigurationMetadata(
                metadata.getAttributeNS(version.uri, "StandardName"),
                metadata.getAttributeNS(version.uri, "StandardVersion")
            ));
        }

        Map<String, Codelist> codelists = new HashMap<String, Codelist>();
        Map<String, ValueList> valueLists = new HashMap<String, ValueList>();
        Map<String, Definition> variables = new KeyMap<Definition>();

        this.parseCodeAndValueLists(version, metadata, variables, codelists, valueLists, errors);

        Map<String, String[]> prototypes = new KeyMap<String[]>();

        // Now get the variables
        ElementList itemDefs = new ElementList(metadata.getElementsByTagName("ItemDef"));

        if (!itemDefs.hasElements()) {
            // TODO: Improve configuration error handling
            System.out.println("There should be ItemDef elements in the document");

            // This is an error condition
            return false;
        }

        List<ValueList.Resolver> resolvers = new LinkedList<ValueList.Resolver>();

        this.parseItemDefs(version, isDefineDocument, itemDefs, variables, codelists, valueLists, resolvers);

        // Now we go back and resolve and value lists since they require us to have read all the ItemDefs first
        for (ValueList.Resolver resolver : resolvers) {
            if (!resolver.resolve(variables)) {
                // TODO: Improve configuration error handling
                System.out.println(String.format("Unable to resolve value list definition %s for %s",
                    resolver.getValueList().getOid(), resolver.getVariable().getTargetName()));
            }
        }

        // Check for prototypes, but not if this is define.xml
        if (!isDefineDocument) {
            ElementList prototypeDefs = new ElementList(
                    metadata.getElementsByTagNameNS(CONFIG_NAMESPACE_URI, "Prototype")
            );

            for (Element prototypeDef : prototypeDefs) {
                String oid = prototypeDef.getAttribute("ItemGroupOID");
                String[] keys = Helpers.trimSplit(
                        prototypeDef.getAttribute("KeyVariables"), ","
                );

                prototypes.put(oid, keys);
            }
        }

        // Now get the ItemGroupDefs
        ElementList itemGroupDefs = new ElementList(
                metadata.getElementsByTagName("ItemGroupDef")
        );

        if (!itemGroupDefs.hasElements()) {
            // TODO: Improve configuration error handling
            System.out.println("There should be ItemGroupDefs elements in the document");

            // This is an error condition
            return false;
        }

        for (Element itemGroupDef : itemGroupDefs) {
            Template template = null;
            String name = itemGroupDef.getAttribute("Name");
            String oid = itemGroupDef.getAttribute("OID");

            // Check if define.xml already specified this configuration
            if (manager.defines(name)) {
                template = manager.getConfiguration(name);
            }

            boolean isPrototype = prototypes.containsKey(oid);

            // Do some sanity checking
            if (isPrototype) {
                // This should always be true
                if (template == null) {
                    template = new Template(name, prototypes.get(oid));
                } else {
                    // TODO: Improve configuration error handling
                    System.out.println(String.format(
                        "The ItemGroupDef named %s conflicts with a configuration prototype" +
                        " (oid: %s). This shouldn't happen!",
                        name, oid
                    ));

                    // TODO: I don't understand how people manage to produce the scenario where this is needed
                    template.defineKeys(prototypes.get(oid));
                }
            } else if (template == null) {
                template = new Template(name, null);
            }

            Definition current = new ElementDefinition(Target.Domain, itemGroupDef);

            if (version == DefineVersion.V2) {
                current.setProperty("Label", readDescription(itemGroupDef));
            }

            Definition.copyTo(current, template, "DomainKeys");

            if (isDefineDocument) {
                template.setProperty("Define", "Y");
                template.setDefined(true);

                String keys = cleanDomainKeys(itemGroupDef.getAttribute("DomainKeys"));

                if (keys != null) {
                    template.setProperty("Define.DomainKeys", keys);
                }
            } else {
                template.setProperty("Config", "Y");
            }

            // Look for leaves in case we're pulling a Schematron path.
            // TODO: This seems very unreliable. Try finding a way to tag the leaf.
            if (!isDefineDocument && !isPrototype) {
                ElementList leaves = new ElementList(
                        itemGroupDef.getElementsByTagNameNS(version.uri, "leaf")
                );

                if (leaves.hasElements()) {
                    String path = this.cwd + File.separator;
                    String schematron = leaves.get(0).getAttributeNS(
                            XmlWriter.XLINK_NAMESPACE_URI,
                            "href"
                    );

                    template.setProperty("Schematron", path + schematron);
                    // This is a hack to mark this as a configuration,
                    // since I don't think it will later on otherwise
                    template.setConfigured(true);
                }
            }

            ElementList itemRefs = new ElementList(
                    itemGroupDef.getElementsByTagName("ItemRef")
            );
            SortedMap<Integer, String> keyVariables = new TreeMap<Integer, String>();

            for (Element itemRef : itemRefs) {
                String referenceOID = itemRef.getAttribute("ItemOID");
                Definition reference = new ElementDefinition(Target.Variable, itemRef);

                if (!variables.containsKey(referenceOID)) {
                    // TODO: Improve configuration error handling
                    System.out.println(String.format("The ItemGroupDef %s contains a "
                            + "reference to an unknown ItemDef %s",
                            name, referenceOID));

                    result = false;

                    // Move on to the next one
                    continue;
                }

                Definition variable = Definition.createFrom(
                        variables.get(referenceOID),
                        reference
                );
                String variableName = variable.getTargetName();

                if (template.hasVariable(variableName)) {
                    Definition copy = Definition.createFrom(
                        template.getVariable(variableName),
                        variable
                    );

                    if (!isDefineDocument && variable.hasProperty("OrderNumber")) {
                        copy.setProperty("Config.OrderNumber", variable.getProperty("OrderNumber"));
                    }

                    variable = copy;
                } else if (!isDefineDocument && variable.hasProperty("OrderNumber")) {
                    variable.setProperty("Config.OrderNumber", variable.getProperty("OrderNumber"));
                }

                if (version == DefineVersion.V2 && variable.hasProperty("KeySequence")) {
                    try {
                        keyVariables.put(Integer.valueOf(variable.getProperty("KeySequence")), variableName);
                    } catch (NumberFormatException ignore) {}
                }

                template.defineVariable(variable);
            }

            if (!keyVariables.isEmpty()) {
                String keys = "";
                boolean isFirst = true;

                for (String variable : keyVariables.values()) {
                    if (!isFirst || (isFirst = false)) {
                        keys += ",";
                    }

                    keys += variable;
                }

                template.setProperty((isDefineDocument ? "Define." : "") + "DomainKeys", keys);
            }

            if (!isPrototype) {
                manager.define(template);
            } else {
                manager.prototype(template);
            }
        }

        return result;
    }

    private void parseItemDefs(DefineVersion version, boolean isDefineDocument, ElementList itemDefs,
            Map<String, Definition> variables, Map<String, Codelist> codelists, Map<String, ValueList> valueLists,
            List<ValueList.Resolver> resolvers) {
        for (Element itemDef : itemDefs) {
            String oid = itemDef.getAttribute("OID");
            Definition definition = new ElementDefinition(
                Target.Variable,
                itemDef,
                itemDef.getAttribute("Name"),
                isDefineDocument ? "Define" : ""
            );

            if (definition.hasProperty("DataType")) {
                String type = definition.getProperty("DataType");
                String basicType = "Char";
                String regexType = ".*";

                // Check if the basic type (references a SAS transport type) is numeric
                if (type.equalsIgnoreCase("Integer") || type.equalsIgnoreCase("Float")) {
                    basicType = "Num";
                }

                // TODO: This seems...very hackish. Should we move it to the rule?
                //   I also think that some of these regular expressions might be a bit
                //   off. We should look into that too...
                if (type.equalsIgnoreCase("DateTime")) {
                    regexType = "^(-|[0-9]{4})(-(-|0[0-9]|1[0-2])"
                            + "(-(-|[0-2][0-9]|3[0-1])(T(-|[0-1][0-9]|2[0-3])"
                            + "(:(-|[0-5][0-9])(:[0-5][0-9])?)?)?)?)?(/(-|[0-9]{4})"
                            + "(-(-|0[0-9]|1[0-2])(-(-|[0-2][0-9]|3[0-1])"
                            + "(T(-|[0-1][0-9]|2[0-3])(:(-|[0-5][0-9])"
                            + "(:[0-5][0-9])?)?)?)?)?)?$";
                } else if (type.equalsIgnoreCase("Date")) {
                    regexType = "^(-|[0-9]{4})(-(-|0[0-9]|1[0-2])"
                            + "(-(-|[0-2][0-9]|3[0-1]))?)?(/(-|[0-9]{4})"
                            + "(-(-|0[0-9]|1[0-2])(-(-|[0-2][0-9]|3[0-1]))?)?)?$";
                } else if (type.equalsIgnoreCase("Time")) {
                    regexType = "^T(-|[0-1][0-9]|2[0-3])(:(-|[0-5][0-9])"
                            + "(:[0-5][0-9])?)?(/T(-|[0-1][0-9]|2[0-4])"
                            + "(:(-|[0-5][0-9])(:[0-5][0-9])?)?)?$";
                } else if (type.equalsIgnoreCase("Integer")) {
                    regexType = "-?[0-9]+";
                } else if (type.equalsIgnoreCase("Float")) {
                    regexType = "-?[0-9]+(\\.[0-9]+)?";
                }

                definition.setProperty("Type", type);
                definition.setProperty("Type.Basic", basicType);
                definition.setProperty("Type.Regex", regexType);
            }

            ElementList codelistRefs = new ElementList(
                    itemDef.getElementsByTagName("CodeListRef")
            );

            if (version == DefineVersion.V2) {
                definition.setProperty("Label", readDescription(itemDef));
            }

            if (isDefineDocument) {
                definition.clearPrefix();
                definition.setProperty("Define", "Y");
            } else {
                definition.setProperty("Config", "Y");
                definition.setPrefix("Config");
            }

            // Check for a code list reference
            if (codelistRefs.hasElements()) {
                Element codelistRef = codelistRefs.get(0);
                Codelist codelist = codelists.get(
                    codelistRef.getAttribute("CodeListOID").toUpperCase()
                );

                if (codelist != null) {
                    if (!codelist.isExternal()) {
                        StringBuilder codes = new StringBuilder();
                        StringBuilder decodes = new StringBuilder();
                        boolean first = true;

                        for (Code code : codelist) {
                            if (!first) {
                                codes.append(GROUPING_SEPARATOR);

                                if (decodes.length() > 0) {
                                    decodes.append(GROUPING_SEPARATOR);
                                }
                            }

                            codes.append(code.getValue());

                            if (code.hasDecode()) {
                                decodes.append(code.getValue())
                                    .append(PAIR_GROUPING_SEPARATOR)
                                    .append(code.getDecode());
                            }

                            first = false;
                        }

                        definition.setProperty("CodeList", "Y");
                        definition.setProperty("CodeList.Name", codelist.getName());
                        definition.setProperty("CodeList.Values", codes.toString());
                        definition.setProperty("CodeList.Delimiter", GROUPING_SEPARATOR);
                        definition.setProperty("CodeList.PairDelimiter", PAIR_GROUPING_SEPARATOR);
                        definition.setProperty("CodeList.Extensible", codelist.isExtensible() ? "Y" : "N");

                        if (decodes.length() > 0) {
                            definition.setProperty("CodeList.PairedValues", decodes.toString());
                        }

                        if (isDefineDocument) {
                            // TODO: This is a legacy property
                            definition.setProperty("Define.WithCodeList", "Y");
                        }
                    } else {
                        definition.setProperty("CodeList.External", "Y");
                        definition.setProperty("CodeList.Name", codelist.getName());
                        definition.setProperty("CodeList.Dictionary",
                                codelist.getDictionary());
                        definition.setProperty("CodeList.Version", codelist.getVersion());
                    }
                } else {
                    // TODO: Improve configuration error handling
                    System.out.println(String.format(
                        "The ItemDef %s contains a reference to the unknown codelist %s",
                        oid, codelistRef.getAttribute("CodeListOID")
                    ));
                }
            }

            if (version == DefineVersion.V2) {
                // TODO: Complain if they have more than one, since this returns null in that case too
                Element valueListRef = only(version.uri, "ValueListRef", itemDef);

                if (valueListRef != null) {
                    ValueList valueList = valueLists.get(
                            valueListRef.getAttribute("ValueListOID").toUpperCase()
                    );

                    if (valueList != null) {
                        resolvers.add(valueList.resolverFor(definition));
                    } else {
                        System.out.println(String.format(
                            "The ItemDef %s contains a reference to the unknown value list %s",
                            oid, valueListRef.getAttribute("ValueListOID")
                        ));
                    }
                }
            }

            definition.clearPrefix();

            variables.put(oid, definition);
        }
    }

    private void parseCodeAndValueLists(DefineVersion version, Element metadata, Map<String, Definition> variables,
            Map<String, Codelist> codelists, Map<String, ValueList> valueLists, ErrorAccumulator errors) {
        this.parseCodeListElements(
            version,
            new ElementList(metadata.getElementsByTagName("CodeList")),
            codelists,
            errors
        );

        if (version == DefineVersion.V2) {
            this.parseValueListElements(
                new ElementList(metadata.getElementsByTagNameNS(
                    DefineVersion.V2.uri, "ValueListDef"
                )),
                new ElementList(metadata.getElementsByTagNameNS(
                    DefineVersion.V2.uri, "WhereClauseDef"
                )),
                valueLists,
                errors
            );
        }

        ElementList terminologyReferences = new ElementList(
            metadata.getElementsByTagNameNS(CONFIG_NAMESPACE_URI, "TerminologyRef")
        );

        for (Element terminologyReference : terminologyReferences) {
            String path = this.replaceSystemProperties(
                terminologyReference.getAttributeNS(XmlWriter.XLINK_NAMESPACE_URI, "href")
            );

            File include = new File(path);

            if (!include.isAbsolute()) {
                include = new File(this.cwd, path);
            }

            if (include.isFile() && include.canRead()) {
                try {
                    Document document = this.builder.parse(include);

                    Element metaDataVersionElement = only("MetaDataVersion", document);

                    if (metaDataVersionElement == null) {
                        // TODO: Improve configuration error handling
                        System.out.println("There are not the right number of MetaDataVersion "
                            + "sections");

                        // This is an error condition
                        continue;
                    }

                    this.parseCodeListElements(
                        DefineVersion.V2,
                        new ElementList(metaDataVersionElement.getElementsByTagName("CodeList")),
                        codelists,
                        errors
                    );

                    this.parseValueListElements(
                        new ElementList(metaDataVersionElement.getElementsByTagNameNS(
                            DefineVersion.V2.uri, "ValueListDef"
                        )),
                        new ElementList(metaDataVersionElement.getElementsByTagNameNS(
                            DefineVersion.V2.uri, "WhereClauseDef"
                        )),
                        valueLists,
                        errors
                    );

                    this.parseItemDefs(
                        DefineVersion.V2,
                        false,
                        new ElementList(metaDataVersionElement.getElementsByTagName("ItemDef")),
                        variables,
                        codelists,
                        valueLists,
                        Collections.<ValueList.Resolver>emptyList()
                    );
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            } else {
                errors.record(String.format("%s can't be read / does not exist",
                    include.getAbsolutePath()));
            }
        }
    }

    private void parseCodeListElements(DefineVersion version, ElementList codelistDefs,
            Map<String, Codelist> codelists, ErrorAccumulator errors) {
        for (Element codelistDef : codelistDefs) {
            Codelist codelist = null;
            String name = codelistDef.getAttribute("Name");
            String type = codelistDef.getAttribute("DataType");
            String oid = codelistDef.getAttribute("OID").toUpperCase();
            String extensible = codelistDef.getAttributeNS(NCI_NAMESPACE_URI, "CodeListExtensible");

            errors.startContext("CodeList", oid);

            ElementList codeDefs = new ElementList(
                    codelistDef.getElementsByTagName("CodeListItem")
            );

            if (!codeDefs.hasElements() && version == DefineVersion.V2) {
                codeDefs = new ElementList(
                        codelistDef.getElementsByTagName("EnumeratedItem")
                );
            }

            if (codeDefs.hasElements()) {
                codelist = new Codelist(name, type, oid, "yes".equalsIgnoreCase(extensible));

                for (Element codeDef : codeDefs) {
                    Code code = new Code(codeDef.getAttribute("CodedValue"));
                    ElementList decodeDefs = new ElementList(
                            codeDef.getElementsByTagName("Decode")
                    );

                    for (Element decodeDef : decodeDefs) {
                        Element translatedText = only("TranslatedText", decodeDef);

                        if (translatedText == null) {
                            continue;
                        }

                        String lang = translatedText.getAttributeNS(
                            XmlWriter.XML_NAMESPACE_URI, "lang"
                        );

                        if (StringUtils.isEmpty(lang)) {
                            lang = "en";
                        }

                        code.setDecode(translatedText.getTextContent(), lang);
                    }

                    codelist.addCode(code);
                }

                codelists.put(oid, codelist);
            } else {
                codeDefs = new ElementList(
                        codelistDef.getElementsByTagName("ExternalCodeList")
                );

                if (codeDefs.hasElements()) {
                    if (codeDefs.size() == 1) {
                        Element codeDef = codeDefs.get(0);

                        codelist = new Codelist(
                                name,
                                type,
                                oid,
                                codeDef.getAttribute("Dictionary"),
                                codeDef.getAttribute("Version")
                        );
                    } else {
                        errors.record("There are multiple ExternalCodeList elements");
                    }
                }
            }

            if (codelist != null) {
                codelists.put(oid, codelist);
            } else {
                errors.record("The codelist lacks valid children elements");
            }

            errors.endContext();
        }
    }

    private void parseValueListElements(ElementList valueListDefs, ElementList whereClauseDefs,
            Map<String, ValueList> valueLists, ErrorAccumulator errors) {
        Map<String, Element> whereClauses = new HashMap<String, Element>();

        for (Element whereClauseDef : whereClauseDefs) {
            whereClauses.put(whereClauseDef.getAttribute("OID"), whereClauseDef);
        }

        outer: for (Element valueListDef : valueListDefs) {
            ValueList valueList = new ValueList(valueListDef.getAttribute("OID"));

            for (Element itemRef : new ElementList(valueListDef.getElementsByTagName("ItemRef"))) {
                ValueList.Clause clause = valueList.addClause(itemRef.getAttribute("ItemOID"));

                Element whereClauseRef = only(DefineVersion.V2.uri, "WhereClauseRef", itemRef);
                Element whereClauseDef = whereClauses.get(whereClauseRef.getAttribute("WhereClauseOID"));

                if (whereClauseDef == null) {
                    errors.record(String.format("No WhereClauseDef with OID %s exists",
                        whereClauseRef.getAttribute("WhereClauseOID")));

                    continue outer;
                }

                for (Element rangeCheck : new ElementList(whereClauseDef.getElementsByTagName("RangeCheck"))) {
                    Set<String> values = new HashSet<String>();

                    for (Element checkValue : new ElementList(rangeCheck.getElementsByTagName("CheckValue"))) {
                        values.add(checkValue.getTextContent());
                    }

                    clause.addCheck(
                        rangeCheck.getAttributeNS(DefineVersion.V2.uri, "ItemOID"),
                        rangeCheck.getAttribute("Comparator"),
                        values
                    );
                }
            }

            valueLists.put(valueList.getOid(), valueList);
        }
    }

    /**
     *
     * @param document
     * @param manager
     * @return
     */
    private boolean parseConfigurationLevelContent(Document document,
                                                   ConfigurationManager manager) {
        boolean result = true;

        Element metaDataVersionElement = only("MetaDataVersion", document);

        if (metaDataVersionElement == null) {
            // TODO: Improve configuration error handling
            System.out.println("There are not the right number of MetaDataVersion "
                    + "sections");

            // This is an error condition
            return false;
        }

        Element validationRuleElement = only(
                CONFIG_NAMESPACE_URI,
                "ValidationRules",
                metaDataVersionElement
        );

        if (validationRuleElement == null) {
            // TODO: Improve configuration error handling
            System.out.println("There are not the right number of ValidationRules "
                    + "sections");

            // This is an error condition
            return false;
        }

        NodeList ruleList = validationRuleElement.getElementsByTagNameNS(
                CONFIG_NAMESPACE_URI,
                "*"
        );

        Map<String, Definition> rules = new KeyMap<Definition>();
        Map<String, List<Definition>> filterGroups = new KeyMap<List<Definition>>();

        // Prepare the rule definitions
        int ruleListCount = ruleList.getLength();

        for (int i = 0; i < ruleListCount; ++i) {
            Element element = (Element)ruleList.item(i);

            // This is so stupid...I hate the DOM
            if (!element.getParentNode().equals(validationRuleElement)) {
                continue;
            }

            Definition definition = new ElementDefinition(Target.Rule, element);
            String id = definition.getProperty("ID");

            if (definition.getTargetName().equalsIgnoreCase("filter")) {
                NodeList filterList = element.getElementsByTagNameNS(
                        CONFIG_NAMESPACE_URI,
                        "*"
                );

                int filterListCount = filterList.getLength();
                List<Definition> filters = new ArrayList<Definition>();

                for (int j = 0; j < filterListCount; ++j) {
                    Definition filter = new ElementDefinition(
                            Target.Rule, (Element)filterList.item(j)
                    );

                    filter.setProperty("ID", String.format("Filter-%s-%d", id, j));
                    filters.add(filter);
                }

                if (!filterGroups.containsKey(id)) {
                    filterGroups.put(id, filters);
                } else {
                    // TODO: Improve configuration error handling
                    System.out.println(String.format("Can't redefine filter %s", id));
                }
            } else {
                if (!rules.containsKey(id)) {
                    rules.put(id, definition);

                    // Also add the rule to the ConfigurationManager instance so that we can
                    // access its generic information later on
                    manager.store(definition);
                } else {
                    // TODO: Improve configuration error handling
                    System.out.println(String.format("Can't redefine rule %s", id));
                }
            }
        }

        // Now get the ItemGroupDefs
        NodeList itemGroupDefs = metaDataVersionElement.getElementsByTagName(
                "ItemGroupDef"
        );

        if (itemGroupDefs.getLength() == 0) {
            // TODO: Improve configuration error handling
            System.out.println("There should be ItemGroupDefs elements in the document");

            // This is an error condition
            return false;
        }

        for (int i = 0; i < itemGroupDefs.getLength(); ++i) {
            Element itemGroupDefElement = (Element)itemGroupDefs.item(i);
            String itemGroupDefName = itemGroupDefElement.getAttribute("Name");
            Template itemGroupDef = null;

            if (manager.defines(itemGroupDefName)) {
                itemGroupDef = manager.getConfiguration(itemGroupDefName);
            } else if (manager.prototypes(itemGroupDefName)) {
                itemGroupDef = manager.getPrototype(itemGroupDefName);
            } else {
                // We didn't opt to create a configuration for this ItemGroupDef
                continue;
            }

            NodeList validationRuleRefs = itemGroupDefElement.getElementsByTagNameNS(
                    CONFIG_NAMESPACE_URI,
                    "ValidationRuleRef"
            );

            if (validationRuleRefs.getLength() == 0) {
                // Skip ItemGroupDefs without any rules referenced
                continue;
            }

            for (int j = 0; j < validationRuleRefs.getLength(); ++j) {
                Element validationRuleRef = (Element)validationRuleRefs.item(j);

                this.parseConfigurationRule(
                        rules,
                        new ElementDefinition(Target.Rule, validationRuleRef),
                        itemGroupDef
                );
            }

            NodeList filterRefs = itemGroupDefElement.getElementsByTagNameNS(
                    CONFIG_NAMESPACE_URI,
                    "FilterRef"
            );

            if (filterRefs.getLength() > 0) {
                for (int j = 0; j < filterRefs.getLength(); ++j) {
                    Element filterRef = (Element)filterRefs.item(j);

                    this.parseConfigurationFilter(
                            filterGroups,
                            new ElementDefinition(Target.Filter, filterRef),
                            itemGroupDef
                    );
                }
            }

            itemGroupDef.setConfigured(true);
        }

        return result;
    }

    private void parseConfigurationRule(Map<String, Definition> definitions,
                                        Definition reference, Template template) {
        String id = reference.getProperty("RuleID");
        boolean isActive = !reference.getProperty("Active").equalsIgnoreCase("No");
        boolean isMetadata = reference.getProperty("Target").equalsIgnoreCase("Metadata");

        if (isActive) {
            if (definitions.containsKey(id)) {
                // Make sure to operate on a COPY of the definition, so that we don't
                // overwrite the generic rule reference
                Definition definition = this.replaceSystemProperties(
                    Definition.createFrom(definitions.get(id))
                );

                if (definition.getProperty("Active").equalsIgnoreCase("No")) {
                    return;
                }

                if (!isMetadata) {
                    isMetadata = definition.getProperty(
                        "Target"
                    ).equalsIgnoreCase("Metadata");
                }

                template.defineRule(
                    isMetadata ? Reference.Metadata : Reference.Data,
                    definition
                );
            } else {
                // TODO: Improve configuration error handling
                System.out.println(String.format(
                        "Referenced rule %s is not defined in this configuration", id
                ));
            }
        }
    }

    private void parseConfigurationFilter(Map<String, List<Definition>> definitions,
                                          Definition reference, Template template) {
        String id = reference.getProperty("FilterID");
        boolean isActive = !reference.getProperty("Active").equalsIgnoreCase("No");

        if (isActive) {
            if (definitions.containsKey(id)) {
                for (Definition definition : definitions.get(id)) {
                    template.defineFilter(
                            this.replaceSystemProperties(Definition.createFrom(definition))
                    );
                }
            } else {
                // TODO: Improve configuration error handling
                System.out.println(String.format(
                        "Referenced filter %s is not defined in this configuration", id
                ));
            }
        }
    }

    private Definition replaceSystemProperties(Definition definition) {
        for (String property : definition.getProperties()) {
            definition.setProperty(property,
                this.replaceSystemProperties(definition.getProperty(property)));
        }

        return definition;
    }

    private String replaceSystemProperties(String value) {
        value = value.replace(
            "%System.ConfigDirectory%", this.cwd
        );

        for (Map.Entry<String, String> property : this.defaults.entrySet()) {
            value = value.replaceAll("(?i)%" + Pattern.quote("System." + property.getKey()) + "%",
                Matcher.quoteReplacement(property.getValue()));
        }

        return value;
    }

    private String readDescription(Element element) {
        Element description = only("Description", element);

        if (description == null) {
            return "";
        }

        // TODO: This isn't correct if they have multiple TranslatedTexts...
        Element text = only("TranslatedText", description);

        return text.getTextContent();
    }

    private DefineVersion determineDefineVersion(Document document) {
        DefineVersion version = null;

        if (this.verifyNamespace(document, DefineVersion.V1.uri)) {
            version = DefineVersion.V1;
        } else if (this.verifyNamespace(document, DefineVersion.V2.uri)) {
            version = DefineVersion.V2;
        }

        return version;
    }

    private boolean verifyNamespace(Document document, String namespaceURI) {
        boolean result = false;

        if (document != null) {
            String namespacePrefix = document.lookupPrefix(namespaceURI);

            if (namespacePrefix != null) {
                result = true;
            }
        }

        return result;
    }

    private Element only(String selector, Document context) {
        return only(selector, context.getDocumentElement());
    }

    private Element only(String selector, Element context) {
        return only(null, selector, context);
    }

    private Element only(String namespace, String selector, Element context) {
        NodeList nodes;

        if (namespace != null) {
            nodes = context.getElementsByTagNameNS(
                    namespace,
                    selector
            );
        } else {
            nodes = context.getElementsByTagName(selector);
        }

        return nodes.getLength() != 1 ? null : (Element)nodes.item(0);
    }

    private static String cleanDomainKeys(String raw) {
        String[] pieces = raw.split("\\W+");

        if (pieces.length == 1 || StringUtils.isEmpty(pieces[0])) {
            return null;
        }

        return StringUtils.join(pieces, ",");
    }
}
