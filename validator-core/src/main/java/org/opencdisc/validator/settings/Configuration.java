/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.opencdisc.validator.model.EntityDetails.Reference;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.util.KeyMap;

/**
 *
 * @author Tim Stone
 */
public class Configuration extends Definition {
    private final Set<ValidationRule> filters = new HashSet<ValidationRule>();
    private final Map<Reference, Set<ValidationRule>> rules =
        new HashMap<Reference, Set<ValidationRule>>();
    private Map<String, Definition> variables = new KeyMap<Definition>();

    Configuration(String name) {
        super(Definition.Target.Domain, name);

        for (Reference target : Reference.values()) {
            this.rules.put(target, new LinkedHashSet<ValidationRule>());
        }
    }

    public Set<ValidationRule> getRules(Reference target) {
        return this.rules.get(target);
    }

    public Set<ValidationRule> getFilters() {
        return this.filters;
    }

    public Definition getVariable(String variable) {
        return this.variables.get(variable);
    }

    public Set<Definition> getVariables() {
        return new HashSet<Definition>(this.variables.values());
    }

    public boolean hasVariable(String name) {
        return this.variables.containsKey(name);
    }

    void defineRule(Reference target, ValidationRule rule) {
        this.rules.get(target).add(rule);
    }

    void defineFilter(ValidationRule filter) {
        this.filters.add(filter);
    }

    void defineVariable(Definition variable) {
        this.variables.put(variable.getTargetName(), variable);
    }
}
