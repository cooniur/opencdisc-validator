/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.settings;

/**
 *
 * @author Tim Stone
 */
public class MagicVariableSyntaxException extends IllegalArgumentException {
	private static final long serialVersionUID = 1292967138059439031L;

	private final String property;
	private final String syntax;

	/**
	 *
	 * @param property
	 * @param syntax
	 * @param message
	 */
	MagicVariableSyntaxException(String property, String syntax, String message) {
		super(message);

		this.property = property;
		this.syntax = syntax;
	}

	/**
	 *
	 * @return
	 */
	String getSyntax() {
		return this.syntax;
	}

	/**
	 *
	 * @return
	 */
	String getProperty() {
		return this.property;
	}

	public String toString() {
		return String.format("Property '%s' contains syntax error in '%s': %s",
			this.property, this.syntax, this.getMessage());
	}
}
