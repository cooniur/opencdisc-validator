/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import javax.xml.transform.TransformerConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.data.*;
import org.opencdisc.validator.data.InvalidDataException.Codes;
import org.opencdisc.validator.engine.BlockValidator;
import org.opencdisc.validator.engine.DefineGenerator;
import org.opencdisc.validator.engine.DefineGenerator.DefineMetadata;
import org.opencdisc.validator.engine.Engine;
import org.opencdisc.validator.engine.EngineEntity;
import org.opencdisc.validator.engine.GenerationEngine;
import org.opencdisc.validator.engine.HierarchicalValidator;
import org.opencdisc.validator.engine.ValidationEngine;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.model.EntityDetails.Reference;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.SimpleMetrics;
import org.opencdisc.validator.report.Reporter;
import org.opencdisc.validator.report.ReporterFactory;
import org.opencdisc.validator.settings.Configuration;
import org.opencdisc.validator.settings.ConfigurationManager;
import org.opencdisc.validator.settings.ConfigurationParser;
import org.opencdisc.validator.settings.XmlConfigurationParser;
import org.opencdisc.validator.util.Helpers;
import org.opencdisc.validator.util.ProcessToken;
import org.opencdisc.validator.util.Settings;
import org.xml.sax.SAXException;

/**
 *
 * @author Tim Stone
 */
final class DefaultValidator implements Validator {
    private static final Map<String, Class<? extends ValidationEngine>> ENGINES =
        new HashMap<String, Class<? extends ValidationEngine>>();

    static {
        for (Class<? extends ValidationEngine> engine :
                Helpers.find(ValidationEngine.class)) {
            ENGINES.put(engine.getSimpleName(), engine);
        }
    }

    private static final Comparator<EngineEntity> SDTM_DEFINE_ORDERING =
        new Comparator<EngineEntity>() {
            public int compare(EngineEntity o1, EngineEntity o2) {
                EntityDetails e1 = o1.getDetails();
                EntityDetails e2 = o2.getDetails();
                String name1 = e1.getString(Property.Name).toUpperCase();
                String name2 = e2.getString(Property.Name).toUpperCase();
                String class1 = e1.hasProperty(Property.Class) ?
                        e1.getString(Property.Class) : null;
                String class2 = e2.hasProperty(Property.Class) ?
                        e2.getString(Property.Class) : null;

                if (class1 == null || class2 == null) {
                    if (class1 != null) {
                        return -1;
                    } else {
                        if (class2 != null) {
                            return 1;
                        } else {
                            return name1.compareToIgnoreCase(name2);
                        }
                    }
                }

                if (name1.equalsIgnoreCase(name2) &&
                        class1.equalsIgnoreCase(class2)) {
                    return 0;
                }

                if (name1.equals("DM")) {
                    return -1;
                } else if (name2.equals("DM")) {
                    return 1;
                }

                boolean special1 = name1.startsWith("SUPP") || name1.equals("RELREC");
                boolean special2 = name2.startsWith("SUPP") || name2.equals("RELREC");

                if (special1 || special2) {
                    if (special1 && special2) {
                        return name1.compareTo(name2);
                    } else if (special1) {
                        return 1;
                    } else {
                        return -1;
                    }
                }

                if (class1.equalsIgnoreCase(class2)) {
                    return name1.compareTo(name2);
                }

                if (class1.equalsIgnoreCase("Special Purpose")) {
                    return -1;
                } else if (class2.equalsIgnoreCase("Special Purpose")) {
                    return 1;
                } else if (class1.equalsIgnoreCase("Interventions")) {
                    return -1;
                } else if (class2.equalsIgnoreCase("Interventions")) {
                    return 1;
                } else if (class1.equalsIgnoreCase("Events")) {
                    return -1;
                } else if (class2.equalsIgnoreCase("Events")) {
                    return 1;
                } else if (class1.equalsIgnoreCase("Findings")) {
                    return -1;
                } else if (class2.equalsIgnoreCase("Findings")) {
                    return 1;
                } else if (class1.equalsIgnoreCase("Trial Design")) {
                    return -1;
                } else if (class2.equalsIgnoreCase("Trial Design")) {
                    return 1;
                } else if (class1.equalsIgnoreCase("Related")) {
                    return -1;
                }

                return 1;
            }
        };

    /**
     *
     */
    private volatile boolean abort = false;
    private EventDispatcher dispatcher = new EventDispatcher();
    private Engine engine = null;
    private volatile boolean isRunning = true;
    private SimpleMetrics metrics   = null;

    public synchronized void abort() {
        if (this.isRunning) {
            this.abort = true;
            this.metrics.setAbortStatus(true);

            if (this.engine != null) {
                this.engine.cancel();
            }
        }
    }

    public void addValidatorListener(ValidatorListener listener) {
        this.dispatcher.add(listener);
    }

    public void generate(SourceOptions source, ConfigOptions template,
            File destination) {
        this.generate(Type.Sdtm, source, template, destination);
    }

    public void generate(List<SourceOptions> sources, ConfigOptions template,
            File destination) {
        this.generate(Type.Sdtm, sources, template, destination);
    }

    public void generate(Type type, SourceOptions source, ConfigOptions template,
            File destination) {
        this.generate(type, Arrays.asList(source), template, destination);
    }

    public void generate(Type type, List<SourceOptions> sources, ConfigOptions template,
            File destination) {
        this.isRunning = true;

        ProcessToken token = ProcessToken.getRandomToken();

        this.dispatcher.fireStarted();
        this.metrics = new SimpleMetrics();
        this.dispatcher.fireParsing(template.getConfig());

        // Prepare the input information
        SourceProvider provider = this.createProvider(token, sources, template.getDefine());
        ConfigurationManager manager = this.createManager(token, template);

        // Prepare the generator
        String codelistPath = template.getCodeList();
        File codelist = null;

        if (codelistPath != null && codelistPath.length() > 0) {
            codelist = new File(codelistPath);

            if (!(codelist.isFile() && codelist.canRead())) {
                // TODO: Improve generation error handling
                codelist = null;
            }
        }

        GenerationEngine engine = null;
        ConfigurationManager.ConfigurationMetadata metadata = manager.getMetadata().iterator().next();

        try {
            this.engine = engine = new DefineGenerator(destination, new DefineMetadata(
                metadata.standardName,
                metadata.standardVersion,
                // TODO: We know the ADaM ordering too now
                metadata.standardName.contains("SDTM") ? SDTM_DEFINE_ORDERING : null,
                codelist
            ));
            Set<String> sourceNames = provider.getSourceNames();

            for (String sourceName : sourceNames) {
                ///////////////////////
                //    Abort Point    //
                ///////////////////////
                if (this.abort) {
                    return;
                }

                DataSource source = provider.getSource(sourceName);

                engine.prepare(new EngineEntity(
                    source,
                    manager.prepare(sourceName, source.getVariables(), false)
                ));
            }

            // Generate the file
            this.dispatcher.fireGenerating(destination.getName());

            engine.generate();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } catch (TransformerConfigurationException ex) {
            // TODO: Improve generation error handling
            ex.printStackTrace();
        } catch (InvalidDataException ex) {
            throw new RuntimeException(ex);
        } catch (SAXException ex) {
            throw new RuntimeException(ex);
        } finally {
            if (engine != null) {
                try {
                    engine.close();
                } catch (IOException ex) {}
            }

            this.cleanup(token);
        }
    }

    public SimpleMetrics getMetrics() {
        return this.metrics;
    }

    public void removeAllValidatorListeners() {
        this.dispatcher.clear();
    }

    public boolean removeValidatorListener(ValidatorListener listener) {
        return this.dispatcher.remove(listener);
    }

    public void validate(Type type, SourceOptions source, ConfigOptions config,
            ReportOptions report) {
        this.validate(type, Arrays.asList(source), config, report);
    }

    public void validate(Type type, List<SourceOptions> sources, ConfigOptions config,
            ReportOptions report) {
        this.isRunning = true;

        ProcessToken token = ProcessToken.getRandomToken();
        Format format = type.getFormat();

        // Signal the start of the validation process
        this.dispatcher.fireStarted();

        if (format == Format.Tabular) {
            LookupProviderFactory.getLookupProvider().setDispatcher(this.dispatcher);
        }

        this.metrics = new SimpleMetrics();

        SourceProvider provider = this.createProvider(token, sources, config.getDefine());
        ConfigurationManager manager = null;
        Reporter reporter = null;
        ValidationEngine engine = null;

        try {
            this.dispatcher.fireParsing(config.getConfig());

            manager  = this.createManager(token, config);
            reporter = ReporterFactory.newReporter(report, config, format,
                    manager.getMessages());

            if (format == Format.Tabular) {
                Class<? extends ValidationEngine> implementation = null;

                if (!Settings.defines("Engine.TabularImplementation") ||
                        (implementation = ENGINES.get(
                            Settings.get("Engine.TabularImplementation")
                        )) == null) {
                    implementation = BlockValidator.class;
                }

                Constructor<? extends ValidationEngine> constructor;
                Class<?>[] signature = {
                    EventDispatcher.class,
                    SimpleMetrics.class,
                    Reporter.class,
                    Configuration.class
                };

                try {
                    constructor = implementation.getConstructor(signature);
                    this.engine = engine = constructor.newInstance(
                        this.dispatcher,
                        this.metrics,
                        reporter,
                        manager.prepare("GLOBAL", null, true)
                    );
                } catch (NoSuchMethodException ex) {
                    throw new RuntimeException(ex);
                } catch (InvocationTargetException ex) {
                    throw new RuntimeException(ex);
                } catch (IllegalAccessException ex) {
                    throw new RuntimeException(ex);
                } catch (InstantiationException ex) {
                    throw new RuntimeException(ex);
                }
            } else if (format == Format.Hierarchical) {
                this.engine = engine = new HierarchicalValidator(this.dispatcher,
                        this.metrics, reporter);
            }

            if (engine != null) {
                // Take care of sources we couldn't parse
                for (String sourceName : provider.getInvalidSourceNames()) {
                    ///////////////////////
                    //    Abort Point    //
                    ///////////////////////
                    if (this.abort) {
                        return;
                    }

                    // TODO: This assumes the source is definitely a file
                    File sourceFile = new File(sourceName);

                    this.metrics.updateMessageCount(
                        reporter.write(new Message(
                            "SD0062",
                            "System",
                            Text.get("Messages.InvalidSource"),
                            String.format(
                                Text.get("Descriptions.InvalidSource"), sourceName
                            ),
                            Message.Type.Error,
                            new InternalEntityDetails(
                                Reference.Data,
                                sourceFile.getName(),
                                sourceName
                            )
                        )) ? 1 : 0
                    );
                }

                Set<String> sourceNames = provider.getSourceNames();

                manager.declare(sourceNames);

                // Add the data
                for (String sourceName : sourceNames) {
                    ///////////////////////
                    //    Abort Point    //
                    ///////////////////////
                    if (this.abort) {
                        return;
                    }

                    DataSource source = provider.getSource(sourceName);

                    try {
                        // We have to do this because calling getVariables() on an
                        // XmlDataSource will throw an UnsupportedOperationException.
                        // There should be a better way around this problem, but for
                        // the time being this is good enough.
                        Set<String> variables = format != Format.Hierarchical ?
                                source.getVariables() : Collections.<String>emptySet();

                        engine.prepare(new EngineEntity(
                            source,
                            manager.prepare(sourceName, variables, true)
                        ));
                    } catch (InvalidDataException ex) {
                        Codes errorCode = ex.getCode();

                        if (errorCode == Codes.NoVariables) {
                            this.metrics.updateMessageCount(
                                reporter.write(new Message(
                                    "NO_VARIABLES",
                                    "Presence",
                                    Text.get("Messages.NoVariables"),
                                    String.format(
                                        Text.get("Descriptions.NoVariables"), sourceName
                                    ),
                                    Message.Type.Warning,
                                    source.getDetails()
                                )) ? 1 : 0
                            );
                        } else {
                            // Something else is wrong, and since we don't foresee this
                            // coming, the best action is just to bail on the whole
                            // process.
                            throw new RuntimeException(ex);
                        }
                    }
                }

                // Perform the validation step (which blocks until complete)
                engine.validate();
            }
        } finally {
            this.dispatcher.fireGenerating(report.getReport());

            if (reporter != null) {
                reporter.close();

                this.metrics.setMessageMetrics(reporter.getMetrics());
                this.metrics.setOverflowed(reporter.hasOverflowed());
            }

            if (manager != null) {
                this.metrics.setRuleMetrics(manager.getMetrics());
            }

            this.cleanup(token);
        }
    }

    /**
     *
     */
    private void cleanup(ProcessToken token) {
        this.metrics.finalizeTime();
        this.dispatcher.fireStopped();

        LookupProviderFactory.clear(token);
        SourceProviderFactory.clear(token);

        this.isRunning = false;
        this.abort = false;
        this.engine = null;

        ProcessToken.free(token);
    }

    /**
     *
     * @param token
     * @param config
     * @return
     */
    private ConfigurationManager createManager(ProcessToken token,
            ConfigOptions config) {
        File define = null;
        String definePath = config.getDefine();

        if (definePath != null && definePath.length() > 0) {
            define = new File(definePath);
        }

        ConfigurationParser parser = new XmlConfigurationParser(
            new File(config.getConfig()), define, config.getProperties()
        );
        ConfigurationManager manager = new ConfigurationManager(
            token, config.getProperties()
        );

        parser.parse(manager);

        return manager;
    }

    /**
     *
     * @param token
     * @param sources
     * @return
     */
    private SourceProvider createProvider(ProcessToken token,
            List<SourceOptions> sources, String define) {
        SourceProvider provider = SourceProviderFactory.getSourceProvider(token);

        for (SourceOptions source : sources) {
            if (StringUtils.isNotEmpty(define)) {
                source.setDefine(define);
            }

            provider.addSource(source);
        }

        return provider;
    }
}
