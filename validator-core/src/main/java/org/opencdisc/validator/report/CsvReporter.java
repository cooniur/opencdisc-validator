/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.report;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.opencdisc.validator.ConfigOptions;
import org.opencdisc.validator.ReportOptions;
import org.opencdisc.validator.Validator.Format;
import org.opencdisc.validator.model.DataIdentity;
import org.opencdisc.validator.model.Message;

import au.com.bytecode.opencsv.CSVWriter;

/**
 *
 * @author Max Kanevsky
 */
public class CsvReporter extends AbstractReporter implements Reporter {
    private final CSVWriter report;

    public CsvReporter(ReportOptions options, ConfigOptions config,
            Format format, List<MessageTemplate> messages) throws IOException {
        super(options, config, format, messages);

        File reportFile = new File(options.getReport());

        // overwrite existing report
        if (options.getOverwrite() && reportFile.exists()) {
            reportFile.delete();
        }

        // create a new report
        this.report = new CSVWriter(new FileWriter(reportFile));

        // add header row
        String[] row = { "Name", "Record", "Variable", "Value", "OpenCDISC ID", "Publisher ID", "Message", "Category", "Type", "Severity" };
        this.report.writeNext(row);
    }

    public void close() {
        // Close report
        try {
            this.report.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean write(Message message) {
        boolean wrote = false;

        if (super.checkFilters(message)) {
            wrote = true;

            // add row
            String[] row = new String[10];

            DataIdentity identity = message.getIdentity();
            if (identity != null && identity.getName() != null)
                row[0] = identity.getName();
            if (identity != null && identity.getRecord() != null)
                row[1] = identity.getRecord().toString();

            // retrieve variables and values
            StringBuilder variables = new StringBuilder();
            StringBuilder values = new StringBuilder();
            int variableCount = 0;
            for (String variable : message.getVariables()) {
                variableCount++;
                if (variableCount > 1) {
                    variables.append(", ");
                    values.append(", ");
                }

                variables.append(variable);
                values.append(message.getValueOf(variable));
            }

            row[2] = variables.toString();

            // TODO: Remove the hard-coding, should be handled by the rule
            if (!message.getID().equals("PARSE_ERROR") && !message.getID().equals("SD0061"))
                row[3] = values.toString();

            if (message.getID().equals("SD0061"))
                row[0] = message.getValueOf("DOMAIN");

            row[4] = message.getID();
            row[5] = super.getPublisherId(message.getID());
            row[6] = message.getMessage();
            row[7] = message.getCategory();
            row[8] = message.getType().toString();
            row[9] = message.getSeverity() != null ? message.getSeverity().toString() : "";

            this.report.writeNext(row);
        }

        return wrote;
    }
}
