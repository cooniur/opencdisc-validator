/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.report;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencdisc.validator.ConfigOptions;
import org.opencdisc.validator.ReportOptions;
import org.opencdisc.validator.ReportOptions.Type;
import org.opencdisc.validator.Validator.Format;

/**
 *
 * @author Tim Stone
 */
public class ReporterFactory {
    private static final Map<Type, Class<? extends Reporter>> REPORT_TYPES =
        new HashMap<Type, Class<? extends Reporter>>();

    static {
        REPORT_TYPES.put(Type.Excel, ExcelReporter.class);
        REPORT_TYPES.put(Type.Csv,   CsvReporter.class);
        REPORT_TYPES.put(Type.Queue, QueueReporter.class);
        REPORT_TYPES.put(Type.Xml,   XmlReporter.class);
        REPORT_TYPES.put(Type.Html,  XmlReporter.class);
    }

    private ReporterFactory() {}

    /**
     *
     * @param options
     * @param config
     * @param define
     * @return
     * @throws IOException
     */
    public static Reporter newReporter(ReportOptions options, ConfigOptions config,
            Format format, List<MessageTemplate> messages) {
        Reporter instance = null;
        Type reportType = options.getType();

        if (!REPORT_TYPES.containsKey(reportType)) {
            throw new IllegalArgumentException(String.format(
                    "The report type %s is not assigned an implementation",
                    reportType.toString()
            ));
        }

        try {
            Constructor<? extends Reporter> constructor;
            Class<?>[] constructorSignature = { ReportOptions.class, ConfigOptions.class,
                    Format.class, List.class };
            Class<? extends Reporter> reportClass = REPORT_TYPES.get(reportType);

            constructor = reportClass.getConstructor(constructorSignature);
            instance = constructor.newInstance(options, config, format, messages);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return instance;
    }
}
