/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.report;

import static org.apache.commons.lang.time.DateFormatUtils.ISO_DATETIME_FORMAT;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.awt.Color;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
import org.opencdisc.validator.ConfigOptions;
import org.opencdisc.validator.ReportOptions;
import org.opencdisc.validator.Text;
import org.opencdisc.validator.Validator.Format;
import org.opencdisc.validator.model.DataDetails;
import org.opencdisc.validator.model.DataDetails.Info;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.EntityDetails.Reference;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.util.KeyMap;

/**
 *
 * @author Max Kanevsky
 */
public class ExcelReporter extends AbstractReporter {
    private static final int DETAIL_HEADER_OFFSET = 1;
    private static final int MAX_RECORD_COUNT = 1048576;
    private static final String DISPLAY_FONT = "Arial";
    private static final String CORE_PACKAGE_NAME = "org.opencdisc.validator";

    private enum Colors {
        ErrorFont(new Color(156, 0, 6)),
        ErrorFill(new Color(255, 199, 206)),
        WarningFont(new Color(156, 101, 0)),
        WarningFill(new Color(255, 235, 156)),
        InfoFont(new Color(33, 88, 103)),
        InfoFill(new Color(182, 221, 232)),
        ReportHeaderFill(new Color(234, 241, 221)),
        HeaderFont(new Color(79, 98, 40)),
        HeaderFill(new Color(194, 214, 154)),
        TableTitleFill(new Color(117, 146, 60)),
        SourceHeaderFont(new Color(73, 73, 73)),
        SourceHeaderFill(new Color(242, 242, 242));

        private final XSSFColor color;

        Colors(Color color) {
            this.color = new XSSFColor(color);
        }

        public Font color(Font font) {
            ((XSSFFont)font).setColor(this.color);

            return font;
        }

        public CellStyle color(CellStyle style) {
            ((XSSFCellStyle)style).setFillForegroundColor(this.color);

            return style;
        }

        public CellStyle colorBottomBorder(CellStyle style) {
            return colorBorder(XSSFCellBorder.BorderSide.BOTTOM, style);
        }

        public CellStyle colorTopBorder(CellStyle style) {
            return colorBorder(XSSFCellBorder.BorderSide.TOP, style);
        }

        private CellStyle colorBorder(XSSFCellBorder.BorderSide side, CellStyle style) {
            ((XSSFCellStyle)style).setBorderColor(side, this.color);

            return style;
        }
    }

    private final long creationTime = System.currentTimeMillis();
    private final Map<String, Integer> locations = new KeyMap<Integer>();
    private final List<Message> messages = new LinkedList<Message>();
    private final Workbook report;
    private final FileOutputStream reportStream;
    private CellStyle styleErrorFooter;
    private CellStyle styleErrorHeader;
    private CellStyle styleErrorOddRows;
    private CellStyle styleEvenRows;
    private CellStyle styleInfoFooter;
    private CellStyle styleInfoHeader;
    private CellStyle styleInfoOddRows;
    private CellStyle styleHyperlink;
    private CellStyle styleHyperlinkAlt;
    private CellStyle styleRecordFooter;
    private CellStyle styleReportHeader;
    private CellStyle styleReportTitle;
    private CellStyle styleSourceHeader;
    private CellStyle styleTableTitle;
    private CellStyle styleNormalFooter;
    private CellStyle styleNormalHeader;
    private CellStyle styleNormalHeaderRight;
    private CellStyle styleOddRows;
    private CellStyle styleWarnFooter;
    private CellStyle styleWarnHeader;
    private CellStyle styleWarnOddRows;
    private final Map<String, Map<Message.Type, SortedSet<String>>> issueMap =
        new TreeMap<String, Map<Message.Type, SortedSet<String>>>(new Comparator<String>() {
            public int compare(String lhs, String rhs) {
                int compared = lhs.compareTo(rhs);

                if (compared == 0) {
                    return compared;
                }

                boolean lhsGlobal = lhs.equals("GLOBAL");

                if (lhsGlobal || rhs.equals("GLOBAL")) {
                    return lhsGlobal ? -1 : 1;
                }

                return compared;
            }
        });
    private final Map<String, SortedMap<String, Long>> termMap =
        new HashMap<String, SortedMap<String, Long>>();
    private final Map<String, String> unprocessedReasons = new KeyMap<String>();

    public ExcelReporter(ReportOptions options, ConfigOptions config,
            Format format, List<MessageTemplate> messages) throws FileNotFoundException {
        super(options, config, format, messages);

        // We always want to generate metrics here, because we use this information
        options.setGenerateMetrics(true);
        options.setHeader("Engine Version",
            Package.getPackage(CORE_PACKAGE_NAME).getImplementationVersion());

        // Create the new workbook
        this.report = new XSSFWorkbook();

        File reportFile = new File(options.getReport());

        // Check if we need to overwrite an existing file
        if (options.getOverwrite() && reportFile.exists()) {
            reportFile.delete();
        }

        // Create the ouput stream
        this.reportStream = new FileOutputStream(reportFile);
    }

    public void close() {
        // Begin the long and arduous process of creating the Excel details
        this.createStyles();

        Sheet datasets = null;

        // We don't have a summary sheet on non-tabular validations
        if (super.format == Format.Tabular) {
            datasets = this.report.createSheet(Text.get("ExcelReport.DatasetTab"));
        }

        Sheet issues = this.report.createSheet(Text.get("ExcelReport.IssueTab"));
        Sheet details = this.report.createSheet(Text.get("ExcelReport.DetailsTab"));
        Sheet rules = this.report.createSheet(Text.get("ExcelReport.RulesTab"));

        // Print out the raw messages first, so we can link to them
        this.createRules(rules);

        // Then get rid of that list of messages we have stored...
        this.createDetails(details);

        // Now take care of the issue summary
        this.createIssueSummary(issues);

        // And finally the dataset summary, if we have one
        if (datasets != null) {
            this.createDatasetSummary(datasets);
        }

        // Print it out, and we're done
        try {
            this.report.write(this.reportStream);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                this.reportStream.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public boolean write(Message message) {
        if (!super.checkFilters(message)) {
            return false;
        }

        String domain = message.getEntityDetails().getString(Property.Name);
        String id = message.getID();

        // Certain messages indicate that the source wasn't processed. Translate those
        // particular IDs into human-friendly unprocessed reasons.
        // TODO: Why haven't we found a way to remove this yet?
        if (id.equalsIgnoreCase("SD0001")) {
            this.unprocessedReasons.put(
                domain, Text.get("ExcelReport.UnprocessedEmpty")
            );
        } else if (id.equalsIgnoreCase("SD0061")) {
            String source = message.getValueOf("DATASET");

            if (source == null) {
                source = message.getValueOf("DOMAIN");
            }

            if (source != null) {
                this.unprocessedReasons.put(
                    source, Text.get("ExcelReport.UnprocessedMissing")
                );
            }
        } else if (id.equalsIgnoreCase("MISSING_CONFIG")) {
            this.unprocessedReasons.put(
                domain, Text.get("ExcelReport.UnprocessedNoConfig")
            );
        } else if (id.equalsIgnoreCase("PARSE_ERROR")) {
            this.unprocessedReasons.put(
                domain, Text.get("ExcelReport.UnprocessedCorrupt")
            );
        } else if (id.equalsIgnoreCase("SD0062")) {
            this.unprocessedReasons.put(
                domain, Text.get("ExcelReport.UnprocessedInvalid")
            );
        }

        Map<Message.Type, SortedSet<String>> typeMap = this.issueMap.get(domain);

        if (typeMap == null) {
            typeMap = new HashMap<Message.Type, SortedSet<String>>();

            typeMap.put(Message.Type.Error, new TreeSet<String>());
            typeMap.put(Message.Type.Warning, new TreeSet<String>());
            typeMap.put(Message.Type.Information, new TreeSet<String>());

            this.issueMap.put(domain, typeMap);
        }

        // Add this to the type map
        typeMap.get(message.getType()).add(id);

        // Check if this message should be included based on the cutoff limit
        int limit = super.options.getLimit();
        long count;
        boolean isTerminology = isTerminology(message);

        if (isTerminology) {
            Set<String> variables = message.getVariables();
            String key = getTerminologyKey(message);
            SortedMap<String, Long> terms = this.termMap.get(key);

            if (terms == null) {
                terms = new TreeMap<String, Long>();

                this.termMap.put(key, terms);
            }

            StringBuilder term = new StringBuilder();

            for (String variable : variables) {
                String value = message.getValueOf(variable);

                // TODO: This is very hacky to support the code/decode case right now
                if (variable.endsWith("CD")) {
                    if (term.length() > 0) {
                        term.insert(0, ", ");
                    }

                    term.insert(0, value);
                } else {
                    if (term.length() > 0) {
                        term.append(", ");
                    }

                    term.append(value);
                }
            }

            key = term.toString();
            count = terms.containsKey(key) ? terms.get(key) : 0;
            count++;

            terms.put(key, count);
        }

        count = 0;

        if (limit > 0 || isTerminology) {
            count = super.metrics.getDomainMessageCount(domain, id, message.getMessage());
        }

        if ((limit <= 0 || count <= limit) && (!isTerminology || count < 2)) {
            // Now check to make sure that we haven't overflowed
            if (this.messages.size() + DETAIL_HEADER_OFFSET < MAX_RECORD_COUNT) {
                if (!id.startsWith("SKIP_")) {
                    this.messages.add(message);
                }
            } else {
                super.triggerOverflow();
            }
        }

        return true;
    }

    private Totals createDatasetProcessedList(Sheet sheet) {
        // Sort the source names alphabetically
        List<String> sources = new ArrayList<String>(super.details.keySet());
        Collections.sort(sources);

        Cell cell;
        Row row;
        int currentRowNumber = sheet.getLastRowNum() + 1;

        // Add the group header
        cell = sheet.createRow(currentRowNumber).createCell(0);
        cell.setCellValue(Text.get("ExcelReport.GroupProcessed"));
        cell.setCellStyle(this.styleTableTitle);
        sheet.addMergedRegion(new CellRangeAddress(currentRowNumber, currentRowNumber,
                0, 7));

        // Add the column header
        row = sheet.createRow(++currentRowNumber);

        cell = row.createCell(0);
        cell.setCellValue(Text.get("ExcelReport.HeaderName"));
        cell.setCellStyle(this.styleNormalHeader);

        cell = row.createCell(1);
        cell.setCellValue(Text.get("ExcelReport.HeaderLabel"));
        cell.setCellStyle(this.styleNormalHeader);

        cell = row.createCell(2);
        cell.setCellValue(Text.get("ExcelReport.HeaderClass"));
        cell.setCellStyle(this.styleNormalHeader);

        cell = row.createCell(3);
        cell.setCellValue(Text.get("ExcelReport.HeaderSource"));
        cell.setCellStyle(this.styleNormalHeader);

        cell = row.createCell(4);
        cell.setCellValue(Text.get("ExcelReport.HeaderRecords"));
        cell.setCellStyle(this.styleNormalHeader);

        cell = row.createCell(5);
        cell.setCellValue(Text.get("ExcelReport.HeaderErrors"));
        cell.setCellStyle(this.styleErrorHeader);

        cell = row.createCell(6);
        cell.setCellValue(Text.get("ExcelReport.HeaderWarnings"));
        cell.setCellStyle(this.styleWarnHeader);

        cell = row.createCell(7);
        cell.setCellValue(Text.get("ExcelReport.HeaderInfos"));
        cell.setCellStyle(this.styleInfoHeader);

        // List all of the processed sources
        int count = 1;
        Totals totals = new Totals();

        // Add an entry for global metadata
        row = sheet.createRow(sheet.getLastRowNum() + 1);

        row.createCell(0).setCellValue("GLOBAL");
        row.createCell(1).setCellValue("Global Metadata");
        row.createCell(2).setCellValue("--");
        row.createCell(3).setCellValue("--");
        row.createCell(4).setCellValue("--");
        row.createCell(5).setCellValue(
            super.metrics.getDomainCount("GLOBAL", Message.Type.Error)
        );
        row.createCell(6).setCellValue(
            super.metrics.getDomainCount("GLOBAL", Message.Type.Warning)
        );
        row.createCell(7).setCellValue(
            super.metrics.getDomainCount("GLOBAL", Message.Type.Information)
        );

        totals.errors   += super.metrics.getDomainCount("GLOBAL", Message.Type.Error);
        totals.warnings += super.metrics.getDomainCount("GLOBAL", Message.Type.Warning);
        totals.notices  += super.metrics.getDomainCount("GLOBAL", Message.Type.Information);

        for (Cell currentCell : row) {
            currentCell.setCellStyle(count % 2 > 0 ?
                    this.styleOddRows : this.styleEvenRows);
        }

        for (String source : sources) {
            EntityDetails details = super.details.get(source);
            int records = details.hasProperty(Property.Records) ?
                    details.getInteger(Property.Records) : 0;
            boolean completed = details.hasProperty(Property.Validated) &&
                    details.getBoolean(Property.Validated);
            String location = null;

            for (String file : details.getString(Property.Location)
                    .split(File.pathSeparator)) {
                if (location != null) {
                    location += ", ";
                } else {
                    location = "";
                }

                location += (new File(file)).getName();
            }

            if (records > 0 && completed) {
                ++count;

                row = sheet.createRow(sheet.getLastRowNum() + 1);

                row.createCell(0).setCellValue(source);
                row.createCell(1).setCellValue(details.hasProperty(Property.Label) ?
                        details.getString(Property.Label) :
                        Text.get("ExcelReport.LabelUnknown"));
                row.createCell(2).setCellValue(details.hasProperty(Property.Class) ?
                        details.getString(Property.Class) : "");
                row.createCell(3).setCellValue(location);
                row.createCell(4).setCellValue(records);
                row.createCell(5).setCellValue(
                    super.metrics.getDomainCount(source, Message.Type.Error)
                );
                row.createCell(6).setCellValue(
                    super.metrics.getDomainCount(source, Message.Type.Warning)
                );
                row.createCell(7).setCellValue(
                    super.metrics.getDomainCount(source, Message.Type.Information)
                );

                totals.records  += records;
                totals.errors   += super.metrics.getDomainCount(source, Message.Type.Error);
                totals.warnings += super.metrics.getDomainCount(source, Message.Type.Warning);
                totals.notices  += super.metrics.getDomainCount(source, Message.Type.Information);

                for (Cell currentCell : row) {
                    currentCell.setCellStyle(count % 2 > 0 ?
                            this.styleOddRows : this.styleEvenRows);
                }
            } else if (!completed) {
                this.unprocessedReasons.put(source,
                        Text.get("ExcelReport.UnprocessedAborted"));
            }
        }

        row = sheet.createRow(sheet.getLastRowNum() + 1);
        cell = row.createCell(0);
        cell.setCellValue(Text.get("ExcelReport.LabelTotal"));
        cell.setCellStyle(this.styleNormalFooter);

        row.createCell(1).setCellStyle(this.styleNormalFooter);
        row.createCell(2).setCellStyle(this.styleNormalFooter);
        row.createCell(3).setCellStyle(this.styleNormalFooter);

        cell = row.createCell(4);
        cell.setCellStyle(this.styleRecordFooter);
        cell.setCellValue(totals.records);

        cell = row.createCell(5);
        cell.setCellStyle(this.styleErrorFooter);
        cell.setCellValue(totals.errors);

        cell = row.createCell(6);
        cell.setCellStyle(this.styleWarnFooter);
        cell.setCellValue(totals.warnings);

        cell = row.createCell(7);
        cell.setCellStyle(this.styleInfoFooter);
        cell.setCellValue(totals.notices);

        return totals;
    }

    private Totals createDatasetUnprocessedList(Sheet sheet) {
        // Sort the source names alphabetically
        List<String> sources = new ArrayList<String>(this.unprocessedReasons.keySet());
        Collections.sort(sources);

        Cell cell;
        Row row;
        int currentRowNumber = sheet.getLastRowNum() + 1;

        // Add the group header
        cell = sheet.createRow(currentRowNumber).createCell(0);
        cell.setCellValue(Text.get("ExcelReport.GroupUnprocessed"));
        cell.setCellStyle(this.styleTableTitle);
        sheet.addMergedRegion(new CellRangeAddress(currentRowNumber, currentRowNumber,
                0, 7));

        // Add the column header
        row = sheet.createRow(++currentRowNumber);

        cell = row.createCell(0);
        cell.setCellValue(Text.get("ExcelReport.HeaderName"));
        cell.setCellStyle(this.styleNormalHeader);

        cell = row.createCell(1);
        cell.setCellValue(Text.get("ExcelReport.HeaderLabel"));
        cell.setCellStyle(this.styleNormalHeader);

        cell = row.createCell(2);
        cell.setCellValue(Text.get("ExcelReport.HeaderClass"));
        cell.setCellStyle(this.styleNormalHeader);

        cell = row.createCell(3);
        cell.setCellValue(Text.get("ExcelReport.HeaderReason"));
        cell.setCellStyle(this.styleNormalHeader);
        sheet.addMergedRegion(new CellRangeAddress(currentRowNumber, currentRowNumber,
                3, 4));

        cell = row.createCell(5);
        cell.setCellValue(Text.get("ExcelReport.HeaderErrors"));
        cell.setCellStyle(this.styleErrorHeader);

        cell = row.createCell(6);
        cell.setCellValue(Text.get("ExcelReport.HeaderWarnings"));
        cell.setCellStyle(this.styleWarnHeader);

        cell = row.createCell(7);
        cell.setCellValue(Text.get("ExcelReport.HeaderInfos"));
        cell.setCellStyle(this.styleInfoHeader);

        // List all of the unprocessed sources
        int count = 0;
        Totals totals = new Totals();

        for (String source : sources) {
            EntityDetails details = super.details.get(source);
            String label = Text.get("ExcelReport.LabelUnknown");
            String domainClass = Text.get("ExcelReport.LabelUnknown");

            ++count;

            if (details != null) {
                if (details.hasProperty(Property.Label)) {
                    label = details.getString(Property.Label);
                }

                if (details.hasProperty(Property.Class)) {
                    domainClass = details.getString(Property.Class);
                }
            }

            row = sheet.createRow(++currentRowNumber);
            row.createCell(0).setCellValue(source);
            row.createCell(1).setCellValue(label);
            row.createCell(2).setCellValue(domainClass);
            row.createCell(3).setCellValue(this.unprocessedReasons.get(source));
            row.createCell(4);
            row.createCell(5).setCellValue(
                super.metrics.getDomainCount(source, Message.Type.Error)
            );
            row.createCell(6).setCellValue(
                super.metrics.getDomainCount(source, Message.Type.Warning)
            );
            row.createCell(7).setCellValue(
                super.metrics.getDomainCount(source, Message.Type.Information)
            );

            totals.errors   += super.metrics.getDomainCount(source, Message.Type.Error);
            totals.warnings += super.metrics.getDomainCount(source, Message.Type.Warning);
            totals.notices  += super.metrics.getDomainCount(source, Message.Type.Information);

            for (Cell currentCell : row) {
                currentCell.setCellStyle(count % 2 > 0 ?
                        this.styleOddRows : this.styleEvenRows);
            }

            sheet.addMergedRegion(new CellRangeAddress(currentRowNumber,
                    currentRowNumber, 3, 4));
        }

        row = sheet.createRow(++currentRowNumber);
        cell = row.createCell(0);
        cell.setCellValue(Text.get("ExcelReport.LabelTotal"));
        cell.setCellStyle(this.styleNormalFooter);

        row.createCell(1).setCellStyle(this.styleNormalFooter);
        row.createCell(2).setCellStyle(this.styleNormalFooter);
        row.createCell(3).setCellStyle(this.styleNormalFooter);
        row.createCell(4).setCellStyle(this.styleNormalFooter);

        cell = row.createCell(5);
        cell.setCellStyle(this.styleErrorFooter);
        cell.setCellValue(totals.errors);

        cell = row.createCell(6);
        cell.setCellStyle(this.styleWarnFooter);
        cell.setCellValue(totals.warnings);

        cell = row.createCell(7);
        cell.setCellStyle(this.styleInfoFooter);
        cell.setCellValue(totals.notices);

        return totals;
    }

    private void createDatasetSummary(Sheet sheet) {
        sheet.setAutobreaks(true);
        sheet.getPrintSetup().setFitWidth((short)1);
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(1, 7800);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 3500);
        sheet.setColumnWidth(4, 2600);
        sheet.setColumnWidth(5, 2600);
        sheet.setColumnWidth(6, 2600);
        sheet.setColumnWidth(7, 2600);

        Totals totals = new Totals();

        // Create the standard header
        this.createReportHeader(sheet, 7);

        // Create the list of processed sources
        totals.add(this.createDatasetProcessedList(sheet));

        int processedLocation = sheet.getLastRowNum() + 1;

        // Skip a row
        sheet.createRow(sheet.getLastRowNum() + 1);

        // Create the list of unprocessed sources
        totals.add(this.createDatasetUnprocessedList(sheet));

        int unprocessedLocation = sheet.getLastRowNum() + 1;

        // Skip a row
        sheet.createRow(sheet.getLastRowNum() + 1);

        Row row;
        Cell cell;

        // Create the grand total footer
        row = sheet.createRow(sheet.getLastRowNum() + 1);

        cell = row.createCell(0);
        cell.setCellValue(Text.get("ExcelReport.LabelGrandTotal"));
        cell.setCellStyle(this.styleNormalFooter);

        row.createCell(1).setCellStyle(this.styleNormalFooter);
        row.createCell(2).setCellStyle(this.styleNormalFooter);
        row.createCell(3).setCellStyle(this.styleNormalFooter);

        cell = row.createCell(4);
        cell.setCellStyle(this.styleRecordFooter);
        cell.setCellValue(totals.records);

        cell = row.createCell(5);
        cell.setCellStyle(this.styleErrorFooter);
        cell.setCellValue(totals.errors);

        cell = row.createCell(6);
        cell.setCellStyle(this.styleWarnFooter);
        cell.setCellValue(totals.warnings);

        cell = row.createCell(7);
        cell.setCellStyle(this.styleInfoFooter);
        cell.setCellValue(totals.notices);
    }

    private void createDetails(Sheet sheet) {
        sheet.setColumnWidth(0, super.format == Format.Tabular ? 2600 : 14000);
        sheet.setColumnWidth(1, super.format == Format.Tabular ? 2600 : 5600);

        int offset = 0;
        boolean isTabular = this.format == Format.Tabular;

        if (super.format == Format.Tabular) {
            offset = 2;
            sheet.setColumnWidth(2, 2600);
            sheet.setColumnWidth(3, 5500);
        }

        sheet.setColumnWidth(2 + offset, 5500);
        sheet.setColumnWidth(3 + offset, 3800);
        sheet.setColumnWidth(4 + offset, 3500);
        sheet.setColumnWidth(5 + offset, 14000);
        sheet.setColumnWidth(6 + offset, 3500);
        sheet.setColumnWidth(7 + offset, 2600);

        if (super.useSeverity) {
            sheet.setColumnWidth(8 + offset, 2600);
        }

        // Create the freezepane
        sheet.createFreezePane(0, 1, 0, 1);

        Row row;

        // Create the header record
        row = sheet.createRow(0);

        row.createCell(0).setCellValue(this.format == Format.Tabular ?
                Text.get("ExcelReport.HeaderName") :
                Text.get("ExcelReport.HeaderXpath"));

        if (isTabular) {
            row.createCell(1).setCellValue(Text.get("ExcelReport.HeaderRecord"));
            row.createCell(2).setCellValue(Text.get("ExcelReport.HeaderCount"));
        }

        row.createCell(1 + offset).setCellValue(Text.get("ExcelReport.HeaderVariable"));
        row.createCell(2 + offset).setCellValue(Text.get("ExcelReport.HeaderValue"));
        row.createCell(3 + offset).setCellValue(Text.get("ExcelReport.HeaderRuleID"));
        row.createCell(4 + offset).setCellValue(Text.get("ExcelReport.HeaderPublisherID"));
        row.createCell(5 + offset).setCellValue(Text.get("ExcelReport.HeaderMessage"));
        row.createCell(6 + offset).setCellValue(Text.get("ExcelReport.HeaderCategory"));
        row.createCell(7 + offset).setCellValue(Text.get("ExcelReport.HeaderType"));

        if (super.useSeverity) {
            row.createCell(8 + offset).setCellValue(
                    Text.get("ExcelReport.HeaderSeverity")
            );
        }

        for (Cell cell : row) {
            cell.setCellStyle(this.styleTableTitle);
        }

        // Sort the list of messages first
        Collections.sort(this.messages, new Comparator<Message>() {
            public int compare(Message lhs, Message rhs) {
                EntityDetails lhsEntity = lhs.getEntityDetails();
                EntityDetails rhsEntity = rhs.getEntityDetails();
                String lhsName = lhsEntity.getString(Property.Name);
                String rhsName = rhsEntity.getString(Property.Name);

                int compared = lhsName.compareTo(rhsName);

                if (compared != 0) {
                    boolean lhsGlobal = lhsName.equals("GLOBAL");

                    if (lhsGlobal || rhsName.equals("GLOBAL")) {
                        return lhsGlobal ? -1 : 1;
                    }

                    return compared;
                }

                Reference lhsReference = lhs.getEntityDetails().getReference();
                Reference rhsReference = rhs.getEntityDetails().getReference();

                if (!lhsReference.equals(rhsReference)) {
                    if (lhsReference == Reference.Metadata) {
                        return -1;
                    }

                    return 1;
                }

                if (lhsEntity.hasProperty(Property.Subname) &&
                        rhsEntity.hasProperty(Property.Subname)) {
                    lhsName = lhsEntity.getString(Property.Subname);
                    rhsName = rhsEntity.getString(Property.Subname);

                    if (!lhsName.equals(rhsName)) {
                        return lhsName.compareTo(rhsName);
                    }
                }

                DataDetails lhsRecord = lhs.getRecordDetails();
                DataDetails rhsRecord = rhs.getRecordDetails();

                if (lhsRecord != null && isTerminology(lhs)) {
                    lhsRecord = null;
                }

                if (rhsRecord != null && isTerminology(rhs)) {
                    rhsRecord = null;
                }

                int comparison = lhs.getID().compareTo(rhs.getID());

                if (comparison != 0) {
                    return comparison;
                }

                if (lhsRecord != null || rhsRecord != null) {
                    if (lhsRecord == null) {
                        return -1;
                    } else if (rhsRecord == null) {
                        return 1;
                    }

                    if (lhsRecord.getID() != rhsRecord.getID()) {
                        return lhsRecord.getID() - rhsRecord.getID();
                    }
                }

                return lhs.getMessage().compareTo(rhs.getMessage());
            }
        });

        Iterator<Message> messages = this.messages.iterator();

        Message previous = null;

        while (messages.hasNext() && sheet.getLastRowNum() + 1 < MAX_RECORD_COUNT) {
            Message message = messages.next();
            DataDetails record = message.getRecordDetails();
            String name = getName(message);
            String id = message.getID();

            boolean isTerminology = isTerminology(message);
            boolean isFirst = isTerminology || previous == null || !(
                previous.getID().equals(message.getID()) &&
                getName(previous).equals(name) &&
                previous.getMessage().equals(message.getMessage())
            );

            previous = message;

            if (isTerminology) {
                String key = getTerminologyKey(message);
                Map<String, Long> terms = this.termMap.get(key);

                StringBuilder variables = new StringBuilder();

                for (String variable : message.getVariables()) {
                    // TODO: This is very hacky to support the code/decode case right now
                    if (variable.endsWith("CD")) {
                        if (variables.length() > 0) {
                            variables.insert(0, ", ");
                        }

                        variables.insert(0, variable);
                    } else {
                        if (variables.length() > 0) {
                            variables.append(", ");
                        }

                        variables.append(variable);
                    }
                }

                for (Map.Entry<String, Long> term : terms.entrySet()) {
                    if (sheet.getLastRowNum() + 1 == MAX_RECORD_COUNT) {
                        break;
                    }

                    this.createDetailRecord(
                        sheet,
                        offset,
                        name,
                        true,
                        isFirst,
                        term.getValue(),
                        variables.toString(),
                        term.getKey(),
                        id,
                        message.getMessage(),
                        message.getCategory(),
                        message.getType(),
                        message.getSeverity()
                    );

                    isFirst = false;
                }
            } else {
                StringBuilder variables = new StringBuilder();
                StringBuilder values = new StringBuilder();
                int length = 0;

                for (String variable : message.getVariables()) {
                    variables.append(variable).append(", ");
                    values.append(
                        transformLeadingSpaces(message.getValueOf(variable))
                    ).append(", ");
                }

                length = variables.length();

                this.createDetailRecord(
                    sheet,
                    offset,
                    name,
                    false,
                    isFirst,
                    record != null && isTabular && record.getInfo() == Info.Data ?
                        (long)record.getID() : null,
                    length > 2 ? variables.substring(0, length - 2) : "",
                    length > 2 ? values.substring(0, values.length() - 2) : "",
                    id,
                    message.getMessage(),
                    message.getCategory(),
                    message.getType(),
                    message.getSeverity()
                );
            }

            // Remove the message from the list
            messages.remove();
        }
    }

    private void createDetailRecord(Sheet sheet, int offset, String name, boolean isTerminology,
            boolean isFirst, Long record, String variables, String values, String id,
            String message, String category, Message.Type type, Message.Severity severity) {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        Cell cell;

        cell = row.createCell(0);
        cell.setCellValue(name);
        cell.setCellStyle(this.styleEvenRows);

        if (super.format == Format.Tabular) {
            cell = row.createCell(1);
            cell.setCellStyle(this.styleEvenRows);

            if (record != null && !isTerminology) {
                cell.setCellValue(record);
            }

            cell = row.createCell(2);
            cell.setCellStyle(this.styleEvenRows);

            if (record != null && isTerminology) {
                cell.setCellValue(record);
            }
        }

        cell = row.createCell(1 + offset);
        cell.setCellValue(variables);
        cell.setCellStyle(this.styleEvenRows);

        cell = row.createCell(2 + offset);
        cell.setCellValue(values);
        cell.setCellStyle(this.styleEvenRows);

        cell = row.createCell(3 + offset);
        cell.setCellValue(id);
        cell.setCellStyle(this.styleEvenRows);

        if (isFirst) {
            this.makeHyperlink(cell, id, false);
        }

        cell = row.createCell(4 + offset);
        cell.setCellValue(super.getPublisherId(id));
        cell.setCellStyle(this.styleEvenRows);

        if (isFirst && super.getPublisherId(id).length() > 0) {
            this.makeHyperlink(cell, id, false);
        }

        cell = row.createCell(5 + offset);
        cell.setCellValue(message);
        cell.setCellStyle(this.styleEvenRows);

        cell = row.createCell(6 + offset);
        cell.setCellValue(category);
        cell.setCellStyle(this.styleEvenRows);

        cell = row.createCell(7 + offset);
        cell.setCellValue(type.toString());
        cell.setCellStyle(this.styleEvenRows);

        if (super.useSeverity) {
            cell = row.createCell(8 + offset);
            cell.setCellValue(translateSeverity(severity, type).toString());
            cell.setCellStyle(this.styleEvenRows);
        }
    }

    private void createIssueSummary(Sheet sheet) {
        sheet.setAutobreaks(true);
        sheet.getPrintSetup().setFitWidth((short) 1);
        sheet.setColumnWidth(0, 2600);
        sheet.setColumnWidth(1, 3800);
        sheet.setColumnWidth(2, 3500);
        sheet.setColumnWidth(3, 22600);
        sheet.setColumnWidth(4, 3500);
        sheet.setColumnWidth(5, 2600);

        int width = 5;

        // Create the standard header
        this.createReportHeader(sheet, width);

        Cell cell;
        Row row;
        int currentRowNumber = sheet.getLastRowNum() + 1;

        cell = sheet.createRow(currentRowNumber).createCell(0);
        cell.setCellValue(Text.get("ExcelReport.GroupIssues"));
        cell.setCellStyle(this.styleTableTitle);
        sheet.addMergedRegion(new CellRangeAddress(currentRowNumber, currentRowNumber,
                0, width));

        row = sheet.createRow(++currentRowNumber);

        cell = row.createCell(0);
        cell.setCellValue(Text.get("ExcelReport.HeaderSource"));
        cell.setCellStyle(this.styleNormalHeader);
        cell = row.createCell(1);
        cell.setCellValue(Text.get("ExcelReport.HeaderRuleID"));
        cell.setCellStyle(this.styleNormalHeader);
        cell = row.createCell(2);
        cell.setCellValue(Text.get("ExcelReport.HeaderPublisherID"));
        cell.setCellStyle(this.styleNormalHeader);
        cell = row.createCell(3);
        cell.setCellValue(Text.get("ExcelReport.HeaderMessage"));
        cell.setCellStyle(this.styleNormalHeader);
        cell = row.createCell(4);
        cell.setCellValue(Text.get("ExcelReport.HeaderType"));
        cell.setCellStyle(this.styleNormalHeader);
        cell = row.createCell(5);
        cell.setCellValue(Text.get("ExcelReport.HeaderFound"));
        cell.setCellStyle(this.styleNormalHeaderRight);

        for (Map.Entry<String, Map<Message.Type, SortedSet<String>>> entry :
                this.issueMap.entrySet()) {
            Map<Message.Type, SortedSet<String>> typeMap = entry.getValue();

            boolean empty = true;

            for (Message.Type type : Message.Type.values()) {
                if (!typeMap.get(type).isEmpty()) {
                    empty = false;
                    break;
                }
            }

            if (empty) {
                continue;
            }

            currentRowNumber = sheet.getLastRowNum();
            row = sheet.createRow(++currentRowNumber);

            // We have to apply the border to each cell for whatever reason
            for (int i = 0; i <= width; ++i) {
                cell = row.createCell(i);
                cell.setCellStyle(this.styleSourceHeader);

                if (i == 0) {
                    cell.setCellValue(entry.getKey());
                }
            }

            sheet.addMergedRegion(new CellRangeAddress(currentRowNumber, currentRowNumber, 0, width));

            boolean isEven = true;

            isEven = this.createIssueSummaryGroup(sheet, entry.getKey(), isEven,
                    typeMap.get(Message.Type.Error), Message.Type.Error);
            isEven = this.createIssueSummaryGroup(sheet, entry.getKey(), isEven,
                    typeMap.get(Message.Type.Warning), Message.Type.Warning);
            this.createIssueSummaryGroup(sheet, entry.getKey(), isEven,
                    typeMap.get(Message.Type.Information), Message.Type.Information);
        }
    }

    private boolean createIssueSummaryGroup(Sheet sheet, String domain, boolean isEven, SortedSet<String> ids, Message.Type type) {
        // Check if we'll have anything to print out
        if (ids.isEmpty()) {
            return isEven;
        }

        int currentRowNumber = sheet.getLastRowNum();
        Row row;
        Cell cell;

        for (String id : ids) {
            List<String> messages = new ArrayList<String>(super.metrics.getDomainMessages(domain, id));
            Collections.sort(messages);

            for (String message : messages) {
                long total = super.metrics.getDomainMessageCount(domain, id, message);

                if (total > 0) {
                    isEven = !isEven;

                    row = sheet.createRow(++currentRowNumber);

                    cell = row.createCell(0);
                    cell.setCellStyle(isEven ?
                            this.styleEvenRows : this.styleOddRows);

                    cell = row.createCell(1);
                    cell.setCellValue(id);

                    if (!this.makeHyperlink(cell, id, !isEven)) {
                        cell.setCellStyle(isEven ? this.styleEvenRows : this.styleOddRows);
                    }

                    cell = row.createCell(2);
                    cell.setCellValue(super.getPublisherId(id));
                    cell.setCellStyle(isEven ?
                            this.styleEvenRows : this.styleOddRows);

                    if (super.getPublisherId(id).length() == 0 || !this.makeHyperlink(cell, id, !isEven)) {
                        cell.setCellStyle(isEven ? this.styleEvenRows : this.styleOddRows);
                    }

                    cell = row.createCell(3);
                    cell.setCellValue(message);
                    cell.setCellStyle(isEven ?
                            this.styleEvenRows : this.styleOddRows);

                    cell = row.createCell(4);
                    cell.setCellValue(type.toString());

                    if (type == Message.Type.Error) {
                        cell.setCellStyle(this.styleErrorOddRows);
                    } else if (type == Message.Type.Warning) {
                        cell.setCellStyle(this.styleWarnOddRows);
                    } else {
                        cell.setCellStyle(this.styleInfoOddRows);
                        // FIXME: Hack to change Information -> Notice
                        cell.setCellValue("Notice");
                    }

                    cell = row.createCell(5);
                    cell.setCellValue(total);
                    cell.setCellStyle(isEven ?
                            this.styleEvenRows : this.styleOddRows);
                }
            }
        }

        return isEven;
    }

    private void createReportHeader(Sheet sheet, int width) {
        Cell cell;

        // Report title
        cell = sheet.createRow(0).createCell(0);
        cell.setCellValue(Text.get("ExcelReport.ReportTitle"));
        cell.setCellStyle(this.styleReportTitle);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, width));

        // Configuration listing
        cell = sheet.createRow(2).createCell(0);
        cell.setCellValue(String.format(
                Text.get("ExcelReport.ConfigurationListing"),
                super.config.getConfig()
        ));
        cell.setCellStyle(this.styleReportHeader);
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, width));

        // Define listing
        String define = super.config.getDefine();

        if (super.format == Format.Hierarchical) {
            if (super.details.containsKey("define")) {
                define = super.details.get("define").getString(Property.Location);
            }
        }

        cell = sheet.createRow(3).createCell(0);
        cell.setCellValue(String.format(
                Text.get("ExcelReport.DefineListing"),
                define != null ? define : Text.get("ExcelReport.DefineMissing")
        ));
        cell.setCellStyle(this.styleReportHeader);
        sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, width));

        // Generation date listing
        cell = sheet.createRow(4).createCell(0);
        cell.setCellValue(String.format(
                Text.get("ExcelReport.GenerationListing"),
                ISO_DATETIME_FORMAT.format(this.creationTime)
        ));
        cell.setCellStyle(this.styleReportHeader);
        sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, width));

        int index;

        for (Map.Entry<String, String> header : super.options.getHeaders().entrySet()) {
            index = sheet.getLastRowNum() + 1;
            cell = sheet.createRow(index).createCell(0);

            cell.setCellValue(String.format("%s: %s", header.getKey(), header.getValue()));
            cell.setCellStyle(this.styleReportHeader);

            sheet.addMergedRegion(new CellRangeAddress(index, index, 0, width));
        }

        // Skip a row
        sheet.createRow(sheet.getLastRowNum() + 1);
    }

    private void createRules(Sheet sheet) {
        sheet.setColumnWidth(0, 3800);
        sheet.setColumnWidth(1, 3500);
        sheet.setColumnWidth(2, 10600);
        sheet.setColumnWidth(3, 20500);
        sheet.setColumnWidth(4, 3500);
        sheet.setColumnWidth(5, 2600);

        Row row = sheet.createRow(0);

        row.createCell(0).setCellValue(Text.get("ExcelReport.HeaderRuleID"));
        row.createCell(1).setCellValue(Text.get("ExcelReport.HeaderPublisherID"));
        row.createCell(2).setCellValue(Text.get("ExcelReport.HeaderMessage"));
        row.createCell(3).setCellValue(Text.get("ExcelReport.HeaderDescription"));
        row.createCell(4).setCellValue(Text.get("ExcelReport.HeaderCategory"));
        row.createCell(5).setCellValue(Text.get("ExcelReport.HeaderType"));

        // If we're using severity, tack on an extra column
        if (super.useSeverity) {
            sheet.setColumnWidth(6, 2600);
            row.createCell(6).setCellValue(Text.get("ExcelReport.HeaderSeverity"));
        }

        for (Cell cell : row) {
            cell.setCellStyle(this.styleTableTitle);
        }

        // Create the freezepane
        sheet.createFreezePane(0, 1, 0, 1);

        List<MessageTemplate> messages = super.getMessages();

        // Sort rules by ID
        Collections.sort(messages, new Comparator<MessageTemplate>() {
            public int compare(MessageTemplate o1, MessageTemplate o2) {
                return o1.getID().compareTo(o2.getID());
            }
        });

        // Print out the list of messages
        for (MessageTemplate message : messages) {
            int currentRowNumber = sheet.getLastRowNum() + 1;
            String messageID = message.getID();
            Message.Type type = message.getType();
            row = sheet.createRow(currentRowNumber);

            row.createCell(0).setCellValue(messageID);
            row.createCell(1).setCellValue(message.getPublisherId());
            row.createCell(2).setCellValue(message.getMessage().replace("%Domain%", "--"));
            row.createCell(3).setCellValue(message.getDescription());
            row.createCell(4).setCellValue(message.getCategory());
            row.createCell(5).setCellValue(type.toString());

            if (super.useSeverity) {
                Message.Severity severity = translateSeverity(message.getSeverity(),
                        type);

                row.createCell(6).setCellValue(severity.toString());
            }

            for (Cell cell : row) {
                cell.setCellStyle(currentRowNumber % 2 > 0 ?
                        this.styleOddRows : this.styleEvenRows);
            }

            this.locations.put(messageID, currentRowNumber);
        }
    }

    private void createStyles() {
        CellStyle style;
        Font font;

        CellStyle styleDefault = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setFontHeightInPoints((short)10);
        style.setFont(font);

        // Report title style
        this.styleReportTitle = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setFontHeightInPoints((short)14);
        style.setFont(font);
        style.setAlignment(CellStyle.ALIGN_CENTER);

        // Report header style
        this.styleReportHeader = style = this.report.createCellStyle();
        style.cloneStyleFrom(styleDefault);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        Colors.ReportHeaderFill.color(style);

        // Table title style
        this.styleTableTitle = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setColor(IndexedColors.WHITE.getIndex());
        font.setFontHeightInPoints((short)11);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        Colors.TableTitleFill.color(style);

        // Normal header style
        this.styleNormalHeader = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setFontHeightInPoints((short)11);
        Colors.HeaderFont.color(font);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        Colors.HeaderFill.color(style);

        this.styleNormalHeaderRight = style = this.report.createCellStyle();
        style.cloneStyleFrom(this.styleNormalHeader);
        style.setAlignment(CellStyle.ALIGN_RIGHT);

        // Normal footer style
        this.styleNormalFooter = style = this.report.createCellStyle();
        style.setFont(font);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        Colors.TableTitleFill.colorTopBorder(style);
        Colors.TableTitleFill.colorBottomBorder(style);

        // Even row style
        this.styleEvenRows = style = this.report.createCellStyle();
        style.cloneStyleFrom(styleDefault);
        style.setWrapText(true);

        // Odd row style
        this.styleOddRows = style = this.report.createCellStyle();
        style.cloneStyleFrom(styleDefault);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setWrapText(true);
        Colors.ReportHeaderFill.color(style);
        Colors.HeaderFill.colorTopBorder(style);
        Colors.HeaderFill.colorBottomBorder(style);

        // Record footer style
        this.styleRecordFooter = style = this.report.createCellStyle();
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        Colors.HeaderFill.color(style);
        Colors.TableTitleFill.colorTopBorder(style);
        Colors.TableTitleFill.colorBottomBorder(style);

        // Domain header style
        this.styleSourceHeader = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setFontHeightInPoints((short)11);
        Colors.SourceHeaderFont.color(font);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_THIN);
        Colors.SourceHeaderFill.color(style);
        Colors.HeaderFill.colorTopBorder(style);

        // Error header style
        this.styleErrorHeader = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setFontHeightInPoints((short)11);
        Colors.ErrorFont.color(font);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        Colors.ErrorFill.color(style);

        // Error footer style
        this.styleErrorFooter = style = this.report.createCellStyle();
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        Colors.ErrorFill.color(style);
        Colors.TableTitleFill.colorTopBorder(style);
        Colors.TableTitleFill.colorBottomBorder(style);

        this.styleErrorOddRows = style = this.report.createCellStyle();
        style.cloneStyleFrom(this.styleOddRows);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        Colors.ErrorFill.color(style);

        // Warning header style
        this.styleWarnHeader = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setFontHeightInPoints((short)11);
        Colors.WarningFont.color(font);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        Colors.WarningFill.color(style);

        // Warning footer style
        this.styleWarnFooter = style = this.report.createCellStyle();
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        Colors.WarningFill.color(style);
        Colors.TableTitleFill.colorTopBorder(style);
        Colors.TableTitleFill.colorBottomBorder(style);

        this.styleWarnOddRows = style = this.report.createCellStyle();
        style.cloneStyleFrom(this.styleOddRows);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        Colors.WarningFill.color(style);

        // Info header style
        this.styleInfoHeader = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setFontHeightInPoints((short)11);
        Colors.InfoFont.color(font);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        Colors.InfoFill.color(style);

        // Info footer style
        this.styleInfoFooter = style = this.report.createCellStyle();
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        Colors.InfoFill.color(style);
        Colors.TableTitleFill.colorTopBorder(style);
        Colors.TableTitleFill.colorBottomBorder(style);

        this.styleInfoOddRows = style = this.report.createCellStyle();
        style.cloneStyleFrom(this.styleOddRows);
        style.setFont(font);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        Colors.InfoFill.color(style);

        // Hyperlink style
        this.styleHyperlink = style = this.report.createCellStyle();
        font = this.report.createFont();
        font.setFontName(DISPLAY_FONT);
        font.setUnderline(Font.U_SINGLE);
        font.setFontHeightInPoints((short)10);
        Colors.InfoFont.color(font);
        style.setFont(font);

        this.styleHyperlinkAlt = style = this.report.createCellStyle();
        style.cloneStyleFrom(this.styleHyperlink);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        Colors.ReportHeaderFill.color(style);
        Colors.HeaderFill.colorTopBorder(style);
        Colors.HeaderFill.colorBottomBorder(style);
    }

    private boolean makeHyperlink(Cell cell, String id, boolean useAlt) {
        if (this.locations.get(id) != null) {
            CreationHelper helper = this.report.getCreationHelper();
            Hyperlink link = helper.createHyperlink(Hyperlink.LINK_DOCUMENT);
            link.setAddress(String.format(
                    "'Rules'!A%d",
                    this.locations.get(id) + 1
            ));
            cell.setHyperlink(link);
            cell.setCellStyle(useAlt ? this.styleHyperlinkAlt : this.styleHyperlink);

            return true;
        }

        return false;
    }

    private static String getName(Message message) {
        EntityDetails entity = message.getEntityDetails();
        DataDetails record = message.getRecordDetails();
        String name = record != null && record.getInfo() == Info.Xpath ?
                record.getName() : entity.getString(Property.Name);
        String subname = entity.hasProperty(Property.Split) && entity.hasProperty(Property.Subname) ?
                entity.getString(Property.Subname) : null;

        if (subname != null) {
            name += " (" + subname + ")";
        }

        return name;
    }

    private static Message.Severity translateSeverity(Message.Severity severity,
            Message.Type type) {
        if (severity == null) {
            if (type == Message.Type.Error) {
                severity = Message.Severity.High;
            } else if (type == Message.Type.Warning) {
                severity = Message.Severity.Medium;
            } else {
                severity = Message.Severity.Low;
            }
        }

        return severity;
    }

    private static String getTerminologyKey(Message message) {
        // We're making a compound key to avoid map nesting hell
        String messageText = message.getMessage();
        StringBuilder buffer = new StringBuilder(11 + messageText.length())
            .append(message.getEntityDetails().getString(Property.Name))
            .append('.')
            .append(message.getID())
            .append('.')
            .append(messageText);

        return buffer.toString();
    }

    private static boolean isTerminology(Message message) {
        return message.getCategory().equalsIgnoreCase("Terminology");
    }

    private static String transformLeadingSpaces(String text) {
        if (text == null) {
            return text;
        }

        String trimmed = StringUtils.stripStart(text, null);
        int difference = text.length() - trimmed.length();

        if (difference > 0) {
            text = String.format("[%d spaces]%s", difference, trimmed);
        }

        return text;
    }

    private static class Totals {
        int records = 0;
        int errors = 0;
        int warnings = 0;
        int notices = 0;

        void add(Totals totals) {
            this.records  += totals.records;
            this.errors   += totals.errors;
            this.warnings += totals.warnings;
            this.notices  += totals.notices;
        }
    }
}
