/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.report;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.TransformerConfigurationException;

import org.opencdisc.validator.ConfigOptions;
import org.opencdisc.validator.ReportOptions;
import org.opencdisc.validator.Text;
import org.opencdisc.validator.Validator.Format;
import org.opencdisc.validator.model.DataDetails;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.util.XmlWriter;
import org.opencdisc.validator.util.XmlWriter.SimpleAttributes;
import org.xml.sax.SAXException;

/**
 *
 * @author Tim Stone
 */
public class XmlReporter extends AbstractReporter implements Reporter {
    private final static int BUFFER_SIZE = 20;
    private final static String REPORT_NAMESPACE =
        "http://www.opencdisc.org/schema/validator-report";
    private final static String SCHEMA_LOCATION =
        "http://www.opencdisc.org/schema/opencdisc-validator-report-1.0.xsd";

    private final List<Message> buffer = new ArrayList<Message>();
    private final XmlWriter writer;

    /**
     *
     * @param options
     * @param config
     * @param format
     * @param messages
     * @throws SAXException
     * @throws IOException
     * @throws TransformerConfigurationException
     */
    public XmlReporter(ReportOptions options, ConfigOptions config, Format format,
            List<MessageTemplate> messages) throws TransformerConfigurationException,
            IOException, SAXException {
        super(options, config, format, messages);

        File report = new File(options.getReport());

        if (options.getOverwrite() && report.exists()) {
            report.delete();
        }

        this.writer = new XmlWriter(report, REPORT_NAMESPACE);

        SimpleAttributes attributes = this.writer.newAttributes()
            .addAttribute(XmlWriter.SCHEMA_NAMESPACE_URI, "schemaLocation",
                    String.format("%s %s", REPORT_NAMESPACE, SCHEMA_LOCATION)
            );

        this.writer.startElement("report", attributes);
    }

    /**
     *
     */
    public void close() {
        try {
            this.flushBuffer();
            this.writer.endElement("report");
            this.writer.close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } catch (SAXException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     *
     */
    public boolean write(Message message) {
        boolean wrote = false;

        if (this.checkFilters(message)) {
            wrote = true;

            this.bufferMessage(message);
        }

        return wrote;
    }

    /**
     *
     * @param message
     */
    private void bufferMessage(Message message) {
        this.buffer.add(message);

        if (this.buffer.size() == BUFFER_SIZE) {
            this.flushBuffer();
        }
    }

    /**
     *
     */
    private void flushBuffer() {
        for (Message message : this.buffer) {
            EntityDetails entity = message.getEntityDetails();
            DataDetails details = message.getRecordDetails();
            SimpleAttributes attributes = this.writer.newAttributes()
                .addAttribute("code", message.getID())
                .addAttribute("time", message.getPostTimestamp());

            try {
                this.writer.startElement("issue", attributes);

                attributes = this.writer.newAttributes()
                    .addAttribute("name", entity.getString(Property.Name))
                    .addAttribute("location", entity.getString(Property.Location));

                if (entity.hasProperty(Property.Label)) {
                    attributes.addAttribute("label", entity.getString(Property.Label));
                }

                if (entity.hasProperty(Property.Class)) {
                    attributes.addAttribute("class", entity.getString(Property.Class));
                }

                if (entity.hasProperty(Property.Subname) &&
                        entity.hasProperty(Property.Split) &&
                        entity.getBoolean(Property.Split)) {
                    attributes.addAttribute("subname",
                            entity.getString(Property.Subname));
                }

                this.writer.startElement("source", attributes);

                if (details != null) {
                    DataDetails.Info type = details.getInfo();

                    attributes = this.writer.newAttributes()
                        .addAttribute("type", type.toString())
                        .addAttribute("identifier", type == DataDetails.Info.Data ?
                                Integer.toString(details.getID()) : details.getName());

                    if (type == DataDetails.Info.Variable) {
                        attributes.addAttribute("position",
                                Integer.toString(details.getID()));
                    }
                } else {
                    attributes = this.writer.newAttributes();

                    // Only system-generated messages should not have details
                    if (message.getCategory().equalsIgnoreCase("System")) {
                        attributes.addAttribute("type", "System")
                            .addAttribute("identifier", "system-internal");
                    }
                }

                this.writer.simpleElement("details", attributes);

                this.writer.endElement("source");

                this.writer.simpleElement("message", message.getMessage());
                this.writer.simpleElement("description", message.getDescription());
                this.writer.simpleElement("type", message.getType().toString());
                this.writer.simpleElement("category", message.getCategory());

                if (message.getSeverity() != null) {
                    this.writer.simpleElement("severity",
                            message.getSeverity().toString());
                }

                this.writer.endElement("issue");
            } catch (SAXException ex) {
                // TODO: This isn't recoverable, right?
                throw new RuntimeException(ex);
            }
        }
    }
}
