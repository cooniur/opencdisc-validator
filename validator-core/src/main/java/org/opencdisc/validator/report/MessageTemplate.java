/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.report;

import org.opencdisc.validator.model.Message.Severity;
import org.opencdisc.validator.model.Message.Type;

/**
 *
 * @author Tim Stone
 */
public class MessageTemplate {
    private final String category;
    private final String description;
    private final String publisherId;
    private final String ID;
    private final String message;
    private final Severity severity;
    private final Type type;

    public MessageTemplate(String id, String message, String description,
           String category, Type type, Severity severity) {
        this(id, null, message, description, category, type, severity);
    }

    public MessageTemplate(String ID, String publisherId, String message, String description,
            String category, Type type, Severity severity) {
        this.ID = ID;
        this.publisherId = publisherId;
        this.message = message;
        this.description = description;
        this.category = category;
        this.type = type;
        this.severity = severity;
    }

    public String getCategory() {
        return this.category;
    }

    public String getDescription() {
        return this.description;
    }

    public String getID() {
        return this.ID;
    }

    public String getPublisherId() { return this.publisherId; }

    public String getMessage() {
        return this.message;
    }

    public Severity getSeverity() {
        return this.severity;
    }

    public Type getType() {
        return this.type;
    }
}
