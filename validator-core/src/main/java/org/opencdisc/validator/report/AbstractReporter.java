/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.report;

import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.validator.ConfigOptions;
import org.opencdisc.validator.ReportOptions;
import org.opencdisc.validator.Validator.Format;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.MessageFilter;
import org.opencdisc.validator.model.MessageMetrics;
import org.opencdisc.validator.model.EntityDetails.Property;
import org.opencdisc.validator.util.KeyMap;

/**
 *
 * @author Tim Stone
 */
public abstract class AbstractReporter implements Reporter {
    protected final ConfigOptions config;
    protected final Map<String, EntityDetails> details = new KeyMap<EntityDetails>();
    protected final Format format;
    protected final Map<String, MessageTemplate> messages = new HashMap<String, MessageTemplate>();
    protected final MessageMetrics metrics;
    protected final ReportOptions options;
    protected final boolean useSeverity;

    private final Set<MessageFilter> filters = new HashSet<MessageFilter>();
    private boolean overflowed = false;

    protected AbstractReporter(ReportOptions options, ConfigOptions config,
            Format format, List<MessageTemplate> messages) {
        if (options == null) {
            throw new IllegalArgumentException("options cannot be null");
        }

        this.config = config;
        this.format = format;
        this.metrics = new MessageMetrics();
        this.options = options;
        List<MessageFilter> filters = options.getFilters();

        boolean useSeverity = false;

        for (MessageTemplate message : messages) {
            this.messages.put(message.getID(), message);

            if (message.getSeverity() != null) {
                useSeverity = true;
            }
        }

        this.useSeverity = useSeverity;

        if (filters != null) {
            this.filters.addAll(filters);
        }
    }

    public void declare(EntityDetails details) {
        String name = details.getString(Property.Name);

        if (!this.details.containsKey(name)) {
            this.details.put(name, details);
        }
    }

    public List<MessageTemplate> getMessages() {
        return new ArrayList<MessageTemplate>(this.messages.values());
    }

    public MessageMetrics getMetrics() {
        return this.metrics;
    }

    public boolean hasOverflowed() {
        return this.overflowed;
    }

    public int write(List<Message> messages) {
        int wrote = 0;

        for (Message message : messages) {
            if (this.write(message)) {
                ++wrote;
            }
        }

        return wrote;
    }

    protected String getPublisherId(String ruleId) {
        MessageTemplate template = this.messages.get(ruleId);

        return template != null ? template.getPublisherId() : "";
    }

    protected boolean checkFilters(Message message) {
        if (this.options.getGenerateMetrics()) {
            this.metrics.processMessage(message);
        }

        for (MessageFilter filter : this.filters) {
            if (!filter.accept(message)) {
                return false;
            }
        }

        return true;
    }

    protected void triggerOverflow() {
        this.overflowed = true;
    }
}
