/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.report;

import java.util.List;
import java.util.Queue;

import org.opencdisc.validator.ConfigOptions;
import org.opencdisc.validator.ReportOptions;
import org.opencdisc.validator.Validator.Format;
import org.opencdisc.validator.model.Message;

/**
 *
 * @author Tim Stone
 */
public class QueueReporter extends AbstractReporter implements Reporter {
    private Queue<Message> messages;

    /**
     *
     * @param messages
     */
    public QueueReporter(ReportOptions options, ConfigOptions config,
            Format format, List<MessageTemplate> messages) {
        super(options, config, format, messages);

        this.messages = options.getReportQueue();
    }

    public void close() {
        // Do nothing
    }

    public boolean write(Message message) {
        boolean wrote = false;

        if (super.checkFilters(message)) {
            wrote = true;

            this.messages.add(message);
        }

        return wrote;
    }
}
