/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.report;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.TransformerException;

import org.opencdisc.validator.model.DataDetails;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.Message.Severity;
import org.opencdisc.validator.model.Message.Type;
import org.opencdisc.validator.model.SimpleMetrics;
import org.opencdisc.validator.util.Helpers;
import org.opencdisc.validator.util.KeyMap;
import org.opencdisc.validator.util.Settings;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 *
 * @author Tim Stone
 */
public class SchematronResultTransformer implements ContentHandler, ErrorListener {
    private static final String ALT_TAG_NAME = "fired-rule";
    private static final String MESSAGE_TAG_NAME = "successful-report";
    private static final String MESSAGE_ALT_TAG_NAME = "failed-assert";
    private static final String MESSAGE_INFO_TAG_NAME = "diagnostic-reference";
    private static final String MESSAGE_MESSAGE_TAG_NAME = "text";
    private static final String MESSAGE_INFO_ATTRIBUTE_NAME = "diagnostic";
    private static final String EXCEPTION_MESSAGE_ID = "OD0001";

    private enum Info {
        Message,
        Rule,
        XPath,
        Variable,
        Value
    }

    private String alternateXpath  = "";
    private Info currentInfo = null;
    private final EntityDetails entity;
    private StringBuilder message = new StringBuilder();
    private Map<String, MessageTemplate> messages = new KeyMap<MessageTemplate>();
    private SimpleMetrics metrics;
    private final Reporter reporter;
    private StringBuilder ruleID = new StringBuilder();
    private boolean startedMessage = false;
    private StringBuilder value = new StringBuilder();
    private StringBuilder variable = new StringBuilder();
    private StringBuilder xpath = new StringBuilder();
    private final MessageDigest digest;

    /**
     * Creates a new <code>SchematronResultTransformer</code> that will convert
     * Schematron-generated log SAX events into <code>Message</code> objects that can be
     * passed on to the provided <code>Reporter</code>, using the provided list of
     * <code>Message</code> objects to supplement the information returned by the
     * Schematron result.
     *
     * @param entity
     * @param reporter
     * @param metrics
     */
    public SchematronResultTransformer(EntityDetails entity, Reporter reporter,
            SimpleMetrics metrics) {
        if (reporter == null) {
            throw new NullPointerException("Reporter cannot be null");
        }

        for (MessageTemplate message : reporter.getMessages()) {
            this.messages.put(message.getID(), message);
        }

        this.entity = entity;
        this.reporter = reporter;
        this.metrics = metrics;

        MessageDigest digest = null;

        if (Settings.defines("Engine.GenerateKeys")) {
            try {
                digest = MessageDigest.getInstance("SHA-1");
            } catch (NoSuchAlgorithmException ignore) {}
        }

        this.digest = digest;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.currentInfo != null) {
            switch (this.currentInfo) {
                case Message:
                    this.message.append(ch);
                    break;

                case Rule:
                    this.ruleID.append(ch);
                    break;

                case XPath:
                    this.xpath.append(ch);
                    break;

                case Variable:
                    this.variable.append(ch);
                    break;

                case Value:
                    this.value.append(ch);
                    break;
            }
        }
    }

    public void endDocument() throws SAXException {

    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        // Gather metrics
        if (localName.equalsIgnoreCase(ALT_TAG_NAME) ||
                localName.equalsIgnoreCase(MESSAGE_TAG_NAME) ||
                localName.equalsIgnoreCase(MESSAGE_ALT_TAG_NAME)) {
            this.metrics.updateChecksPerformed();
        }

        if (localName.equalsIgnoreCase(MESSAGE_TAG_NAME) ||
                localName.equalsIgnoreCase(MESSAGE_ALT_TAG_NAME)) {
            if (!this.startedMessage) {
                // TODO: Something is wrong with the output (unlikely)
            } else {
                // Assemble the Message object
                MessageTemplate template = this.messages.get(
                        this.ruleID.toString().trim()
                );

                String category = null;
                String description = null;
                Severity severity = null;
                Type type = null;
                String xpath = this.xpath.toString().trim();

                if (template != null) {
                    category = template.getCategory();
                    description = template.getDescription();
                    severity = template.getSeverity();
                    type = template.getType();
                }

                if (xpath.length() == 0) {
                    xpath = this.alternateXpath;
                }

                DataDetails details = new DataDetails(xpath, DataDetails.Info.Xpath);

                if (this.digest != null) {
                    details.setKey(Helpers.toHexString(
                        this.digest.digest(xpath.getBytes())
                    ));
                }

                Message message  = new Message(
                    this.ruleID.toString().trim(),
                    category,
                    this.message.toString().trim(),
                    description,
                    type,
                    severity,
                    this.entity,
                    details
                );

                message.setValueOf(this.variable.toString().trim(),
                    this.value.toString().trim());

                // Write the message to the log
                this.metrics.updateMessageCount();
                this.reporter.write(message);

                // Clear the StringBuffers
                this.message.setLength(0);
                this.ruleID.setLength(0);
                this.xpath.setLength(0);
                this.variable.setLength(0);
                this.value.setLength(0);
                this.alternateXpath = "";
            }

            this.startedMessage = false;
        } else if (localName.equalsIgnoreCase(MESSAGE_MESSAGE_TAG_NAME) ||
                localName.equalsIgnoreCase(MESSAGE_INFO_TAG_NAME)) {
            this.currentInfo = null;
        }
    }

    public void endPrefixMapping(String prefix) throws SAXException {
        // Ignore
    }

    public void error(TransformerException ex) throws TransformerException {
        this.parseException(ex, Message.Type.Error, null);
    }

    public void fatalError(TransformerException ex) throws TransformerException {
        this.parseException(ex, Message.Type.Error, null);
    }

    public void ignorableWhitespace(char[] ch, int start, int length)
            throws SAXException {
        // Ignore
    }

    public void processingInstruction(String target, String data) throws SAXException {
        // Ignore
    }

    public void setDocumentLocator(Locator locator) {
        // Ignore
    }

    public void skippedEntity(String name) throws SAXException {
        // Ignore
    }

    public void startDocument() throws SAXException {

    }

    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        if (localName.equalsIgnoreCase(MESSAGE_TAG_NAME) ||
                localName.equalsIgnoreCase(MESSAGE_ALT_TAG_NAME)) {
            if (this.startedMessage) {
                // TODO: Something is wrong with the output (unlikely)
            }

            String location = attributes.getValue("location");

            if (location != null) {
                this.alternateXpath = location.replaceAll("\\[namespace-uri[^\\]]+\\]",
                    "");
            }

            this.startedMessage = true;
        } else if (localName.equalsIgnoreCase(MESSAGE_MESSAGE_TAG_NAME)) {
            this.currentInfo = Info.Message;
        } else if (localName.equalsIgnoreCase(MESSAGE_INFO_TAG_NAME)) {
            String diagnostic = attributes.getValue(MESSAGE_INFO_ATTRIBUTE_NAME);
            String[] pieces   = diagnostic.split("-");
            String infoType   = pieces[0];

            if (infoType.equalsIgnoreCase(Info.Rule.toString())) {
                this.currentInfo = Info.Rule;
            } else if (infoType.equalsIgnoreCase(Info.XPath.toString())) {
                this.currentInfo = Info.XPath;
            } else if (infoType.equalsIgnoreCase(Info.Variable.toString())) {
                this.currentInfo = Info.Variable;
            } else if (infoType.equalsIgnoreCase(Info.Value.toString())) {
                this.currentInfo = Info.Value;
            }
        }
    }

    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        // Ignore
    }

    public void warning(TransformerException ex) throws TransformerException {
        this.parseException(ex, Message.Type.Warning, null);
    }

    private void parseException(TransformerException ex, Message.Type type,
            Message.Severity severity) {
        // Assemble the Message object
        MessageTemplate template = this.messages.get(EXCEPTION_MESSAGE_ID);

        String exceptionMessage = "";
        String locationMessage  = "";
        SourceLocator locator   = ex.getLocator();

        String category = null;
        String description = null;
        String messageText = null;

        if (template != null) {
            category = template.getCategory();
            description = template.getDescription();
            messageText = template.getMessage();
            severity = template.getSeverity();
            type = template.getType();
        }

        if (ex.getCause() != null) {
            exceptionMessage = ex.getCause().getMessage();
        } else {
            exceptionMessage = ex.getMessage();
        }

        if (locator != null) {
            int columnNumber = locator.getColumnNumber();
            int lineNumber   = locator.getLineNumber();

            if (lineNumber > -1) {
                locationMessage = "Line: " + lineNumber;

                if (columnNumber > -1) {
                    locationMessage = locationMessage + ", Column: " + columnNumber;
                }
            }
        }

        DataDetails details = new DataDetails(locationMessage, DataDetails.Info.Xpath);

        if (this.digest != null) {
            details.setKey(Helpers.toHexString(
                this.digest.digest(locationMessage.getBytes())
            ));
        }

        Message message  = new Message(
            EXCEPTION_MESSAGE_ID,
            category,
            messageText + ": " + exceptionMessage,
            description,
            type,
            severity,
            this.entity,
            details
        );

        this.metrics.updateMessageCount();
        this.reporter.write(message);
    }
}
