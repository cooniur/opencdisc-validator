/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.extensions.rules;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.rules.AbstractScriptableValidationRule;
import org.opencdisc.validator.rules.CorruptRuleException;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tim Stone
 */
@ValidationRule.RuleAssociation("property")
public class DomainPropertyValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "Properties", "Test"
    };

    private final List<EntityDetails.Property> properties =
        new LinkedList<EntityDetails.Property>();

    /**
     *
     * @param ruleDefinition
     * @param token
     * @param metrics
     * @throws ConfigurationException
     */
    public DomainPropertyValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Dataset, REQUIRED_VARIABLES, token, metrics);
        super.prepareExpression(ruleDefinition.getProperty("Test"), false);

        for (String property : ruleDefinition.getProperty("Properties").split(",")) {
            this.properties.add(EntityDetails.Property.valueOf(property.trim()));
        }
    }

    /**
     *
     * @param entity
     * @return
     */
    protected List<Outcome> performDatasetValidation(EntityDetails entity) {
        List<Outcome> results = new LinkedList<Outcome>();
        List<EntityDetails> entities = entity.hasProperty(EntityDetails.Property.Combined) ?
            entity.getSubentities() : Arrays.asList(entity);

        for (EntityDetails dataset : entities) {
            DataRecord record = new DataRecord(null, null);
            int count = 1;

            for (EntityDetails.Property property : this.properties) {
                record.setValue(
                    "P" + count,
                    dataset.hasProperty(property) ? new DataEntry(
                        dataset.getString(property)
                    ) : DataEntry.NULL_ENTRY
                );
                ++count;
            }

            try {
                boolean result = super.checkExpression(record);
                Outcome outcome = new Outcome(
                    (byte)(result ? 1 : 0), dataset
                );

                if (!result) {
                    count = 1;

                    for (EntityDetails.Property property : this.properties) {
                        outcome.display.put(
                            property.toString(), record.getValue("P" + count).toString()
                        );
                        ++count;
                    }
                }

                results.add(outcome);
            } catch (CorruptRuleException ex) {
                // TODO: This is definitely not the right thing to do here
                //    ...But we're unlikely to trigger this anyway
                throw new RuntimeException(ex);
            }
        }

        return results;
    }
}
