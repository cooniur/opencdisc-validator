/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.extensions.rules;

import org.opencdisc.validator.data.*;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.model.VariableDetails;
import org.opencdisc.validator.rules.AbstractValidationRule;
import org.opencdisc.validator.rules.CorruptRuleException;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.Helpers;
import org.opencdisc.validator.util.ProcessToken;

import java.util.*;

/**
 * @author Tim Stone
 */
@ValidationRule.RuleAssociation("varorder")
public class VariableOrderValidationRule extends AbstractValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] { "Variable" };

    private final Map<String, Integer> ordering = new HashMap<String, Integer>();
    private final Map<String, Set<String>> placed = new HashMap<String, Set<String>>();

    /**
     *
     * @param ruleDefinition
     * @param token
     * @param metrics
     * @throws org.opencdisc.validator.settings.ConfigurationException
     */
    public VariableOrderValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Record, REQUIRED_VARIABLES, token, metrics);

        super.addVariable("VARIABLE");
        super.addVariable("DATASET");

        for (Definition variable : ruleDefinition.getDependencies()) {
            String name = variable.getTargetName().toUpperCase();

            try {
                this.ordering.put(name, Integer.valueOf(variable.getProperty("Config.OrderNumber")));
            } catch (NumberFormatException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    String.format(
                        "Invalid value for Order attribute of variable %s",
                        name
                    ),
                    ex
                );
            }
        }
    }

    public void setup(EntityDetails entity) {
        Map<String, List<Integer>> orders = new HashMap<String, List<Integer>>();
        Map<String, List<String>> namesets = new HashMap<String, List<String>>();

        List<EntityDetails> entities = entity.hasProperty(EntityDetails.Property.Combined) ?
            entity.getSubentities() : Arrays.asList(entity);

        for (EntityDetails dataset : entities) {
            // This rule is applied against the metadata, but we need the parent entity
            dataset = dataset.getParent();
            String datasetName = dataset.getString(EntityDetails.Property.Subname);
            Set<String> placed = this.placed.get(datasetName);

            if (placed == null) {
                this.placed.put(datasetName, placed = new HashSet<String>());
                namesets.put(datasetName, new ArrayList<String>());
                orders.put(datasetName, new ArrayList<Integer>());
            }

            List<String> names = namesets.get(datasetName);
            List<Integer> order = orders.get(datasetName);

            for (VariableDetails variable : dataset.getVariables()) {
                String variableName = variable.getString(VariableDetails.Property.Name);
                Integer expected = this.ordering.get(variableName);

                if (expected != null) {
                    names.add(variableName);
                    order.add(expected);
                } else {
                    // An unknown variable is correctly placed because we have no idea
                    // where it *should* be, relative to anything else
                    placed.add(variableName);
                }
            }
        }

        for (String dataset : this.placed.keySet()) {
            Set<String> placed = this.placed.get(dataset);
            List<String> names = namesets.get(dataset);
            List<Integer> order = orders.get(dataset);

            int ordered[] = new int[names.size()];

            for (int i = 0; i < ordered.length; ++i) {
                ordered[i] = order.get(i);
            }

            ordered = Helpers.determineLargestIncreasingSubsequence(ordered, true);

            for (int index : ordered) {
                placed.add(names.get(index));
            }
        }
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        Set<String> placed = this.placed.get(
            dataRecord.getValue("DATASET").toString().toUpperCase()
        );

        boolean result = placed.contains(
            dataRecord.getValue("VARIABLE").toString().toUpperCase()
        );

        return (byte)(result ? 1 : 0);
    }
}
