/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.extensions.rules;

import org.opencdisc.validator.data.*;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.RuleMetrics;
import org.opencdisc.validator.model.VariableDetails;
import org.opencdisc.validator.rules.AbstractScriptableValidationRule;
import org.opencdisc.validator.rules.CorruptRuleException;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.settings.ConfigurationException;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

import java.util.*;

/**
 * @author Tim Stone
 */
@ValidationRule.RuleAssociation("varlength")
public class VariableLengthValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] { "Variable" };

    private final int min;
    private final int max;
    private final Map<String, Integer> lengths = new HashMap<String, Integer>();
    private final boolean countExcess;

    public VariableLengthValidationRule(Definition ruleDefinition, ProcessToken token,
            RuleMetrics metrics) throws ConfigurationException {
        super(ruleDefinition, Target.Dataset, REQUIRED_VARIABLES, token, metrics);

        for (Definition variable : ruleDefinition.getDependencies()) {
            this.lengths.put(variable.getTargetName().toUpperCase(), 0);
        }

        if (!ruleDefinition.hasProperty("Minimum") && !ruleDefinition.hasProperty("Maximum")) {
            throw new ConfigurationException(
                ConfigurationException.Type.RuleDefinition,
                "VariableLength rules must have one of (Minimum, Maximum)"
            );
        }

        int min = 0;
        int max = -1;

        if (ruleDefinition.hasProperty("Minimum")) {
            try {
                min = Integer.parseInt(ruleDefinition.getProperty("Minimum"));
            } catch (NumberFormatException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    "Invalid value for Minimum, expected number",
                    ex
                );
            }
        }

        if (ruleDefinition.hasProperty("Maximum")) {
            try {
                max = Integer.parseInt(ruleDefinition.getProperty("Maximum"));
            } catch (NumberFormatException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    "Invalid value for Maximum, expected number",
                    ex
                );
            }
        }

        this.min = Math.max(0, min);
        this.max = max;
        this.countExcess = !ruleDefinition.getProperty("Count").equalsIgnoreCase("Length");
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        for (String variable : this.lengths.keySet()) {
            if (!dataRecord.definesVariable(variable)) {
                continue;
            }

            DataEntry entry = dataRecord.getValue(variable);

            if (entry.hasValue()) {
                int length = entry.toString().length();
                int current = this.lengths.get(variable);

                if (length > current) {
                    this.lengths.put(variable, length);
                }
            }
        }

        return -1;
    }

    protected List<Outcome> performDatasetValidation(EntityDetails entity) {
        List<EntityDetails> entities = entity.hasProperty(EntityDetails.Property.Combined) ?
            entity.getSubentities() : Arrays.asList(entity);
        Map<String, Integer> maximums = new HashMap<String, Integer>();

        for (EntityDetails dataset : entities) {
            for (String name : this.lengths.keySet()) {
                VariableDetails variable = dataset.getVariable(name);

                if (variable != null &&
                        variable.hasProperty(VariableDetails.Property.Length)) {
                    int length = variable.getInteger(VariableDetails.Property.Length);
                    Integer max = maximums.get(name);

                    if (max == null || max < length) {
                        maximums.put(name, length);
                    }
                }
            }
        }

        List<Outcome> results = new ArrayList<Outcome>();

        for (String variable : this.lengths.keySet()) {
            Integer max = maximums.get(variable);
            Outcome result;

            if (max != null || !this.countExcess) {
                int length = this.lengths.get(variable);
                boolean failed;

                if (this.countExcess) {
                    if (length > 0) {
                        length = max - length;
                        failed = length >= this.min &&
                            (length <= this.max || this.max == -1);
                    } else {
                        failed = false;
                    }
                } else {
                    failed = length < this.min || (this.max != -1 && length > this.max);
                }

                result = new Outcome((byte)(!failed ? 1 : 0));

                if (failed) {
                    result.display.put("Variable", variable);
                    result.display.put(this.countExcess ? "Excess" : "Length" , Integer.toString(length));
                }
            } else {
                result = new Outcome((byte)-1);
            }

            results.add(result);
        }

        return results;
    }
}
