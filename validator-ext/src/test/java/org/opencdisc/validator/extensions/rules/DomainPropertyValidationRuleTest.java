/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.extensions.rules;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

import java.util.Arrays;

/**
 * @author Tim Stone
 */
public class DomainPropertyValidationRuleTest {
    /**
     * Assertion: You can validate a domain property with a test condition.
     */
    @Test
    public void ValidatingDomainPropertySuccess() throws Exception {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Properties")).thenReturn(true);
        when(definition.getProperty("Properties")).thenReturn("FileSize");
        when(definition.hasProperty("Test")).thenReturn(true);
        when(definition.getProperty("Test")).thenReturn("P1 == 4");

        ValidationRule rule = new DomainPropertyValidationRule(definition, token, null);

        EntityDetails entity = mock(EntityDetails.class);

        when(entity.getString(EntityDetails.Property.Name)).thenReturn("YY");
        when(entity.getString(EntityDetails.Property.Subname)).thenReturn("YY");
        when(entity.hasProperty(EntityDetails.Property.FileSize)).thenReturn(true);
        when(entity.getString(EntityDetails.Property.FileSize)).thenReturn("4");

        assertTrue(rule.validateDataset(entity));
    }

    /**
     * Assertion: You can validate a split domain property with a test condition,
     *     and get the expected number of results.
     */
    @Test
    public void ValidatingSplitDomainPropertyFailure() throws Exception {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Properties")).thenReturn(true);
        when(definition.getProperty("Properties")).thenReturn("FileSize");
        when(definition.hasProperty("Test")).thenReturn(true);
        when(definition.getProperty("Test")).thenReturn("P1 == 5");

        ValidationRule rule = new DomainPropertyValidationRule(definition, token, null);

        EntityDetails entity = mock(EntityDetails.class);
        EntityDetails sub1 = mock(EntityDetails.class);
        EntityDetails sub2 = mock(EntityDetails.class);

        when(sub1.getString(EntityDetails.Property.Name)).thenReturn("YY");
        when(sub1.getString(EntityDetails.Property.Subname)).thenReturn("XXYY");
        when(sub1.hasProperty(EntityDetails.Property.FileSize)).thenReturn(true);
        when(sub1.getString(EntityDetails.Property.FileSize)).thenReturn("4");

        when(sub2.getString(EntityDetails.Property.Name)).thenReturn("YY");
        when(sub2.getString(EntityDetails.Property.Subname)).thenReturn("XXYY");
        when(sub2.hasProperty(EntityDetails.Property.FileSize)).thenReturn(true);
        when(sub2.getString(EntityDetails.Property.FileSize)).thenReturn("6");

        when(entity.hasProperty(EntityDetails.Property.Combined)).thenReturn(true);
        when(entity.getSubentities()).thenReturn(Arrays.asList(sub1, sub2));

        assertFalse(rule.validateDataset(entity));
        assertEquals(2, rule.getMessages().size());
    }
}
