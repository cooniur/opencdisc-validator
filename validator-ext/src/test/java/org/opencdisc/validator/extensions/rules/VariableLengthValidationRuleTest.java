/*
 * Copyright © 2008-2015 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.extensions.rules;

import org.junit.Test;
import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.settings.Definition;
import org.opencdisc.validator.util.ProcessToken;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VariableLengthValidationRuleTest {
    @Test
    public void countLengthTypeFailsWhenExpected() throws Exception {
        ProcessToken token = ProcessToken.getRandomToken();
        Definition definition = mock(Definition.class);
        Definition subdefinition = mock(Definition.class);

        when(subdefinition.getTargetName()).thenReturn("AA");

        when(definition.hasProperty("ID")).thenReturn(true);
        when(definition.getProperty("ID")).thenReturn("XX");
        when(definition.hasProperty("Count")).thenReturn(true);
        when(definition.getProperty("Count")).thenReturn("Length");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("AA");
        when(definition.hasProperty("Minimum")).thenReturn(true);
        when(definition.getProperty("Minimum")).thenReturn("1");
        when(definition.getDependencies()).thenReturn(new HashSet<Definition>(Arrays.asList(subdefinition)));

        ValidationRule rule = new VariableLengthValidationRule(definition, token, null);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("AA",
            new DataEntry(""),
            new DataEntry(null)
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        EntityDetails entity = mock(EntityDetails.class);

        assertFalse(rule.validateDataset(entity));
    }
}
