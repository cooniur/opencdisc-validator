/*
 * Copyright © 2008-2015 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.extensions.rules;

import org.opencdisc.validator.data.DataEntry;
import org.opencdisc.validator.data.DataRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tim Stone
 */
public class ValidationRuleTestHelper {
    /**
     * Creates the specified number of <code>DataRecord</code> objects.
     *
     * @param count  the number of records to create
     * @return  the generated <code>List</code> of <code>DataRecord</code> objects
     */
    public static List<DataRecord> createRecords(int count) {
        List<DataRecord> records = new ArrayList<DataRecord>(count);

        for (int i = 0; i < count; ++i) {
            records.add(new DataRecord(null, null));
        }

        return records;
    }

    /**
     * Creates as many <code>DataRecord</code> objects as there are passed <code>DataEntry</code> objects,
     * setting each <code>DataEntry</code> as the specified <code>variable</code> on the respective record.
     *
     * @param variable  the variable to put the entry into for each record
     * @param entries  the entries to populate the <code>DataRecord</code> objects with
     * @return the generated <code>List</code> of <code>DataRecord</code> objects
     */
    public static List<DataRecord> createRecords(String variable, DataEntry... entries) {
        List<DataRecord> records = createRecords(entries.length);

        for (int i = 0; i < entries.length; ++i) {
            records.get(i).setValue(variable, entries[i]);
        }

        return records;
    }

    /**
     *
     * @param variables
     * @param entries
     * @return
     */
    public static List<DataRecord> createRecords(String[] variables, DataEntry[]... entries) {
        List<DataRecord> records = createRecords(entries.length);

        for (int i = 0; i < entries.length; ++i) {
            for (int j = 0; j < variables.length; ++j) {
                records.get(i).setValue(variables[j], entries[i][j]);
            }
        }

        return records;
    }
}
