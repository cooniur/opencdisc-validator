/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.cli;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import org.opencdisc.validator.ConfigOptions;
import org.opencdisc.validator.ReportOptions;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.Validator;
import org.opencdisc.validator.ValidatorFactory;
import org.opencdisc.validator.util.FileUtils;

/**
 *
 * @author Tim Stone
 */
public class CommandParser {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss");
    private static final List<String> HELP_PROMPTS = Arrays.asList("?", "h", "help");
    private static final String COMMAND_SEPARATOR = "=";
    private static final String COMMAND_ESCAPE = "\"";
    private static final String COMMAND_PREFIX = "-";
    private static final int LINE_LENGTH = 75;
    private static final Map<String, Terminology> terminologies = new HashMap<String, Terminology>();
    private static CommandParser CLIENT_INSTANCE = null;

    static  {
        terminologies.put("cdisc", new Terminology("CDISC/%s", "CDISC CT", "CDISC"));
        terminologies.put("meddra", new Terminology("MedDRA", "MedDRA"));
        terminologies.put("snomed", new Terminology("SNOMED", "SNOMED"));
        terminologies.put("ndfrt", new Terminology("NDF-RT", "NDF-RT"));
        terminologies.put("unii", new Terminology("UNII", "UNII"));
    }

    private CommandParser() {}

    public static CommandParser getInstance() {
        if (CLIENT_INSTANCE == null) {
            CLIENT_INSTANCE = new CommandParser();
        }

        return CLIENT_INSTANCE;
    }

    public void parse(String[] commandArguments) {
        Map<String, String> arguments = new Hashtable<String, String>();

        if (Integer.parseInt(System.getProperty("java.version").substring(2, 3)) < 6) {
            println(String.format(
                "OpenCDISC Validator requires Java 1.6 or later to run, but was" +
                " launched with version %s. Please upgrade your Java instalaltion.",
                System.getProperty("java.version")
            ));

            System.exit(1);
        }

        // Check if there are any parameters in the first place
        if (commandArguments.length == 0) {
            this.printHelp(false);

            return;
        }

        // First, search for help parameters
        for (String argument : commandArguments) {
            String command = this.getCommand(argument.toLowerCase());
            String value   = this.getValue(argument);

            if (HELP_PROMPTS.contains(command)) {
                this.printHelp(true);

                return;
            } else {
                arguments.put(command, value);
            }
        }

        // Now check if we have the correct arguments
        int reportCutoff = 1000;
        Validator.Type type;
        ReportOptions report;
        ConfigOptions config;
        List<SourceOptions> sources   = new ArrayList<SourceOptions>();
        SourceOptions.Type sourceType;
        ReportOptions.Type reportType;

        String value;

        value = arguments.get("type");
        if (arguments.containsKey("type")) {
            if (value.equalsIgnoreCase("sdtm")) {
                type = Validator.Type.Sdtm;
            } else if (value.equalsIgnoreCase("define")) {
                type = Validator.Type.Define;
            } else if (value.equalsIgnoreCase("custom")) {
                type = Validator.Type.Custom;
            } else if (value.equalsIgnoreCase("adam")) {
                type = Validator.Type.Adam;
            } else if (value.equalsIgnoreCase("send")) {
                type = Validator.Type.Send;
            } else {
                this.printHelp(false);
                System.out.println();
                CommandParser.println(String.format(
                        Text.get("CommandParser.Error.UnknownParamVal"), value, "type"));

                return;
            }
        } else {
            // The default value
            type = Validator.Type.Sdtm;
        }

        if (!type.equals(Validator.Type.Define)) {
            value = arguments.get("source:type");
            if (arguments.containsKey("source:type")) {
                if (value.equalsIgnoreCase("sas")) {
                    sourceType = SourceOptions.Type.SasTransport;
                } else if (value.equalsIgnoreCase("delimited")) {
                    sourceType = SourceOptions.Type.Delimited;
                } else if (value.equalsIgnoreCase("datasetxml")) {
                    sourceType = SourceOptions.Type.DatasetXml;
                } else {
                    this.printHelp(false);
                    System.out.println();
                    CommandParser.println(String.format(
                            Text.get("CommandParser.Error.UnknownParamVal"), value, "source:type"));

                    return;
                }
            } else {
                // The default value
                sourceType = SourceOptions.Type.SasTransport;
            }
        } else {
            sourceType = SourceOptions.Type.Xml;
        }

        value = arguments.get("source");
        if (arguments.containsKey("source")) {
            String[] paths = value.split(Pattern.quote(File.pathSeparator));

            for (String path : paths) {
                File sourcePath = new File(path);
                String sourceName = sourcePath.getName();
                File[] sourcePaths;

                if (sourcePath.getName().matches("\\*\\.[A-Za-z]+")) {
                    final String filter = sourceName.substring(sourceName.indexOf('.')).toLowerCase();
                    sourcePath = sourcePath.getParentFile();

                    sourcePaths = sourcePath.listFiles(new FileFilter() {
                        public boolean accept(File file) {
                            return file.isFile() && file.getName().toLowerCase().endsWith(filter);
                        }
                    });
                } else if (sourcePath.isDirectory()) {
                    sourcePaths = sourcePath.listFiles();
                } else {
                    sourcePaths = new File[] {sourcePath};
                }

                if (sourcePaths == null) {
                    sourcePaths = new File[0];
                }

                for (File sourceFile : sourcePaths) {
                    SourceOptions sourceOptions = new SourceOptions(sourceFile.getAbsolutePath());

                    sourceOptions.setType(sourceType);

                    if (type == Validator.Type.Sdtm) {
                        String[] pieces = sourceFile.getName().split("\\.");
                        String name = pieces[0].toUpperCase();

                        if (pieces.length == 2 && (name.length() == 4 || name.length() == 3) && !name.startsWith("SUPP") &&
                                !name.startsWith("AD")) {
                            sourceOptions.setName(name.substring(0, 2));
                        }
                    }

                    sources.add(sourceOptions);
                }

                if (sources.size() == 0) {
                    this.printHelp(false);
                    System.out.println();
                    CommandParser.println(String.format(
                            Text.get("CommandParser.Error.NotValidInput"), sourcePath.toString(), "source"));

                    return;
                }
            }
        } else {
            this.printHelp(false);
            System.out.println();
            CommandParser.println(String.format(
                    Text.get("CommandParser.Error.RequiresValue"), "source"));

            return;
        }

        value = arguments.get("config");
        if (arguments.containsKey("config")) {
            File configPath = new File(value);

            if (configPath.isFile()) {
                config = new ConfigOptions(configPath.getAbsolutePath());
            } else {
                configPath = FileUtils.getAbsolutePath(
                    CommandParser.class, "../config/" + value
                );

                if (configPath.isFile()) {
                    config = new ConfigOptions(configPath.getAbsolutePath());
                } else {
                    this.printHelp(false);
                    System.out.println();
                    CommandParser.println(String.format(
                            Text.get("CommandParser.Error.NotValidInput"), configPath.toString(), "config"));

                    return;
                }
            }
        } else {
            this.printHelp(false);
            System.out.println();
            CommandParser.println(String.format(
                    Text.get("CommandParser.Error.RequiresValue"), "config"));

            return;
        }

        if (!type.equals(Validator.Type.Define)) {
            value = arguments.get("config:define");
            if (arguments.containsKey("config:define")) {
                File definePath = new File(value);

                if (definePath.isFile()) {
                    config.setDefine(definePath.getAbsolutePath());
                } else {
                    this.printHelp(false);
                    System.out.println();
                    CommandParser.println(String.format(
                            Text.get("CommandParser.Error.NotValidInput"), definePath.toString(), "config:define"));

                    return;
                }
            }

            for (Map.Entry<String, Terminology> entry : terminologies.entrySet()) {
                String key = "config:" + entry.getKey();

                value = arguments.get(key);

                if (arguments.containsKey(key)) {
                    String standard = type.toString().toUpperCase();
                    Terminology terminology = entry.getValue();

                    // TODO: Should just change the type to be the right case
                    if (standard.equals("ADAM")) {
                        standard = "ADaM";
                    }

                    File path = FileUtils.getAbsolutePath(
                        CommandParser.class, "../config/data/" + String.format(terminology.path, standard) + "/" + value
                    );

                    if (path.isDirectory() && path.list().length > 0) {
                        for (String token : terminology.tokens) {
                            config.setProperty(token + ".Version", value);
                        }
                    } else {
                        this.printHelp(false);
                        System.out.println();
                        CommandParser.println(String.format(
                            Text.get("CommandParser.Error.NotValidInput"), value, key));

                        return;
                    }
                }
            }
        }

        if (sourceType.equals(SourceOptions.Type.Delimited)) {
            value = arguments.get("source:delimiter");
            if (arguments.containsKey("source:delimiter")) {
                for (SourceOptions source : sources) {
                    source.setDelimiter(value);
                }
            }

            value = arguments.get("source:qualifier");
            if (arguments.containsKey("source:qualifier")) {
                for (SourceOptions source : sources) {
                    source.setQualifier(value);
                }
            }
        }

        value = arguments.get("report:type");
        if (arguments.containsKey("report:type")) {
            if (value.equalsIgnoreCase("Excel")) {
                reportType = ReportOptions.Type.Excel;
            } else if (value.equalsIgnoreCase("CSV")) {
                reportType = ReportOptions.Type.Csv;
            } else if (value.equalsIgnoreCase("XML")) {
                reportType = ReportOptions.Type.Xml;
            } else {
                this.printHelp(false);
                System.out.println();
                CommandParser.println(String.format(
                        Text.get("CommandParser.Error.UnknownParamVal"), value, "report:type"));

                return;
            }
        } else {
            reportType = ReportOptions.Type.Excel;
        }

        if (reportType.equals(ReportOptions.Type.Excel)) {
            value = arguments.get("report:cutoff");
            if (arguments.containsKey("report:cutoff")) {
                try {
                    reportCutoff = Integer.parseInt(value);
                } catch (NumberFormatException ignore) {
                    // TODO: I guess we shouldn't ignore this
                }
            }
        }

        value = arguments.get("report");
        if (arguments.containsKey("report")) {
            File reportPath = new File(value);

            if (reportPath.exists()) {
                if (arguments.containsKey("report:overwrite") && arguments.get("report:overwrite").equalsIgnoreCase("yes")) {
                    if (reportPath.canWrite()) {
                        report = new ReportOptions(reportPath.getAbsolutePath());
                        report.setOverwrite(true);
                    } else {
                        this.printHelp(false);
                        System.out.println();
                        CommandParser.println(String.format(
                                Text.get("CommandParser.Error.ReadyOnly"), reportPath.getAbsolutePath()));

                        return;
                    }
                } else {
                    this.printHelp(false);
                    System.out.println();
                    CommandParser.println(String.format(
                            Text.get("CommandParser.Error.ExistingReport"), reportPath.toString(), "report"));

                    return;
                }
            } else {
                try {
                    if (reportPath.createNewFile()) {
                        report = new ReportOptions(reportPath.getAbsolutePath());
                    } else {
                        this.printHelp(false);
                        System.out.println();
                        CommandParser.println(String.format(
                                Text.get("CommandParser.Error.ReadOnly"), reportPath.getAbsolutePath()));
                        return;
                    }
                } catch (IOException ex) {
                    this.printHelp(false);
                    System.out.println();
                    CommandParser.println(String.format(
                            Text.get("CommandParser.Error.NotValidOutput"), reportPath.toString(), "report"));

                    return;
                }
            }
        } else {
            // Decide the report name automatically
            value = "opencdisc-report-" + DATE_FORMAT.format(new Date()) + reportType.getExtension();

            File reportPath = FileUtils.getAbsolutePath(
                CommandParser.class, "../reports"
            );

            if (reportPath.exists()) {
                reportPath = new File(reportPath, value);
            }

            try {
                if (reportPath.createNewFile()) {
                    report = new ReportOptions(reportPath.getAbsolutePath());
                } else {
                    this.printHelp(false);
                    System.out.println();
                    CommandParser.println(String.format(
                            Text.get("CommandParser.Error.ReadOnly"), reportPath.getAbsolutePath()));
                    return;
                }
            } catch (IOException ex) {
                this.printHelp(false);
                System.out.println();
                CommandParser.println(String.format(
                        Text.get("CommandParser.Error.NotValidOutput"), reportPath.toString(), "report"));

                return;
            }
        }

        for (Terminology terminology : terminologies.values()) {
            String key = terminology.tokens[0] +  ".Version";

            if (config.hasProperty(key)) {
                report.setHeader(terminology.tokens[0] + " Version", config.getProperty(key));
            }
        }

        report.setLimit(reportCutoff);
        report.setType(reportType);

        Validator validator = ValidatorFactory.newInstance().newValidator();

        CommandParser.println(Text.get("CommandParser.StartingValidation"));
        validator.validate(type, sources, config, report);
        CommandParser.println(Text.get("CommandParser.FinishedValidation"));
    }

    private void printHelp(boolean verbose) {
        System.out.println();
        CommandParser.println(CommandParser.repeat("-", LINE_LENGTH - 1), 0);
        CommandParser.println(Text.get("CommandParser.Title"), true);
        CommandParser.println(CommandParser.repeat("-", LINE_LENGTH - 1), 0);
        System.out.println();

        if (!verbose) {
            CommandParser.println(Text.get("CommandParser.Error.InvalidParams"));
            CommandParser.println(Text.get("CommandParser.Help.HelpSwitches"));
        } else {
            CommandParser.println(Text.get("CommandParser.Help.Overview"));
            System.out.println();
            CommandParser.println(Text.get("CommandParser.Help.AdditionalInfo"));
            System.out.println();
            CommandParser.println(Text.get("CommandParser.Help.GeneralOptions"));
            CommandParser.println("-type                SDTM|ADaM|SEND|Define|Custom (SDTM)", 3);
            System.out.println();
            CommandParser.println(Text.get("CommandParser.Help.SourceOptions"));
            CommandParser.println("-source              <path>", 3);
            CommandParser.println("-source:type         SAS|Delimited (SAS)", 3);
            CommandParser.println("-source:delimiter    <delimiter> (,)", 3);
            CommandParser.println("-source:qualifier    <qualifier> (\")", 3);
            System.out.println();
            CommandParser.println(Text.get("CommandParser.Help.ConfigOptions"));
            CommandParser.println("-config              <path>", 3);
            CommandParser.println("-config:define       <path>", 3);
            CommandParser.println("-config:codelists    <path>", 3);

            for (String terminology : terminologies.keySet()) {
                CommandParser.println(
                    String.format("-config:%-13s<version>", terminology), 3
                );
            }

            System.out.println();
            CommandParser.println(Text.get("CommandParser.Help.ReportOptions"));
            CommandParser.println("-report              <path>", 3);
            CommandParser.println("-report:type         Excel|CSV|XML (Excel)", 3);
            CommandParser.println("-report:cutoff       <#> (1000)", 3);
            CommandParser.println("-report:overwrite    yes|no", 3);
        }
    }

    private String getCommand(String commandString) {
        if (commandString.startsWith(COMMAND_PREFIX)) {
            commandString = commandString.substring(COMMAND_PREFIX.length());
        }

        if (commandString.contains(COMMAND_SEPARATOR)) {
            commandString = commandString.substring(0, commandString.indexOf(COMMAND_SEPARATOR));
        }

        return commandString.trim();
    }

    private String getValue(String commandString) {
        if (commandString.contains(COMMAND_SEPARATOR)) {
            String[] components = commandString.split(Pattern.quote(COMMAND_SEPARATOR), 2);

            if (components.length == 2) {
                commandString = components[1];
            } else {
                commandString = "";
            }
        }

        if (commandString.startsWith(COMMAND_ESCAPE) && commandString.endsWith(COMMAND_ESCAPE)) {
            commandString = commandString.substring(1, commandString.length() - 1);
        }

        return commandString.trim();
    }

    private static void println(String text) {
        CommandParser.println(text, 0);
    }

    private static void println(String text, boolean center) {
        if (!center || text.length() >= LINE_LENGTH) {
            CommandParser.println(text);
        } else {
            int remaining = LINE_LENGTH - text.length();

            CommandParser.println(text, remaining / 2);
        }
    }

    private static void println(String text, int indent) {
        text = text.trim();
        ++indent;

        if (text.length() + indent <= LINE_LENGTH) {
            System.out.println(CommandParser.repeat(" ", indent) + text);
        } else {
            while (text.length() > 0) {
                if (text.length() < LINE_LENGTH - indent) {
                    System.out.println(CommandParser.repeat(" ", indent) + text.trim());

                    text = "";
                } else {
                    int cutoff  = text.substring(0, LINE_LENGTH - indent).lastIndexOf(" ");

                    if (cutoff == -1) {
                        cutoff = LINE_LENGTH - indent;
                    }

                    String line = text.substring(0, cutoff);

                    System.out.println(CommandParser.repeat(" ", indent) + line.trim());

                    text = text.substring(line.length()).trim();
                }
            }
        }
    }

    private static String repeat(String text, int times) {
        StringBuilder result = new StringBuilder(text.length() * times);

        for (int i = 0; i < times; ++i) {
            result.append(text);
        }

        return result.toString();
    }

    private static class Terminology {
        final String path;
        final String[] tokens;

        Terminology(String path, String...tokens) {
            this.path = path;
            this.tokens = tokens;
        }
    }
}
