/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.cli;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 *
 * @author Tim Stone
 */
public final class Text {
    private static final String DEFAULT_LANG = "en";

    private static final String BUNDLE_NAME = "validator-cli-text";

    private static ResourceBundle RESOURCE_BUNDLE = null;

    private static boolean ATTEMPTED = false;

    /**
     * Gets a string from the resource file.
     *
     * @param key
     *
     * @return The text associated with the given <code>key</code>.
     */
    public static String get(String key) {
        if (ATTEMPTED && RESOURCE_BUNDLE == null) {
            return '!' + key + '!';
        }

        if (RESOURCE_BUNDLE == null) {
            try {
                Locale locale   = new Locale(DEFAULT_LANG);
                RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, locale);
            } catch (MissingResourceException iex) {
                ATTEMPTED = true;

                return Text.get(key);
            }
        }

        try {
            return RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
