/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.cli;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.opencdisc.ipc.Bridge;
import org.opencdisc.ipc.buffers.EventProtos.*;
import org.opencdisc.validator.*;
import org.opencdisc.validator.model.SimpleMetrics;
import org.opencdisc.validator.util.Settings;
import rx.Observer;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class Connector {
    private static final Logger logger = LogManager.getLogger();

    static void attach() {
        logger.trace("Attaching to IPC bridge...");

        Bridge.start().subscribe(new Observer<IpcControlEvent>() {
            private final Validator validator = ValidatorFactory.newInstance().newValidator();

            public void onCompleted() {
                logger.trace("IPC event stream terminated");

                this.stop();
            }

            public void onError(Throwable ex) {
                logger.error("Unexpected exception in IPC event emitter", ex);

                this.stop();
            }

            public void onNext(IpcControlEvent event) {
                IpcControlEvent.Control code = event.getControl();

                logger.trace("Received new control event of type {}", code.name());

                try {
                    if (code == IpcControlEvent.Control.START) {
                        this.start(event);
                    } else if (code == IpcControlEvent.Control.ABORT) {
                        logger.debug("Attempting to abort validation");

                        this.stop();
                    }
                } catch (Exception ex) {
                    logger.error("Unexpected exception acting on control event", ex);

                    Bridge.onException(ex);
                }
            }

            private void start(IpcControlEvent event) {
                logger.debug("Triggering start of validation");

                // TODO: Strictly speaking we could have done this as a protobuf extension, but...
                //       this is easier for now, so. And this is a one-off operation.
                JsonObject parameters = JsonObject.readFrom(event.getJson());
                JsonObject configParameters = parameters.get("config").asObject();
                JsonObject reportParameters = parameters.get("report").asObject();
                JsonObject sourceParameters = parameters.get("sources").asObject();
                JsonObject terminnologyParameters = configParameters.get("terminologies").asObject();
                JsonValue threads = parameters.get("threads");

                if (threads != null && !threads.isNull()) {
                    Settings.set("Engine.ThreadCount", Integer.toString(threads.asInt()));
                }

                ReportOptions.Type reportType;

                if (reportParameters.get("type").asString().equalsIgnoreCase("CSV")) {
                    reportType = ReportOptions.Type.Csv;
                } else {
                    reportType = ReportOptions.Type.Excel;
                }

                final ConfigOptions config = new ConfigOptions(asAbsolutePath(configParameters.get("path")));
                final ReportOptions report = new ReportOptions(
                    asAbsolutePath(reportParameters.get("path")) + reportType.getExtension()
                ).setType(reportType);
                final List<SourceOptions> sources = new LinkedList<SourceOptions>();

                JsonValue messageLimit = reportParameters.get("messageLimit");

                if (messageLimit != null && !messageLimit.isNull()) {
                    report.setLimit(messageLimit.asInt());
                }

                boolean isDefine = parameters.get("isDefine").asBoolean();
                final Validator.Type validationType = isDefine ? Validator.Type.Define : Validator.Type.Sdtm;

                if (isDefine) {
                    sources.add(SourceOptions.fromFile(asFile(configParameters.get("define")),
                        SourceOptions.Type.Xml).setName("define"));
                } else {
                    config.setDefine(asAbsolutePath(configParameters.get("define")));

                    for (String key : terminnologyParameters.names()) {
                        String value = terminnologyParameters.get(key).asString();

                        if (value.length() > 0) {
                            config.setProperty(key + ".Version", value);
                            // TODO: Is this the best place to do this? Requires all callers to do the same thing
                            report.setHeader(key + " Version", value);
                        }
                    }

                    // TODO: We should probably enum these or something instead of using names...
                    SourceOptions.Type sourceType = SourceOptions.Type.parse(
                        sourceParameters.get("type").asString()
                    );

                    String delimiter = sourceParameters.get("delimiter").asString();
                    String qualifier = sourceParameters.get("qualifier").asString();

                    for (JsonValue value : sourceParameters.get("paths").asArray()) {
                        sources.add(
                            SourceOptions.fromFile(asFile(value), sourceType)
                                .setDelimiter(delimiter)
                                .setQualifier(qualifier)
                                .setDefine(config.getDefine())
                        );
                    }
                }

                validator.addValidatorListener(new ValidatorListener() {
                    public void validatorProgressUpdated(ValidatorEvent event) {
                        IpcStateEvent message = null;
                        ValidatorEvent.State state = event.getState();
                        ValidatorEvent subevent = event.getSubevent();
                        SimpleMetrics metrics = validator.getMetrics();

                        switch (state) {
                            case Parsing:
                                message = IpcStateEvent.newBuilder()
                                    .setState(IpcStateEvent.State.TASK_PROCESSING)
                                    .setIndeterminate(true)
                                    .build();
                                break;
                            case Processing:
                                message = IpcStateEvent.newBuilder()
                                    .setState(IpcStateEvent.State.TASK_PROCESSING)
                                    .setLabel(event.getName())
                                    .setTotal((int) event.getMaximum())
                                    .build();

                                if (subevent != null) {
                                    message = message.toBuilder()
                                        .setCurrent(subevent.getCurrent())
                                        .build();
                                }

                                break;
                            case Subprocessing:
                                message = IpcStateEvent.newBuilder()
                                    .setState(IpcStateEvent.State.TASK_SUBPROCESSING)
                                    .setLabel(event.getName())
                                    .setCurrent(event.getCurrent())
                                    .build();
                                break;
                            case Completed:
                                message = IpcStateEvent.newBuilder()
                                    .setState(IpcStateEvent.State.TASK_COMPLETED)
                                    .setLabel(event.getName())
                                    .setCurrent(metrics.getItemsProcessed())
                                    .setTotal((int) event.getMaximum())
                                    .build();

                                break;
                        }

                        if (message != null) {
                            Bridge.emit(message);
                        }
                    }

                    public void validatorStarted(ValidatorEvent event) {
                        Bridge.onStart();
                    }

                    public void validatorStopped(ValidatorEvent event) {
                        SimpleMetrics metrics = validator.getMetrics();
                        JsonObject stats = new JsonObject();

                        stats.set("reportOverflow", metrics.hasOverflowed());
                        stats.set("aborted", metrics.getAbortStatus());
                        stats.set("duration", metrics.getElapsedTime());
                        stats.set("records", metrics.getSubitemsProcessed());
                        stats.set("datasetsProcessed", metrics.getItemsProcessed());
                        stats.set("datasetsTotal", metrics.getTotalItems());
                        stats.set("messageCount", metrics.getMessagesGenerated());
                        stats.set("checkCount", metrics.getChecksPerformed());

                        Bridge.onComplete(stats);
                    }
                });

                // Dispatch on separate thread otherwise we'll block in the subscriber
                new Thread() {
                    public void run() {
                        try {
                            validator.validate(validationType, sources, config, report);
                        } catch (Exception ex) {
                            logger.error("Unexpected exception while running validator", ex);

                            Bridge.onException(ex);
                        }

                        logger.info("Validation complete, threaded process ending");
                    }
                }.start();
            }

            private void stop() {
                this.validator.abort();
            }
        });

        logger.trace("Subscription ended, exiting program");
    }

    private static String asAbsolutePath(JsonValue value) {
        if (value == null || value.isNull()) {
            return null;
        }

        return asFile(value).getAbsolutePath();
    }

    private static File asFile(JsonValue value) {
        if (value == null || value.isNull()) {
            return null;
        }

        return new File(value.asString());
    }
}
