/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.model;

import java.util.*;

import org.apache.commons.lang.StringUtils;

/**
 * Provides an informational overview of an engine entity. Essentially, this assigns an
 * identity to a given source of data after it has been paired with a configuration.
 *
 * @author Tim Stone
 * @since 1.2
 */
public interface EntityDetails {
    /**
     * A pre-defined list of properties that a given <code>EntityDetails</code> may
     * provide. Note that the values associated with these properties may change while
     * the underlying source is still being validated.
     *
     * @author Tim Stone
     */
    public enum Property {
        Class(false, false),
        Configuration(false, false),
        Combined(false, true),
        DatasetLabel(false, false),
        FileSize(false, true),
        Filtered(true, true),
        Keys(false, false),
        Label(false, false),
        // FIXME: This isn't supposed to be updatable, but it was the easiest workaround
        Location(true, false),
        Name(false, false),
        Records(true, true),
        Split(false, true),
        Subname(false, false),
        Validated(false, true),
        Variables(true, true);

        private final boolean isConvertible;
        private final boolean updatable;

        Property(boolean updatable, boolean isConvertible) {
            this.isConvertible = isConvertible;
            this.updatable = updatable;
        }

        /**
         *
         * @return <code>true</code> if the property is convertible, <code>false</code>
         *     otherwise
         */
        public boolean isConvertible() {
            return this.isConvertible;
        }

        /**
         *
         * @return <code>true</code> if the property is updatable, <code>false</code>
         *     otherwise
         */
        public boolean isUpdatable() {
            return this.updatable;
        }
    }

    /**
     * Defines the reference types that an entity may represent. In particular, tabular
     * data may have both data and metadata entities which are validated separately.
     *
     * @author Tim Stone
     */
    public enum Reference {
        Data,
        Metadata
    }

    /**
     * Attempts to get the value of the specified <code>property</code> as a boolean.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return the truthiness of the property
     * @throws IllegalArgumentException  if the property value is not convertible, or is
     *     not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    public boolean getBoolean(Property property);

    /**
     * Attempts to get the value of the specified <code>property</code> as an integer.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return the numeric value of the property
     * @throws IllegalArgumentException  if the property value is not convertible, or is
     *     not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    public int getInteger(Property property);

    /**
     *
     * @return the parent entity, if this entity represents metadata
     * @see Reference
     * @since 1.4
     */
    public EntityDetails getParent();

    /**
     * Gets this <code>EntityDetail</code>'s reference type.
     *
     * @return the reference type
     * @see Reference
     */
    public Reference getReference();

    /**
     * Gets the value of the specified <code>property</code> as a string.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return the value of the property as a string
     * @throws IllegalArgumentException  if the property is not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    public String getString(Property property);

    /**
     *
     * @param name  the name of the variable to look for
     * @return  <code>true</code> if the variable is defined, otherwise <code>false</code>
     * @since 1.5
     */
    public boolean hasVariable(String name);

    /**
     *
     * @param name  the name of the variable to look for
     * @return  the <code>VariableDetails</code> for the variable with the provided
     *     <code>name</code>, otherwise <code>null</code>
     * @see VariableDetails
     * @since 1.4
     */
    public VariableDetails getVariable(String name);

    /**
     *
     * @return the list of variables defined for the source this entity
     *     represents
     * @see VariableDetails
     * @since 1.4
     */
    public List<VariableDetails> getVariables();

    /**
     * Checks whether or not this <code>EntityDetails</code> defines the given
     * <code>property</code>.
     *
     * @param property  the <code>Property</code> to check the existence of
     * @return <code>true</code> if the property is defined, <code>false</code> otherwise
     * @see Property
     */
    public boolean hasProperty(Property property);

    /**
     *
     * @return
     */
    public List<String> getExamined();

    /**
     *
     * @return
     * @since 1.4
     */
    public List<EntityDetails> getSubentities();
}
