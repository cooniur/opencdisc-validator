/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.model;

import static org.apache.commons.lang.time.DateFormatUtils.ISO_DATETIME_FORMAT;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.opencdisc.validator.model.DataDetails.Info;
import org.opencdisc.validator.model.EntityDetails.Property;

/**
 * A container for information about a validation issue that may be relevant to
 * calling code. <code>Message</code> objects are typically consumed by a reporting
 * implementation internal to the validation process, though they may also be used
 * by external code by providing a <code>ConfigOptions</code> object that's based on
 * a message queue when invoking the <code>Validator</code> validation methods.
 *
 * @author Tim Stone
 */
public class Message {
    /**
     *
     * @author Tim Stone
     * @deprecated
     */
    public enum Severity {
        High,
        Medium,
        Low
    }

    /**
     * The type of issue a <code>Message</code> object represents that suggests how
     * significant a given issue may be.
     *
     * @author Tim Stone
     */
    public enum Type {
        Error,
        Warning,
        Information;

        /**
         * Returns the appropriate <code>Type</code> based on the provided
         * <code>string</code> representation. If the argument does not match the name
         * of a known type, the default value of <code>Type.Warning</code> is returned
         * instead.
         *
         * @param string  the case-insensitive name of the type
         * @return the appropriate <code>Type</code> if possible, otherwise the default
         *     <code>Type.Warning</code>
         */
        public static Type fromString(String string) {
            Type type = Type.Warning;

            if("Error".equalsIgnoreCase(string)) {
                type = Type.Error;
            } else if("Information".equalsIgnoreCase(string)) {
                type = Type.Information;
            }

            return type;
        }
    }

    private final String category;
    private final String context;
    private final String description;
    private final EntityDetails entity;
    private final String ID;
    private final String message;
    private final String posted;
    private final DataDetails record;
    private final Severity severity;
    private final Type type;
    private Map<String, String> values = new HashMap<String, String>();

    /**
     *
     * @param string
     * @return The <code>Message.Type</code> that is represented by this
     * <code>String</code>, or the default value of <code>Warning</code> if a suitable
     * match could not be found.
     * @deprecated
     */
    public static Type typeFromString(String string) {
        return Type.fromString(string);
    }

    /**
     *
     * @param string
     * @return The <code>Message.Severity</code> that is represented by this
     * <code>String</code>, or the default value of <code>Medium</code> if a suitable
     * match could not be found.
     * @deprecated
     */
    public static Severity severityFromString(String string) {
        Severity severity = Severity.Medium;

        if("High".equalsIgnoreCase(string)) {
            severity = Severity.High;
        } else if("Low".equalsIgnoreCase(string)) {
            severity = Severity.Low;
        }

        return severity;
    }

    /**
     * <em>Note: Not intended for external use</em>
     *
     * @param ID
     * @param category
     * @param message
     * @param description
     * @param type
     * @param entity
     */
    public Message(String ID, String category, String message, String description,
            Type type, EntityDetails entity) {
        this(ID, category, message, description, type, null, entity);
    }

    /**
     * <em>Note: Not intended for external use</em>
     *
     * @param ID
     * @param category
     * @param message
     * @param description
     * @param type
     * @param severity
     * @param entity
     */
    public Message(String ID, String category, String message, String description,
            Type type, Severity severity, EntityDetails entity) {
        this(ID, category, message, description, type, severity, entity, null);
    }

    /**
     * <em>Note: Not intended for external use</em>
     *
     * @param ID
     * @param category
     * @param message
     * @param description
     * @param type
     * @param severity
     * @param entity
     * @param record
     */
    public Message(String ID, String category, String message, String description,
                   Type type, Severity severity, EntityDetails entity, DataDetails record) {
        this(ID, category, message, description, null, type, severity, entity, record);
    }

    /**
     * <em>Note: Not intended for external use</em>
     *
     * @param ID
     * @param category
     * @param message
     * @param description
     * @param context
     * @param type
     * @param severity
     * @param entity
     * @param record
     */
    public Message(String ID, String category, String message, String description,
            String context, Type type, Severity severity, EntityDetails entity,
            DataDetails record) {
        this.ID = ID;
        this.category = category;
        this.context = context;
        this.description = description;
        this.entity = entity;
        this.message = message;
        this.posted = ISO_DATETIME_FORMAT.format(System.currentTimeMillis());
        this.record = record;
        this.severity = severity;
        this.type = type;
    }

    /**
     * Gets the general category of issue that this <code>Message</code> represents.
     *
     * @return the category of issue this message belongs to
     */
    public String getCategory() {
        return this.category;
    }

    /**
     *
     * @return
     */
    public String getContext() {
        return this.context;
    }

    /**
     * Gets the detailed description of the issue that this <code>Message</code>
     * represents.
     *
     * @return the verbose issue description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Gets the ID of the rule which was responsible for generating this message in
     * response to an issue with the data.
     *
     * @return the ID of the rule that triggered this message
     */
    public String getID() {
        return this.ID;
    }

    /**
     *
     * @return
     * @deprecated
     */
    public DataIdentity getIdentity() {
        Integer record = null;

        if (this.record != null) {
            record = this.record.getID();
        }

        String name = this.entity.getString(Property.Name);

        if (this.record != null && this.record.getInfo() == Info.Xpath) {
            name = this.record.getName();
        }

        String label = null;

        if (this.entity.hasProperty(Property.Label)) {
            label = this.entity.getString(Property.Label);
        }

        return new DataIdentity(name, record, label);
    }

    /**
     * Gets identifying information about the source data file which caused this
     * <code>Message</code> to be generated.
     *
     * @return details about the source that contained faulty data
     * @see EntityDetails
     */
    public EntityDetails getEntityDetails() {
        return this.entity;
    }

    /**
     * Gets the brief message text that describes what issue occurred that caused this
     * <code>message</code> to be generated.
     *
     * @return the specific message detailing the problem with the data
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Gets the time that this <code>Message</code> object was generated.
     *
     * @return the ISO 6801 formatted timestamp at the time of creation
     */
    public String getPostTimestamp() {
        return this.posted;
    }

    /**
     * Gets identifying information about the specific location in the source data that
     * contained data which triggered the issue this <code>Message</code> represents.
     *
     * @return details about the specific location in the source data that was at fault
     * @see DataDetails
     */
    public DataDetails getRecordDetails() {
        return this.record;
    }

    /**
     *
     * @return
     * @deprecated
     */
    public Severity getSeverity() {
        return this.severity;
    }

    /**
     * Gets the type of issue that this <code>Message</code> represents.
     *
     * @return the issue type
     * @see Type
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Gets data relevant to this specific <code>Message</code> instance based on the
     * provided <code>variable</code> name.
     *
     * @param variable  the variable to get data for
     * @return the value assigned to the given <code>variable</code>
     * @see #getVariables()
     */
    public String getValueOf(String variable) {
        return this.values.get(variable);
    }

    /**
     * Gets the set of variable names that were relevant to the rule that generated this
     * <code>Message</code>. These names can be used to retreive the values that were
     * involved in the validation via <code>{@link #getValueOf(String)}</code>.
     *
     * @return a <code>Set</code> of variable names
     * @see #getValueOf(String)
     */
    public Set<String> getVariables() {
        return this.values.keySet();
    }

    /**
     * <em>Note: Not intended for external use</code>
     *
     * @param variable
     * @param value
     */
    public void setValueOf(String variable, String value) {
        this.values.put(variable, value);
    }
}
