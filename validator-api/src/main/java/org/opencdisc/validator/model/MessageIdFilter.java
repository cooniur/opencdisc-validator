/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.model;


/**
 *
 * @author Tim Stone
 */
public class MessageIdFilter implements MessageFilter {
    private final String id;

    /**
     *
     * @param id
     */
    public MessageIdFilter(String id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     */
    public boolean accept(Message message) {
        return !message.getID().equalsIgnoreCase(this.id);
    }

}
