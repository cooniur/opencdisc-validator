/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.model;

/**
 * Represents the identity of a given piece of data. This typically corresponds to the
 * particular dataset and record that a piece of data came from. Additionally, some
 * sources may have associated label information, which this class also provides access
 * to.
 *
 * @author Tim Stone
 * @deprecated
 */
public class DataIdentity {
    private final String name;
    private final String label;
    private final Integer record;

    /**
     *
     * @param name
     */
    public DataIdentity(String name) {
        this(name, null, null);
    }

    /**
     *
     * @param name
     * @param record
     */
    public DataIdentity(String name, Integer record) {
        this(name, record, null);
    }

    /**
     *
     * @param name
     * @param label
     */
    public DataIdentity(String name, String label) {
        this(name, null, label);
    }

    /**
     *
     * @param name
     * @param record
     * @param label
     */
    public DataIdentity(String name, Integer record, String label) {
        this.name   = name;
        this.record = record;
        this.label  = label;
    }

    /**
     * Gets the record number of this <code>DataIdentity</code>, if one was provided.
     *
     * @return the record number this <code>DataIdentity</code> represents, or
     *     <code>null</code> if one was not provided
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Gets the name of this <code>DataIdentity</code>
     *
     * @return the name of this <code>DataIdentity</code>
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the label of this <code>DataIdentity</code>, if one was provided
     *
     * @return the label of this <code>DataIdentity</code>, or <code>null</code> if one
     *     was not provided
     */
    public Integer getRecord() {
        return this.record;
    }

    public String toString() {
        String textIdentity = this.name;

        if(this.label != null && this.label.length() > 0) {
            textIdentity += " (" + this.label + ")";
        }

        if(this.record != null && this.record > 0) {
            textIdentity += " on record number " + this.record;
        }

        return textIdentity;
    }
}
