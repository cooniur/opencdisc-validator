/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.model;

/**
 * Specifically identifies a piece of data that was validated during the validation
 * process. In the case of tabular data, this is most often record-based information.
 * However, the data represented could also be metadata from a tabular data source or
 * an xpath that identifies a node in a hierarchical data structure.
 *
 * @author Tim Stone
 * @since 1.2
 */
public class DataDetails {
    /**
     * The type of information a given <code>DataDetails</code> instance represents.
     *
     * @author Tim Stone
     */
    public enum Info {
        /**
         * Identifies record-based information
         */
        Data,

        /**
         * Identifies metadata-based information
         */
        Variable,

        /**
         * Identifies node-based information (hierarchical sources)
         */
        Xpath
    }

    private final int id;
    private final Info info;
    private final String name;
    private String key;

    /**
     * <em>Note: Not intended for external use</em>
     *
     * @param name
     * @param info
     */
    public DataDetails(String name, Info info) {
        this(-1, name, info);
    }

    /**
     * <em>Note: Not intended for external use</em>
     *
     * @param id
     * @param info
     */
    public DataDetails(int id, Info info) {
        this(id, null, info);
    }

    /**
     * <em>Note: Not intended for external use</em>
     *
     * @param id
     * @param info
     */
    public DataDetails(int id, String name, Info info) {
        this.id = id;
        this.info = info;
        this.name = name;
    }

    /**
     * Gets the numerical position information of this data, for appropriate
     * <code>Info</code> types. This may be the record number where the data can be
     * found, or the column position of the variable in the source's metadata.
     *
     * @return the numerical position of data where appropriate, otherwise the value
     *     <code>-1</code>
     * @see #getInfo()
     */
    public int getID() {
        return this.id;
    }

    /**
     * Gets the <code>Info</code> type associated with this data detail information.
     *
     * @return the type of location information
     * @see Info
     */
    public Info getInfo() {
        return this.info;
    }

    /**
     *
     * @return
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Gets the name information of this data, for appropriate <code>Info</code> types.
     * This is typically something like the name of the variable from a source's
     * metadata.
     *
     * @return the name that identifies this data
     * @see #getInfo()
     */
    public String getName() {
        return this.name;
    }

    /**
     * <em>Note: Not intended for external use</em>
     *
     * @param key
     */
    public void setKey(String key) {
        if (this.key != null) {
            throw new UnsupportedOperationException("Cannot reassign the key value");
        }

        this.key = key;
    }
}
