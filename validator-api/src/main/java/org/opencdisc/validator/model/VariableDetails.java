/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.model;

/**
 * @author Tim Stone
 * @since 1.4
 */
public interface VariableDetails {
    /**
     * A pre-defined list of properties that a given <code>VariableDetails</code> may
     * provide.
     *
     * @author Tim Stone
     */
    public enum Property {
        Format(false, false),
        FullFormat(false, false),
        Label(false, false),
        Length(false, true),
        Name(false, false),
        Order(false, true),
        Type(false, false);

        private final boolean isConvertible;
        private final boolean updatable;

        Property(boolean updatable, boolean isConvertible) {
            this.isConvertible = isConvertible;
            this.updatable = updatable;
        }

        /**
         *
         * @return <code>true</code> if the property is convertible, <code>false</code>
         *     otherwise
         */
        public boolean isConvertible() {
            return this.isConvertible;
        }

        /**
         *
         * @return <code>true</code> if the property is updatable, <code>false</code>
         *     otherwise
         */
        public boolean isUpdatable() {
            return this.updatable;
        }
    }

    /**
     * Attempts to get the value of the specified <code>property</code> as a boolean.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return the truthiness of the property
     * @throws IllegalArgumentException  if the property value is not convertible, or is
     *     not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    public boolean getBoolean(Property property);

    /**
     * Attempts to get the value of the specified <code>property</code> as an integer.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return the numeric value of the property
     * @throws IllegalArgumentException  if the property value is not convertible, or is
     *     not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    public int getInteger(Property property);

    /**
     * Gets the value of the specified <code>property</code> as a string.
     *
     * @param property  the <code>Property</code> to get the value of
     * @return the value of the property as a string
     * @throws IllegalArgumentException  if the property is not defined
     * @see Property
     * @see #hasProperty(Property)
     */
    public String getString(Property property);

    /**
     * Checks whether or not this <code>VariableDetails</code> defines the given
     * <code>property</code>.
     *
     * @param property  the <code>Property</code> to check the existence of
     * @return <code>true</code> if the property is defined, <code>false</code> otherwise
     * @see Property
     */
    public boolean hasProperty(Property property);
}
