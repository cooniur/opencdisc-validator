/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.model;


/**
 * Provides filtering capabilities for <code>Reporter</code>s, allowing them to
 * selectively avoid printing out certain <code>Message</code> objects.
 *
 * @author Tim Stone
 */
public interface MessageFilter {
    /**
     * Determines whether or not a given <code>Message</code> object should be printed by
     * the <code>Reporter</code> that this <code>MessageFilter</code> is applied to.
     *
     * @param message  the <code>Message</code> to evaluate
     *
     * @return <code>true</code> if the <code>Message</code> should be printed,
     *     <code>false</code> otherwise
     */
    public boolean accept(Message message);
}
