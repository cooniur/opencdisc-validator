/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Tim Stone
 */
public class RuleMetrics {
    private final Map<String, Domain> domains = new HashMap<String, Domain>();

    /**
     *
     * @return
     */
    public Set<String> getDomains() {
        return Collections.unmodifiableSet(this.domains.keySet());
    }

    /**
     *
     * @param name
     * @return
     */
    public Set<String> getDomainRules(String name) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return Collections.emptySet();
        }

        return Collections.unmodifiableSet(domain.rules.keySet());
    }

    /**
     *
     * @param name
     * @return
     */
    public Map<String, Set<String>> getDomainContexts(String name) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return Collections.emptyMap();
        }

        Map<String, Set<String>> contexts = new HashMap<String, Set<String>>();

        for (Map.Entry<String, Map<String, Rule>> rule : domain.rules.entrySet()) {
            contexts.put(rule.getKey(),
                Collections.unmodifiableSet(rule.getValue().keySet()));
        }

        return contexts;
    }

    /**
     *
     * @param name
     * @param id
     * @param context
     * @return
     */
    public int getDomainExecutionCount(String name, String id, String context) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return 0;
        }

        return domain.getExecutionCount(id, context);
    }

    /**
     *
     * @param name
     * @param id
     * @param context
     * @return
     */
    public int getDomainFailureCount(String name, String id, String context) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return 0;
        }

        return domain.getFailureCount(id, context);
    }

    /**
     *
     * @param name
     * @param id
     * @param context
     * @return
     */
    public int getDomainInvocationCount(String name, String id, String context) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return 0;
        }

        return domain.getInvocationCount(id, context);
    }

    /**
     *
     * @param name
     * @param id
     * @param context
     * @return
     */
    public long getDomainElapsedCount(String name, String id, String context) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return 0;
        }

        return domain.getElapsedTime(id, context);
    }

    /**
     *
     * @param name
     * @param id
     * @param context
     * @return
     */
    public EntityDetails.Reference getReference(String name, String id, String context) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return null;
        }

        return domain.getRule(id, context, false).reference;
    }

    /**
     *
     * @param name
     * @param id
     * @param context
     * @return
     */
    public String getMessage(String name, String id, String context) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return null;
        }

        return domain.getRule(id, context, false).message;
    }

    /**
     *
     * @param entity
     * @param id
     * @param context
     * @param message
     */
    public void processRuleStart(EntityDetails entity, String id, String context,
             String message) {
        getRule(entity, id, context, message).start = System.nanoTime();
    }

    /**
     *
     * @param entity
     * @param id
     * @param context
     * @param executed
     * @param failed
     */
    public void processRuleStop(EntityDetails entity, String id, String context,
            boolean executed, boolean failed) {
        Rule rule = getRule(entity, id, context, null);
        long finish = System.nanoTime();

        ++rule.invocations;

        if (executed) {
            ++rule.executions;

            if (failed) {
                ++rule.failures;
            }
        }

        rule.elapsed += finish - rule.start;
    }

    private Domain getDomain(String name) {
        return this.domains.get(name.toUpperCase());
    }

    private Rule getRule(EntityDetails entity, String id, String context,
             String message) {
        String name = entity.getString(EntityDetails.Property.Name);
        Domain domain;

        synchronized(this.domains) {
            domain = this.domains.get(name);

            if (domain == null) {
                this.domains.put(name, (domain = new Domain()));
            }
        }

        return domain.getRule(id, context, true, entity.getReference(), message);
    }

    private static class Domain {
        private final Map<String, Map<String, Rule>> rules =
            new HashMap<String, Map<String, Rule>>();

        int getExecutionCount(String id, String context) {
            Rule rule = getRule(id, context, false);

            if (rule == null) {
                return 0;
            }

            return rule.executions;
        }

        int getFailureCount(String id, String context) {
            Rule rule = getRule(id, context, false);

            if (rule == null) {
                return 0;
            }

            return rule.failures;
        }

        int getInvocationCount(String id, String context) {
            Rule rule = getRule(id, context, false);

            if (rule == null) {
                return 0;
            }

            return rule.invocations;
        }

        long getElapsedTime(String id, String context) {
            Rule rule = getRule(id, context, false);

            if (rule == null) {
                return 0;
            }

            return rule.elapsed;
        }

        Rule getRule(String id, String context, boolean create) {
            return this.getRule(id, context, create, null, null);
        }

        Rule getRule(String id, String context, boolean create,
                EntityDetails.Reference reference, String message) {
            Map<String, Rule> instances;

            synchronized (this.rules) {
                instances = this.rules.get(id);

                if (instances == null) {
                    if (create) {
                        this.rules.put(id, (instances = new HashMap<String, Rule>()));
                    } else {
                        return null;
                    }
                }
            }

            Rule rule = instances.get(context);

            if (rule == null) {
                if (create) {
                    instances.put(context, (rule = new Rule(reference, message)));
                } else {
                    return null;
                }
            }

            return rule;
        }
    }

    private static class Rule {
        final EntityDetails.Reference reference;
        final String message;
        int invocations = 0;
        int executions = 0;
        int failures = 0;
        long elapsed = 0;
        long start = 0;

        Rule (EntityDetails.Reference reference, String message) {
            this.reference = reference;
            this.message = message;
        }
    }
}
