/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

import java.io.File;
import java.util.List;

import org.opencdisc.validator.model.SimpleMetrics;

/**
 * An engine for validating input data against a set of rules to produce a validation
 * report and producing metadata documents from similar input data.
 * <p>
 * Note: Implementations of this interface are not required to be thread-safe, and
 * therefore different instances should be used to run multiple simultaneous validation
 * processes.
 * <p>
 * <code>Validator</code> implementations should be obtained from a
 * <code>{@link ValidatorFactory}</code> instance, which helps guarantee that a returned
 * implementation provides particular internal functionality.
 * <p>
 * The <code>Validator</code> provides the two primary functions of data validation and
 * document generation, accessible through the <code>validate</code> and
 * <code>generate</code> methods, respectively.
 *
 * @author Max Kanevsky
 * @since 1.0
 * @see ValidatorFactory
 * @see SourceOptions
 * @see ConfigOptions
 * @see ReportOptions
 */
public interface Validator {
    /**
     * The defined validation types. Anything that is not explicitly defined but
     * supported should be declared as being <code>Custom</code>.
     *
     * @author Tim Stone
     */
    public enum Type {
        Custom(Format.Tabular),
        Define(Format.Hierarchical),
        Adam(Format.Tabular),
        Sdtm(Format.Tabular),
        Send(Format.Tabular),
        Odm(Format.Hierarchical),
        SdtmAdam(Format.Tabular),
        Lab(Format.Tabular);

        private final Format format;

        Type(Format format) {
            this.format = format;
        }

        /**
         * Gets the <code>Format</code> that this data type expects for validation.
         *
         * @return the <code>Format</code> structure that this data type expects
         * @see Format
         */
        public Format getFormat() {
            return this.format;
        }
    }

    /**
     * A defined list of data storage formats, used for appropriately parsing and
     * validating source data.
     *
     * @author Tim Stone
     */
    public enum Format {
        Tabular,
        Hierarchical
    }

    /**
     * Aborts the validation process started by a call to one of of the
     * <code>validate()</code> methods, if one is currently running.
     */
    public void abort();

    /**
     * Adds the provided <code>ValidatorListener</code> to this <code>Validator</code>
     * instance so that it can receive progress update events.
     *
     * @param eventListener  the <code>ValidatorListener</code> to add
     */
    public void addValidatorListener(ValidatorListener eventListener);

    /**
     * Generates a define.xml document at <code>destination</code>, using information
     * from the passed <code>source</code> and configuration <code>template</code>.
     *
     * @param source  information about the source data to extract metadata from
     * @param template  the configuration to base the generated define.xml off of
     * @param destination  the location where the file should be saved
     */
    public void generate(SourceOptions source, ConfigOptions template, File destination);

    /**
     * Generates a define.xml document at <code>destination</code>, using information
     * from the passed <code>sources</code> and configuration <code>template</code>.
     *
     * @param sources  information about the source data to extract metadata from
     * @param template  the configuration to base the generated define.xml off of
     * @param destination  the location where the file should be saved
     */
    public void generate(List<SourceOptions> sources, ConfigOptions template,
            File destination);

    /**
     * Generates a define.xml document at <code>destination</code>, using information
     * from the passed <code>source</code> and configuration <code>template</code>.
     *
     * @param type  the validation type
     * @param source  information about the source data to extract metadata from
     * @param template  the configuration to base the generated define.xml off of
     * @param destination  the location where the file should be saved
     * @since 1.3
     */
    public void generate(Type type, SourceOptions source, ConfigOptions template,
            File destination);

    /**
     * Generates a define.xml document at <code>destination</code>, using information
     * from the passed <code>sources</code> and configuration <code>template</code>.
     *
     * @param type  the validation type
     * @param sources  information about the source data to extract metadata from
     * @param template  the configuration to base the generated define.xml off of
     * @param destination  the location where the file should be saved
     * @since 1.3
     */
    public void generate(Type type, List<SourceOptions> sources, ConfigOptions template,
            File destination);

    /**
     * Gets the <code>SimpleMetrics</code> object that contains validation metrics
     * from the most recent validation run.
     *
     * @return the <code>SimpleMetrics</code> from the latest validation run
     * @see SimpleMetrics
     */
    public SimpleMetrics getMetrics();

    /**
     * Removes all of the registered <code>ValidatorListener</code> objects.
     */
    public void removeAllValidatorListeners();

    /**
     * Removes a <code>ValidatorListener</code> from receiving
     * <code>ValidatorEvent</code> messages, if it was added by
     * <code>{@link #addValidatorListener(ValidatorListener)}</code>.
     *
     * @param eventListener  the <code>ValidatorEventListener</code> to remove
     * @return <code>true</code> if the provided <code>ValidatorListener</code> was
     *     present and was removed, <code>false</code> otherwise
     */
    public boolean removeValidatorListener(ValidatorListener eventListener);

    /**
     * Validates a single data source containing data of type <code>type</code> using
     * the provided configuration information, writing all validation issues out to the
     * report specified by <code>report</code>.
     *
     * @param type  the validation type
     * @param source  information about the source data to validate
     * @param config  the configuration to use to validate the data
     * @param report  information about the location and type of report to be generated
     */
    public void validate(Type type, SourceOptions source, ConfigOptions config,
            ReportOptions report);

    /**
     * Validates a set of data sources containing data of type <code>type</code> using
     * the provided configuration information, writing all validation issues out to the
     * report specified by <code>report</code>.
     *
     * @param type  the validation type
     * @param sources  information about the source data to validate
     * @param config  the configuration to use to validate the data
     * @param report  information about the location and type of report to be generated
     */
    public void validate(Type type, List<SourceOptions> sources, ConfigOptions config,
            ReportOptions report);
}
