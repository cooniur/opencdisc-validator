/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides utilities that help perform file operations not covered by the Java core
 * classes.
 *
 * @author Tim Stone
 */
public class FileUtils {
    private static Map<Class<?>, File> PATH_CACHE = new HashMap<Class<?>, File>();

    /**
     * Creates an new <code>File</code> with an absolute path determined by combining the
     * location of the provided class with the given <code>relativePath</code>. If the
     * given path is already absolute (per <code>File.isAbsolute()</code>), it will be
     * returned directly.
     *
     * @param relativeClass  the class whose location on disk the path should be mapped
     *     relatively against
     * @param relativePath  the relative path to map to an absolute path
     * @return the absolute path of the given <code>relativePath</code>, if one could be
     *     determined, or <code>relativePath</code> if that path was already absolute
     */
    public static File getAbsolutePath(Class<?> relativeClass, String relativePath) {
        File absoluteFile = new File(relativePath);

        if (absoluteFile.isAbsolute()) {
            return absoluteFile;
        }

        try {
            if (!PATH_CACHE.containsKey(relativeClass)) {
                CodeSource source = relativeClass.getProtectionDomain().getCodeSource();

                if (source != null) {
                    File parent = new File(
                            URLDecoder.decode(source.getLocation().getFile(), "UTF-8")
                    );

                    if (parent.isFile()) {
                        parent = parent.getParentFile();
                    }

                    PATH_CACHE.put(relativeClass, parent);
                }
            }

            absoluteFile = new File(PATH_CACHE.get(relativeClass), relativePath);

            try {
                absoluteFile = absoluteFile.getCanonicalFile();
            } catch (IOException ex) {
                absoluteFile = absoluteFile.getAbsoluteFile();
            }
        } catch (UnsupportedEncodingException ex) {
            // Never happens
        }

        return absoluteFile;
    }
}
