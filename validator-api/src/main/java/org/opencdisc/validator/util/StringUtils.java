/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

/**
 * Provides utility functions that perform various common <code>String</code>
 * manipulation tasks across the various Validator packages.
 *
 * @author Tim Stone
 * @since  1.0
 */
public class StringUtils {
    /**
     * Automatically combines several disparate strings into a single <code>String</code>
     * object, which is created in a <code>StringBuilder</code> for efficiency and
     * automatically interned.
     *
     * @param strings  the <code>String</code> objects to combine
     * @return the resulting combined, interned <code>String</code>
     * @since 1.1
     */
    public static String combine(String ... strings) {
        int capacity = ((strings[0].length() + strings[strings.length - 1].length()) / 2)
                * strings.length;
        StringBuilder buffer = new StringBuilder(capacity);

        for (int i = 0; i < strings.length; ++i) {
            buffer.append(strings[i]);
        }

        return buffer.toString().intern();
    }

    /**
     * Repeats a given <code>String</code> value a specific number of times.
     *
     * @param string  the <code>String</code> that should be repeated
     * @param times  the number of times to repeat the given <code>string</code>
     * @return the given <code>string</code>, repeated <code>times</code> times
     * @since 1.0
     */
    public static String repeat(String string, int times) {
        int capacity = times * string.length();
        StringBuilder buffer = new StringBuilder(capacity);

        for (int i = 0; i < times; ++i) {
            buffer.append(string);
        }

        return buffer.toString().intern();
    }

    public static String rtrim(String string) {
        int length = string.length();
        int i = length - 1;

        for ( ; i > -1; --i) {
            if (string.charAt(i) != ' ') {
                break;
            }
        }

        return i < length - 1 ? string.substring(0, i + 1) : string;
    }
}
