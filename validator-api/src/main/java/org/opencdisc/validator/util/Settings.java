/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Provides access to properties that can impact various internal program functionality.
 * Code can set properties directly, or they can be defined in a
 * <code>settings.properties</code> located on the  application's CLASSPATH.
 *
 * @author Tim Stone
 */
public final class Settings {
    private static final Map<String, String> SETTINGS = new HashMap<String, String>();
    private static final String BUNDLE_NAME = "settings";

    static {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME);
            Enumeration<String> properties = bundle.getKeys();

            while (properties.hasMoreElements()) {
                String property = properties.nextElement();

                Settings.set(property, bundle.getString(property));
            }
        } catch (MissingResourceException ignore) {}
    }

    private Settings() {}

    /**
     * Checks to see if a given <code>setting</code> name has been defined by the calling
     * code or through the external properties file.
     *
     * @param setting  the name of the property that may or may not be defined
     * @return <code>true</code> if the property is defined, <code>false</code> otherwise
     */
    public static boolean defines(String setting) {
        return SETTINGS.containsKey(setting.toUpperCase());
    }

    /**
     * Gets the <code>String</code> value associated with the given <code>setting</code>
     * name. The calling code should attempt to convert this value to the appropriate
     * type from the text representation returned.
     *
     * @param setting  the name of the property whose value should be retrieved
     * @return the value associated with the given property name
     * @throws IllegalArgumentException  if the requested property is not defined
     */
    public static String get(String setting) {
        if (!defines(setting)) {
            throw new IllegalArgumentException(String.format("Setting %s is undefined",
                    setting));
        }

        return SETTINGS.get(setting.toUpperCase());
    }

    /**
     *
     * @param setting
     * @param fallback
     * @return
     */
    public static String get(String setting, String fallback) {
        if (!defines(setting)) {
            return fallback;
        }

        return SETTINGS.get(setting.toUpperCase());
    }

    /**
     * Sets the value of the specified <code>setting</code> to the given value.
     *
     * @param setting  the name of the property to set
     * @param value  the value to assign to this property
     */
    public static void set(String setting, String value) {
        SETTINGS.put(setting.toUpperCase(), value);
    }
}
