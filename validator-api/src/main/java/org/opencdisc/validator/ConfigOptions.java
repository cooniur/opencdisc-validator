/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

import java.util.HashMap;
import java.util.Map;

/**
 * Acts as a wrapper for all configuration related properties that can be passed to
 * <code>Validator.validate()</code> methods. Required properties are enforced by the
 * constructor.
 *
 * @author Tim Stone
 */
public class ConfigOptions {
    private final String connectionString;
    private final Map<String, String> properties = new HashMap<String, String>();
    private String defineConnectionString = null;
    private String codeListConnectionString = null;

    /**
     * Creates a new <code>ConfigOptions</code> with the specified
     * <code>connectionString</code>. Currently, this should be a path to the
     * configuration file to be used by a <code>Validator</code> implementation.
     * <p>
     * <em>Note: Future releases may allow for other types of connection strings</em>
     *
     * @param connectionString  a file system path to the configuration file
     * @throws NullPointerException  if <code>connectionString</code> is
     *     <code>null</code>
     */
    public ConfigOptions(String connectionString) {
        if (connectionString == null) {
            throw new NullPointerException(
                    "ConfigOptions: connectionString cannot be null"
            );
        }

        this.connectionString = connectionString;
    }

    /**
     * Gets the connection string information for the associated codelist template.
     *
     * @return the connection string pointing to the codelist template, if provided
     */
    public String getCodeList() {
        return this.codeListConnectionString;
    }

    /**
     * Gets the connection string information pointing to the configuration data.
     *
     * @return the connection string pointing to the configuration data
     */
    public String getConfig() {
        return this.connectionString;
    }

    /**
     * Gets the connection string information for associated define.xml metadata.
     *
     * @return the connection string pointing to the define.xml metadata, if provided
     */
    public String getDefine() {
        return this.defineConnectionString;
    }

    public String getProperty(String property) {
        return this.properties.get(key(property));
    }

    public Map<String, String> getProperties() {
        return this.properties;
    }

    public boolean hasProperty(String property) {
        return this.properties.containsKey(key(property));
    }

    /**
     * Sets the <code>connectionString</code> used to locate the codelist template
     * associated with the data to be validated or generated, if applicable.
     * <p>
     * <em>Note: Future releases may allow for other types of connection strings</em>
     *
     * @param connectionString  a file system path to the define.xml file
     * @return this instance
     */
    public ConfigOptions setCodeList(String connectionString) {
        this.codeListConnectionString = connectionString;

        return this;
    }

    /**
     * Sets the <code>connectionString</code> used to locate the define.xml document
     * associated with the data to be validated, if applicable. If left unset, a
     * <code>Validator</code> implementation is expected to proceed with the validation
     * as if there were no associated define.xml metadata for the provided source data.
     * <p>
     * <em>Note: Future releases may allow for other types of connection strings</em>
     *
     * @param connectionString  a file system path to the define.xml file
     * @return this instance
     */
    public ConfigOptions setDefine(String connectionString) {
        this.defineConnectionString = connectionString;

        return this;
    }

    public ConfigOptions setProperty(String property, String value) {
        this.properties.put(key(property), value);

        return this;
    }

    private String key(String key) {
        return key.toLowerCase();
    }
}
