/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

/**
 * Provides creation and management of <code>Validator</code> instances.
 *
 * @author Max Kanevsky
 */
public final class ValidatorFactory {
    /**
     * Main constructor.
     */
    private ValidatorFactory() {}


    /**
     * Returns a <code>ValidatorFactory</code> instance that can create
     * <code>Validator</code> objects.
     *
     * @return the requested <code>ValidatorFactory</code> instance
     */
    public static ValidatorFactory newInstance() {
        return new ValidatorFactory();
    }


    /**
     * Creates and returns a new <code>Validator</code> implementation instance.
     *
     * @return the new <code>Validator</code> instance
     * @throws RuntimeException  if creation of the Validator implementation object
     *     unexpectedly failed
     */
    public Validator newValidator() {
        Validator validator = null;

        try {
            Class<?> clazz = Class.forName("org.opencdisc.validator.DefaultValidator");
            validator = (Validator)clazz.newInstance();
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(
                    "Unable to locate Validator implementation, is the validator-core "
                    + "jar not on the classpath?",
                    ex
            );
        } catch (InstantiationException ex) {
            throw new RuntimeException(
                    "Creation of the Validator instance failed, which should never "
                    + "happen",
                    ex
            );
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(
                    "You are somehow permitted to access the API, but not the validator "
                    + "implementation class",
                    ex
            );
        }

        return validator;
    }
}
