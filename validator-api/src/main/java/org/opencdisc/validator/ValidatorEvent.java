/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

/**
 * A container for validation run state information, used to communicate the current
 * progress of a running validation process.
 *
 * @author Tim Stone
 */
public class ValidatorEvent {
    private static int NULL_INT = -1;

    /**
     * The available validator state that a <code>ValidatorEvent</code> can represent.
     *
     * @author Tim Stone
     */
    public enum State {
        Parsing,
        Processing,
        Subprocessing,
        Generating,
        Completed;
    }

    private final long timestamp;
    private final State state;
    private final long current;
    private final long maximum;
    private final String name;

    private ValidatorEvent subevent = null;

    /**
     * Creates a new <code>ValidatorEvent</code> with the specified <code>state</code>,
     * <code>current</code> index, <code>maximum</code> index, and origin
     * <code>name</code>.
     *
     * @param state  the state this event represents
     * @param current  the current progress index
     * @param maximum  the maximum progress index
     * @param name  the name of the source of this event
     */
    public ValidatorEvent(State state, long current, long maximum, String name) {
        this.timestamp = System.currentTimeMillis();
        this.state     = state;
        this.current   = current;
        this.maximum   = maximum;
        this.name      = name;
    }

    /**
     * Creates a new <code>ValidatorEvent</code> with the specified <code>state</code>,
     * <code>current</code> index, and origin <code>name</code>.
     *
     * @param state  the state this event represents
     * @param current  the current progress index
     * @param name  the name of the source of this event
     */
    public ValidatorEvent(State state, long current, String name) {
        this(state, current, NULL_INT, name);
    }

    /**
     * Creates a new <code>ValidatorEvent</code> with the specified <code>state</code>
     * and origin <code>name</code>.
     *
     * @param state  the state this event represents
     * @param name  the name of the source of this event
     */
    public ValidatorEvent(State state, String name) {
        this(state, NULL_INT, NULL_INT, name);
    }

    /**
     * Creates a new <code>ValidatorEvent</code> with the specified <code>state</code>,
     * and <code>current</code> index.
     *
     * @param state  the state this event represents
     * @param current  the current progress index
     */
    public ValidatorEvent(State state, long current) {
        this(state, current, NULL_INT, null);
    }

    /**
     * Creates a new blank <code>ValidatorEvent</code>.
     */
    public ValidatorEvent() {
        this(null, NULL_INT, NULL_INT, null);
    }

    /**
     * Gets the current progress index. This can represent the current dataset or record
     * being processed, or something like the current record being stored in a cache.
     *
     * @return the current progress index
     */
    public long getCurrent() {
        return this.current;
    }

    /**
     * Gets the maximum progress index. This is typically the total number of datasets
     * being validated.
     *
     * @return the maximum progress index
     */
    public long getMaximum() {
        return this.maximum;
    }

    /**
     * Gets the name of the item currently being processed, if one was provided.
     *
     * @return the name of the item being processed
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the state of the <code>Validator</code> that this event represents.
     *
     * @return the state represented by this event
     * @see State
     */
    public State getState() {
        return this.state;
    }

    /**
     * Gets the subevent, usually representative of the current record being processed.
     *
     * @return the subevent, or <code>null</code> if one was not defined
     */
    public ValidatorEvent getSubevent() {
        return this.subevent;
    }

    /**
     * The timestamp that marks the creation time of this event.
     *
     * @return the time this event occurred
     */
    public long getTimestamp() {
        return this.timestamp;
    }

    /**
     * Sets the subevent to the provided <code>event</code>.
     *
     * @param event the subevent to set
     */
    public void setSubevent(ValidatorEvent event) {
        this.subevent = event;
    }
}
