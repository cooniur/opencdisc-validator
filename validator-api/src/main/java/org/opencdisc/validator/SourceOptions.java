/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

import org.opencdisc.validator.Validator.Format;

import java.io.File;

/**
 * Acts as a wrapper for all source related properties that can be passed to
 * <code>Validator.validate()</code> methods. Required properties are enforced by the
 * constructor.
 *
 * @author Max Kanevsky
 */
public final class SourceOptions {
    /**
     * Describes the file type and format of a particular source.
     *
     * @author Max Kanevsky
     */
    public enum Type {
        SasTransport(Format.Tabular),
        Delimited(Format.Tabular),
        Database(Format.Tabular),
        DatasetXml(Format.Tabular),
        Xml(Format.Hierarchical),
        UseProtocol(null);

        private final Validator.Format format;

        Type(Validator.Format format) {
            this.format = format;
        }

        /**
         * Gets the <code>Format</code> that this source type uses to store data for the
         * purposes of parsing and validation.
         *
         * @return the <code>Format</code> structure that this source type uses
         * @see org.opencdisc.validator.Validator.Format
         */
        public Validator.Format getFormat() {
            return this.format;
        }

        /**
         * Returns a <code>Type</code> based on a string name, with flexible matching.
         *
         * @param name  the name of the options
         * @return the <code>Type</code>, or <code>null</code> if one couldn't be determined
         */
        public static Type parse(String name) {
            name = name.replace(" ", "");

            for (Type type : values()) {
                if (type.name().equalsIgnoreCase(name)) {
                    return type;
                }
            }

            return null;
        }
    }

    private final String connectionString;
    private Type type = Type.SasTransport;
    private String delimiter = null;
    private String qualifier = null;
    private String name = null;
    private String subname = null;
    private boolean hasHeader = true;
    private String define;

    /**
     * Creates a new <code>SourceOptions</code> object that describes the source located
     * at the provided <code>connectionString</code>.
     *
     * @param connectionString  the file containing the source data to parse
     */
    public SourceOptions(String connectionString) {
        this.connectionString = connectionString;
    }

    /**
     * Creates a new <code>SourceOptions</code> object that describes the source located
     * at the provided <code>connectionString</code>, using the additional source options
     * set on the provided <code>template</code>.
     *
     * @param connectionString  the file containing the source data to parse
     * @param template  the <code>SourceOptions</code> object to inherit other options
     *     from
     */
    public SourceOptions(String connectionString, SourceOptions template) {
        this(connectionString);

        this.type = template.type;
        this.delimiter = template.delimiter;
        this.qualifier = template.qualifier;
        this.name = template.name;
        this.hasHeader = template.hasHeader;
        this.define = template.define;
    }

    /**
     * Gets the delimiter for this source, if one was set.
     *
     * @return the source's assigned delimiter, or <code>null</code> if one was not set
     */
    public String getDelimiter() {
        return this.delimiter;
    }

    /**
     *
     * @return
     */
    public boolean hasHeader() {
        return this.hasHeader;
    }

    /**
     *
     * @param hasHeader
     * @return
     */
    public boolean hasHeader(boolean hasHeader) {
        return this.hasHeader = hasHeader;
    }

    /**
     * Gets the define.xml associated with this source, if one was set.
     *
     * @return the define.xml, or <code>null</code> if one was not set
     */
    public String getDefine() {
        return this.define;
    }

    /**
     * Gets the name of this source, if one was set. If one was not set, the
     * <code>Validator</code> will derive one automatically.
     *
     * @return the source's assigned name, or <code>null</code> if one was not set
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the qualifier for this source, if one was set.
     *
     * @return the source's qualifier, or <code>null</code> if one was not set
     */
    public String getQualifier() {
        return this.qualifier;
    }

    /**
     * Gets the subname for this source, if one was set.
     *
     * @return the source's subname, or <code>null</code> if one was not set
     */
    public String getSubname() {
        return this.subname;
    }

    /**
     * Gets the connection string that points to this source's data.
     *
     * @return the source's connection string
     */
    public String getSource() {
        return this.connectionString;
    }

    /**
     * Gets the format type of this source. If one was not set, defaults to
     * <code>SasTransport</code>.
     *
     * @return the source <code>Type</code>
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Sets the define.xml for parsing the source data. Only used if the source
     * <code>Type</code> is <code>DatasetXml</code>.
     *
     * @param define  the path to the define.xml file
     * @return this instance
     */
    public SourceOptions setDefine(String define) {
        this.define = define;

        return this;
    }

    /**
     * Sets the delimiter for parsing the source data. Only used if the source
     * <code>Type</code> is <code>Delimited</code>.
     *
     * @param delimiter  the delimited file field delimiter
     * @return this instance
     */
    public SourceOptions setDelimiter(String delimiter) {
        if (delimiter.equalsIgnoreCase("\\t")) {
            delimiter = "\t";
        }

        this.delimiter = delimiter;

        return this;
    }

    /**
     * Sets the name to reference the created source by. If not provided, the name will
     * be taken from the file name, excluding the path and extension. This should be set
     * explicitly in cases where the derived source name differs from the configuration
     * name it is intended to correspond to.
     *
     * @param name  the name to reference the created source by
     * @return this instance
     */
    public SourceOptions setName(final String name) {
        this.name = name;

        return this;
    }

    /**
     * Sets the qualifier for parsing the source data. Only used if the source
     * <code>Type</code> is <code>Delimited</code>.
     *
     * @param qualifier  the delimited file field qualifier
     * @return this instance
     */
    public SourceOptions setQualifier(final String qualifier) {
        this.qualifier = qualifier;

        return this;
    }

    /**
     * Sets the subname of this source.
     *
     * @param subname  the subname of this source
     * @return this instance
     */
    public SourceOptions setSubname(final String subname) {
        this.subname = subname;

        return this;
    }

    /**
     * Sets the source format type.
     *
     * @param type  the source <code>Type</code>
     * @see Type
     * @return this instance
     */
    public SourceOptions setType(final Type type) {
        this.type = type;

        return this;
    }

    /**
     * Creates a new <code>SourceOptions</code> instance with name and subname set as appropriate
     * per split dataset naming conventions.
     *
     * @param file  the file to create the options based on
     * @return the prepared <code>SourceOptions</code> object
     * @since 2.0
     */
    public static SourceOptions fromFile(File file) {
        return fromFile(file, null);
    }

    /**
     * Creates a new <code>SourceOptions</code> instance with name and subname set as appropriate
     * per split dataset naming conventions.
     *
     * @param file  the file to create the options based on
     * @param type  the type of the source
     * @return the prepared <code>SourceOptions</code> object
     * @since 2.0
     */
    public static SourceOptions fromFile(File file, Type type) {
        String name = file.getName();
        boolean isDefine = name.equalsIgnoreCase("define.xml");

        name = name.substring(0, name.indexOf('.')).toUpperCase();

        SourceOptions options = new SourceOptions(file.getAbsolutePath())
            .setSubname(name);

        boolean isADaM = name.startsWith("AD");
        int size = name.length();
        int padding = name.startsWith("SUPP") ? 4 : 0;

        if (!isDefine && !isADaM && (size == 3 + padding || size == 4 + padding)) {
            name = name.substring(0, 2 + padding);
        }

        if (type != null) {
            options.setType(type);
        }

        return options.setName(name);
    }
}
