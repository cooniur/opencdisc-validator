/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

/**
 * <code>ValidatorListener</code> implementations can receive update notifications from
 * <code>Validator</code> implementations about the status of a current process, either
 * generation or validation. In the case of intermediary progress updates,
 * implementations of this interface can query methods on the passed event object to get
 * more detailed information about exactly what the <code>Validator</code> is doing.
 *
 * @author Tim Stone
 */
public interface ValidatorListener {
    /**
     * Called by a <code>Validator</code> implementation when progress is made on a
     * process.
     *
     * @param event  a <code>ValidatorEvent</code> with detailed information about the
     *     current progress
     */
    public void validatorProgressUpdated(ValidatorEvent event);

    /**
     * Called by a <code>Validator</code> implementation when a process starts.
     *
     * @param event a blank <code>ValidatorEvent</code> object
     */
    public void validatorStarted(ValidatorEvent event);

    /**
     * Called by a <code>Validator</code> implementation when a process stops.
     *
     * @param event a blank <code>ValdiatorEvent</code> object
     */
    public void validatorStopped(ValidatorEvent event);
}
