/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator;

import java.util.*;

import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.model.MessageFilter;

/**
 * Acts as a wrapper for all report related properties that can be passed to
 * <code>Validator.validate()</code> methods. Required properties are enforced by the
 * constructor.
 *
 * @author Max Kanevsky
 */
public final class ReportOptions {
    /**
     * Defines the available report types, complete with the expected extension.
     *
     * @author Max Kanevsky
     */
    public enum Type {
        Xml(".xml"),
        Html(".html"),
        Excel(".xlsx"),
        Csv(".csv"),
        Queue(null);

        private final String extension;

        Type(String extension) {
            this.extension = extension;
        }

        /**
         * Gets the file extension associated with this report type.
         *
         * @return the file extension, with leading dot
         */
        public String getExtension() {
            return this.extension;
        }
    }

    private final String connectionString;
    private Queue<Message> messageQueue;
    private List<MessageFilter> filters = null;
    private Map<String, String> headers = new LinkedHashMap<String, String>();
    private int limit = 1000;
    private Type type = Type.Xml;
    private boolean metrics;
    private boolean overwrite;

    /**
     * Creates a new <code>ReportOptions</code> object that should represent a file-based
     * report type, with the provided <code>connectionString</code> where the file will
     * be saved.
     *
     * @param connectionString  the file to save the report to
     * @throws NullPointerException  if <code>connectionString</code> is
     *     <code>null</code>
     */
    public ReportOptions(String connectionString) {
        if (connectionString == null) {
            throw new NullPointerException("connectionString cannot be null");
        }

        this.connectionString = connectionString;
    }

    /**
     * Creates a new <code>ReportOptions</code> object that will be of type
     * <code>Queue</code>, passing any generated messages to the provided
     * <code>messageQueue</code>.
     *
     * @param messageQueue  a <code>Queue</code> implementation that will receive
     *     <code>Message</code> objects created by the Validator
     * @throws NullPointerException if <code>messageQueue</code> is <code>null</code>
     */
    public ReportOptions(Queue<Message> messageQueue) {
        if (messageQueue == null) {
            throw new NullPointerException("messageQueue cannot be null");
        }

        this.messageQueue = messageQueue;
        this.connectionString = null;
        this.setType(Type.Queue);
    }

    /**
     * Gets the defined <code>MessageFilter</code>s that are applied to the report.
     *
     * @return a <code>List</code> of <code>MessageFilter<code> objects
     */
    public List<MessageFilter> getFilters() {
        return this.filters;
    }

    /**
     * Gets the flag that indicates whether or not the report should generate message
     * metrics to be used by the calling code after a validation call.
     *
     * @return <code>true</code> if messages metrics will be generated,
     *     <code>false</code> otherwise
     */
    public boolean getGenerateMetrics() {
        return this.metrics;
    }

    /**
     * Gets the map of caller-defined headers that will be displayed on reports that
     * support additional header information.
     *
     * @return the map of headers
     */
    public Map<String, String> getHeaders() {
        return this.headers;
    }

    /**
     * Gets the repeat limit enforced by the report. Duplicate messages passed to the
     * report after this number of repeats will not be written to the destination format.
     *
     * @return the number of repeat messages after which the report stops printing a
     *     particular message
     */
    public int getLimit() {
        return this.limit;
    }

    /**
     * Gets a boolean indicating whether an existing report should be overwritten or not.
     * When <code>false</code>, the report generator may append new results to an existing
     * report at the same location, or it may throw an exception.
     *
     * @return <code>true</code> if an existing report should be overwritten,
     *     <code>false</code> otherwise
     */
    public boolean getOverwrite() {
        return this.overwrite;
    }

    /**
     * Gets the connection string identifying the intended location of the report.
     *
     * @return the connection string representing where the report should be stored
     */
    public String getReport() {
        return this.connectionString;
    }

    /**
     * Gets the <code>Queue</code> that receives messages produced during the process.
     *
     * @return the <code>Queue</code> that acts as the destination for all generated
     *     <code>Message</code>
     */
    public Queue<Message> getReportQueue() {
        return this.messageQueue;
    }

    /**
     * Gets the type of report that should be generated at the end of the process.
     *
     * @return the report <code>Type</code>
     * @see Type
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Sets the <code>MessageFilter</code>s that the report should use to selectively
     * display messages.
     *
     * @param filters  a <code>List</code> of filters to apply
     * @see org.opencdisc.validator.model.MessageFilter
     * @return this instance
     */
    public ReportOptions setFilters(final List<MessageFilter> filters) {
        this.filters = filters;

        return this;
    }

    /**
     * Sets the flag indicating whether the report should generate detailed message
     * metrics about the validation process.
     * <p>
     * Note that this is only a suggestion. The Validator may collect message metrics
     * even when this parameter is set to false, if the metrics information is necessary
     * for the report type selected.
     *
     * @param shouldGenerate  a <code>boolean</code> flag indicating whether message
     *     metrics should be collected
     * @see org.opencdisc.validator.model.MessageMetrics
     * @see org.opencdisc.validator.model.SimpleMetrics#getMessageMetrics()
     * @return this instance
     */
    public ReportOptions setGenerateMetrics(boolean shouldGenerate) {
        this.metrics = shouldGenerate;

        return this;
    }

    /**
     * Sets a caller-defined header and its value for report types that support
     * including additional header information.
     *
     * @param header  the header name
     * @param value  the header value
     * @return this instance
     */
    public ReportOptions setHeader(String header, String value) {
        this.headers.put(header, value);

        return this;
    }

    /**
     * Sets the repeat limit enforced by the report. Duplicate messages passed to the
     * report after this number of repeats will not be written to the destination format.
     *
     * @param limit  the number of repeat messages after which the report stops printing
     *     a particular message
     * @return this instance
     */
    public ReportOptions setLimit(final int limit) {
        this.limit = limit;

        return this;
    }

    /**
     * Sets the option to overwrite an existing report if the report generator finds that
     * a file at the same location is already present.
     *
     * @param overwrite  whether or not to overwrite an existing report at the same
     *     location
     * @return this instance
     */
    public ReportOptions setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;

        return this;
    }

    /**
     * Sets the type of report that should be produced.
     *
     * @param type  the <code>Type</code> of report that should be produced
     * @see Type
     * @return this instance
     */
    public ReportOptions setType(final Type type) {
        this.type = type;

        return this;
    }
}
