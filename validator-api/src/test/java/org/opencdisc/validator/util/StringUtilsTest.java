/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.validator.util;/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Validator.
 *
 * OpenCDISC Validator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenCDISC Validator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OpenCDISC Validator.  If not, see <http://www.gnu.org/licenses/>.
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class StringUtilsTest {
    @Test
    public void RtrimTestSpacesRight() {
        String test = "abc  ";
        String result = StringUtils.rtrim(test);

        assertEquals("abc", result);
    }

    @Test
    public void RtrimTestSpacesLeft() {
        String test = "  abc";
        String result = StringUtils.rtrim(test);

        assertEquals("  abc", result);
    }

    @Test
    public void RtrimTestNoSpaces() {
        String test = "ab c";
        String result = StringUtils.rtrim(test);

        assertEquals("ab c", result);
    }
}
